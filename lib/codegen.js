/**
* Javascript utilities to generate APEX code.
* Jorge Luna
* Date: 2014-11-18
*/
importPackage(java.io); 

if (!String.prototype.trim) {
  (function() {
    // Make sure we trim BOM and NBSP
    var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
    String.prototype.trim = function() {
      return this.replace(rtrim, '');
    };
  })();
}

var println = function(str){

}

var BASE_CLASS_PATH = './src/classes/';
var TEMPLATE_PATH = './codegen_templates/';
var CODEGEN_STATS_FILE = './codestats/stats.json';

function parseFile(fileName){
  var file = new File(fileName);
  var br = new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(file))));
  var parsedFile = [];
  
  while ((line = br.readLine()) !== null)   
  {
    parsedFile[parsedFile.length] = line;
  }
  
  return parsedFile;
}

function writeFile(fileName, fileContent){
  var fileOutput = new File(fileName);
  if(fileOutput.exists()){
    fileOutput['delete']();
    fileOutput = new File(fileName);
  }
  echo = buildSystem.createTask('echo');
  echo.setMessage(fileContent);
  echo.setFile(fileOutput);
  echo.setAppend(true);
  echo.perform();  
}

function removeCommentedCode(codeFile){
  var newCode = [];
  var inComment = false;

  for(i = 0; i < codeFile.length; i++){
    var line = String(codeFile[i]);
    var skipping = false;
    var skipToken = '';
    var newline = '';
  
    for(j = 0; j < line.length; j++){
      if((line[j] === '\'' || line[j] === '"') && !inComment){
        if(!skipping){
          skipping = true;
          skipToken = line[j];
        }else if(line[j-1] !== '\\' && line[j] === skipToken){
          skipping = false;          
        }        
      }else if(!skipping){
        if(j < line.length - 1 && line[j] === '/' && line[j+1] === '/' && !inComment){
          break;
        }
        if(j < line.length - 1 && line[j] === '/' && line[j+1] === '*'){
          inComment = true;
        }
        if(j < line.length - 1 && line[j] === '*' && line[j+1] === '/'){
          inComment = false;
          j++;
          continue;
        }
        if(!inComment){
          newline += line[j];
        }
          
      }else{
        newline += line[j];
      }
    }    
    
    if(newline !== ''){
      newCode[newCode.length] = newline;
    }
  }
  
  return newCode;
}

function removePadding(processFile){
  var newCode = [];

  for(i = 0; i < processFile.length; i++){    
    var temp = String(processFile[i]).trim();
    do{
      temp = temp.replace('\t',' ');        
    }while(temp.indexOf('\t') >= 0);
    
    do{
      temp = temp.replace('  ',' ');        
    }while(temp.indexOf('  ') >= 0);
    
    if(temp.length > 0){
      newCode[newCode.length] = temp.trim();
    }
  }
  
  return newCode;
}

function normalizeLines(processFile){
  var singleLineCode = processFile.join(' ');
  var newCode = [];
  var line = 0;
  var skipQuote = false;
  var skipChar = '';
  
  //split Lines on '{' '}' or ';' skip quoted strings
  for(i = 0; i < singleLineCode.length; i++){
    var Character = singleLineCode[i];    
    var PrevCharacter = (i>0)? singleLineCode[i-1] : '';
    newCode[line] = (newCode[line])? newCode[line] + Character : Character;
    if(Character === '\'' || Character === '"'){
      if(skipQuote && Character === skipChar && PrevCharacter !== '\\'){
        skipChar = '';
        skipQuote = false;
      }else if(!skipQuote){
        skipChar = Character;
        skipQuote = true;
      }
    }
    if(!skipQuote){
      line += ( Character === ';' || Character === '}' || Character === '{')? 1 : 0;
    }
  }
  
  for(i = 0; i < newCode.length; i++){
    newCode[i] = newCode[i].trim();
  }
  
  return newCode;
}

function extractBlockSection(tokenPhrase, processCode){
  var newCode = [];
  var extractFlag = false;
  
  for(i = 0; i < processCode.length; i++){
    if(processCode[i] === '}' && extractFlag){
      break;
    }
    if(extractFlag){
      newCode[newCode.length] = processCode[i];
    }
    if(processCode[i].toUpperCase() === tokenPhrase){
      extractFlag = true;
    }
  }
  
  return newCode;
}

function addMockedFunction(functionDef){
  var mockFunc = [];
  var returnType = functionDef.split(' ')[0];
  var functionName = functionDef.split(' ')[1].split('(')[0].trim();
  var parameters = functionDef.split('(')[1].split(')')[0].trim();
  var arrayParms = parameters.split(',');
  
  mockFunc[mockFunc.length] = '  public ' + functionDef.replace(';','{');
  mockFunc[mockFunc.length] = '    Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, \'' + 
                              functionName + '\');';
  
  for(var i=0; i < arrayParms.length; i++){
    var parmName = arrayParms[i].trim().split(' ')[1];
    mockFunc[mockFunc.length] = '    parms.put(\'' + parmName + '\',' + parmName + ');';
  }
  
  mockFunc[mockFunc.length] = '    Return (' + returnType + ') icTestMockUtilities.Tracer.GetReturnValue(this, \'' + 
                              functionName + '\');';
  mockFunc[mockFunc.length] = '  }';
  
  return mockFunc.join('\n');
}

function generateMock(className){
  var filename = className + '.cls';
  var code = parseFile('./src/classes/' + filename);
  var mockFile = '/*******************************************\n';
  mockFile +=    '*                                          *\n';
  mockFile +=    '*     MOCK Generated By Auto-Mock APEX     *\n';
  mockFile +=    '*                                          *\n';
  mockFile +=    '*******************************************/\n';
  mockFile +=    '\n';
  mockFile +=    '@isTest\n';
  mockFile +=    'public class ' + className + 'Mock implements ' + className + '.IClass{\n\n';
  
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  code = extractBlockSection('PUBLIC INTERFACE ICLASS{', code);
  
  for(i = 0; i < code.length; i++){
     mockFile += addMockedFunction(code[i]) + '\n\n';
  }
  
  mockFile += '}';

  writeFile(BASE_CLASS_PATH + className + 'Mock.cls', mockFile);
}

function extractImplementedMethods(inputCode){
  var outputCode = [];
  
  for(i = 0; i < inputCode.length; i++){
    if(inputCode[i].toUpperCase().indexOf('PUBLIC') === 0){
      var lineUpper = inputCode[i].toUpperCase();
      if(lineUpper.indexOf('INTERFACE') >= 0 || lineUpper.indexOf('CLASS') >= 0){
        continue;
      }
      if(inputCode[i].indexOf('{') === inputCode[i].length - 1 &&
        inputCode[i].toUpperCase().indexOf('GETINSTANCE()') < 0){
        outputCode[outputCode.length] = inputCode[i].substring(inputCode[i].indexOf(' ') + 1).replace('{', ';');
      }
    }  
  }
  
  return outputCode;
}

function removeExistingInterface(inputCode, className){
  var outputCode = [];
  var skipInterface = false;
  var instanceFn = false;
  var insertLine = 0;
  var implementClass = false;

  for(i = 0; i < inputCode.length; i++){
    if(inputCode[i].toUpperCase().indexOf('CLASS ') >= 0 && 
       inputCode[i].toUpperCase().indexOf(' ' + className.toUpperCase()) >= 0){
      insertLine = i + 1;      
    }else if(inputCode[i].toUpperCase().indexOf('PUBLIC INTERFACE ICLASS') >= 0){
      insertLine = i;
      if(inputCode[i].indexOf('}') < 0){
        skipInterface = true;
      }
    }else if(inputCode[i].toUpperCase().indexOf('IMPLEMENTS ICLASS') > 0){
      implementClass = true;
    }else if(inputCode[i].toUpperCase().indexOf('GETINSTANCE()') > 0){
      instanceFn = true;
    }else if(skipInterface && inputCode[i].indexOf('}') >= 0){
      skipInterface = false;
      continue;
    }
    
    if(!skipInterface){
        outputCode[outputCode.length] = inputCode[i];
    }
  }
  
  return {code: outputCode, 
          insertAt: insertLine, 
          isImplemented: implementClass,
          getInstanceFn: instanceFn};
}

function addInterface(interfaceCode, insertionCode){
  var newCode = [];
  var i = 0;
  newCode[newCode.length] = '  public Interface IClass{';
  for(i=0; i< interfaceCode.length; i++){
    newCode[newCode.length] = '    ' + interfaceCode[i].trim();
  }
  newCode[newCode.length] = '  ' + '}';

  if(!insertionCode.getInstanceFn){
    insertionCode.code.splice(insertionCode.insertAt ++,0,
                              '  public Object GetInstance(){\n    return new ClassImpl();\n  }\n');
  }
  insertionCode.code.splice(insertionCode.insertAt ++,0,newCode.join('\n'));
  if(!insertionCode.isImplemented){
    insertionCode.code.splice(insertionCode.insertAt ++,0,'\n  class ClassImpl implements IClass{');
    for(i=insertionCode.insertAt; i < insertionCode.code.length; i++){
      insertionCode.code[i] = '  ' + insertionCode.code[i];
    }
    insertionCode.code.push('}');
  }
  return insertionCode.code;
}

function extractInterface(className){
  var filename = className + '.cls';
  var originalCode = parseFile('./src/classes/' + filename);
  var code = originalCode;
  
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  code = extractImplementedMethods(code);
  
  var insertionCode = removeExistingInterface(originalCode, className);
  var newCode = addInterface(code, insertionCode);  
  var newFile = newCode.join('\n');

  writeFile(BASE_CLASS_PATH + className + '.cls', newFile);
}

function createClass(className, template){
  var file = parseFile(TEMPLATE_PATH + template + '.cls');
  var fileStr = file.join('\n');
  fileStr = fileStr.replace('%CLASSNAME%',className);
  writeFile(BASE_CLASS_PATH + className + '.cls', fileStr);
}

function retrieveListOfClasses(){
  var fs = project.createDataType('fileset');
  fs.setDir(new File(BASE_CLASS_PATH));
  fs.setIncludes('*.cls');

  var ds = fs.getDirectoryScanner(project);
  return ds.getIncludedFiles();
}

function getFunctionName(line){
  var lineParts = line.split(' ');
  for(i=0; i < lineParts.length; i++){
    if(lineParts[i] ==='('){ 
      return lineParts[i-1];
    }
    if(lineParts[i].indexOf('(') >=0){
      return lineParts[i].substring(0,lineParts[i].indexOf('('));
    }
  }
  
  return '';
}

function computeLines(blockCode, stats){
  stats.lines = (stats.lines)? stats.lines : 0;
  stats.nesting = (stats.nesting)? stats.nesting : 0;
  for(var i=0; i < blockCode.length; i++){
    stats.lines++;
    if(blockCode[i].innerBlock){
      stats = computeLines(blockCode[i].innerBlock, stats);
    }
  }
  stats.nesting++;
  return stats;
}


function parseFunctions(blockCode){
  var outputFunctions = [];
  
  for(var i=0; i < blockCode.length; i++){
    var blockLine = blockCode[i].line; 
    if(blockLine[blockLine.length-1] === '{' &&
       blockLine.toUpperCase().indexOf('INTERFACE') < 0 &&
       blockLine.toUpperCase().indexOf('CLASS') < 0){
      var innerStat = computeLines(blockCode[i].innerBlock,{});
      outputFunctions[outputFunctions.length] = {
        name: getFunctionName(blockLine),
        length: innerStat.lines,
        nesting: innerStat.nesting
      };
    }
  }
  
  return outputFunctions;
}

function parseProperties(code){
  return [];
}

function parseSubClasses(code){
  return [];
}

function parseClass(blockCode){
  var i=0;
  do{
    if(blockCode[i].line.toUpperCase().indexOf('CLASS') >= 0){
      var phrases = blockCode[i].line.split(' ');
      for(var j=0; j< phrases.length; j++){
        if(phrases[j].toUpperCase() === 'CLASS'){
          var name = phrases[j+1].replace('{','');
          return {name : name, innerBlock : blockCode[i].innerBlock};
        }
      }
    }
  }while(++i < blockCode.length);
  return '';
}

function extractBlocks(code){
  var blockCode = [];
  do{
    var newBlock = {line : code.shift()};
    blockCode[blockCode.length] = newBlock;
    if(newBlock.line[newBlock.line.length-1] === '{'){
      var retBlock = extractBlocks(code);
      newBlock.innerBlock = retBlock.innerBlock;
      code = retBlock.code;
    }
    if(newBlock.line[newBlock.line.length-1] === '}'){
      return {innerBlock: blockCode, code: code};
    }
  }while(code.length > 0);
  
  return {innerBlock: blockCode, code : []};
}

function addStatsOfClass(fileName, stats){
  var code = parseFile(BASE_CLASS_PATH + fileName);
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  
  var classIdx = stats.classes.length;
  stats.classes[classIdx] = {
    lines : 0,
    name : '',
    functions : [],
    properties : [],
    subclasses : []
  };
  stats.classes[classIdx].lines = code.length;
  var blockCode = extractBlocks(code).innerBlock;
  var classRet = parseClass(blockCode);
  stats.classes[classIdx].name =  classRet.name;
  if (stats.classes[classIdx].name === ''){
    //Maybe an Interface or other
    //for now ignore it.
    stats.classes.pop();
    return stats;
  }   
  println('class parsed:' +  stats.classes[classIdx].name);
  if(classRet.innerBlock){
    stats.classes[classIdx].functions = parseFunctions(classRet.innerBlock, stats);
  }
//   stats.classes[classIdx].properties = parseProperties(code);
//   stats.classes[classIdx].subclasses = parseSubClasses();
  return stats;
}

function analyzeCode(){
  var fileset = retrieveListOfClasses();
  
  var statsObject = {classes : []};
  for(var i=0; i < fileset.length; i++){
    statsObject = addStatsOfClass(fileset[i], statsObject);      
  }
//   computeStatsTotals(statsObject);
 //println(statsObject.toString());
 writeFile(CODEGEN_STATS_FILE, JSON.stringify(statsObject));
}

function main(){  
  var method = String(project.getProperty('method'));
  var className = String(project.getProperty('className'));
  println(method);
  if(method === 'generateMock'){
    generateMock(className);
  }else if(method === 'extractInterface'){
    extractInterface(className);    
  }else if(method === 'createClass'){
    var template = String(project.getProperty('template'));
    template = (template === 'null')? 'simpleclass' : template;
    println(template);
    createClass(className, template);
  }else if(method === 'analyzeCode'){
    analyzeCode();
  }
} 

main();
