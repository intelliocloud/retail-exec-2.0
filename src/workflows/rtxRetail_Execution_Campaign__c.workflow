<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved_Status</fullName>
        <field>rtxApproved__c</field>
        <literalValue>0</literalValue>
        <name>Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Archived_Checked</fullName>
        <description>Quand le statut change à Archivée, cocher le champ Archiveé.</description>
        <field>rtxArchived__c</field>
        <literalValue>1</literalValue>
        <name>Archived Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checkbox_Approved</fullName>
        <field>rtxApproved__c</field>
        <literalValue>1</literalValue>
        <name>Checkbox Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unchecked_Approved</fullName>
        <field>rtxApproved__c</field>
        <literalValue>0</literalValue>
        <name>Unchecked Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unchecked_Archived</fullName>
        <field>rtxArchived__c</field>
        <literalValue>0</literalValue>
        <name>Unchecked Archived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approved Status Checked</fullName>
        <actions>
            <name>Checkbox_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>rtxRetail_Execution_Campaign__c.rtxStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Quand le statut change à Approvée, le champ Approuvée est coché</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved Status Unchecked</fullName>
        <actions>
            <name>Unchecked_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>rtxRetail_Execution_Campaign__c.rtxStatus__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Quand on modifie le statut de Approuvée à quelque chose d’autre, le champ Approuvée se décoche</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved Status Unchecked not archived</fullName>
        <actions>
            <name>Approved_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>rtxRetail_Execution_Campaign__c.rtxStatus__c</field>
            <operation>notEqual</operation>
            <value>Approved,Archived</value>
        </criteriaItems>
        <description>Quand on modifie le statut de Approuvée  à quelque chose d’autre, le champ Approuvée se décoche mais pas Archived</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Archived Status Checked</fullName>
        <actions>
            <name>Archived_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>rtxRetail_Execution_Campaign__c.rtxStatus__c</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Quand le statut change à Archivée, cocher le champ Archiveé.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Archived Unchecked</fullName>
        <actions>
            <name>Unchecked_Archived</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>rtxRetail_Execution_Campaign__c.rtxStatus__c</field>
            <operation>notEqual</operation>
            <value>Archived</value>
        </criteriaItems>
        <description>Unchecked the field when the status is not Archived</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
