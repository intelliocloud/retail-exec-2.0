<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Checkbox_Completed</fullName>
        <field>rtxCompleted__c</field>
        <literalValue>1</literalValue>
        <name>Checkbox Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checkbox_Read_Only</fullName>
        <field>rtxRead_Only__c</field>
        <literalValue>1</literalValue>
        <name>Checkbox Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Checked_Read_Only</fullName>
        <field>rtxRead_Only__c</field>
        <literalValue>1</literalValue>
        <name>Checked Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Fill_time_trigger_date_field</fullName>
        <field>Time_trigger_5__c</field>
        <formula>NOW()  -  0.03800</formula>
        <name>Fill time trigger date field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Read_Only_ckecked_after_5</fullName>
        <field>rtxRead_Only__c</field>
        <literalValue>1</literalValue>
        <name>Read Only ckecked after 5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Status changes to Completed</fullName>
        <actions>
            <name>Checkbox_Completed</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Fill_time_trigger_date_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>ISPICKVAL(rtxVisitStatus__c , &quot;Completed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Checked_Read_Only</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>StoreVisit__c.Time_trigger_5__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>