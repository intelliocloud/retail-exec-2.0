<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Approved_Checked</fullName>
        <field>rtxApproved__c</field>
        <literalValue>1</literalValue>
        <name>Approved Checked</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Archived_Checked_Promotion</fullName>
        <field>rtxArchived__c</field>
        <literalValue>1</literalValue>
        <name>Archived Checked Promotion</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Unchecked_Approved</fullName>
        <field>rtxApproved__c</field>
        <literalValue>0</literalValue>
        <name>Unchecked Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approved Status Checked</fullName>
        <actions>
            <name>Approved_Checked</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Promotion__c.rtxStatus__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Approved Status Unchecked</fullName>
        <actions>
            <name>Unchecked_Approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Promotion__c.rtxStatus__c</field>
            <operation>notEqual</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Checked Archived Promotion</fullName>
        <actions>
            <name>Archived_Checked_Promotion</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Promotion__c.rtxStatus__c</field>
            <operation>equals</operation>
            <value>Archived</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
