/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestSchedulableBatchVisitActivation {

    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();

    public static testMethod void testScheduled(){

        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        Account acc = accountCreator.createAccount();
        storeVisitCreator.createStoreVisit(acc);

        Test.startTest();
        DateTime dt = DateTime.now().addMinutes(+1);
        String CRON_XP = '0 0 0 12 3 ? 2022';
        System.schedule('testing', CRON_XP, new icSchedulableBatchVisitActivation ());
        Test.stopTest();
    }

}