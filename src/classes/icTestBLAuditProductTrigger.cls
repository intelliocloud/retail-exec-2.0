/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLAuditProductTrigger {

    private static icTestHelperProduct productCreator = new icTestHelperProduct();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAuditParameter aP = new icTestHelperAuditParameter();

    public static  icBLAuditProductTrigger.IClass tap = (icBLAuditProductTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditProductTrigger');

    public static testMethod void testOnBeforeUpdate(){
        Product__c p = productCreator.createAuditableProduct();
        Account a = accountCreator.createAccount();
        ap.createAuditParameterDetail(aP.createActiveAuditParameter());
        StoreVisit__c s= storeVisitCreator.createStoreVisit(a);
        s.rtxCompleted__c = TRUE;
        s.rtxRead_Only__c = TRUE;
        update s;

        List<AuditProduct__c> apL = new List<AuditProduct__c>();

        AuditProduct__c apP = new AuditProduct__c();
        apP.Name = 'testing';
        apP.StoreVisit__c = s.Id;
        apP.Product__c = p.Id;
        insert apP;

        apL.add(apP);

        tap.onBeforeUpdate(apL);

    }



}