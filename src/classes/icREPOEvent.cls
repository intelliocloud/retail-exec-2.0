/**
 * Created by Francois Poirier on 2018-11-09.
 */

public with sharing class icREPOEvent  implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Event> saveEvents(List<Event> events);
        List<Event> getEventsByVisitIds(List<Id> visitIds);

    }

    public class Impl implements IClass{

        public List<Event> saveEvents(List<Event> events){

            upsert events;

            return events;

        }

        public List<Event> getEventsByVisitIds(List<Id> visitIds){

            List<Event> events = new List<Event>();

            events = [
                    SELECT  Id,
                            rtxStoreVisit__c
                    FROM    Event
                    WHERE   rtxStoreVisit__c IN :visitIds
            ];

            return events;
        }
    }
}