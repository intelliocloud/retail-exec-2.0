/**
 * Created by François Poirier on 1/22/2018.
 */

public with sharing class icMapRetailExecutionCampaign implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<icDTOretailExecutionCampaign> mapToDTO(Map<Id, List<AuditTask__c>> mapCampaignsAts);
        icDTOretailExecutionCampaign mapToDTO(List<AuditTask__c> auditTasks);
    }

    public class Impl implements IClass {

        private icMAPauditTask.IClass auditTaskMap = (icMAPauditTask.IClass) icObjectFactory.GetSingletonInstance('icMAPauditTask');

        public List<icDTOretailExecutionCampaign> mapToDTO(Map<Id, List<AuditTask__c>> mapCampaignsAts){

            List<icDTOretailExecutionCampaign> dtos = new List<icDTOretailExecutionCampaign>();

            for(Id campaignId : mapCampaignsAts.keySet()){

                icDTOretailExecutionCampaign dto = new icDTOretailExecutionCampaign();
                dto = mapToDTO(mapCampaignsAts.get(campaignId));
                dtos.add(dto);
            }

            return dtos;
        }

        public icDTOretailExecutionCampaign mapToDTO(List<AuditTask__c> auditTasks){

            icDTOretailExecutionCampaign dto = new icDTOretailExecutionCampaign();

            if(auditTasks.size()>0){

                dto.id = auditTasks[0].Priority_Task__r.rtxRetail_Execution_Campaign__r.Id;
                dto.name = auditTasks[0].Priority_Task__r.rtxRetail_Execution_Campaign__r.Name;
                dto.priority = auditTasks[0].Priority_Task__r.rtxRetail_Execution_Campaign__r.rtxCampaign_Priority__c;
                dto.auditTasks = auditTaskMap.mapToDTO(auditTasks);

            }

            return dto;
        }
    }


}