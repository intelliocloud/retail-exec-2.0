/**
 * Created by François Poirier on 2/15/2018.
 */

global with sharing class icDTOoperator {

    @AuraEnabled global String label {get;set;}
    @AuraEnabled global String value {get;set;}
    @AuraEnabled global String operator {get;set;}
    @AuraEnabled global List<String> applicableTypes {get;set;}
    @AuraEnabled global String jsonOperator {get;set;}

}