/**
 * Created by georg on 2018-02-26.
 */

public with sharing class icREPOAuditParameterDetail implements icIClass{


    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        List<Audit_Parameter_Detail__c> returnAPDWithFullAUActiveList();
    }

    public class Impl implements IClass {
        public List<Audit_Parameter_Detail__c> returnAPDWithFullAUActiveList(){
            return [SELECT Id, Name, rtxAdd_Audit_Task_Products__c, rtxAdd_Promotional_Products_to_Audit__c,
                            Audit_Parameters__c, rtxAudit_Task_Sorting_Field__c,
                            rtxAudit_Task_Sorting_Order__c, CreatedById,
                            rtxDisplay_Audit_Filter_Set__c, rtxDisplay_Audit_Multiedit_Set__c,
                            rtxDisplay_Audit_Sorting_Field__c, rtxDisplay_Audit_Sorting_Order__c,
                            rtxDisplay_Audit_Type__c, rtxDisplay_Field_Set__c,
                            rtxDisplay_Product_Field_Set__c, rtxExecution_Campaign_Field_Set__c,
                            rtxExecution_Campaign_Sorting_Field__c, rtxExecution_Campaign_Sorting_Order__c,
                            LastModifiedById, rtxProduct_Audit_Field_Set__c, rtxProduct_Audit_Filter_Set__c,
                            rtxProduct_Audit_Multi_Edit_Set__c, rtxProduct_Audit_Sorting_Field__c,
                            rtxProduct_Audit_Sorting_Order__c, rtxPromotion_Audit__c,
                            rtxShelf_Audit_Field_Set__c, rtxShelf_Audit_Subtype__c,
                            rtxShelf_Audit_Type__c, rtxStore_Audit_Code__c, rtxTask_Audit_Type__c,
                            rtxUser_Audit_Profile__c, rtxVisit_Audit_Code__c,
                            Audit_Parameters__r.Id, Audit_Parameters__r.rtxIsActive__c, Audit_Parameters__r.Name,
                            Audit_Parameters__r.rtxDescription__c, Audit_Parameters__r.rtxUnique_ID__c
                    FROM Audit_Parameter_Detail__c
                    WHERE Audit_Parameters__r.rtxIsActive__c = true];
        }
    }
}