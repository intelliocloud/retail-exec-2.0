/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestBLStoreVisitTrigger {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();

    public static testMethod void testStoreVisitCreation(){
        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);

        OrgCallSettings__c og = new OrgCallSettings__c();
        og.Visit_Duration_Default_Value__c = 60;
        og.DeleteAuditProducts__c = TRUE;
        og.DeleteAuditTasks__c = TRUE;
        insert og;

        update rtx;
        rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];

        StoreVisit__c visitOpen = storeVisitCreator.createStoreVisitNoInsert(acc);
        visitOpen.rtxVisitStatus__c = 'Planned';
        StoreVisit__c visitCompleted = storeVisitCreator.createStoreVisitNoInsert(acc);
        visitCompleted.rtxCompleted__c = TRUE;

        insert visitOpen;
        insert visitCompleted;

        StoreVisit__c newVisitOpen = storeVisitCreator.createStoreVisitNoInsert(acc);
        insert newVisitOpen;

        visitOpen.rtxVisitStatus__c = 'Completed';
        visitOpen.rtxNextVisitDate__c = DateTime.now().addDays(+3);
        update visitOpen;

        newVisitOpen.rtxDate__c = System.now().addDays(+3);
        update newVisitOpen;

        StoreVisit__c readOnly = storeVisitCreator.createStoreVisitNoInsert(acc);
        readOnly.rtxCompleted__c = TRUE;
        readOnly.rtxRead_Only__c = TRUE;
        insert readOnly;
            try {
                readOnly.rtxRead_Only__c = TRUE;
                update readOnly;
            }catch(Exception e){

            }
        StoreVisit__c dateNotToday = storeVisitCreator.createStoreVisitNoInsert(acc);
        dateNotToday.rtxDate__c = DateTime.now().addDays(+5);
        insert dateNotToday;

        dateNotToday.rtxDate__c = DateTime.now();
        update dateNotToday;
    }
}