/**
 * Created by François Poirier on 12/21/2017.
 */

public with sharing class icBLGeneric implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public interface IClass{

        void updateValue(sObject obj);
        icDTOGenericListView getFilteredItems(String filterId, String objectType);
    }

    public class Impl implements IClass{

        private icREPOGeneric.IClass repo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMapfieldSet');
        private icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');

        public void updateValue(sObject obj){
            system.debug('in update value BL');
            repo.updateValues(obj);
        }

        public icDTOGenericListView getFilteredItems(String filterId, String objectType){

            icDTOGenericListView retour = new icDTOGenericListView();
            List<icDTOGenericRecord> items = new List<icDTOGenericRecord>();
            List<icDTOfieldSet> tableFieldSet = new List<icDTOfieldSet>();

            system.debug('in getFilteredItems controller');
            HttpRequest req = new HttpRequest();
            String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
            String endPointURL = 'callout:rtxCallMeBack/services/data/v41.0/sobjects/' +objectType+'/listviews/'+filterId+'/describe';
            req.setEndpoint(endPointURL);
            req.setMethod('GET');
            req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
            req.setHeader('Content-Type', 'application/json');
            Http http = new Http();
            HTTPResponse response = http.send(req);
            system.debug('response ===> ' + response);
            system.debug('response body ===> ' + response.getBody());
            if(response.getStatus() == 'OK' ){
                icDTOlistView tokenResponse = (icDTOlistView) JSON.deserialize(response.getBody(), icDTOlistView.class);
                system.debug('tokenResponse ===> ' + tokenResponse);
                List<Account> sfItems = new List<Account>();
                sfItems = repo.getValues(tokenResponse.query);
                system.debug('query ===> ' + tokenResponse.query);
                system.debug('sfItems ===> ' + sfItems);
                tableFieldSet = fieldSetMap.mapToDTO(tokenResponse.columns);
                items = recordMap.mapToDTO(tableFieldSet, sfItems, objectType);
            }

            retour.records = items;
            retour.tableFieldSet = tableFieldSet;

            return retour;
        }

    }
}