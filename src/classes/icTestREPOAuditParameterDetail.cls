/**
 * Created by georg on 2018-03-09.
 */

@IsTest
public class icTestREPOAuditParameterDetail {
    static icREPOAuditParameterDetail.IClass theRepo =
            (icREPOAuditParameterDetail.IClass)icObjectFactory.GetSingletonInstance('icREPOAuditParameterDetail');
    private static icTestHelperAuditParameter aP = new icTestHelperAuditParameter();

    @IsTest
    static void testBehavior() {

        AuditParameters__c theAP = aP.createActiveAuditParameter();
        Audit_Parameter_Detail__c theAPD = aP.createAuditParameterDetail(theAP);

        List<Audit_Parameter_Detail__c> result = theRepo.returnAPDWithFullAUActiveList();

        System.assertNotEquals(0, result.size());
    }
}