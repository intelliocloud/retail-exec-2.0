/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLMarketListAccount {

    public static icTestHelperMarketListAccount mlaCreator = new icTestHelperMarketListAccount();
    public static icTestHelperAccount accCreator = new icTestHelperAccount();
    public static icTestHelperMarketList mlCreator = new icTestHelperMarketList();

    public static testMethod void getMarketListAccountsByMarketIds(){
        List<Id> mlIds = new List<Id>();
        List<Market_List_Account__c> mlaL = new List<Market_List_Account__c>();
        List<ML__c> mlsL = new List<ML__c>();

        mlIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));
        mlIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));
        Account a = accCreator.createAccount();
        ML__c ml = mlCreator.createActiveML();
        Market_List_Account__c ma = mlaCreator.createMarketListAccount(ml.Id, a.Id);

        mlaL.add(ma);

        mlsL.add(ml);

        icBLMarketListAccount.IClass apBL = (icBLMarketListAccount.IClass) icObjectFactory.getsingletoninstance('icBLMarketListAccount');
        apBL.getMarketListAccountsByMarketIds(mlIds);
        apBL.generateMLAMap(mlaL);
        apBL.createMarketListAccounts(mlsL);

    }

    public static testMethod void testCreateMarketListAccount(){

        List<Id> mlIds = new List<Id>();
        List<Market_List_Account__c> mlaL = new List<Market_List_Account__c>();
        List<ML__c> mlsL = new List<ML__c>();

        mlIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));
        mlIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));
        Account a = accCreator.createAccount();
        ML__c ml = mlCreator.createActiveML();
        Market_List_Account__c ma = mlaCreator.createMarketListAccount(ml.Id, a.Id);
        ma.rtxSelection_Type__c = 'Automatic';
        update ma;
        mlaL.add(ma);

        mlsL.add(ml);

        icBLMarketListAccount.IClass apBL = (icBLMarketListAccount.IClass) icObjectFactory.getsingletoninstance('icBLMarketListAccount');
        apBL.getMarketListAccountsByMarketIds(mlIds);
        apBL.generateMLAMap(mlaL);
        apBL.createMarketListAccounts(mlsL);

    }

}