/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOAuditParameters {

    static testMethod void testdto(){
        icDTOAuditParameters dto = new icDTOAuditParameters();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.Name);
        System.assertEquals(null, dto.description);
        System.assertEquals(null, dto.active);
        System.assertEquals(null, dto.uniqueId);
    }
}