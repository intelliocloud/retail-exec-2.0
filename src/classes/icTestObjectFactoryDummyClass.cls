/**
 * Created by François Poirier on 7/10/2017.
 */

@isTest
public class icTestObjectFactoryDummyClass implements ICIClass {

    public Object GetInstance()
    {
        return new Impl();
    }

    public Interface IClass { }

    public class Impl implements IClass { }
}