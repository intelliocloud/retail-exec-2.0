/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLAuditProduct {

    private static icTestHelperProduct productCreator = new icTestHelperProduct();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAuditParameter aP = new icTestHelperAuditParameter();

    static testMethod void testgetAuditProducts(){
        aP.createAuditParameterDetail(aP.createActiveAuditParameter());

        icBLauditProduct.IClass apBL = (icBLauditProduct.IClass) icObjectFactory.getsingletoninstance('icBLauditProduct');
        try {
            apBL.getAuditProducts(icTestHelperUtility.getFakeId(StoreVisit__c.SObjectType));
        }catch (Exception e){

        }
    }

    static testMethod void testSaveAuditProducts(){

        List<AuditProduct__c> apL = new List<AuditProduct__c>();
        List<Id> ids = new List<Id>();
        ids.add(icTestHelperUtility.getFakeId(AuditProduct__c.SObjectType));
        ids.add(icTestHelperUtility.getFakeId(AuditProduct__c.SObjectType));

        icBLauditProduct.IClass apBL = (icBLauditProduct.IClass) icObjectFactory.getsingletoninstance('icBLauditProduct');
        apBL.saveAuditProducts(apL);

    }

    static testMethod  void testCreateAuditProducts(){
        Product__c p = productCreator.createAuditableProduct();
        Account a = accountCreator.createAccount();
        ap.createAuditParameterDetail(aP.createActiveAuditParameter());
        StoreVisit__c s= storeVisitCreator.createStoreVisit(a);

        List<StoreVisit__c> sl = new List<StoreVisit__c>();
        sl.add(s);

        icBLauditProduct.IClass apBL = (icBLauditProduct.IClass) icObjectFactory.getsingletoninstance('icBLauditProduct');
        apbl.createAuditProducts(sl);
        apBL.deleteAuditProductsByStoreVisits(sl);
    }

}