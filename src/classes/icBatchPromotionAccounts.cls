/**
 * Created by Francois Poirier on 2018-11-22.
 */

global with sharing class icBatchPromotionAccounts implements Database.Batchable<sObject> {

    String query;
    String marketListId;
    String promoId;

    global icBatchPromotionAccounts(Id mlId, Id promotionId){

        query = 'SELECT Id, account__c, account__r.RecordType.Name,  marketList__c FROM Market_List_Account__c WHERE marketList__c = \'' + String.valueOf(mlId) + '\' AND rtxExcluded__c = false';
        marketListId = mlId;
        promoId = promotionId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        list<rtxPromotion_Store__c> promotionAccountsToSave = new List<rtxPromotion_Store__c>();

        for (SObject s : scope) {
            Market_List_Account__c mlAct = (Market_List_Account__c) s;
            rtxPromotion_Store__c newPromotionStore = new rtxPromotion_Store__c();
            newPromotionStore.rtxStore__c = mlAct.account__c;
            newPromotionStore.rtxPromotion__c = promoId;
            promotionAccountsToSave.add(newPromotionStore);
        }

        insert promotionAccountsToSave;
    }

    global void finish(Database.BatchableContext bc){

    }



}