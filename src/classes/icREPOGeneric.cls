
/**
 * Created by François Poirier on 11/29/2017.
 */

public with sharing class icREPOGeneric implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public interface IClass{
        List<sObject> getValues(String sObjectType, List<String> fieldsList, List<String> criteriaList, List<String> orderByList, Integer limitSize);
        List<Account> getValues(String query);
        icDTOServerResponse deleteValues(String sObjectType, List<String> criteriaList);
        Attachment saveFile(String parentId, String fileName, String base64Data, String contentType);
        icDTOServerResponse updateValues(sObject obj);
        String insertValues(sObject obj);
        icDTOServerResponse upsertValues(sObject obj);
        icDTOServerResponse upsertValues(sObject[] objs);

    }

    public class Impl implements IClass{

        icBLValidateAccess.IClass validateAccessBL = (icBLValidateAccess.IClass) icObjectFactory.GetSingletonInstance('icBLValidateAccess');

        public List<sObject> getValues(String sObjectType, List<String> fieldsList, List<String> criteriaList, List<String> orderByList, Integer limitSize){

            String soql;
            List<sObject> results = null;
            List<String> orderByDefaultList = new List<String>();
            orderByDefaultList.add('CreatedDate desc');
            orderByList = (orderByList != null && orderByList.size() > 0) ? orderByList : orderByDefaultList;
            limitSize = (limitSize != null) ? limitSize : 100;

            // validate fields access
            Boolean isValidated = true; //validateAccessBL.fieldsAreAccessible(fieldsList, sObjectType);

            if (isValidated){
                try {
                    soql = ' SELECT Id, ' + String.escapeSingleQuotes(String.join(fieldsList, ', '));
                    soql +=' FROM '   + String.escapeSingleQuotes(sObjectType);

                    if(criteriaList != null && criteriaList.size() > 0){
                        soql +=' WHERE ' + String.join(criteriaList, ' ');
                    }

                    soql +=' ORDER BY ' + String.escapeSingleQuotes(String.join(orderByList, ', '));
                    soql +=' LIMIT ' + limitSize;
                    System.debug('SOQL ===> ' + soql);
                    results = Database.query(soql);
                }catch (Exception ex){
                    System.debug   ('Error in icrREPOGeneric:getValues' +
                            '. Error message: '+ ex.getMessage() +
                            '. Stack trace: ' + ex.getStackTraceString());
                    throw ex;
                }
            }
           // System.debug('@@@result in repo generic=' + results);
            return results;
        }

        public List<Account> getValues(String query){

            return Database.query(query);
        }

        public icDTOServerResponse deleteValues(String sObjectType, List<String> criteriaList){

            String soql;
            List<sObject> results = null;
            icDTOServerResponse result = new icDTOServerResponse();
            try {
                soql = ' SELECT Id ';
                soql +=' FROM '   + String.escapeSingleQuotes(sObjectType);

                if(criteriaList != null && criteriaList.size() > 0){
                    soql +=' WHERE ' + String.join(criteriaList, ' ');
                }

                //System.debug('SOQL=' + soql);
                results = Database.query(soql);

            } catch (Exception ex) {
                System.debug   ('Error in icREPOGeneric:deleteValues' +
                        '. Error message: '+ ex.getMessage() +
                        '. Stack trace: ' + ex.getStackTraceString());
                throw ex;
            }
            if (results.size() > 0){
                delete results;
                result.isSuccess = true;
            }else{
                result.isSuccess = false;
                result.errorMessage = 'DeleteValues - Criteria not found';
            }
            //System.debug('@@@result in repo generic=' + result);
            return result;
        }

        public Attachment saveFile(String parentId, String fileName, String base64Data, String contentType){

            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

            Attachment a = new Attachment();
            a.parentId = parentId;
            a.Body = EncodingUtil.base64Decode(base64Data);
            a.Name = fileName;
            a.ContentType = contentType;
            insert a;
            Attachment b = new Attachment();
            b.Id = a.Id;
            b.Name = fileName;
            b.ContentType = contentType;
            //System.debug('@@@result in repo generic=' + a);
            return a;
        }

        public icDTOServerResponse updateValues(sObject obj){

            icDTOServerResponse result = new icDTOServerResponse();
            try {
                update obj;
                result.isSuccess = true;
            } catch (Exception ex) {
                System.debug   ('Error in icREPOGeneric:updateValues' +
                        '. Error message: '+ ex.getMessage() +
                        '. Stack trace: ' + ex.getStackTraceString());
                result.isSuccess = false;
                result.errorMessage = 'updateValues - Criteria not found';
            }
            //System.debug('@@@result in repo generic=' + result);
            return result;
        }

        public String insertValues(sObject obj){

           // System.debug('*****obj in insertValues' + obj);
            String result=null;
            try {
                insert obj;
                result = obj.Id;
            } catch (Exception ex) {
                System.debug   ('Error in icREPOGeneric:insertValues' +
                        '. Error message: '+ ex.getMessage() +
                        '. Stack trace: ' + ex.getStackTraceString());
            }
            //System.debug('@@@result in repo generic=' + result);
            return result;
        }

        public icDTOServerResponse upsertValues(sObject obj){

            icDTOServerResponse result = new icDTOServerResponse();
            try {
                upsert obj;
                result.isSuccess = true;
            } catch (Exception ex) {
                System.debug   ('Error in icREPOGeneric:upsertValues' +
                        '. Error message: '+ ex.getMessage() +
                        '. Stack trace: ' + ex.getStackTraceString());
                result.isSuccess = false;
                result.errorMessage = 'upsertValues - Criteria not found';
            }
            //System.debug('@@@result in repo generic=' + result);
            return result;
        }

        //TODO Standby need R&D for working in generic way
        public icDTOServerResponse upsertValues(sObject[] objs){

            icDTOServerResponse result = new icDTOServerResponse();
            try {
                upsert objs;
                result.isSuccess = true;
            } catch (Exception ex) {
                System.debug   ('Error in icREPOGeneric:upsertValues' +
                        '. Error message: '+ ex.getMessage() +
                        '. Stack trace: ' + ex.getStackTraceString());
                result.isSuccess = false;
                result.errorMessage = 'upsertValues - Criteria not found';
            }
            //System.debug('@@@result in repo generic=' + result);
            return result;
        }



    }
}