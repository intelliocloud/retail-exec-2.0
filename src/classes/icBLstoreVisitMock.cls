/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icBLstoreVisitMock implements icBLstoreVisit.IClass{

    public StoreVisit__c createStoreVisit(String accountId, Integer mois, Integer jour, Integer an, Integer heure){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createStoreVisit');
        params.put('accountId',accountId);
        params.put('mois',mois);
        params.put('jour',jour);
        params.put('an',an);
        params.put('heure',heure);
        return (StoreVisit__c) icTestMockUtilities.Tracer.GetReturnValue(this, 'createStoreVisit');

    }

    public List<StoreVisit__c> getCompletedVisitsByAccountIds(List<Id> accountIds){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getCompletedVisitsByAccountIds');
        params.put('accountIds',accountIds);
        return (List<StoreVisit__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getCompletedVisitsByAccountIds');
    }

    public List<StoreVisit__c> saveStoreVisits(List<StoreVisit__c> storeVisits){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveStoreVisits');
        params.put('storeVisits',storeVisits);
        return (List<StoreVisit__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveStoreVisits');
    }

    public Map<Id, StoreVisit__c> getReadOnlyVisitMapByIds(List<Id> storeVisitIds){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getReadOnlyVisitMapByIds');
        params.put('storeVisitIds',storeVisitIds);
        return ( Map<Id, StoreVisit__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getReadOnlyVisitMapByIds');
    }

    public StoreVisit__c getStoreVisitById(Id storeVisitId){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getStoreVisitById');
        params.put('storeVisitId',storeVisitId);
        return (StoreVisit__c) icTestMockUtilities.Tracer.GetReturnValue(this, 'getStoreVisitById');
    }

    public List<StoreVisit__c> retrieveStoreVisitsToActivate(List<StoreVisit__c> storevisits){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'retrieveStoreVisitsToActivate');
        params.put('storeVisits',storeVisits);
        return (List<StoreVisit__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'retrieveStoreVisitsToActivate');
    }



}