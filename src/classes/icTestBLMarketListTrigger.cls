/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLMarketListTrigger {

    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();

    public static testMethod void testOnBefore(){

        ML__c mlNA = marketListCreator.createNonActiveML();
        mlNA.rtxActive__c = TRUE;
        update mlNA;

        ML__c ml = marketListCreator.createActiveML();
        ml.rtxJSON_Filters__c = 'change';
        update ml;

        ML__c mlNR = marketListCreator.createNonActiveML();
        mlNR.rtxRefresh__c = TRUE;
        update mlNR;
    }
}