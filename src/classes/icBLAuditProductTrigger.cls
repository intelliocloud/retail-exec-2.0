/**
 * Created by Andrea Pissinis on 2018-02-09.
 */

public with sharing class icBLAuditProductTrigger implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        void onBeforeUpdate(List<AuditProduct__c> updatedAuditProduct);

    }

    public class Impl implements IClass {

        private icBLstoreVisit.IClass svBL = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLStoreVisit');

        public void onBeforeUpdate(List<AuditProduct__c> updatedAuditProduct) {

            List<Id> storeVisitIds = new List<Id>();
            for(AuditProduct__c ap : updatedAuditProduct){
                storeVisitIds.add(ap.StoreVisit__c);
            }

            Map<Id, StoreVisit__c> readOnlyVisitMap = new Map<Id, StoreVisit__c>();
            readOnlyVisitMap = svBL.getReadOnlyVisitMapByIds(storeVisitIds);

            for (AuditProduct__c ap : updatedAuditProduct) {

                if (readOnlyVisitMap.containsKey(ap.StoreVisit__c)){

                    ap.addError(Label.Read_Only_AuditProducts_Warning);

                }
            }

        }

    }
}