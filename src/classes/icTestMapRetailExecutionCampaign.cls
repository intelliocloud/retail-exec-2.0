/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestMapRetailExecutionCampaign {


    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();
    private static icTestHelperAuditTask auditTaskCreator = new icTestHelperAuditTask();

    public static testMethod void testing(){

        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
        Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
        Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
        AuditTask__c aDone = auditTaskCreator.createAudiTaskDone(acc, p, s);
        AuditTask__c aNotDone = auditTaskCreator.createAuditTaskNotDone(acc, p, s);
        update rtx;
        rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];

        List<AuditTask__c> atL = new List<AuditTask__c>();
        atL.add(aDone);

        Map<Id, List<AuditTask__c>> mapL = new Map<Id, List<AuditTask__c>>();
        mapL.put(icTestHelperUtility.getFakeId(rtxRetail_Execution_Campaign__c.SObjectType), atL);

        icMapRetailExecutionCampaign.IClass atBLbl = (icMapRetailExecutionCampaign.IClass) icObjectFactory.getsingletoninstance('icMapRetailExecutionCampaign');
        atBLbl.mapToDTO(mapL);
        atBLbl.mapToDTO(atL);

    }

}