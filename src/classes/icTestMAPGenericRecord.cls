/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestMAPGenericRecord {

    private static icTestHelperAccount accCreator = new icTestHelperAccount();

    public static testMethod void testmet(){
        List<icDTOfieldSet> fdL = new List<icDTOfieldSet>();

        icDTOfieldSet fd = new icDTOfieldSet();
        fd.apiName = 'Name';
        fd.label = 'Account Name';
        fd.type = 'Text';
        fd.required = TRUE;
        fd.hidden = false;

        fdL.add(fd);

        List<SObject> obj = new List<SObject>();
        Account acc = accCreator.createAccount();
        obj.add(acc);

        icMAPGenericRecord.IClass atBLbl = (icMAPGenericRecord.IClass) icObjectFactory.getsingletoninstance('icMAPGenericRecord');
        atBLbl.mapToDTO(fdL, obj, 'Account');

    }
}