/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDragAndDropImageMirrorController {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperProduct productCreator = new icTestHelperProduct();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();

    public static testMethod void testM(){
        Account acc = accountCreator.createAccount();

        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());

        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);

        Product__c prod = productCreator.createAuditableProduct();

        AuditProduct__c auditP = new AuditProduct__c();
        auditP.StoreVisit__c = s.Id;
        auditP.Product__c = prod.Id;
        auditP.Name ='tstingauditp';
        insert auditP;

        rtxPlanogram__c planogram = new rtxPlanogram__c();
        planogram.Name = 'testingPlano';
        planogram.rtxWidth__c = 10;
        planogram.rtxHeight__c = 10;
        insert planogram;

        rtxEquipment__c eq = new rtxEquipment__c();
        eq.Name = 'testEq';
        eq.rtxPlanogram__c = planogram.Id;
        insert eq;

        rtxPlanogram_Version__c pl = new rtxPlanogram_Version__c();
        pl.Planogram__c = planogram.Id;
        pl.Start_Date__c = Date.newInstance(1995,01,01);
        pl.End_Date__c =Date.newInstance(2095,01,01);
        insert pl;

        Section__c sectiondisplay = new Section__c();
        sectiondisplay.Name = 'testS';
        sectiondisplay.RecordTypeId = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Store Display').getRecordTypeId();
        sectiondisplay.Account__c = acc.Id;
        sectiondisplay.rtxType__c = 'Frozen';
        insert sectiondisplay;

        update pl;
        rtxPlanogram_Version__c checking = [SELECT Id,Current__c FROM rtxPlanogram_Version__c WHERE Id = :pl.Id];

        try{
            icDragAndDropImageMirrorController.getProfilePicture(auditP.Id);
        }catch(Exception e){
        }
        try{
            icDragAndDropImageMirrorController.getProfilePicture(planogram.Id);
        }catch(Exception e){
        }
        try{
            icDragAndDropImageMirrorController.getProfilePicture(eq.Id);
        }catch(Exception e){
        }
        try{
            icDragAndDropImageMirrorController.getProfilePicture(sectiondisplay.Id);
        }catch(Exception e){
        }



    }
}