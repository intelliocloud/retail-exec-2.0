/**
 * Created by Francois Poirier on 2018-11-07.
 */

public with sharing class icBLPriority implements icIClass {
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Priority__c> getPrioritiesByCampaignIds(List<Id> campaignIds);
    }

    public class Impl implements IClass {

        private icREPOPriority.IClass repoPriority = (icREPOPriority.IClass) icObjectFactory.GetSingletonInstance('icREPOPriority');

        public List<Priority__c> getPrioritiesByCampaignIds(List<Id> campaignIds){

            return repoPriority.getPrioritiesByCampaignIds(campaignIds);

        }

    }

}