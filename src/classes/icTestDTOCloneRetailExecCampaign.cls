/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOCloneRetailExecCampaign {

    static testMethod void testdto(){
        icDTOCloneRetailExecCampaign dto = new icDTOCloneRetailExecCampaign();
        System.assertEquals(null, dto.recRecord);
        System.assertEquals(null, dto.targetRoleOptions);
        System.assertEquals(null, dto.statusOptions);
        System.assertEquals(null, dto.priorityChildren);
    }
}