/**
 * Created by Andrea Pissinis on 2018-03-07.
 */

public with sharing class icTestHelperProduct {

    public Product__c createAuditableProduct(){

        Product__c p = new Product__c();
        p.Name = 'productTest';
        p.rtxIsActive__c = TRUE;
        p.rtxAllow_Audit__c = TRUE;
        RecordType rt = [SELECT Id, Name FROM RecordType WHERE SobjectType='Product__c' AND Name ='Item' Limit 1];
        p.RecordTypeId = rt.Id;
        insert p;
        return p;
    }

}