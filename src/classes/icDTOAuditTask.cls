/**
 * Created by François Poirier on 1/22/2018.
 */

global with sharing class icDTOAuditTask {

    @AuraEnabled global String id {get;set;}
    @AuraEnabled global String name {get;set;}
    @AuraEnabled global String comment {get;set;}
    @AuraEnabled global Boolean done {get;set;}
    @AuraEnabled global String priority {get;set;}
    @AuraEnabled global String value {get;set;}
    @AuraEnabled global String answerType {get;set;}
    @AuraEnabled global String stringChoices {get;set;}
    @AuraEnabled global Boolean mustDo {get;set;}

}