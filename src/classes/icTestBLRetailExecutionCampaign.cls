/**
 * Created by georg on 2018-03-07.
 */

@IsTest
private class icTestBLRetailExecutionCampaign {
    static List<Id> marketIds = new List<Id>();
    public static icBLRetailExecutionCampaignTrigger.IClass obj = (icBLRetailExecutionCampaignTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLRetailExecutionCampaignTrigger');

    static void initTest(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOMarketListAccount', new icREPOMarketListAccountMock());
    }

    @IsTest
    static void testonBeforeUpdateApproved() {
        initTest();

        marketIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));

        icTestMockUtilities.Tracer.SetReturnValue('icREPOMarketListAccountMock','getMarketListAccountsByMarketIds', marketIds);
        icTestMockUtilities.Tracer.SetReturnValue('icREPOMarketListAccountMock','getMarketListAccountsByMarketListAndExcluded', marketIds);

        Map<Id, rtxRetail_Execution_Campaign__c> oldMap = new Map<Id, rtxRetail_Execution_Campaign__c>();
        Map<Id, rtxRetail_Execution_Campaign__c> newMap = new Map<Id, rtxRetail_Execution_Campaign__c>();
        Id theId = icTestHelperUtility.getFakeId(rtxRetail_Execution_Campaign__c.SObjectType);

        rtxRetail_Execution_Campaign__c theREC = new rtxRetail_Execution_Campaign__c();
        theREC.Id = theId;
        theREC.rtxApproved__c = false;
        oldMap.put(theREC.Id, theREC);

        theREC = new rtxRetail_Execution_Campaign__c();
        theREC.Id = theId;
        theREC.rtxApproved__c = true;
        newMap.put(theREC.Id, theREC);

        obj.onBeforeUpdate(oldMap, newMap);
    }
    @isTest
    static void testonBeforeUpdateArchived() {
        initTest();

        marketIds.add(icTestHelperUtility.getFakeId(Market_List_Account__c.SObjectType));

        icTestMockUtilities.Tracer.SetReturnValue('icREPOMarketListAccountMock','getMarketListAccountsByMarketIds', marketIds);
        icTestMockUtilities.Tracer.SetReturnValue('icREPOMarketListAccountMock','getMarketListAccountsByMarketListAndExcluded', marketIds);

        Map<Id, rtxRetail_Execution_Campaign__c> oldMap = new Map<Id, rtxRetail_Execution_Campaign__c>();
        Map<Id, rtxRetail_Execution_Campaign__c> newMap = new Map<Id, rtxRetail_Execution_Campaign__c>();
        Id theId = icTestHelperUtility.getFakeId(rtxRetail_Execution_Campaign__c.SObjectType);

        rtxRetail_Execution_Campaign__c theREC = new rtxRetail_Execution_Campaign__c();
        theREC.Id = theId;
        theREC.rtxArchived__c = false;
        oldMap.put(theREC.Id, theREC);

        theREC = new rtxRetail_Execution_Campaign__c();
        theREC.Id = theId;
        theREC.rtxArchived__c = true;
        newMap.put(theREC.Id, theREC);

        obj.onBeforeUpdate(oldMap, newMap);
    }
}