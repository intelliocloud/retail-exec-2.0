/**
 * Created by Andrea Pissinis on 2018-04-06.
 */

public with sharing class icBLEventTrigger implements icIClass{


    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        void onBeforeUpdate(List<Event> newEvents);
    }

    public class Impl implements IClass {

        public void onBeforeUpdate(List<Event> newEvents) {

            for (Event e : newEvents){

                Event oldEvent = (Event)Trigger.oldMap.get(e.Id);

                if (e.rtxStoreVisit__c != null){

                    if (oldEvent.Status__c == 'Completed'){
                        e.addError(System.Label.EditEventCompletedStore);

                    }
                }

            }

        }

    }
}