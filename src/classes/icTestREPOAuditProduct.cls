/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOAuditProduct {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static List<SObject> auditPs = new List<SObject>();

    public static testMethod void testRepo(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOGeneric', new icREPOGenericMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOGenericMock','getValues', auditPs);
        List<AuditProduct__c> apL = new List<AuditProduct__c>();

        Account a = accountCreator.createAccount();
        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(a);

        Product__c p = new Product__c();
        p.RecordTypeId = Schema.SObjectType.Product__c.getRecordTypeInfosByName().get('Item').getRecordTypeId();
        p.Name = 'testP';
        insert p;

        AuditProduct__c aP = new AuditProduct__c();
        aP.Name = 'testAPName';
        aP.StoreVisit__c = s.Id;
        aP.Product__c = p.Id;
        insert aP;

        apL.add(aP);

        icREPOauditProduct.IClass aPS = (icREPOauditProduct.IClass) icObjectFactory.getsingletoninstance('icREPOauditProduct');
        aPS.getAuditProducts(s.Id);
        aPS.saveAuditProducts(apL);
        apS.getNotDoneAuditProducts();
    }
}