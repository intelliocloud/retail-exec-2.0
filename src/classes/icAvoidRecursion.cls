/**
 * Created by Andrea Pissinis on 2018-04-16.
 */

public with sharing class icAvoidRecursion {

    private static boolean firstRun = TRUE;

    public static boolean isFirstRun(){
        if (firstRun){
            firstRun = FALSE;
            return TRUE;
        }else{
            return firstRun;
        }
    }
}