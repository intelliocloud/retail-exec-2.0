/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOGenericRecord {

    static testMethod void testdto(){
        icDTOGenericRecord dto = new icDTOGenericRecord();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.fields);
    }
}