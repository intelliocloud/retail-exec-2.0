/**
 * Created by georg on 2018-03-07.
 */

@IsTest
private class icTestPlanogramTrigger {
    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLPlanogramTrigger', new icBLPlanogramTriggerMock());
    }

    @IsTest
    static void testonBeforeInsert() {
        testSetup();

        rtxPlanogram__c theP = new rtxPlanogram__c();

        insert theP;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLPlanogramTriggerMock', 'onBeforeInsert'));
    }

    @IsTest
    static void testononBeforeUpdate() {
        testSetup();

        rtxPlanogram__c theP = new rtxPlanogram__c();
        theP.rtxWidth__c = 9;

        insert theP;

        theP.rtxWidth__c = 10;
        update theP;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLPlanogramTriggerMock', 'onBeforeUpdate'));
    }
}