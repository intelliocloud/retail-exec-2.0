/**
 * Created by Andrea Pissinis on 2018-03-06.
 */
@isTest
public with sharing class icTestHelperAccount {

    public Account createAccount(){
        Account acc = new Account();
        acc.Name = 'test';
        acc.rtxAccount_ID__c = icTestHelperUtility.getFakeId(Account.SObjectType).substring(10,15);
        acc.rtxTerritory__c = '1001';
        acc.rtxDistrict__c = 'A';
        acc.rtxSegmentation__c = 'A';
        acc.rtxVisit_Strategy__c = 'Increase';
        acc.rtxVisit_Frequency__c = '1';
        acc.rtxAudit_Code__c = 'Standard';
        insert acc;
        return acc;
    }
}