/**
 * Created by Andrea Pissinis on 2018-04-16.
 */
@isTest
public with sharing class icTestBLAuditParameterTrigger {

    private static icTestHelperAuditParameter apCreator = new icTestHelperAuditParameter();

    public static testMethod void testBLAUTrigger(){

        icBLAuditParameterTrigger.IClass blAU = (icBLAuditParameterTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameterTrigger');

        AuditParameters__c a = apCreator.createActiveAuditParameter();

        AuditParameters__c b = apCreator.createActiveAuditParameter();

        List<AuditParameters__c> apLs = new List<AuditParameters__c>();

        apLs.add(a);
        apLS.add(b);
        blAU.onBeforeUpdate(apLs);
        blAU.onBeforeInsert(apLs);

    }

}