/**
 * Created by Francois Poirier on 2018-09-25.
 */

global class icBatchActivateStoreVisits implements Database.Batchable<sObject> {

    private icBLauditTasks.IClass atBL = (icBLauditTasks.IClass) icObjectFactory.GetSingletonInstance('icBLauditTasks');
    private icBLauditProduct.IClass apBL = (icBLauditProduct.IClass) icObjectFactory.GetSingletonInstance('icBLauditProduct');
    private icBLAuditParameter.IClass acBL = (icBLAuditParameter.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameter');

    String query;

    global icBatchActivateStoreVisits() {


        DateTime today = system.today();
        Datetime tomorrow = System.today().addDays(1);

        query = 'SELECT  Id, rtxDate__c, rtxCompleted__c, rtxLastActivated__c, rtxAudit_Code__c, Store__c FROM StoreVisit__c WHERE rtxCompleted__c = false AND rtxDate__c >= ';
        query += today + ' AND rtxDate__c < ' +tomorrow;
        query = query.replaceAll(' 00:00:00', 'T00:00:00z');
    }

    global Database.QueryLocator start(Database.BatchableContext bc){
        system.debug('Query ===> ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<sObject> scope){

        icFireTrigger.stopRunning();
        List<StoreVisit__c> visitesToUpdate = new List<StoreVisit__c>();
        atBL.createAuditTasks(scope, acBL.getAuditRole());
        apBL.createAuditProducts(scope);
        for(sObject s : scope){
            StoreVisit__c visit = (StoreVisit__c) s;
            visit.rtxLastActivated__c = Datetime.now();
            visitesToUpdate.add(visit);
        }
        if(!visitesToUpdate.isEmpty()) {
            update visitesToUpdate;
        }
        icFireTrigger.allowTorun();
    }

    global void finish(Database.BatchableContext bc){}
}