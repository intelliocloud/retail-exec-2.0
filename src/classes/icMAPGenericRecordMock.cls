/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icMAPGenericRecordMock {

    List<icDTOGenericRecord> mapToDTO(List<icDTOfieldSet> fieldset, List<SObject> objects, String sObjectName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('fieldset', fieldset);
        params.put('objects', objects);
        params.put('sObjectName', sObjectName);
        return (List<icDTOGenericRecord>) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');
    }


}