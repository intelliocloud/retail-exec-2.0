/**
 * Created by Andrea Pissinis on 2018-03-15.
 */

global class icCTRLNewMasterDisplay {


    @AuraEnabled
    global static icDTOEquipment getEquipments(Id accountId) {

         icBLFieldSetManager.IClass fieldSetBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
         icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
         icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMAPfieldSet');
         icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');
         icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

        icDTOEquipment eq = new icDTOEquipment();

        List<String> criterias = new List<String>();
        List<String> crit = new List<String>{'\'Banner\'','\'Standard\''};
        criterias.add('rtxTarget_Audience__c IN ' + crit );
        criterias.add('AND rtxActive__c = TRUE');

        List<String> apFields = fieldSetBL.getAPINames('rtxEquipment__c', 'temporaryfieldset');
        List<String> orderBy = new List<String>();
        orderBy.add('Name ASC');
        List<rtxEquipment__c> equipmentsL = (List<rtxEquipment__c>) genericRepo.getValues('rtxEquipment__c', apFields, criterias,
                orderBy, 50000);

        List<icDTOfieldSet> tableFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('rtxEquipment__c','temporaryfieldset'));
        system.debug('tablefieldSet ===> ' + tableFieldSet);
        eq.records = recordMap.mapToDTO(tableFieldSet, equipmentsL, 'rtxEquipment__c');
        eq.filterFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('rtxEquipment__c', 'temporaryfieldset'));
        eq.tableFieldSet = tableFieldSet;

        system.debug('dtoEquipment ===> ' + eq);

        return eq;

    }

    @AuraEnabled
    global static void createDisplays(Id accountId, List<Id> eqs){

        List<rtxEquipment__c> checkingExistance = [SELECT Id, Name, rtxEquipment_Type__c FROM rtxEquipment__c WHERE Id IN :eqs];
        Id presentoirModele = [SELECT Id FROM RecordType WHERE SobjectType = 'Section__c' and Name = 'Master Display' LIMIT 1].Id;

        List<Section__c> sectionsToCreate = new List<Section__c>();

        for (Id idEq : eqs){

            rtxEquipment__c eqTarget;

            for (rtxEquipment__c equipments : checkingExistance){
                if (equipments.Id == idEq){
                    eqTarget = equipments;
                }
            }

            Section__c masterDisplay = new Section__c();
            masterDisplay.Name = eqTarget.Name;
            masterDisplay.rtxType__c = eqTarget.rtxEquipment_Type__c;
            masterDisplay.Account__c = accountId;
            masterDisplay.rtxEquipment__c = idEq;
            masterDisplay.rtxIsActive__c = true;
            //Id presentoirModele = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Master Display').getRecordTypeId();
            masterDisplay.RecordTypeId = presentoirModele;

            sectionsToCreate.add(masterDisplay);
        }

        insert sectionsToCreate;

    }

}