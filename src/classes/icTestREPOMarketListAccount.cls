/**
 * Created by georg on 2018-03-07.
 */

@IsTest
public class icTestREPOMarketListAccount {
    static icREPOMarketListAccount.IClass theRepo =
            (icREPOMarketListAccount.IClass)icObjectFactory.GetSingletonInstance('icREPOMarketListAccount');
    @isTest
    private static void testgetMarketListAccountsByMarketIds()
    {
        Account acc = (new icTestHelperAccount()).createAccount();
        ML__c ml = (new icTestHelperMarketList()).createActiveML();
        Market_List_Account__c mla = (new icTestHelperMarketListAccount()).createMarketListAccount(ml.Id, acc.Id);
        List<Id> marketIds = new List<Id>();

        marketIds.add(ml.Id);
        List<Market_List_Account__c> result = theRepo.getMarketListAccountsByMarketIds(marketIds);
        System.assertNotEquals(0, result.size());
    }
    @isTest
    private static void testgetMarketListAccountsByMarketListAndExcluded(){
        Account acc = (new icTestHelperAccount()).createAccount();
        ML__c ml = (new icTestHelperMarketList()).createActiveML();
        Market_List_Account__c mla = (new icTestHelperMarketListAccount()).createMarketListAccount(ml.Id, acc.Id);
        mla.rtxExcluded__c = false;
        Set<Id> marketIds = new Set<Id>();

        marketIds.add(ml.Id);
        List<Market_List_Account__c> result = theRepo.getMarketListAccountsByMarketListAndExcluded(marketIds);
        System.assertNotEquals(0, result.size());
    }
}