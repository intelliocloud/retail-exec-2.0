/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestEndVisitAction {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    public static testMethod void testEndVisitAction(){

        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);

        StoreVisit__c sCompleted = new StoreVisit__c();
        s.Name = 'testingS';
        s.Store__c = acc.Id;
        s.rtxDate__c = DateTime.now();
        s.rtxAudit_Code__c = 'Standard';
        sCompleted.rtxCompleted__c = TRUE;
        insert sCompleted;

        icEndVisitAction.getStoreVisit(s.Id);
        icEndVisitAction.getStoreVisit(sCompleted.Id);
        icEndVisitAction.updateStoreVisitRecord(s);

    }
}