/**
 * Created by Andrea Pissinis on 2018-03-19.
 */

global class icDTOSection {

    @AuraEnabled global List<icDTOfieldSet> filterFieldSet {get;set;}
    @AuraEnabled global List<icDTOfieldSet> tableFieldSet {get;set;}
    @AuraEnabled global List<icDTOGenericRecord> records {get;set;}

}