/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOGenericField {

    static testMethod void testdto(){
        icDTOGenericField dto = new icDTOGenericField();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.label);
        System.assertEquals(null, dto.value);
        System.assertEquals(null, dto.bValue);
        System.assertEquals(null, dto.apiName);
        System.assertEquals(null, dto.fieldType);
        System.assertEquals(null, dto.sObjectType);
        System.assertEquals(null, dto.readOnly);
        System.assertEquals(null, dto.showLabel);

    }
}