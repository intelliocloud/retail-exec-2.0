/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestBLutilities {

    public static testmethod void testGetPicklistValues(){

        icBLutilities.IClass atBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
        atBL.getSessionId();
        atbl.getPickListValues('Account','rtxDistrict__c');
        atBL.getLookupName('StoreVisit__c');
        atBL.getLookupName('Account__r');
    }

    public static testMethod void testConvertFields(){

        icBLutilities.IClass atBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
        List<Schema.FieldSetMember> fL = new List<Schema.FieldSetMember>();
        fL.addAll(SObjectType.Account.FieldSets.visit_schedule_filter.getFields());

        atBL.convertFields(fL, Account.SObjectType);
        Account a = new Account();
        a.rtxLastVisitDate__c = System.now();
        a.SLAExpirationDate__c = DateTime.now().date();
        atBL.getRelationshipFieldValue((SObject)a,'rtxLastVisitDate__c');
        atBL.getRelationshipFieldValue((SObject)a,'SLAExpirationDate__c');
        atBL.getRelationshipFieldValue((SObject)a,'Owner.Id');
        atBL.getRelationshipFieldValue(null,'rtxDistrict__c');

    }


}