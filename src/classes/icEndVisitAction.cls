public without sharing class icEndVisitAction {

    @AuraEnabled
    public static StoreVisit__c getStoreVisit(Id storevisitId){
        StoreVisit__c storevis = [ SELECT Id, rtxEnd__c, rtxCompleted__c FROM StoreVisit__c WHERE Id = :storevisitId];
        if (!storevis.rtxCompleted__c){
            return storevis;
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static StoreVisit__c updateStoreVisitRecord(StoreVisit__c sc){
        sc.rtxEnd__c = System.NOW();
        sc.rtxVisitStatus__c = 'Completed';
        sc.rtxCompleted__c = TRUE;
        upsert sc;
        return sc;
    }
}