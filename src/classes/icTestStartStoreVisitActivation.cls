/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestStartStoreVisitActivation {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    public static testMethod void testStartStore(){

        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(accountCreator.createAccount());
        StoreVisit__c sCompleted = storeVisitCreator.createStoreVisitNoInsert(accountCreator.createAccount());
        icStartStoreVisitAction.getStoreVisit(s.Id);
        icStartStoreVisitAction.updateStoreVisitRecord(s);

        sCompleted.rtxCompleted__c = TRUE;
        insert sCompleted;
        icStartStoreVisitAction.getStoreVisit(sCompleted.Id);
    }

}