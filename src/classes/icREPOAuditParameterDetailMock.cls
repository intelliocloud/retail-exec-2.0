/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icREPOAuditParameterDetailMock implements icREPOAuditParameterDetail.IClass{

    public List<Audit_Parameter_Detail__c> returnAPDWithFullAUActiveList() {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'returnAPDWithFullAUActiveList');
        return (List<Audit_Parameter_Detail__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'returnAPDWithFullAUActiveList');
    }
}