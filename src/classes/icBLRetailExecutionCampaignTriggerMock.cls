/**
 * Created by georg on 2018-02-09.
 */
@isTest
public with sharing class icBLRetailExecutionCampaignTriggerMock implements icBLRetailExecutionCampaignTrigger.IClass {

    public void onBeforeUpdate(Map<Id, rtxRetail_Execution_Campaign__c> oldMap, Map<Id, rtxRetail_Execution_Campaign__c> newMap){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
        params.put('oldMap', oldMap);
        params.put('newMap', newMap);
    }
}