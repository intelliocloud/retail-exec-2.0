/**
 * Created by François Poirier on 2/14/2018.
 */

public with sharing class icMAPMarketList  implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOMarketListScreen mapToDTO(ML__c ml);
    }

    public class Impl implements IClass {

        public icDTOMarketListScreen mapToDTO(ML__c ml){

            icDTOMarketListScreen dtoMl = new icDTOMarketListScreen();
            dtoMl.id = ml.Id;
            dtoMl.filtersJSON = ml.rtxJSON_Filters__c;
            dtoMl.filtersLogic = ml.rtxFiltersLogic__c;

            return dtoMl;
        }
    }
}