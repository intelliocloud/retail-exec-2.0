/**
 * Created by Andrea Pissinis on 2018-03-16.
 */
@isTest
public with sharing class icTestCTRLNewMasterDisplay {

    private static icTestHelperAccount accCreator = new icTestHelperAccount();

    public static testMethod void testingCTRLNewMasterDisplay(){

        Account acc = accCreator.createAccount();

        List<Id> eqs = new List<Id>();
        rtxEquipment__c eq = new rtxEquipment__c();
        eq.Name = 'testing';
        insert eq;
        eqs.add(eq.Id);
        try {
            icCTRLNewMasterDisplay.createDisplays(acc.Id, eqs);
            icCTRLNewMasterDisplay.getEquipments(icTestHelperUtility.getFakeId(Account.SObjectType));
        }catch (Exception e){

        }
    }

}