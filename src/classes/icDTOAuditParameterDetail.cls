/**
 * Created by georg on 2018-02-26.
 */

global with sharing class icDTOAuditParameterDetail {
    @AuraEnabled global String id {get; set;}
    @AuraEnabled global String name {get; set;}
    @AuraEnabled global String addAuditTaskProducts {get; set;}
    @AuraEnabled global String addPromotionalProductsToAudit {get; set;}
    @AuraEnabled global String auditParameter {get; set;}
    @AuraEnabled global icDTOAuditParameters auditParameterRecord {get; set;}
    @AuraEnabled global String auditTaskSortingField {get; set;}
    @AuraEnabled global String auditTaskSortingOrder {get; set;}
    @AuraEnabled global String displayAuditFilterSet {get; set;}
    @AuraEnabled global String displayAuditMultiEditSet {get; set;}
    @AuraEnabled global String displayAuditSortingField {get; set;}
    @AuraEnabled global String displayAuditSortingOrder {get; set;}
    @AuraEnabled global String displayAuditType {get; set;}
    @AuraEnabled global String displayFieldSet {get; set;}
    @AuraEnabled global String displayProductFieldSet {get; set;}
    @AuraEnabled global String executionCampaignFieldSet {get; set;}
    @AuraEnabled global String executionCampaignSortingSet {get; set;}
    @AuraEnabled global String executionCampaignSortingOrder {get; set;}
    @AuraEnabled global String productAuditFieldSet {get; set;}
    @AuraEnabled global String productAuditFilterSet {get; set;}
    @AuraEnabled global String productAuditMultiEditSet {get; set;}
    @AuraEnabled global String productAuditSortingField {get; set;}
    @AuraEnabled global String productAuditSortingOrder {get; set;}
    @AuraEnabled global String promotionAudit {get; set;}
    @AuraEnabled global String shelfAuditFieldSet {get; set;}
    @AuraEnabled global String shelfAuditSubtype {get; set;}
    @AuraEnabled global String shelfAuditType {get; set;}
    @AuraEnabled global String storeAuditCode {get; set;}
    @AuraEnabled global String taskAuditType {get; set;}
    @AuraEnabled global String userAuditProfile {get; set;}
    @AuraEnabled global String visitAuditCode {get; set;}
}