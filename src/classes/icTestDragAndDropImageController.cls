/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDragAndDropImageController {


    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperProduct productCreator = new icTestHelperProduct();

    public static testMethod void testM(){
        try{
        icDragAndDropImageController.getProfilePicture(icTestHelperUtility.getFakeId(Product__c.SObjectType));
        }catch(Exception e){

        }
    }

    public static testMethod void testM2(){

        Account acc = accountCreator.createAccount();

        Product__c prod = productCreator.createAuditableProduct();

        rtxEquipment__c eq = new rtxEquipment__c();
        eq.Name = 'testEq';
        insert eq;

        rtxPlanogram__c planogram = new rtxPlanogram__c();
        planogram.Name = 'testingPlano';
        planogram.rtxWidth__c = 10;
        planogram.rtxHeight__c = 10;
        insert planogram;

        rtxPlanogram_Version__c pl = new rtxPlanogram_Version__c();
        pl.Planogram__c = planogram.Id;
        insert pl;

        Section__c sectiondisplay = new Section__c();
        sectiondisplay.Name = 'testS';
        sectiondisplay.RecordTypeId = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Store Display').getRecordTypeId();
        sectiondisplay.Account__c = acc.Id;
        sectiondisplay.rtxType__c = 'Frozen';
        insert sectiondisplay;

        icDragAndDropImageController.saveAttachment(prod.Id, 'testing', 'testbase64', 'jpeg' );
        icDragAndDropImageController.saveAttachment(pl.Id, 'testing', 'testbase64', 'jpeg' );
        icDragAndDropImageController.saveAttachment(eq.Id, 'testing', 'testbase64', 'jpeg' );
        icDragAndDropImageController.saveAttachment(sectiondisplay.Id, 'testing', 'testbase64', 'jpeg' );

    }

}