/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icREPOauditTasksMock {


    public List<AuditTask__c> getAuditTasks(Id storeVisitId){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAuditTasks');
        params.put('storeVisitId', storeVisitId);
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAuditTasks');

    }
    public List<AuditTask__c> saveAuditTasks(List<AuditTask__c> auditTasks){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveAuditTasks');
        params.put('auditTasks', auditTasks);
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveAuditTasks');

    }
    public List<AuditTask__c> getAskOnlyTasks(){

        Map<String,Object> params = icTestMockUtilities.Tracer.RegisterCall(this,'getAskOnlyTasks');
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAskOnlyTasks');
    }
    public List<AuditTask__c> getDoneTasks(){

        Map<String,Object> params = icTestMockUtilities.Tracer.RegisterCall(this,'getDoneTasks');
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getDoneTasks');
    }
    public List<AuditTask__c> getNotDoneTasks(){

        Map<String,Object> params = icTestMockUtilities.Tracer.RegisterCall(this,'getNotDoneTasks');
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getNotDoneTasks');
    }
}