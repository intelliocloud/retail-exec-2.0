/**
 * Created by François Poirier on 1/22/2018.
 */

global with sharing class icDTOAuditTaskScreen {

    @AuraEnabled global List<icDTOretailExecutionCampaign> campaigns {get;set;}

}