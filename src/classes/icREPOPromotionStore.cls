/**
 * Created by georg on 2018-02-12.
 */

    public with sharing class icREPOPromotionStore implements icIClass {
        public Object GetInstance(){
            return new Impl();
        }

        public Interface IClass {
            List<rtxPromotion_Store__c> getPromotionStoreByPromotionIds(List<Id> recIds);
        }

        public class Impl implements IClass {
            public List<rtxPromotion_Store__c> getPromotionStoreByPromotionIds(List<Id> recIds) {
                return [SELECT Id FROM rtxPromotion_Store__c WHERE rtxPromotion__c IN : recIds];
            }
        }
    }