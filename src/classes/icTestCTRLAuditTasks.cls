/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestCTRLAuditTasks {

    static icDTOAuditTaskScreen apdto = new icDTOAuditTaskScreen();

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();
    private static icTestHelperAuditTask auditTaskCreator = new icTestHelperAuditTask();

    static testMethod void test(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLauditTasksMock', new icBLauditTasksMock());
        icTestMockUtilities.Tracer.SetReturnValue('icBLauditTasksMock','getAuditTaskScreen', apdto);

        Account acc = accountCreator.createAccount();
        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
        Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
        Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
        AuditTask__c aDone = auditTaskCreator.createAudiTaskDone(acc, p, s);
        AuditTask__c aNotDone = auditTaskCreator.createAuditTaskNotDone(acc, p, s);
        update rtx;
        rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];


        icDTOAuditTaskScreen ats = icCTRLAuditTasks.getAuditTaskScreen((s.Id));
        icCTRLAuditTasks.saveAuditTasks((JSON.serialize(ats)));
    }
}