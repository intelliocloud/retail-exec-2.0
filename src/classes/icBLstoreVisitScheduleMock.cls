/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icBLstoreVisitScheduleMock implements icBLstoreVisitSchedule.IClass{

    public icDTOvisitSchedule getVisitSchedule(String strStartDate){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getVisitSchedule');
        params.put('strStartDate', strStartDate);
        return (icDTOvisitSchedule) icTestMockUtilities.Tracer.GetReturnValue(this, 'getVisitSchedule');
    }


}