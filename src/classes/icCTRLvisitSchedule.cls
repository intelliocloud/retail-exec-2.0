/**
 * Created by François Poirier on 1/26/2018.
 */

global with sharing class icCTRLvisitSchedule {

    static icBLstoreVisitSchedule.IClass bl = (icBLstoreVisitSchedule.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisitSchedule');
    static icBLstoreVisit.IClass storeVisitBl = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisit');

    @AuraEnabled global static icDTOvisitSchedule getVisitSchedule(String startDate){

        system.debug('start date in controller ===> ' + startDate);
        icDTOvisitSchedule dto = new icDTOvisitSchedule();
        dto = bl.getVisitSchedule(startDate);
        system.debug('dto ===> ' + dto);
        return dto;
    }

    @AuraEnabled global static icDTOvisitSchedule createStoreVisit(String accountId, String mois, String jour, String an, String heure){

        //This is to avoid invalid argument errors when parameters from Aura are received as integers directly.
        system.debug('mois ===> ' + mois);
        system.debug('jour ===> ' + jour);
        system.debug('an ===> ' + an);
        system.debug('heure ===> ' + heure);
        Integer iMois = Integer.valueOf(mois);
        system.debug('iMois ===> ' + iMois);
        Integer iJour = Integer.valueOf(jour);
        system.debug('iJour ===> ' + iJour);
        Integer iAn = Integer.valueOf(an);
        system.debug('iAn ===> ' + iAn);
        Integer iHeure = Integer.valueOf(heure);
        System.debug('iHeure ===> ' + heure);
        icDTOvisitSchedule dto = new icDTOvisitSchedule();

        storeVisitBl.createStoreVisit(accountId, iMois, iJour, iAn, iHeure);
        dto = bl.getVisitSchedule('');
        return  dto;
    }
}