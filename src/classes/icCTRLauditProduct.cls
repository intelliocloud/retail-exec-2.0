/**
 * Created by François Poirier on 11/27/2017.
 */

global class icCTRLauditProduct {

    static icBLauditProduct.IClass auditProductBL = (icBLauditProduct.IClass) icObjectFactory.getSingletonInstance('icBLauditProduct');

    @AuraEnabled
    global static String test(String myMsg){
        return myMsg;
    }

    @AuraEnabled
    global static icDTOAuditProduct getAuditProducts(Id storeVisitId) {

        return auditProductBL.getAuditProducts(storeVisitId);

    }

    @AuraEnabled
    public static void saveAuditProducts(List<AuditProduct__c> auditProducts) {

        system.debug('auditProducts in controller backend ===> ' + auditProducts);
        auditProductBL.saveAuditProducts(auditProducts);

    }
}