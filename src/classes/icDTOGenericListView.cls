/**
 * Created by François Poirier on 1/30/2018.
 */

public with sharing class icDTOGenericListView {

    @AuraEnabled public List<icDTOGenericRecord> records {get;set;}
    @AuraEnabled public List<icDTOfieldSet> tableFieldSet {get;set;}
}