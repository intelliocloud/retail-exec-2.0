/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public class icTestHelperMarketList {

    public ML__c createActiveML(){

        ML__c mml = new ML__c();
        mml.Name = 'testml';
        mml.rtxActive__c = TRUE;
        insert mml;
        return mml;
    }

    public ML__c createNonActiveML(){

        ML__c mml = new ML__c();
        mml.Name = 'testml1';
        mml.rtxActive__c = FALSE;
        mml.rtxRefresh__c = FALSE;
        insert mml;
        return mml;
    }

}