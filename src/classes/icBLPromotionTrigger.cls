/**
 * Created by georg on 2018-02-19.
 */

public with sharing class icBLPromotionTrigger implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        void onBeforeInsert(Map<Id, Promotion__c> oldMap, Map<Id, Promotion__c> newMap);
        void onBeforeUpdate(Map<Id, Promotion__c> oldMap, Map<Id, Promotion__c> newMap);
    }

    public class Impl implements IClass {
        private icREPOMarketListAccount.IClass mlaRepo = (icREPOMarketListAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOMarketListAccount');
        private icREPOPromotionStore.IClass psRepo = (icREPOPromotionStore.IClass) icObjectFactory.GetSingletonInstance('icREPOPromotionStore');

        public void onBeforeInsert(Map<Id, Promotion__c> oldMap, Map<Id, Promotion__c> newMap){}
        public void onBeforeUpdate(Map<Id, Promotion__c> oldMap, Map<Id, Promotion__c> newMap) {
            List<Promotion__c> recordsWhenSetActive = new List<Promotion__c>();
            List<Id> recordsWhenSetArchived = new List<Id>();
            boolean actionIsApproved = false;
            boolean actionIsArchived = false;

            for( Id promotionId : Trigger.newMap.keySet() )
            {
                if( !oldMap.get( promotionId ).rtxApproved__c  && newMap.get( promotionId ).rtxApproved__c)
                {
                    recordsWhenSetActive.add(newMap.get( promotionId ));
                    actionIsApproved = true;
                }
                if(!oldMap.get( promotionId ).rtxArchived__c  && newMap.get( promotionId ).rtxArchived__c){
                    recordsWhenSetArchived.add(newMap.get( promotionId ).id);
                    actionIsArchived = true;
                }
            }
            if (actionIsApproved){
                promotionApprovedCreatePromotionStore(recordsWhenSetActive);
                actionIsApproved = false;
            }
            if (actionIsArchived){
                promotionArchivedDeletePromotionStore(recordsWhenSetArchived);
                actionIsArchived = false;
            }
        }
        private void promotionApprovedCreatePromotionStore(List<Promotion__c> promotions){

            for(Promotion__c promotion : promotions){

                Database.executeBatch(new icBatchPromotionAccounts(promotion.rtxMarket_List__c, promotion.Id));

            }

        }

        private void promotionArchivedDeletePromotionStore(List<Id> records){
            delete psRepo.getPromotionStoreByPromotionIds(records);
        }
    }

}