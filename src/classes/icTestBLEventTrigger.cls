/**
 * Created by Andrea Pissinis on 2018-04-09.
 */
@isTest
public with sharing class icTestBLEventTrigger {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    public static  icBLEventTrigger.IClass tap = (icBLEventTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLEventTrigger');

    public static testMethod void testBLTriggerEvent(){

        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);

        try {
            Event e = new Event();
            e.Subject = 'testing';
            e.StartDateTime = DateTime.now();
            e.EndDateTime = DateTime.now().addHours(+1);
            e.rtxStoreVisit__c = s.Id;
            e.Status__c = '';
            insert e;

            e.Status__c = 'Completed';
            update e;

            List<Event> events = new List<Event>();
            events.add(e);
            tap.onBeforeUpdate(events);
        }catch (Exception e){

        }
    }

}