/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLAuditTaskTrigger {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();
    private static icTestHelperAuditTask auditTaskCreator = new icTestHelperAuditTask();

    public static  icBLAuditTaskTrigger.IClass tap = (icBLAuditTaskTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditTaskTrigger');

    public static testMethod void testOnBeforeUpdate(){

        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
        Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
        AuditTask__c aNotDone = auditTaskCreator.createAuditTaskNotDone(acc, p, s);
        update rtx;
        rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];
        s.rtxCompleted__c = TRUE;
        s.rtxRead_Only__c = TRUE;
        update s;

        List<AuditTask__c> atl = new List<AuditTask__c>();
        atl.add(aNotDone);
        tap.onBeforeUpdate(atl);

    }

}