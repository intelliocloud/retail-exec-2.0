/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestActivateVisitManualAction {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    public static testMethod void testactivate(){

        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(accountCreator.createAccount());
        StoreVisit__c sCompleted = storeVisitCreator.createStoreVisitNoInsert(accountCreator.createAccount());
        icActivateVisitManualAction.getStoreVisit(s.Id);
        icActivateVisitManualAction.updateStoreVisitRecord(s);

        sCompleted.rtxCompleted__c = TRUE;
        insert sCompleted;
        icActivateVisitManualAction.getStoreVisit(sCompleted.Id);
    }

}