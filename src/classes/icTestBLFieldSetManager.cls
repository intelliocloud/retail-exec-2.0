/**
 * Created by François Poirier on 11/29/2017.
 */

@isTest

public with sharing class icTestBLFieldSetManager {

    static icBLFieldSetManager.IClass fieldSetManagerBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');

    @isTest static void testHasFieldSet(){


        Boolean hasFieldSet = fieldSetManagerBL.hasFieldSet('Account', 'aabbcc');

        system.assert(!hasFieldSet);

    }

    @isTest static void testGetFields(){

        List<Schema.FieldSetMember> myFieldSet = fieldSetManagerBL.getFields('Account', 'aabbcc');

        system.assertEquals(null, myFieldSet);
    }

    @isTest static void testGetPickListValues(){
        Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();

        List<SelectOption> selectedOptions = fieldSetManagerBL.GetPickListValues(globalSchema, 'AuditProduct__c', 'rtxNF__c');

        System.assert(selectedOptions!=null);
        System.assertEquals(0, selectedOptions.size());

        selectedOptions = fieldSetManagerBL.GetPickListValues(globalSchema, 'AuditProduct__c', 'rtxOOS__c');

        System.assert(selectedOptions!=null);
        System.assertEquals(2, selectedOptions.size());

        selectedOptions = fieldSetManagerBL.GetPickListValues(globalSchema, 'Product__c', 'rtxType__c');

        System.assert(selectedOptions!=null);
        System.assert(selectedOptions.size()!=0);
    }


    @isTest static void testGetObjectsAndFieldsFromDataSet(){

        Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();

        List<Schema.FieldSetMember> fieldSet = new List<Schema.FieldSetMember>();

        Map<String, List<String>> mapObjectsToFields = fieldSetManagerBL.GetObjectsAndFieldsFromDataSet(globalSchema, fieldSet, 'AuditProduct__c');

        System.assert(mapObjectsToFields!=null);
        System.assertEquals(0, mapObjectsToFields.size());

        List<Schema.FieldSetMember> fieldNamesFilterList = fieldSetManagerBL.getFields('AuditProduct__c', 'Audit_Product_Filter');

        mapObjectsToFields = fieldSetManagerBL.GetObjectsAndFieldsFromDataSet(globalSchema, fieldNamesFilterList, 'AuditProduct__c');

        System.assert(mapObjectsToFields!=null);
        System.assertNotEquals(0, mapObjectsToFields.size());
    }

    @isTest static void testGetMapFieldObjectsToSelectedOptions(){
       Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();

        List<Schema.FieldSetMember> fieldSet = new List<Schema.FieldSetMember>();

        List<Schema.FieldSetMember> fieldNamesFilterList = fieldSetManagerBL.getFields('AuditProduct__c', 'Audit_Product_Filter');

        Map<String, List<String>> mapObjectsToFields = fieldSetManagerBL.GetObjectsAndFieldsFromDataSet(globalSchema, fieldNamesFilterList, 'AuditProduct__c');

        Map<String, List<SelectOption>> mapfieldObjectsToSelectedOptions = fieldSetManagerBL.GetMapFieldObjectsToSelectedOptions(mapObjectsToFields, globalSchema, 'AuditProduct__c');

        System.assert(mapfieldObjectsToSelectedOptions!=null);
        System.assertNotEquals(0, mapfieldObjectsToSelectedOptions.size());
    }

    @isTest static void testgetAPINames(){
        List<String> apiNames = fieldSetManagerBL.getAPINames('AuditProduct__c', 'Audit_Product_filter');

        System.assert(apiNames!=null);
        System.assertNotEquals(0, apiNames.size());
    }

}