/**
 * Created by François Poirier on 1/22/2018.
 */

global with sharing class icDTOretailExecutionCampaign {

    @AuraEnabled global String id {get;set;}
    @AuraEnabled global String name {get;set;}
    @AuraEnabled global Decimal priority {get;set;}
    @AuraEnabled global List<icDTOAuditTask> auditTasks {get;set;}

}