/**
 * Created by Andrea Pissinis on 2018-02-16.
 */

public with sharing class icREPOAuditParameter implements icIClass{


    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<AuditParameters__c> returnAuditParameterList();
        List<Audit_Parameter_Detail__c> returnAuditParameterDetailsList(List<AuditParameters__c> asd);
        Audit_Parameter_Detail__c getAuditParameter(List<Audit_Parameter_Detail__c> auditpds, List<AuditParameters__c> audits, StoreVisit__c s);
        List<AuditParameters__c> getActiveAuditParameters();
        List<AuditParameters__c> saveAuditParameters(List<AuditParameters__c> auditParameters);

    }

    public class Impl implements IClass {

        public List<AuditParameters__c> returnAuditParameterList(){

            List<AuditParameters__c> auditPs = new List<AuditParameters__c>();
            auditPs = [
                    SELECT Id,
                           OwnerId
                    FROM AuditParameters__c
                    where rtxisactive__c = TRUE
            ];


            return auditPs;
        }


        public List<Audit_Parameter_Detail__c> returnAuditParameterDetailsList(List<AuditParameters__c> auditPs){

            List<Audit_Parameter_Detail__c> auditPDs = new List<Audit_Parameter_Detail__c>();

            Id auditId = (auditPs.get(0)).Id;

            auditPDs = [SELECT
                            Id,
                            rtxUser_Audit_Profile__c,
                            rtxVisit_Audit_Code__c,
                            rtxStore_Audit_Code__c,
                            rtxShelf_Audit_Type__c,
                            rtxShelf_Audit_Subtype__c,
                            rtxDisplay_Audit_Type__c ,
                            rtxTask_Audit_Type__c
                            FROM Audit_Parameter_Detail__c
                            WHERE Audit_Parameters__c = :auditId];

            return auditPDs;
        }

        public Audit_Parameter_Detail__c getAuditParameter(List<Audit_Parameter_Detail__c> auditPDs, List<AuditParameters__c> auditPs, StoreVisit__c sv){

            Id auditId = (auditPs.get(0)).Id;
            Id userId = (auditPs.get(0)).OwnerId;
            List<User> AuditUser = [SELECT
                                        rtxAuditRole__c
                                        FROM User
                                        where Id = :userId];

            List<Account> stores = [SELECT Id,
                                        rtxAudit_Code__c
                                        FROM Account];

            for (Audit_Parameter_Detail__c a : auditPDs){
                if (a.rtxUser_Audit_Profile__c != null && a.rtxUser_Audit_Profile__c.equals(AuditUser.get(0).rtxAuditRole__c)){
                    System.debug('INSIDE 1*');
                    if (a.rtxVisit_Audit_Code__c.equals(sv.rtxAudit_Code__c)) {
                        System.debug('INSIDE 2**');
                        String accACode;
                        for (Account at : stores){
                            if (at.Id == sv.Store__c){
                                System.debug('INSIDE 3***');
                                accACode = at.rtxAudit_Code__c;
                            }
                        }
                        if( a.rtxStore_Audit_Code__c.equals(accACode) ){
                            System.debug('INSIDE 4****');
                            if (a.rtxShelf_Audit_Type__c.equals('Products')){
                                System.debug('INSIDE 5*****');
                                if(a.rtxShelf_Audit_Subtype__c == 'None' && a.rtxDisplay_Audit_Type__c == 'None'){
                                    System.debug('INSIDE 6******');
                                    if (a.rtxTask_Audit_Type__c.equals('Standard')){
                                        System.debug('INSIDE 7*******');
                                        return a;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        public List<AuditParameters__c> getActiveAuditParameters(){
            List<AuditParameters__c> auditParameters = new List<AuditParameters__c>();
            auditParameters = [
                    SELECT  Id,
                            rtxIsActive__c
                    FROM    AuditParameters__c
                    WHERE rtxIsActive__c = TRUE
            ];
            return auditParameters;
        }

        public List<AuditParameters__c> saveAuditParameters(List<AuditParameters__c> auditParameters){

            upsert auditParameters;
            return auditParameters;
        }
    }

}