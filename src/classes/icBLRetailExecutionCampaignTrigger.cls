/**
 * Created by georg on 2018-02-08.
 */

public with sharing class icBLRetailExecutionCampaignTrigger  implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        void onBeforeUpdate(Map<Id, rtxRetail_Execution_Campaign__c> oldMap, Map<Id, rtxRetail_Execution_Campaign__c> newMap);
    }

    public class Impl implements IClass {
        private icREPOMarketListAccount.IClass mlaRepo = (icREPOMarketListAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOMarketListAccount');
        private icREPOCampaignAccount.IClass caRepo = (icREPOCampaignAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOCampaignAccount');

        public void onBeforeUpdate(Map<Id, rtxRetail_Execution_Campaign__c> oldMap, Map<Id, rtxRetail_Execution_Campaign__c> newMap) {
            List<rtxRetail_Execution_Campaign__c> recordsWhenSetActive = new List<rtxRetail_Execution_Campaign__c>();
            List<Id> recordsWhenSetArchived = new List<Id>();
            boolean actionIsApproved = false;
            boolean actionIsArchived = false;

            for( Id retailExecCampaign : newMap.keySet() )
            {
                if( !oldMap.get( retailExecCampaign ).rtxApproved__c  && newMap.get( retailExecCampaign ).rtxApproved__c)
                {
                    recordsWhenSetActive.add(newMap.get( retailExecCampaign ));
                    actionIsApproved = true;
                }
                if(!oldMap.get( retailExecCampaign ).rtxArchived__c  && newMap.get( retailExecCampaign ).rtxArchived__c){
                    recordsWhenSetArchived.add(newMap.get( retailExecCampaign ).id);
                    actionIsArchived = true;
                }
            }
            if (actionIsApproved){
                ExecutionCampaignApprovedCreateCampaignAccount(recordsWhenSetActive);
                actionIsApproved = false;
            }
            if (actionIsArchived){
                ExecutionCampaignArchivedDeleteCampaignAccount(recordsWhenSetArchived);
                actionIsArchived = false;
            }
        }
        private void ExecutionCampaignApprovedCreateCampaignAccount(List<rtxRetail_Execution_Campaign__c> reCampaigns){

            for(rtxRetail_Execution_Campaign__c rec : reCampaigns){

                Database.executeBatch(new icBatchRetailExecutionCampaignAccounts(rec.rtxMarket_List__c, rec.Id));
            }

        }

        private void ExecutionCampaignArchivedDeleteCampaignAccount(List<Id> records){
            Database.executeBatch(new icBatchDeleteRetailExecCampaignAccounts(records));
            //delete caRepo.getCampaignAccountsByRetailExecutionCampaignIds(records);
        }
    }
}