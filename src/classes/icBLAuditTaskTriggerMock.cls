/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icBLAuditTaskTriggerMock implements icBLAuditTaskTrigger.IClass{

    public void onBeforeUpdate(List<AuditTask__c> updatedAuditTask){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
        parms.put('updatedAuditTask',updatedAuditTask);
    }
}