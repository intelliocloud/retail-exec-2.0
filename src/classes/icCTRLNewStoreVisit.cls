/**
 * Created by Denis Roy on 2018-04-17.
 */

global with sharing class icCTRLNewStoreVisit {
    static icBLstoreVisit.IClass blStoreVisit = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisit');

    @AuraEnabled
    global static String createNewStoreVisit(String accountId) {
        Datetime dtNow = Datetime.now();
        StoreVisit__c newStoreVisit = blStoreVisit.createStoreVisit(accountId, dtNow.month(), dtNow.day(), dtNow.year(), dtNow.hour());
        return newStoreVisit.Id;
    }
}