/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestHelperRetailExecutionCampaign {

    public rtxRetail_Execution_Campaign__c createApprovedCurrentMerchandiserCampaign(ML__c mml){

        rtxRetail_Execution_Campaign__c rtx = new rtxRetail_Execution_Campaign__c();
        rtx.Name = 'test1';
        rtx.rtxTarget_Role__c = 'Merchandiser';
        rtx.rtxApproved__c = TRUE;
        rtx.rtxCampaign_Priority__c = 1;
        rtx.rtxMarket_List__c = mml.Id;
        rtx.rtxStart_Date__c = Date.newInstance(1995,01,01);
        rtx.rtxEnd_Date__c = Date.newInstance(2095,01,01);
        insert rtx;
        return rtx;
    }

    public rtxCampaign_Account__c createCampaignAccount(rtxRetail_Execution_Campaign__c rtx, Account acc){
        rtxCampaign_Account__c caa = new rtxCampaign_Account__c();

        caa.rtxAccount__c = acc.Id;
        caa.rtxRetail_Execution_Campaign__c = rtx.Id;
        caa.rtxExcluded__c = FALSE;

        insert caa;
        return caa;
    }

}