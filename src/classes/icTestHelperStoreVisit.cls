/**
 * Created by Andrea Pissinis on 2018-03-06.
 */
@isTest
public with sharing class icTestHelperStoreVisit {

    private static void setCustomSettings(){
            OrgCallSettings__c og = new OrgCallSettings__c();
            og.Visit_Duration_Default_Value__c = 60;
            insert og;
    }

    public StoreVisit__c createStoreVisit(Account store){
        setCustomSettings();

        StoreVisit__c s = new StoreVisit__c();
        s.Name = 'testingS';
        s.Store__c = store.Id;
        s.rtxDate__c = DateTime.now();
        s.rtxAudit_Code__c = 'Standard';
        insert s;
        return s;
    }

    public StoreVisit__c createStoreVisitNoInsert(Account store){

        StoreVisit__c s = new StoreVisit__c();
        s.Name = 'testingS';
        s.Store__c = store.Id;
        s.rtxDate__c = DateTime.now();
        s.rtxAudit_Code__c = 'Standard';
        return s;
    }

}