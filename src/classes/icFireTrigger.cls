/**
 * Created by François Poirier on 7/10/2017.
 */

public with sharing class icFireTrigger {

    private static Boolean shouldIRun = true;

    public static Boolean canIRun(){
        return shouldIRun;
    }

    public static void stopRunning(){
        shouldIrun = false;
    }

    public static void allowTorun(){
        shouldIRun = true;
    }
}