/**
 * Created by François Poirier on 9/20/2017.
 */

global with sharing class icCTRLAuditTasks {

    static icBLauditTasks.IClass auditTaskBL = (icBLauditTasks.IClass) icObjectFactory.getSingletonInstance('icBLauditTasks');

    @AuraEnabled
    global static icDTOAuditTaskScreen getAuditTaskScreen(Id storeVisitId){

        system.debug('stroreVisitId ===> ' + storeVisitId);
        return auditTaskBL.getAuditTaskScreen(storeVisitId);

    }

    @AuraEnabled
    global static void saveAuditTasks(String auditTaskScreen){

        icDTOAuditTaskScreen auditTaskScreenDto = new icDTOAuditTaskScreen();
        auditTaskScreenDto = (icDTOAuditTaskScreen) JSON.deserialize(auditTaskScreen, icDTOAuditTaskScreen.class);

        system.debug('auditTaskScreen dto in ctrl ===> ' + auditTaskScreen);
        auditTaskBL.saveAuditTasks(auditTaskScreenDto);

    }
}