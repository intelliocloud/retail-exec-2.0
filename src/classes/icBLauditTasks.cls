/* Created by François Poirier on 16/01/17*/
/* Created by François Poirier on 16/01/17*/

public with sharing class icBLauditTasks implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<AuditTask__c> getAuditTasks(Id storeVisitId);
        icDTOAuditTaskScreen getAuditTaskScreen(Id storeVisitId);
        icDTOAuditTaskScreen saveAuditTasks(icDTOAuditTaskScreen auditTaskScreen);
        void createAuditTasks(List<StoreVisit__c> newStoreVisits, String auditRole);
        void deleteAuditTasksByStoreVisits(List<StoreVisit__c> deactivatedStoreVisits);
        List<AuditTask__c> getNotDoneTasksByVisitIds(List<Id> visitIds);
        void deleteAuditTasks(List<AuditTask__c> auditTasks);

    }

    public class Impl implements IClass{

        private icREPOauditTasks.IClass auditTasksRepo = (icREPOauditTasks.IClass) icObjectFactory.getSingletonInstance('icREPOauditTasks');
        private icMapAuditTaskScreen.IClass atScreenMap = (icMapAuditTaskScreen.IClass) icObjectFactory.GetSingletonInstance('icMapAuditTaskScreen');
        private icMAPauditTask.IClass auditTaskMap = (icMAPauditTask.IClass) icObjectFactory.GetSingletonInstance('icMAPauditTAsk');
        private icBLCampaignAccounts.IClass blCampaignAccount = (icBLCampaignAccounts.IClass) icObjectFactory.GetSingletonInstance('icBLCampaignAccounts');
        private icBLPriority.IClass blPriority = (icBLPriority.IClass) icObjectFactory.GetSingletonInstance('icBLPriority');

        public icDTOAuditTaskScreen getAuditTaskScreen(Id storeVisitId){

            icDTOAuditTaskScreen retour = new icDTOAuditTaskScreen();
            List<AuditTask__c> auditTasks = new List<AuditTask__c>();
            auditTasks = getAuditTasks(storeVisitId);
            Map<Id, List<AuditTask__c>> mapCampaignsATs = new Map<Id, List<AuditTask__c>>();
            mapCampaignsATs = getMapRECampaignAuditTasks(auditTasks);

            retour = atScreenMap.mapToDTO(mapCampaignsATs);

            return retour;

        }

        public List<AuditTask__c> getAuditTasks(Id storeVisitId){

            return auditTasksRepo.getAuditTasks(storeVisitId);

        }

        public icDTOAuditTaskScreen saveAuditTasks(icDTOAuditTaskScreen auditTaskScreen){

           List<AuditTask__c> auditTasks = new List<AuditTask__c>();

            auditTasks = buildAuditTasksList(auditTaskScreen);
            auditTasksRepo.saveAuditTasks(auditTasks);

            return auditTaskScreen;

        }

        public void createAuditTasks(List<StoreVisit__c> newStoreVisits, String auditRole) {

            List<Id> accountIds = new List<Id>();
            List<Id> storeVisitIds = new List<Id>();
            List<Id> campaignsIds = new List<Id>();

            for(StoreVisit__c sv : newStoreVisits){
                accountIds.add(sv.Store__c);
                storeVisitIds.add(sv.Id);
            }

            List<AuditTask__c> auditTs = auditTasksRepo.getAskOnlyTasks();
            List<AuditTask__c> auditTsBeenDone = auditTasksRepo.getDoneTasks();
            List<AuditTask__c> auditTasksNotDone = auditTasksRepo.getNotDoneTasks();

            List<rtxCampaign_Account__c> campaignAccList = new List<rtxCampaign_Account__c>();
            campaignAccList = blCampaignAccount.getCurrentCampaignAccountByAccountIds(accountIds);

            for(rtxCampaign_Account__c ca : campaignAccList){
                campaignsIds.add(ca.rtxRetail_Execution_Campaign__c);
            }

            List<Priority__c> priorities = new List<Priority__c>();
            priorities = blPriority.getPrioritiesByCampaignIds(campaignsIds);

            List<AuditTask__c> tasksToAdd = new List<AuditTask__c>();

            for (StoreVisit__c s : newStoreVisits) {

                Set<Id> execCampaigns = new Set<Id>();
                List<Priority__c> prioritiesAudit = new List<Priority__c>();

                for (rtxCampaign_Account__c ca : campaignAccList){
                    if (ca.rtxAccount__c == s.Store__c && ca.rtxRetail_Execution_Campaign__r.rtxTarget_Role__c.equals(auditRole)){
                        execCampaigns.add(ca.rtxRetail_Execution_Campaign__c);
                    }
                }

                for (Id ids : execCampaigns){
                    for (Priority__c p : priorities){
                        if (p.rtxRetail_Execution_Campaign__c == ids){
                            prioritiesAudit.add(p);
                        }
                    }
                }
                for (Priority__c p : prioritiesAudit){

                    if (p.rtxFrequency__c.equals('Ask Always')) {
                        Integer doneFlag = 0;
                        for (AuditTask__c aTD : auditTsBeenDone){
                            if ((aTD.Account__c == s.Store__c) && (aTD.Priority_Task__c == p.Id) && (aTD.Store_Visit__c == s.Id)){
                                doneFlag ++;
                            }
                        }

                        for (AuditTask__c taskNotDone : auditTasksNotDone){
                            if (taskNotDone.Store_Visit__c == s.Id && taskNotDone.Priority_Task__c == p.Id){
                                doneFlag++;
                            }
                        }
System.debug('THE DONE FLAG IS ' + doneFlag);
                        if (doneFlag == 0){
                            AuditTask__c at = new AuditTask__c();
                            at.Name = p.Name;
                            at.Priority_Task__c = p.Id;
                            at.Account__c = s.Store__c;
                            at.Store_Visit__c = s.Id;

                            tasksToAdd.add(at);
                        }

                    }
                    if (p.rtxFrequency__c.equals('Ask Only Once')) {
                        Integer flag = 0;
                        for (AuditTask__c aTT : auditTs){
                            if ((aTT.Account__c == s.Store__c) && (aTT.Priority_Task__c == p.Id)){
                                // it means that you already have an audit task that is DONE for a priority ask once for that Store.
                                // so you dont need to do it again which means that THIS specific Priority is alreaedy done, skip!
                                flag++;
                            }
                        }
                        for (AuditTask__c taskNotDone : auditTasksNotDone){
                            if (taskNotDone.Store_Visit__c == s.Id && taskNotDone.Priority_Task__c == p.Id){
                                flag++;
                            }
                        }
                        if ( flag == 0 ){ //means you did NOT find an audit task that was already done, create it!
                            AuditTask__c at = new AuditTask__c();
                            at.Name = p.Name;
                            at.Priority_Task__c = p.Id;
                            at.Account__c = s.Store__c;
                            at.Store_Visit__c = s.Id;

                            tasksToAdd.add(at);
                        }
                    }

                }

            }
            System.debug('THE TASKSSS + ' + tasksToAdd);
            insert tasksToAdd;

        }

        public void deleteAuditTasksByStoreVisits(List<StoreVisit__c> st){

            List<AuditTask__c> auditTasksNotDone = auditTasksRepo.getNotDoneTasks();

            List<AuditTask__c> auditTasksToDelete = new List<AuditTask__c>();

            for (StoreVisit__c s : st){
                System.debug('STORE IS' + s + 'THE AUDITS ARE ' + auditTasksNotDone) ;

                for ( AuditTask__c a : auditTasksNotDone ){
                    if (a.Store_Visit__c == s.Id){
                        auditTasksToDelete.add(a);
                    }
                }
            }

            auditTasksRepo.deleteAuditTasks(auditTasksToDelete);
        }

        public List<AuditTask__c> getNotDoneTasksByVisitIds(List<Id> visitIds){
            return auditTasksRepo.getNotDoneTasksByVisitIds(visitIds);
        }

        public void deleteAuditTasks(List<AuditTask__c> auditTasks){
            auditTasksRepo.deleteAuditTasks(auditTasks);
        }

        private List<AuditTask__c> buildAuditTasksList(icDTOAuditTaskScreen auditTaskScreen){

            List<AuditTask__c> auditTasks = new List<AuditTask__c>();

            for(icDTOretailExecutionCampaign reCampaign : auditTaskScreen.campaigns){

                auditTasks.addAll(auditTaskMap.mapToAuditTask(reCampaign.auditTasks));

            }

            return auditTasks;

        }

        private Map<Id, List<AuditTask__c>> getMapRECampaignAuditTasks(List<AuditTask__c> auditTasks){

            Map<Id, List<AuditTask__c>> campaignAtsMap = new Map<Id, List<AuditTask__c>>();

            for(AuditTask__c a : auditTasks){

                //system.debug('audit task ===> ' + a);
                if(campaignAtsMap.containsKey(a.Priority_Task__r.rtxRetail_Execution_Campaign__r.Id)){
                    campaignAtsMap.get(a.Priority_Task__r.rtxRetail_Execution_Campaign__r.Id).add(a);
                }
                else{

                    List<AuditTask__c> ats = new List<AuditTask__c>();
                    ats.add(a);
                    campaignAtsMap.put(a.Priority_Task__r.rtxRetail_Execution_Campaign__r.Id, ats);

                }
            }

            return campaignAtsMap;
        }
    }


}