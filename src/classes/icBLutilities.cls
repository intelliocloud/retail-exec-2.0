public with sharing class icBLutilities implements icIClass {

	public Object GetInstance(){

		return new Impl();
	}

	public interface IClass{
        
        List<icDTOpickList> getPickListValues(String objectApiName, String fieldApiName);
        List<Schema.DescribeFieldResult> convertFields(List<Schema.FieldSetMember> members, Schema.SObjectType type);
        String getLookupName(String crossObjectReference);
        Object getRelationshipFieldValue(SObject sObj, String field);
        Object formatFieldValue(SObject sObj, String field);
        String getSessionId();
        List<icDTOpickList> getObjectFields(String objectApiName);
        List<icDTOoperator> getOperators();
        icDTOAuditParameterDetail getAuditParameters();
        List<icDTOfilterItem> getFiltersFromJSON(String jsonFilters);
    }

    public class Impl implements IClass {

        private icBLAuditParameterDetail.IClass blAPD = (icBLAuditParameterDetail.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameterDetail');
        private icBLuser.IClass blUser = (icBLuser.IClass) icObjectFactory.GetSingletonInstance('icBLuser');

        public String getSessionId(){

            return UserInfo.getSessionId();
        }

        public List<icDTOpickList> getPickListValues(String objectApiName, String fieldApiName){
            List<icDTOpickList> lstPickvals=new List<icDTOpickList>();

            Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(objectApiName);//From the Object Api name retrieving the SObject
            Sobject mySobject = sObjectType.newSObject();
            Schema.sObjectType sobject_type = mySobject.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobjectDescribe = sobject_type.getDescribe(); //describe the sobject
            
            Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap(); //get a map of fields for the passed sobject

            List<Schema.PicklistEntry> pickListValues = fieldMap.get(fieldApiName).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject

            icDTOpickList emptyElement = new icDTOpickList();
            lstPickvals.add(emptyElement);
            system.debug('picklistvalues ===> ' + pickListValues);
            for (Schema.PicklistEntry a : pickListValues) 
            {
                icDTOpickList pl = new icDTOpickList();
                    pl.label = a.getLabel(); 
                    pl.value = a.getValue(); 
                    pl.isActive = a.isActive(); 
                    pl.isDefault = a.isDefaultValue(); 
                    pl.isSelected = false;

                lstPickvals.add(pl);
            }

            return lstPickvals;
        }
        
        public List<Schema.DescribeFieldResult> convertFields(List<Schema.FieldSetMember> members, Schema.SObjectType type){
            
            List<Schema.DescribeFieldResult> describes = new List<Schema.DescribeFieldResult>();

            for (Schema.FieldSetMember member : members)
            {
                describes.add(new Parser(member, type).getDescribe());
            }
            
            return describes;
        }	

        public String getLookupName(String crossObjectReference){

            // would need to think about this more if there are managed packages
            return crossObjectReference.endsWith('__r') ?
                crossObjectReference.replace('__r', '__c') :
                crossObjectReference + 'Id';
        }

        public Object getRelationshipFieldValue(SObject sObj, String field){

            if(sObj == null){
                return null;
            }
        
            if(field.contains('.')){
                String nextField = field.substringAfter('.');
                String relation = field.substringBefore('.');
                
                return getRelationshipFieldValue((SObject)sObj.getSObject(relation), nextField);
            }
            else{
                return formatFieldValue(sObj, field);
            }	
        }		

        public Object formatFieldValue(SObject sObj, String field){
            Object value = sObj.get(field);
            Schema.DisplayType fieldType = sObj.getSObjectType().getDescribe().fields.getMap().get(field).getDescribe().getType();

            if(fieldType == Schema.DisplayType.Date){
                value = Date.valueOf(value).format();
            }
            else if(fieldType == Schema.DisplayType.DateTime){
                value = DateTime.valueOf(value).format();
            }

            return value;
        }

        public List<icDTOpickList> getObjectFields(String objectApiName){

            List<icDTOpickList> dtoFields = new List<icDTOpickList>();
            icDTOpickList emptyRecord = new icDTOpickList();
            dtoFields.add(emptyRecord);

            Map<String, Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objectApiName).getDescribe().fields.getMap(); //get a map of fields for the passed sobject
            for(String fieldName : fieldMap.keySet()){
                system.debug('field in getObjectFields ===> ' + fieldMap.get(fieldName).getDescribe());
                icDTOpickList dtoField = new icDTOpickList();
                Schema.DescribeFieldResult field = fieldMap.get(fieldName).getDescribe();
                dtoField.label = field.label;
                dtoField.value = JSON.serialize(field);
                dtoField.type = String.valueOf(field.getType());
                dtoFields.add(dtoField);
                system.debug('dtoField ===> ' + dtoField);
            }
            return dtoFields;
        }

        public List<icDTOoperator> getOperators(){

            List<icDTOoperator> operators = new List<icDTOoperator>();


            icDTOoperator tmpOperator = new icDTOoperator();
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_Equals;
            tmpOperator.value = 'equals';
            tmpOperator.operator = '=';

            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_NotEqual;
            tmpOperator.value = 'notequal';
            tmpOperator.operator = '!=';
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_LessThan;
            tmpOperator.value = 'less';
            tmpOperator.operator = '<';
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_GreaterThan;
            tmpOperator.value = 'greater';
            tmpOperator.operator = '>';
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_LessOrEqual;
            tmpOperator.value = 'lessorequal';
            tmpOperator.operator = '<=';
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_GreaterOrEqual;
            tmpOperator.value = 'greaterorequal';
            tmpOperator.operator = '>=';
            operators.add(tmpOperator);

            tmpOperator = new icDTOoperator();
            tmpOperator.label = System.Label.Operator_Contains;
            tmpOperator.value = 'contains';
            tmpOperator.operator = 'LIKE';
            operators.add(tmpOperator);

            for(icDTOoperator dto : operators){
                dto = this.createJSON(dto);
            }

            return operators;


        }

        private icDTOoperator createJSON(icDTOoperator dto){

            dto.jsonOperator = '{"value":"' + dto.value + '","label":"' + dto.label + '","operator":"' + dto.operator + '"}';
            return dto;
        }

        public List<icDTOfilterItem> getFiltersFromJSON(String jsonFilters){

            List<icDTOfilterItem> dtos = new List<icDTOfilterItem>();
            return dtos;
        }

        public icDTOAuditParameterDetail getAuditParameters(){
            Map<String, icDTOAuditParameterDetail> parametersMap = new Map<String, icDTOAuditParameterDetail>();
            icDTOAuditParameterDetail currentRoleParameters = new icDTOAuditParameterDetail();
            String userAuditRole;

            parametersMap = blAPD.getDTOAPDForActiveAP();
            system.debug('parametersMap ===> ' + parametersMap);

            userAuditRole = blUser.getCurrentUserAuditRole();

            system.debug('userAuditRole ===> ' + userAuditRole);

            if(parametersMap.containsKey(userAuditRole)){
                currentRoleParameters = parametersMap.get(userAuditRole);
            }

            return currentRoleParameters;
        }


    }
    private class Parser{

        private icBLutilities.IClass UtilBL = (icBLutilities.IClass) icObjectFactory.getSingletonInstance('icBLUtilities');

        public List<String> path {get; set;}
        public Schema.SObjectType type {get; set;}

        public Parser(Schema.FieldSetMember member, Schema.SObjectType type){

            System.debug('member: ' + member.getFieldPath());
            this.path = member.getFieldPath().split('\\.');
            system.debug('Path: ' + path);
            this.type = type;
        }

        public Schema.DescribeFieldResult getDescribe(){

            // could possibly make this recursive
            while (path.size() > 1)
            {
                traverse();
            }

            return describe(path[0]);
        }

        private void traverse(){

            if (path.size() == 1) return;
            
            String lookupName = UtilBL.getLookupName(path.remove(0));
            Schema.DescribeFieldResult describe = describe(lookupName);
            
            this.type = describe.getReferenceTo()[0];
            // this would get a lot more hairy if we want to deal with polymorphic lookups properly.
        }

        Schema.DescribeFieldResult describe(String field){

            return type.getDescribe().fields.getMap().get(field).getDescribe();
        }
    }

}