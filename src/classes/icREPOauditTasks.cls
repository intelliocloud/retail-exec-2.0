/**
 * Created by François Poirier on 12/8/2017.
 */

public with sharing class icREPOauditTasks implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<AuditTask__c> getAuditTasks(Id storeVisitId);
        List<AuditTask__c> saveAuditTasks(List<AuditTask__c> auditTasks);
        List<AuditTask__c> getAskOnlyTasks();
        List<AuditTask__c> getDoneTasks();
        List<AuditTask__c> getNotDoneTasks();
        void deleteAuditTasks(List<AuditTask__c> auditTasks);
        List<AuditTask__c> getNotDoneTasksByVisitIds(List<Id> visitIds);

    }

    public class Impl implements IClass{

        private icREPOGeneric.IClass genREPO = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
        private icDTOAuditParameterDetail auditParameters = utilBL.getAuditParameters();

        public List<AuditTask__c> getAuditTasks(Id storeVisitId){

            String auditTasksSortingOrder = auditParameters.auditTaskSortingOrder;
            String auditTasksSortingField = auditParameters.auditTaskSortingField;

            List<String> apFields = new List<String>();
            String ff = 'Name,rtxPriority__c,rtxValue__c,rtxDone__c,rtxComment__c,Priority_Task__r.rtxRetail_Execution_Campaign__r.Id,Priority_Task__r.rtxRetail_Execution_Campaign__r.Name,Priority_Task__r.rtxRetail_Execution_Campaign__r.rtxCampaign_Priority__c,Priority_Task__r.rtxSequence__c,Priority_Task__r.rtxAnswerType__c,Priority_Task__r.rtxChoices__c, rtxMustDo__c';
            apFields.add(ff);
            List<String> fields = ff.split(',');

            List<String> criterias = new List<String>();
            criterias.add('Store_Visit__c = \'' + storeVisitId + '\'');

            List<String> orderBy = new List<String>();
            String orderByString = 'Priority_Task__r.rtxRetail_Execution_Campaign__r.rtxCampaign_Priority__c';

            if (fields.contains(auditTasksSortingField)){
                orderByString += ','+auditTasksSortingField;
            }else{
                orderByString += ',rtxPriority__c, Priority_Task__r.rtxSequence__c';
            }
            if (auditTasksSortingOrder.equals('Ascending')){
                orderByString += ' ASC';
            }else{
                orderByString += ' DESC';
            }

            orderBy.add(orderByString);
            List<AuditTask__c> auditTasksList = (List<AuditTask__c>) genREPO.getValues('AuditTask__c', apFields, criterias, orderBy, 50000);

            return auditTasksList;
        }

        public List<AuditTask__c> saveAuditTasks(List<AuditTask__c> auditTasks){

            List<AuditTask__c> toUpdate = new List<AuditTask__c>();
            List<AuditTask__c> toInsert = new List<AuditTask__c>();
            List<AuditTask__c> retour = new List<AuditTask__c>();

            for(AuditTask__c auditTask : auditTasks){
                if(String.isBlank(auditTask.Id)){
                    toInsert.add(auditTask);
                }
                else {
                    toUpdate.add(auditTask);
                }
            }

            if(toUpdate.size()>0){
                update toUpdate;
            }

            if(toInsert.size()>0){
                insert toInsert;
            }

            retour.addAll(toInsert);
            retour.addAll(toUpdate);

            return retour;

        }

        public List<AuditTask__c> getAskOnlyTasks(){

            List<String> auditTaskFields = new List<String>{'Store_Visit__c','Priority_Task__c','Account__c','Priority_Task__r.rtxSequence__c'};
            List<String> auditTaskAskOnlyriteria = new List<String>{'rtxDone__c = TRUE', 'AND Priority_Task__r.rtxFrequency__c = \''+ String.escapeSingleQuotes('Ask Only Once')+'\''};

            List<AuditTask__c> auditTs = genREPO.getValues('AuditTask__c', auditTaskFields, auditTaskAskOnlyriteria, new List<String>{'Priority_Task__r.rtxSequence__c ASC'}, 50000);
            return auditTs;
        }

        public List<AuditTask__c> getDoneTasks(){
            List<String> auditTaskFields = new List<String>{'Store_Visit__c','Priority_Task__c','Account__c','Priority_Task__r.rtxSequence__c'};
            List<String> auditTaskDoneCriteria = new List<String> {'rtxDone__c = TRUE'};

            List<AuditTask__c> auditTsBeenDone = genREPO.getValues('AuditTask__c', auditTaskFields, auditTaskDoneCriteria, new List<String>{'Priority_Task__r.rtxSequence__c ASC'}, 50000);
            return auditTsBeenDone;
        }

        public List<AuditTask__c> getNotDoneTasks(){
            List<String> auditTaskFields = new List<String>{'Store_Visit__c','Priority_Task__c','Account__c','Priority_Task__r.rtxSequence__c'};
            List<String> auditTaskNotDoneCriteria = new List<String> {'rtxDone__c = FALSE'};

            List<AuditTask__c> auditTasksNotDone = genREPO.getValues('AuditTask__c', auditTaskFields,auditTaskNotDoneCriteria, new List<String>{'Priority_Task__r.rtxSequence__c ASC'}, 50000 );
            return auditTasksNotDone;
        }

        public void deleteAuditTasks(List<AuditTask__c> auditTasks){
            delete auditTasks;
        }

        public List<AuditTask__c> getNotDoneTasksByVisitIds(List<Id> visitIds){
            List<AuditTask__c> auditTasks = new List<AuditTask__c>();

            auditTasks = [
                    SELECT  Id,
                            rtxDone__c,
                            Store_Visit__c
                    FROM    AuditTask__c
                    WHERE   Store_Visit__c IN :visitIds
                    AND     rtxDone__c = false
            ];
            return auditTasks;
        }
    }

}