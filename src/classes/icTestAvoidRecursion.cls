/**
 * Created by Andrea Pissinis on 2018-04-16.
 */
@isTest
public with sharing class icTestAvoidRecursion {

    public static testmethod void testAvoidRec(){
        icAvoidRecursion.isFirstRun();
        icAvoidRecursion.isFirstRun();
    }

}