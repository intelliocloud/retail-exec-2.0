/**
 * Created by georg on 2018-02-15.
 */

global with sharing class icDTOCloneRetailExecCampaign {
    @AuraEnabled global rtxRetail_Execution_Campaign__c recRecord {get;set;}
    @AuraEnabled global List<icDTOpickList> targetRoleOptions {get; set;}
    @AuraEnabled global List<icDTOpickList> statusOptions {get; set;}
    @AuraEnabled global List<Priority__c> priorityChildren {get; set;}
}