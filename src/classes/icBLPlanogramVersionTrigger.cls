/**
 * Created by georg on 2018-02-20.
 */

public with sharing class icBLPlanogramVersionTrigger implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        void onBeforeInsert(List<rtxPlanogram_Version__c> newList);
        void onAfterInsert(Map<Id, rtxPlanogram_Version__c> newMap);
        void onBeforeUpdate(Map<Id, rtxPlanogram_Version__c> oldMap, Map<Id, rtxPlanogram_Version__c> newMap);
    }

    public class Impl implements IClass {
        private icBLPlanogramVersion.IClass pvBL = (icBLPlanogramVersion.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramVersion');

        public void onBeforeInsert(List<rtxPlanogram_Version__c> newList) {
            List<String> planogramIds = getParentIds(newList);
            newList = pVersionValidateOverlapingDates(newList, planogramIds, new List<String>());
        }

        public void onAfterInsert(Map<Id, rtxPlanogram_Version__c> newMap) {

            Map<Id, rtxPlanogram_Version__c> oldMap;
            planogramCurrentVersionValidation(newMap, oldMap);
        }

        public void onBeforeUpdate(Map<Id, rtxPlanogram_Version__c> oldMap, Map<Id, rtxPlanogram_Version__c> newMap) {
            List<String> planogramIds = getParentIds(newMap.values());
            List<String> newListIds = getListIds(newMap.values());

            List<rtxPlanogram_Version__c> newValuesList = pVersionValidateOverlapingDates(newMap.values(), planogramIds, newListIds);

            newMap = new Map<Id, rtxPlanogram_Version__c>();

            for (rtxPlanogram_Version__c pvTmp : newValuesList){
                newMap.put(pvTmp.Id, pvTmp);
            }

            planogramCurrentVersionValidation(newMap, oldMap);
        }

        private List<rtxPlanogram_Version__c> pVersionValidateOverlapingDates(List<rtxPlanogram_Version__c> newList, List<String> planogramIds, List<String> newIds){

            List<rtxPlanogram_Version__c> pvList = new List<rtxPlanogram_Version__c>();

            pvList = pvBL.getPlanogramVersionNotInListAndByPlanogramIds(planogramIds, newIds);


            for(rtxPlanogram_Version__c newPVTmp : newList) {
                for (rtxPlanogram_Version__c pvTmp : pvList) {
                    if ((newPVTmp.Planogram__c == pvTmp.Planogram__c)&&
                            ((pvTmp.Start_Date__c <= newPVTmp.Start_Date__c && pvTmp.End_Date__c >= newPVTmp.Start_Date__c)||
                            (pvTmp.Start_Date__c <= newPVTmp.End_Date__c && pvTmp.End_Date__c >= newPVTmp.Start_Date__c))){
                        newPVTmp.addError(Label.PlanogramVersionOverlapError);
                    }
                }
            }
            return newList;
        }

        private void planogramCurrentVersionValidation(Map<Id, rtxPlanogram_Version__c> newMap, Map<Id, rtxPlanogram_Version__c> oldMap){

            List<rtxPlanogram__c> planogramToUpdate = new List<rtxPlanogram__c>();
            for(rtxPlanogram_Version__c pvTmp : newMap.values()){
                rtxPlanogram__c tempPlanogram = new rtxPlanogram__c();
                if(pvTmp.Current__c){

                    tempPlanogram.Id = pvTmp.Planogram__c;
                    tempPlanogram.Planogram_Version__c = pvTmp.Id;

                    planogramToUpdate.add(tempPlanogram);

                }
                else{
                    if(oldMap.containsKey(pvTmp.Id)) {
                        rtxPlanogram_Version__c oldPv = oldMap.get(pvTmp.Id);
                        if (oldPv.Current__c) {

                            tempPlanogram.Id = pvTmp.Planogram__c;
                            tempPlanogram.Planogram_Version__c = null;
                            planogramToUpdate.add(tempPlanogram);
                        }
                    }
                }
            }

            if (planogramToUpdate.size()>0){
                system.debug('planogramsToUpdate ===> ' + planogramToUpdate);
                update planogramToUpdate;
            }
        }

        private List<String> getListIds (List<rtxPlanogram_Version__c> sourceList){
            List<String> listIds = new List<String> ();
            for (rtxPlanogram_Version__c pv : sourceList){
                listIds.add('\'' + (String)pv.Id + '\'');
            }
            return listIds;
        }

        private List<String> getParentIds(List<rtxPlanogram_Version__c> newList){
            List<string> result = new List<string>();
            for (rtxPlanogram_Version__c pvTmp : newList){
                result.add('\'' + (String)pvTmp.Planogram__c + '\'');
            }
            return result;
        }

        private Integer recordIndex(List<rtxPlanogram__c> planogramToUpdate, Id searchValue){
            for(rtxPlanogram__c pTmp : planogramToUpdate){
                if (pTmp.Id == searchValue){
                    return planogramToUpdate.indexOf(pTmp);
                }
            }
            return -1;
        }
    }
}