/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icREPOMarketListAccountMock implements icREPOMarketListAccount.IClass{

    public List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMarketListAccountsByMarketIds');
        params.put('marketIds', marketIds);
        return (List<Market_List_Account__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMarketListAccountsByMarketIds');

    }

    public List<Market_List_Account__c> getMarketListAccountsByMarketListAndExcluded(Set<Id> activeRetailExecCampaignAndMarketListIds) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMarketListAccountsByMarketListAndExcluded');
        params.put('activeRetailExecCampaignAndMarketListIds', activeRetailExecCampaignAndMarketListIds);
        return (List<Market_List_Account__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMarketListAccountsByMarketListAndExcluded');
    }

    public void deleteAutomaticAccountsByMLId(List<Id> mlIds){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'deleteAutomaticAccountsByMLId');
        params.put('mlIds', mlIds);
    }
}