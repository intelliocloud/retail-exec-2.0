/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestAuditProductTrigger {

    private static icTestHelperProduct productCreator = new icTestHelperProduct();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAuditParameter aP = new icTestHelperAuditParameter();

    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLAuditProductTrigger', new icBLAuditProductTriggerMock());
    }

    public static testMethod void testOnBeforeU(){

        testSetup();

        Product__c p = productCreator.createAuditableProduct();
        Account a = accountCreator.createAccount();
        ap.createAuditParameterDetail(aP.createActiveAuditParameter());
        StoreVisit__c s= storeVisitCreator.createStoreVisit(a);

        AuditProduct__c auP = new AuditProduct__c();
        auP.StoreVisit__c = s.Id;
        auP.Name = 'testAP';
        auP.Product__c = p.Id;
        insert auP;

        auP.Name = 'testUpdate';
        update auP;


        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLAuditProductTriggerMock', 'onBeforeUpdate'));

    }

}