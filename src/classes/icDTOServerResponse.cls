/**
 * Created by François Poirier on 11/29/2017.
 */

public with sharing class icDTOServerResponse {

    public boolean isSuccess {get;set;}
    public String errorMessage {get;set;}
    
}