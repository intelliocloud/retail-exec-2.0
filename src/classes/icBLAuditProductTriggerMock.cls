/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public class icBLAuditProductTriggerMock implements icBLAuditProductTrigger.IClass{

    public void onBeforeUpdate(List<AuditProduct__c> updatedAuditProduct){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
        parms.put('updatedAuditProduct',updatedAuditProduct);
    }
}