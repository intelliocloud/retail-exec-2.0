/**
 * Created by Francois Poirier on 2018-03-13.
 */

public with sharing class icREPOuser implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        User getCurrentUserInfo();
        User getUserById(Id userId);

    }

    public class Impl implements IClass {

        public User getCurrentUserInfo(){

            String userId = UserInfo.getUserId();

            return getUserById(userId);

        }

        public User getUserById(Id userId){

            List<User> users = new List<User>();
            User retour = new User();

            users = [
                    SELECT  Id,
                            Name,
                            rtxAuditRole__c
                    FROM    User
                    WHERE   Id = :userId
            ];

            if(users.size() > 0 ){
                retour = users[0];
            }

            return retour;
        }
    }

    }