/**
 * Created by François Poirier on 2/14/2018.
 */

public with sharing class icBLMarketList implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOMarketListScreen getMarketListScreenById(String mlId);
        ML__c getMarketListById(String mlId);
        void saveMarketListFilterJSON(String mlId, String filterJSON, String filtersLogic);
    }

    public class Impl implements IClass {

        private icREPOMarketList.IClass mlRepo = (icREPOMarketList.IClass) icObjectFactory.GetSingletonInstance('icREPOMarketList');
        private icMAPMarketList.IClass mlMap = (icMAPMarketList.IClass) icObjectFactory.GetSingletonInstance('icMAPMarketList');
        private icBLaccount.IClass accountBL = (icBLaccount.IClass) icObjectFactory.GetSingletonInstance('icBLaccount');

        public icDTOMarketListScreen getMarketListScreenById(String mlId){

            icDTOMarketListScreen mlDTO = new icDTOMarketListScreen();
        system.debug('mLId ===> ' + mlId);

            mlDTO =  mlMap.mapToDTO(mlRepo.getMarketListById(mlId));
            system.debug('mlDTO ===> ' + mlDTO);
            mlDTO = accountBL.getAccountsForMaketList(mlDTO);

            return mlDTO;
        }

        public ML__c getMarketListById(String mlId){
            return mlRepo.getMarketListById(mlId);
        }

        public void saveMarketListFilterJSON(String mlId, String filterJSON, String filtersLogic){

            ML__c ml = new ML__c();

            ml.Id = mlId;
            ml.rtxJSON_Filters__c = filterJSON;
            ml.rtxFiltersLogic__c = filtersLogic;
            mlRepo.saveMaketList(ml);

        }

    }
}