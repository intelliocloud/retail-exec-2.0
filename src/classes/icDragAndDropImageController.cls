public class icDragAndDropImageController {
    @AuraEnabled
    public static ContentVersion getProfilePicture(Id parentId) {

        List<ContentDocumentLink> attachments = [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :parentId];
        if (!attachments.isEmpty()){
            ContentDocumentLink att = attachments.get(attachments.size()-1);
            List<ContentVersion> attachmentsList = [SELECT Id, Title, FileExtension FROM ContentVersion WHERE ContentDocumentId = :att.ContentDocumentId];
            ContentVersion attachmentFinal = attachmentsList.get(0);

            String newurl = URL.getSalesforceBaseUrl().toExternalForm();
            newurl += '/sfc/servlet.shepherd/version/download/' + attachmentFinal.Id;

            if (parentId.getSobjectType() == (Product__c.SObjectType)) {
                Product__c p = [SELECT Id, rtxProduct_Image_Link__c FROM Product__c WHERE Id= :parentId];
                if(p.rtxProduct_Image_Link__c != newurl){
                    p.rtxProduct_Image_Link__c = newurl;
                    update p;
                }
            }else if(parentId.getSobjectType() == (rtxEquipment__c.SObjectType)){
                rtxEquipment__c e = [SELECT Id,rtxEquipment_Image_Link__c FROM rtxEquipment__c WHERE Id =:parentId];
                if(e.rtxEquipment_Image_Link__c != newurl){
                    e.rtxEquipment_Image_Link__c = newurl;
                    update e;
                }
            }else if(parentId.getSobjectType() == (rtxPlanogram_Version__c.SObjectType)){
                rtxPlanogram_Version__c pv = [SELECT Id, Planogram_Image_Link__c FROM rtxPlanogram_Version__c WHERE Id= :parentId];

                if(pv.Planogram_Image_Link__c != newurl){
                    List<SObject> objs = new List<SObject>();

                    List<rtxPlanogram__c> pl = [SELECT Id, Planogram_Version__c FROM rtxPlanogram__c WHERE Planogram_Version__c = :pv.Id ];
                    if (!pl.isEmpty()){
                        List<rtxEquipment__c> eq = [SELECT Id, rtxPlanogram_Image_Link__c, rtxPlanogram__c FROM rtxEquipment__c WHERE rtxPlanogram__c = :pl.get(0).Id];
                        if (!eq.isEmpty()) {
                            eq.get(0).rtxPlanogram_Image_Link__c = newurl;
                            objs.add(eq.get(0));
                        }
                    }

                    pv.Planogram_Image_Link__c = newurl;
                    objs.add(pv);

                    update objs;
                }
            }

            return attachmentFinal;
        }
        return null;

    }

    @AuraEnabled
    public static String getObjectTypeLabel(Id parentId){

        if (parentId.getSobjectType() == (Product__c.SObjectType)) {
            return SObjectType.Product__c.getLabel();
        }
        if (parentId.getSobjectType() == (rtxEquipment__c.SObjectType)){
            return SObjectType.rtxEquipment__c.getLabel();
        }
        if (parentId.getSobjectType() == (rtxPlanogram_Version__c.SObjectType)){
            return SObjectType.rtxPlanogram_Version__c.getLabel();
        }
        if (parentId.getSobjectType() == (Section__c.SObjectType)){
            return SObjectType.Section__c.getLabel();
        }
        return 'image';
    }

    @AuraEnabled
    public static Id saveAttachment(Id parentId, String fileName, String base64Data, String contentType) {

        List<SObject> objs = new List<SObject>();

        Attachment attachment = new Attachment();
        attachment.parentId = parentId;
        attachment.body = EncodingUtil.base64Decode(base64Data);
        attachment.name = fileName;
		attachment.contentType = contentType;

        ContentVersion cv = new ContentVersion();
        cv.title = fileName;
        cv.versionData = attachment.body;
        cv.title = attachment.name;
        cv.description = attachment.description;
        cv.pathOnClient = '/' + attachment.name;
        cv.firstPublishLocationId = attachment.parentId;

        insert cv;

        String newurl = URL.getSalesforceBaseUrl().toExternalForm();
        newurl += '/sfc/servlet.shepherd/version/download/' + cv.Id;

        if (parentId.getSobjectType() == (Product__c.SObjectType)) {
            Product__c p = [SELECT Id, rtxProduct_Image_Link__c FROM Product__c WHERE Id= :parentId];
            p.rtxProduct_Image_Link__c = newurl;
            update p;
        }else if(parentId.getSobjectType() == (rtxEquipment__c.SObjectType)){

            rtxEquipment__c e = [SELECT Id,rtxEquipment_Image_Link__c FROM rtxEquipment__c WHERE Id =:parentId];
            e.rtxEquipment_Image_Link__c = newurl;

            update e;
        }else if(parentId.getSobjectType() == (rtxPlanogram_Version__c.SObjectType)){

            rtxPlanogram_Version__c pv = [SELECT Id, Planogram_Image_Link__c FROM rtxPlanogram_Version__c WHERE Id= :parentId];
            List<rtxPlanogram__c> pl = [SELECT Id, Planogram_Version__c FROM rtxPlanogram__c WHERE Planogram_Version__c = :pv.Id ];
            if (! pl.isEmpty()){

                List<rtxEquipment__c> eq = [SELECT Id, rtxPlanogram_Image_Link__c, rtxPlanogram__c FROM rtxEquipment__c WHERE rtxPlanogram__c = :pl.get(0).Id];

                pv.Planogram_Image_Link__c = newurl;
                objs.add(pv);

                if (!eq.isEmpty()) {
                    eq.get(0).rtxPlanogram_Image_Link__c = newurl;
                    objs.add(eq.get(0));

                }
            }
            if (!objs.isEmpty()){
                update objs;
            }
        }

        return cv.Id;

    }

    @AuraEnabled
    public static Id uploadImageMobile(Id parentId){
        return parentId;
    }

    @AuraEnabled
    public static void uploadImageLinkMobile(Id parentId){

    }


}