/**
 * Created by Andrea Pissinis on 2018-02-16.
 */

public with sharing class icBLAuditParameter implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        String getAuditRole ();
        List<AuditParameters__c> getActiveAuditParameters();
        List<AuditParameters__c> saveAuditParameters(List<AuditParameters__c> auditParameters);

    }

    public class Impl implements IClass {

        private icREPOAuditParameter.IClass  auditParameterRepo = (icREPOAuditParameter.IClass) icObjectFactory.getSingletonInstance('icREPOAuditParameter');
        private icBLuser.IClass userBL = (icBLuser.IClass) icObjectFactory.GetSingletonInstance('icBLuser');

        public String getAuditRole(){

            return userBL.getCurrentUserAuditRole();

        }

        public List<AuditParameters__c> getActiveAuditParameters(){

            return auditParameterRepo.getActiveAuditParameters();

        }

        public List<AuditParameters__c> saveAuditParameters(List<AuditParameters__c> auditParameters){

            return auditParameterRepo.saveAuditParameters(auditParameters);

        }


    }
}