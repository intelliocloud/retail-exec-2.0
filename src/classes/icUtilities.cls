/**
 * Created by François Poirier on 11/29/2017.
 */

public with sharing class icUtilities {
    public static List<Schema.DescribeFieldResult> convertFields(List<Schema.FieldSetMember> members, Schema.SObjectType type){

        List<Schema.DescribeFieldResult> describes = new List<Schema.DescribeFieldResult>();

        for (Schema.FieldSetMember member : members)
        {
            describes.add(new Parser(member, type).getDescribe());
        }

        return describes;
    }

    public static String getLookupName(String crossObjectReference){

        // would need to think about this more if there are managed packages
        return crossObjectReference.endsWith('__r') ?
                crossObjectReference.replace('__r', '__c') :
                crossObjectReference + 'Id';
    }

    public static Object getRelationshipFieldValue(SObject sObj, String field){

        if(sObj == null){
            return null;
        }

        if(field.contains('.')){
            String nextField = field.substringAfter('.');
            String relation = field.substringBefore('.');

            return getRelationshipFieldValue((SObject)sObj.getSObject(relation), nextField);
        }
        else{
            return formatFieldValue(sObj, field);
        }
    }

    public static Object formatFieldValue(SObject sObj, String field){
        Object value = sObj.get(field);
        Schema.DisplayType fieldType = sObj.getSObjectType().getDescribe().fields.getMap().get(field).getDescribe().getType();

        if(fieldType == Schema.DisplayType.Date){
            value = Date.valueOf(value).format();
        }
        else if(fieldType == Schema.DisplayType.DateTime){
            value = DateTime.valueOf(value).format();
        }

        return value;
    }

    public class Parser{

        public List<String> path {get; set;}
        public Schema.SObjectType type {get; set;}

        public Parser(Schema.FieldSetMember member, Schema.SObjectType type){

            System.debug('member: ' + member.getFieldPath());
            this.path = member.getFieldPath().split('\\.');
            system.debug('Path: ' + path);
            this.type = type;
        }

        public Schema.DescribeFieldResult getDescribe(){

            // could possibly make this recursive
            while (path.size() > 1)
            {
                traverse();
            }

            return describe(path[0]);
        }

        private void traverse(){

            if (path.size() == 1) return;

            String lookupName = getLookupName(path.remove(0));
            Schema.DescribeFieldResult describe = describe(lookupName);

            this.type = describe.getReferenceTo()[0];
            // this would get a lot more hairy if we want to deal with polymorphic lookups properly.
        }

        Schema.DescribeFieldResult describe(String field){

            return type.getDescribe().fields.getMap().get(field).getDescribe();
        }
    }
}