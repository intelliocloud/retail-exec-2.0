/**
 * Created by Andrea Pissinis on 2018-03-14.
 */
@isTest
public with sharing class icMAPfieldSetMock implements icMAPfieldSet.IClass{


    public List<icDTOfieldSet> mapToDTO(List<FieldSetMember> fieldset) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('fieldset', fieldset);
        return (List<icDTOfieldSet>) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');    }

    public List<icDTOfieldSet> mapToDTO(List<icDTOListViewColumn> columns) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('columns', columns);
        return (List<icDTOfieldSet>) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');    }

    public List<icDTOfieldSet> maptoDTOHidden(List<FieldSetMember> fieldset, List<icDTOfieldSet> tablefieldset) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'maptoDTOHidden');
        params.put('fieldset', fieldset);
        params.put('tablefieldset', tablefieldset);
        return (List<icDTOfieldSet>) icTestMockUtilities.Tracer.GetReturnValue(this, 'maptoDTOHidden');
    }
}