/**
 * Created by Andrea Pissinis on 2018-03-05.
 */
@isTest
public with sharing class icMAPauditTaskMock {

    public icDTOAuditTask mapToDTO(AuditTask__c auditTask){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('auditTask', auditTask);
        return (icDTOAuditTask) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');

    }

    public List<icDTOAuditTask> mapToDTO(List<AuditTask__c> auditTasks){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('auditTasks', auditTasks);
        return (List<icDTOAuditTask>) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');
    }

    public AuditTask__c mapToAuditTask(icDTOAuditTask dtoAuditTask){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToAuditTask');
        params.put('dtoAuditTask', dtoAuditTask);
        return (AuditTask__c) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToAuditTask');
    }

    public List<AuditTask__c> mapToAuditTask(List<icDTOAuditTask> dtoAuditTasks){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToAuditTask');
        params.put('dtoAuditTasks', dtoAuditTasks);
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToAuditTask');

    }

}