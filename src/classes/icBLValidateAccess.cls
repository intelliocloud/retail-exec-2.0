/**
 * Created by François Poirier on 11/29/2017.
 */

public with sharing class icBLValidateAccess implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        Boolean fieldsAreAccessible(List<String> fields, String sObjectType);
        Boolean fieldsAreUpdateable(List<String> fields, String sObjectType);
        List<Schema.FieldSetMember> getAccessiblesFieldSetMembers(List<Schema.FieldSetMember> fields, String sObjectType);
    }

    public class Impl implements IClass{

        public Boolean fieldsAreAccessible(List<String> fields, String sObjectType ){
            Boolean retour = true;

            Map<String, Schema.SObjectField> fieldsMap = getFieldsMap(sObjectType);
            list<String> unaccessibleFields = new List<String>();

            system.debug(fieldsMap);

            for(String field : fields){
                system.debug(field + ' ===> ' + fieldsMap.get(field).getDescribe().isAccessible());
                if(!fieldsMap.get(field).getDescribe().isAccessible()) {
                    retour = false;
                    unaccessibleFields.add(field);
                }
            }
            if(!retour){
                throw new AuraHandledException('field(s) not accessible(s): ' + String.join(unaccessibleFields, ','));
            }
            return retour;
        }

        public Boolean fieldsAreUpdateable(List<String> fields, String sObjectType){

            Boolean retour = true;

            Map<String, Schema.SObjectField> fieldsMap = getFieldsMap(sObjectType);
            list<String> unaccessibleFields = new List<String>();

            system.debug(fieldsMap);

            for(String field : fields){

                if(!fieldsMap.get(field).getDescribe().isUpdateable()) {
                    retour = false;
                    unaccessibleFields.add(field);
                }
            }
            if(!retour){
                AuraHandledException a= new AuraHandledException('field(s) not e(s): ' + String.join(unaccessibleFields, ','));
                a.setMessage('field(s) not e(s): ' + String.join(unaccessibleFields, ','));
                throw a;
            }
            return retour;
        }

        public List<Schema.FieldSetMember> getAccessiblesFieldSetMembers(List<Schema.FieldSetMember> fields, String sObjectType){

            List<Schema.FieldSetMember> retour = new List<Schema.FieldSetMember>();
            Map<String, Schema.SObjectField> fieldsMap = getFieldsMap(sObjectType);


            for(Schema.FieldSetMember field : fields){
                system.debug('getFieldPath=' + field.getFieldPath() + '(' + field.getType() + ')');

                if(field.getFieldPath().indexOf('__r') == -1){
                    if(fieldsMap.get(field.getFieldPath()).getDescribe().isAccessible()){
                        retour.add(field);
                    }
                }
                else{

                    List<Schema.FieldSetMember> members = new List<Schema.FieldSetMember>();
                    members.add(field);
                    system.debug('field:' + field);
                    Schema.SObjectType objType = Schema.getGlobalDescribe().get(sObjectType);

                    List<Schema.DescribeFieldResult> dr = icUtilities.convertFields(members, objType);

                    if(dr[0].isAccessible()){
                        system.debug('Accessible: true');
                        retour.add(field);
                    }
                    else{
                        system.debug('Accessible: false');
                    }
                }
            }

            return retour;

        }

        private Map<String, Schema.SObjectField> getFieldsMap(String sObjectType){

            Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();
            Schema.DescribeSObjectResult describeResults = globalSchema.get(sObjectType).getDescribe();
            Map<String, Schema.SObjectField> fieldsMap = describeResults.fields.getMap();

            return fieldsMap;
        }
    }
}