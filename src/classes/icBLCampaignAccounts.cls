/**
 * Created by Francois Poirier on 2018-11-07.
 */

public with sharing class icBLCampaignAccounts implements icIClass {
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<rtxCampaign_Account__c> getCurrentCampaignAccountByAccountIds(List<Id> accountIds);
    }

    public class Impl implements IClass {

        private icREPOCampaignAccount.IClass repoCampaignAccount = (icREPOCampaignAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOCampaignAccount');

        public List<rtxCampaign_Account__c> getCurrentCampaignAccountByAccountIds(List<Id> accountIds){

            return repoCampaignAccount.getCurrentCampaignAccountByAccountIds(accountIds);

        }

    }

}