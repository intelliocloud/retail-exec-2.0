/**
 * Created by François Poirier on 1/29/2018.
 */

public with sharing class icCTRLlistView {

    static icBLutilities.IClass util = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
    static icBLGeneric.IClass bl = (icBLGeneric.IClass) icObjectFactory.GetSingletonInstance('icBLGeneric');

    @AuraEnabled
    public static List<ListView> getListViews(String objectType){

        List<ListView> listviews = new List<ListView>();
        for(ListView lstObj : [SELECT Id, Name, DeveloperName FROM ListView WHERE SobjectType =: objectType  order by name ASC]){
            listviews.add(lstObj);
        }
        return listviews;

    }

    @AuraEnabled
    public static icDTOGenericListView getFilteredItems(String filterId, String objectType){

        icDTOGenericListView dto = new icDTOGenericListView();

        dto = bl.getFilteredItems(filterId, objectType);
        system.debug('dto ===> ' + dto);
        return dto;
    }
}