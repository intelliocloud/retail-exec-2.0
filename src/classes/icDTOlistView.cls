/**
 * Created by François Poirier on 1/30/2018.
 */

public with sharing class icDTOlistView {

    public List<icDTOListViewColumn> columns {get;set;}
    public String id {get;set;}
    public List<icDTOlistViewOrderBy> orderBy {get;set;}
    public String query {get;set;}
    public String scope {get;set;}
    public string sObjectType {get;set;}
    public icDTOlistViewWhere whereCondition {get;set;}

}