/**
 * Created by georg on 2018-03-07.
 */

@IsTest
public class icTestPlanogramVersionTrigger {
    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLPlanogramVersionTrigger', new icBLPlanogramVersionTriggerMock());
    }

    @isTest
    static void testonBeforeAfterInsert() {
        testSetup();

        rtxPlanogram__c theP = new rtxPlanogram__c();
        insert theP;
        rtxPlanogram_Version__c thePV = new rtxPlanogram_Version__c();
        thePV.Planogram__c = theP.Id;
        insert thePV;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLPlanogramVersionTriggerMock', 'onBeforeInsert'));
        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLPlanogramVersionTriggerMock', 'onAfterInsert'));
    }
    @isTest
    static void testonBeforeUpdate() {
        testSetup();

        rtxPlanogram__c theP = new rtxPlanogram__c();
        insert theP;
        rtxPlanogram_Version__c thePV = new rtxPlanogram_Version__c();
        thePV.Planogram__c = theP.Id;
        insert thePV;
        update thePV;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLPlanogramVersionTriggerMock', 'onBeforeUpdate'));
    }
}