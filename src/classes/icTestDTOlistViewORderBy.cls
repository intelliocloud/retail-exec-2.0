/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOlistViewORderBy {

    static testMethod void testdto(){

        icDTOlistViewOrderBy dto = new icDTOlistViewOrderBy();
        System.assertEquals(null, dto.fieldNameOrPath);
        System.assertEquals(null, dto.nullsPosition);
        System.assertEquals(null, dto.sortDirection);
    }

}