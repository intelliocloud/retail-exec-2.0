/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOretailExecutionCampaign {

    static testMethod void testdto(){
        icDTOretailExecutionCampaign dto = new icDTOretailExecutionCampaign();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.name);
        System.assertEquals(null, dto.priority);
        System.assertEquals(null, dto.auditTasks);
    }
}