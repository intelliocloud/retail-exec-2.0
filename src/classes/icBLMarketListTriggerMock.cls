/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public class icBLMarketListTriggerMock implements icBLMarketListTrigger.IClass{

    public void onBeforeUpdate(List<ML__c> newMLs){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
        parms.put('newMLs',newMLs);
    }
}