/**
 * Created by François Poirier on 12/15/2017.
 */

public with sharing class icMAPfieldSet implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<icDTOfieldSet> mapToDTO(List<Schema.FieldSetMember> fieldset);
        List<icDTOfieldSet> mapToDTO(List<icDTOListViewColumn> columns);
        List<icDTOfieldSet> maptoDTOHidden(List<Schema.FieldSetMember> fieldset, List<icDTOfieldSet> tablefieldset);
    }


    public class Impl implements IClass{

        private icDTOfieldSet mapToDTO(icDTOListViewColumn column){

            icDTOfieldSet dto = new icDTOfieldSet();

            dto.apiName = column.fieldNameOrPath;
            dto.type = column.type;
            dto.label = column.label;
            dto.required = false;
            dto.hidden = false;
            return dto;
        }

        public List<icDTOfieldSet> mapToDTO(List<icDTOListViewColumn> columns){

            List<icDTOfieldSet> dtos = new List<icDTOfieldSet>();

            for(icDTOListViewColumn col : columns){

                if(!col.hidden) {
                    icDTOfieldSet dto = new icDTOfieldSet();
                    dto = mapToDTO(col);
                    dtos.add(dto);
                }
            }

            return dtos;
        }

        public List<icDTOfieldSet> mapToDTO(List<Schema.FieldSetMember> fieldset){

            List<icDTOfieldSet> dtoFieldSets = new List<icDTOfieldSet>();

            for(Schema.FieldSetMember f : fieldset){

                icDTOfieldSet dtoFieldSet = new icDTOfieldSet();
                dtoFieldSet.apiName = f.getFieldPath();
                dtoFieldSet.label = f.getLabel();
                dtoFieldSet.required = f.getRequired();
                dtoFieldSet.type = String.valueOf(f.getType());
                dtoFieldSet.hidden = false;
                dtoFieldSets.add(dtoFieldSet);
            }

            return dtoFieldSets;
        }

        public List<icDTOfieldSet> mapToDTOHidden(List<Schema.FieldSetMember> fieldset, List<icDTOfieldSet> tableFieldSet){

            List<String> alreadyInsertedAPINames = new List<String>();
            List<icDTOfieldSet> newFields = new List<icDTOfieldSet>();

            for (icDTOfieldSet dto : tableFieldSet){
                alreadyInsertedAPINames.add(dto.apiName);
            }

            for(Schema.FieldSetMember f : fieldset){

                if(!alreadyInsertedAPINames.contains(f.getFieldPath())) {
                    icDTOfieldSet dtoFieldSet = new icDTOfieldSet();
                    dtoFieldSet.apiName = f.getFieldPath();
                    dtoFieldSet.label = f.getLabel();
                    dtoFieldSet.required = f.getRequired();
                    dtoFieldSet.type = String.valueOf(f.getType());
                    dtoFieldSet.hidden = true;
                    newFields.add(dtoFieldSet);
                }
            }

            return newFields;
        }
    }
}

