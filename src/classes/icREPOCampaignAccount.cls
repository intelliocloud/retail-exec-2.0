/**
 * Created by georg on 2018-02-12.
 */

public with sharing class icREPOCampaignAccount implements icIClass {
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        List<rtxCampaign_Account__c> getCampaignAccountsByRetailExecutionCampaignIds(List<Id> recIds);
        List<rtxCampaign_Account__c> getCurrentCampaignAccountByAccountIds(List<Id> accountIds);
    }

    public class Impl implements IClass {
        public List<rtxCampaign_Account__c> getCampaignAccountsByRetailExecutionCampaignIds(List<Id> recIds) {
            return [SELECT Id FROM rtxCampaign_Account__c WHERE rtxRetail_Execution_Campaign__c IN : recIds];
        }

        public List<rtxCampaign_Account__c> getCurrentCampaignAccountByAccountIds(List<Id> accountIds) {

            List<rtxCampaign_Account__c> campaignAccounts = new List<rtxCampaign_Account__c>();
            campaignAccounts = [
                    SELECT Id,
                            rtxCampaign_is_current__c,
                            rtxExcluded__c,
                            rtxRetail_Execution_Campaign__c,
                            rtxRetail_Execution_Campaign__r.rtxTarget_Role__c,
                            rtxAccount__c,
                            rtxRetail_Execution_Campaign__r.rtxCampaign_Priority__c
                    FROM rtxCampaign_Account__c
                    WHERE rtxCampaign_is_current__c = true
                    AND rtxExcluded__c = false
                    AND rtxAccount__c IN :accountIds
            ];

            return campaignAccounts;
        }

    }
}