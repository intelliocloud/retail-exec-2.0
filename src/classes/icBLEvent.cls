/**
 * Created by Francois Poirier on 2018-11-09.
 */

public with sharing class icBLEvent implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Event> saveEvents(List<Event> events);
        List<Event> getEventsByVisitIds(List<Id> visitIds);


    }

    public class Impl implements IClass {

        private icREPOEvent.IClass eventRepo = (icREPOEvent.IClass) icObjectFactory.GetSingletonInstance('icREPOEvent');

        public List<Event> saveEvents(List<Event> events) {
            return eventRepo.saveEvents(events);
        }

        public List<Event> getEventsByVisitIds(List<Id> visitIds){
            return eventRepo.getEventsByVisitIds(visitIds);
        }
    }
}