/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestCTRLvisitSchedule {

    public static icDTOvisitSchedule i = new icDTOvisitSchedule();
    public static StoreVisit__c s = new StoreVisit__c();

    public static testMethod void testM(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLstoreVisitSchedule', new icBLstoreVisitScheduleMock());
        icTestMockUtilities.Tracer.SetReturnValue('icBLstoreVisitScheduleMock','getVisitSchedule', i);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLstoreVisit', new icBLstoreVisitMock());
        icTestMockUtilities.Tracer.SetReturnValue('icBLstoreVisitMock','createStoreVisit', s);

        icCTRLvisitSchedule.getVisitSchedule('');
        icCTRLvisitSchedule.createStoreVisit(icTestHelperUtility.getFakeId(Account.SObjectType),'2','3','1991','5');

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLstoreVisitScheduleMock', 'getVisitSchedule'));
        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLstoreVisitMock', 'createStoreVisit'));

    }
}