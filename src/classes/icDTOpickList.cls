global with sharing class icDTOpickList {

    @AuraEnabled global String label {get;set;}
    @AuraEnabled global String value {get;set;}
    @AuraEnabled global Boolean isDefault {get;set;}
    @AuraEnabled global Boolean isActive {get;set;}
    @AuraEnabled global Boolean isSelected {get;set;}
    @AuraEnabled global String type {get;set;}
    
}