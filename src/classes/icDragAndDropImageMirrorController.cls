public class icDragAndDropImageMirrorController {

    @AuraEnabled
    public static Attachment getProfilePicture(Id parentId) {
        icREPOGeneric.IClass genREPO = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');

        if (parentId.getSobjectType() == (AuditProduct__c.SObjectType)) {

            List<AuditProduct__c> apL = genREPO.getValues('AuditProduct__c', new List<String>{'Product__c'}, new List<String>{'Id = \''+String.escapeSingleQuotes(parentId)+'\''}, null, 1);
            Id productId = apL.get(0).Product__c;
            return returnAttachment(productId);

        } else if (parentId.getSobjectType() == (rtxPlanogram__c.SObjectType)) {

            List<rtxPlanogram_Version__c> pvL = genREPO.getValues('rtxPlanogram_Version__c', new List<String>{'Current__c'}, new List<String>{'Current__c = TRUE', 'AND Planogram__c = \''+String.escapeSingleQuotes(parentId)+'\''}, null, 1);
            return returnAttachment(pvL.get(0).Id);

        } else if (parentId.getSobjectType() == (rtxEquipment__c.SObjectType)) {

            List<rtxEquipment__c> eL = genREPO.getValues('rtxEquipment__c', new List<String>{'rtxPlanogram__c'}, new List<String>{'Id = \''+String.escapeSingleQuotes(parentId)+'\''}, null, 1);
            List<rtxPlanogram_Version__c> pvL = genREPO.getValues('rtxPlanogram_Version__c', new List<String>{'Current__c'}, new List<String>{'Current__c = TRUE', 'AND Planogram__c = \''+String.escapeSingleQuotes(eL.get(0).rtxPlanogram__c)+'\''}, null, 1);

            return returnAttachment(pvL.get(0).Id);

        } else if (parentId.getSobjectType() == (Section__c.SObjectType)) {

            List<Section__c> sL = genREPO.getValues('Section__c' , new List<String>{'rtxEquipment__c'}, new List<String>{'Id = \''+String.escapeSingleQuotes(parentId)+'\''}, null, 1);
            List<rtxEquipment__c> eL = genREPO.getValues('rtxEquipment__c', new List<String>{'rtxPlanogram__c'}, new List<String>{'Id = \''+String.escapeSingleQuotes(sL.get(0).rtxEquipment__c)+'\''}, null, 1);
            List<rtxPlanogram_Version__c> pL = genREPO.getValues('rtxPlanogram_Version__c', new List<String>{'Current__c'}, new List<String>{'Current__c = TRUE', 'AND Planogram__c = \''+String.escapeSingleQuotes(eL.get(0).rtxPlanogram__c)+'\''}, null, 1);
            return returnAttachment(pL.get(0).Id);

        }else{
            return null;
        }
    }

    private static Attachment returnAttachment(Id objectid){
        return [SELECT Id, Name, LastModifiedDate, ContentType FROM Attachment
        WHERE parentid=:objectid AND ContentType IN ('image/png', 'image/jpeg', 'image/gif')
        ORDER BY LastModifiedDate DESC LIMIT 1];
    }
}