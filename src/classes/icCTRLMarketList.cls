/**
 * Created by François Poirier on 2/14/2018.
 */

global with sharing class icCTRLMarketList {

    static  private icBLMarketList.IClass blMl = (icBLMarketList.IClass) icObjectFactory.GetSingletonInstance('icBLMarketList');
    static private icBLutilities.IClass blUtil = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

    @AuraEnabled global static icDTOMarketListScreen getMarketListById(String mlId){

        icDTOMarketListScreen dto = new icDTOMarketListScreen();

        dto = blMl.getMarketListScreenById(mlId);

        dto.fields = blUtil.getObjectFields('Account');
        dto.operators = blUtil.getOperators();

        return dto;
    }

    @AuraEnabled global static icDTOMarketListScreen saveMarketListFilters(String mlId, String filters, String logic){

        icDTOMarketListScreen dto = new icDTOMarketListScreen();
        try {

            blMl.saveMarketListFilterJSON(mlId, filters, logic);
            dto = blMl.getMarketListScreenById(mlId);
        }
        catch (Exception ex) {

            dto.hasError = true;
            dto.errorCode = ex.getMessage();
        }

        return dto;
    }
}