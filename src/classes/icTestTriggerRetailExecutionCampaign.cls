/**
 * Created by georg on 2018-03-07.
 */

@IsTest
private class icTestTriggerRetailExecutionCampaign {
    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLRetailExecutionCampaignTrigger', new icBLRetailExecutionCampaignTriggerMock());
    }
    @IsTest
    static void testonBeforeUpdate() {
        testSetup();

        rtxRetail_Execution_Campaign__c theREC = new rtxRetail_Execution_Campaign__c();
        insert theREC;

        theREC.rtxDescription__c = 'Sample text';
        update theREC;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLRetailExecutionCampaignTriggerMock', 'onBeforeUpdate'));
    }
}