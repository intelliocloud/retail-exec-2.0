/**
 * Created by François Poirier on 7/11/2017.
 */
@isTest
public with sharing class icTestFireTrigger {

    @isTest static void test(){

        icFireTrigger.stopRunning();
        system.assert(!icFireTrigger.canIRun());

        icFireTrigger.allowTorun();
        system.assert(icFireTrigger.canIRun());
    }
}