/**
 * Created by François Poirier on 2/27/2018.
 */

public with sharing class icDTOfilterItem {

    public String field {get;set;}
    public String jsonOperator {get;set;}
    public icDTOoperator operator {get;set;}
    public String value {get;set;}
    public String fieldName {get;set;}
    public String pickListValue {get;set;}

}