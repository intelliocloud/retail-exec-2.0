/**
 * Created by François Poirier on 1/26/2018.
 */

public with sharing class icBLstoreVisitSchedule  implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOvisitSchedule getVisitSchedule(String strStartDate);
    }

    public class Impl implements IClass{

        private icREPOstoreVisit.IClass svRepo = (icREPOstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icREPOstoreVisit');

        public icDTOvisitSchedule getVisitSchedule(String strStartDate){

            icDTOvisitSchedule dtoVisitSchedule = new icDTOvisitSchedule();
            Date dtStartDate;
            system.debug('strStartDate ===> ' + strStartDate);
            if(String.isEmpty(strStartDate)){
                dtStartDate = Date.today();
            }
            else{
                dtStartDate = Date.valueOf(strStartDate);
            }
            system.debug('dtStartDate ===> ' + dtStartDate);

            List<StoreVisit__c> visits = new List<StoreVisit__c>();
            Map<String, List<StoreVisit__c>> visitsMap = new Map<String, List<StoreVisit__c>>();
            List<icDTOdate> dtodates = this.getDates(dtStartDate);
            system.debug('dtoDates ===> ' + dtodates);
            dtoVisitSchedule.dates = dtodates;

            List<icDTOhour> dtohours = this.getHours();
            dtoVisitSchedule.hours = dtohours;

            if(dtodates.size()>0){
                Date startDate = Date.newInstance(Integer.valueOf(dtodates[0].year), Integer.valueOf(dtodates[0].month), Integer.valueOf(dtodates[0].day));
                system.debug('startDate ===> ' + startDate);
                Date endDate = Date.newInstance(Integer.valueOf(dtodates[dtodates.size()-1].year), Integer.valueOf(dtodates[dtodates.size()-1].month), Integer.valueOf(dtodates[dtodates.size()-1].day));
                system.debug('endDate ===> ' + endDate);
                visits = svRepo.getVisitsByDates(startDate, endDate);
                system.debug('visits ===> ' + visits);
                visitsMap = this.getVisitsMap(visits);
                dtoVisitSchedule.visits = visitsMap;
            }



            return dtoVisitSchedule;
        }

        private List<icDTOdate> getDates(Date startDate){

            //Date startDate = Date.today();
            List<icDTOdate> dates = new List<icDTOdate>();

            //TODO: validate if need a custom setting for the number of days
            Integer numberOfDays = 7;

            for(Integer i = 0; i<numberOfDays ; i++){

                icDTOdate dtoDate = new icDTOdate();
                Date tmpDate = startDate.addDays(i);
                dtoDate.dayDate = String.valueOf(tmpDate);
                dtoDate.day = String.valueOf(tmpDate.day());
                //dtoDate.dayName = this.getDayName(tmpDate.dayOfYear());
                dtoDate.month = String.valueOf(tmpDate.month());
                dtoDate.year = String.valueOf(tmpDate.year());

                dates.add(dtoDate);
            }

            return dates;

        }
/*
        private String getDayName(Integer dayNumber){
            system.debug('dayNumber ===> ' + dayNumber);
            List<String> dayNames = new List<String>();
            dayNames.add(System.Label.Sunday);
            dayNames.add(System.Label.Monday);
            dayNames.add(System.Label.Tuesday);
            dayNames.add(System.Label.Wednesday);
            dayNames.add(System.Label.Thursday);
            dayNames.add(System.Label.Friday);
            dayNames.add(System.Label.Saturday);


            return dayNames[dayNumber];

        }
*/
        private List<icDTOhour> getHours (){

            List<icDTOhour> hours = new List<icDTOhour>();
            //TODO: change to customizable start and end time.  Also validate for interval
            Integer startTime = 9;
            Integer endTime = 17;
            Integer interval = 1;

            for(Integer i=0; i + startTime < endTime; i=i+interval ){

                String strHour;
                Integer tmpHour = startTime + i;

                if(tmpHour < 12){
                    strHour = String.valueOf(tmpHour) + ':00 AM';
                }
                else {
                    /*if(tmpHour == 12){
                        strHour = String.valueOf(tmpHour);
                    }
                    else {
                        strHour = String.valueOf(tmpHour-12);
                    }
                    strHour = strHour + ':00 PM';*/
                    strHour = String.valueOf(tmpHour) + ':00 PM';
                }

                icDTOhour dtoHour = new icDTOhour();
                dtoHour.strHour =  strHour;
                dtoHour.hour = tmpHour;

                hours.add(dtoHour);
            }


            return hours;
        }

        private Map<String, List<StoreVisit__c>> getVisitsMap(List<StoreVisit__c> visits){

            Map<String, List<StoreVisit__c>> visitMap = new Map<String, List<StoreVisit__c>>();

            for(StoreVisit__c sv : visits){
                String formatedDate = sv.rtxDate__c.format('yyyy-MM-ddH:mm');
                Integer hour = sv.rtxDate__c.Hour();
                if(hour < 12){
                    formatedDate = formatedDate + ' AM';
                }
                else {
                    formatedDate = formatedDate + ' PM';
                }
                system.debug('formatedDate ===> ' + formatedDate);

                if(!visitMap.containsKey(formatedDate)){
                    List<StoreVisit__c> tmpList = new List<StoreVisit__c>();
                    visitMap.put(formatedDate, tmpList);
                }
                visitMap.get(formatedDate).add(sv);
            }

            return  visitMap;
        }
    }


}