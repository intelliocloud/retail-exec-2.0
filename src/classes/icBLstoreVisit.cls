/**
 * Created by François Poirier on 2/2/2018.
 */

public with sharing class icBLstoreVisit implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        StoreVisit__c createStoreVisit(String accountId, Integer mois, Integer jour, Integer an, Integer heure);
        List<StoreVisit__c> getCompletedVisitsByAccountIds(List<Id> accountIds);
        List<StoreVisit__c> saveStoreVisits(List<StoreVisit__c> storeVisits);
        Map<Id, StoreVisit__c> getReadOnlyVisitMapByIds(List<Id> storeVisitIds);
        StoreVisit__c getStoreVisitById(Id storeVisitId);
        List<StoreVisit__c> retrieveStoreVisitsToActivate(List<StoreVisit__c> storevisits);

    }

    public class Impl implements IClass {

        private icREPOstoreVisit.IClass repoSV = (icREPOstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icREPOstorevisit');
        private icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

        public StoreVisit__c createStoreVisit(String accountId, Integer mois, Integer jour, Integer an, Integer heure){

            StoreVisit__c visit = new StoreVisit__c();
            List<StoreVisit__c> visits = new List<StoreVisit__c>();
            icDTOAuditParameterDetail params = new icDTOAuditParameterDetail();
            params = utilBL.getAuditParameters();

            TimeZone tz = UserInfo.getTimeZone();

            Datetime dt = Datetime.newInstance(an, mois, jour, heure, 0, 0);

            system.debug('dt ====> ' + dt);

            Integer offset = tz.getOffset(dt);
            // divide by 1000 because offset is in miliseconds and we don't have a add Miliseconds method.
            //dt = dt.addSeconds(offset/1000);

            visit.Store__c = accountId;
            visit.rtxDate__c = dt;
            visit.rtxTimeZoneOffset__c = offset;
            visit.rtxAudit_Code__c = params.visitAuditCode;
            visit.rtxVisitStatus__c = 'Planned';
            visits.add(visit);

            system.debug('visit before save ===> ' + visit);

            visits = repoSV.save(visits);

            system.debug('visit after save ===> ' + visit);
            return visit;
        }

        public List<StoreVisit__c> getCompletedVisitsByAccountIds(List<Id> accountIds){

            return repoSV.getCompletedVisitsByAccountIds(accountIds);

        }

        public List<StoreVisit__c> saveStoreVisits(List<StoreVisit__c> storeVisits){
            return repoSV.save(storeVisits);
        }

        public Map<Id, StoreVisit__c> getReadOnlyVisitMapByIds(List<Id> storeVisitIds){

            Map<Id, StoreVisit__c> storeVisitMap = new Map<Id, StoreVisit__c>();
            List<StoreVisit__c> storeVisits = new List<StoreVisit__c>();
            storeVisits = repoSV.getVisitsByIds(storeVisitIds);

            for(StoreVisit__c sv : storeVisits){

                if(sv.rtxRead_Only__c){
                    storeVisitMap.put(sv.Id, sv);
                }
            }

            return storeVisitMap;

        }

        public StoreVisit__c getStoreVisitById(Id storeVisitId){

            List<StoreVisit__c> storeVisits = new List<StoreVisit__c>();
            StoreVisit__c storeVisit = new StoreVisit__c();
            List<Id> storeVisitIds = new List<Id>();
            storeVisitIds.add(storeVisitId);

            storeVisits = repoSV.getVisitsByIds(storeVisitIds);

            if(!storeVisits.isEmpty()){
                storeVisit = storeVisits[0];
            }

            return storeVisit;
        }

        public List<StoreVisit__c> retrieveStoreVisitsToActivate(List<StoreVisit__c> storevisits){
            List<StoreVisit__c> storestoactivate = new List<StoreVisit__c>();
            for (StoreVisit__c sv : storevisits) {

                if (sv.rtxDate__c.date() == DateTime.now().date()) {
                    if (!sv.rtxCompleted__c){

                        storestoactivate.add(sv);

                    }
                }
            }

            return storestoactivate;
        }
    }

}