/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestBLGeneric {

    private static icDTOServerResponse dtoSR = new icDTOServerResponse();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static List<SObject> objsL = new List<SObject>();
    private static List<icDTOGenericRecord> genRL = new List<icDTOGenericRecord>();

    public static testMethod void testGeneric(){

        icBLGeneric.IClass blGen = (icBLGeneric.IClass) icObjectFactory.getsingletoninstance('icBLGeneric');

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOGeneric', new icREPOGenericMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOGenericMock','updateValues', dtoSR);
        icTestMockUtilities.Tracer.SetReturnValue('icREPOGenericMock', 'gatValues', objsL);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icMAPGenericRecord', new icMAPGenericRecordMock());

        Test.setMock(HttpCalloutMock.class, new icHttpResponseGeneratorMock());
        blGen.getFilteredItems('Name', 'Account');

        Account a = accountCreator.createAccount();

        blGen.updateValue(a);
    }

}