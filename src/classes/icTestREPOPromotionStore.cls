/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOPromotionStore {

    public static testMethod void testRepo(){

        icREPOPromotionStore.IClass psRepo = (icREPOPromotionStore.IClass) icObjectFactory.getsingletoninstance('icREPOPromotionStore');
        List<Id> repId = new List<Id>();
        repId.add(icTestHelperUtility.getFakeId(rtxPromotion_Store__c.SObjectType));

        psRepo.getPromotionStoreByPromotionIds(repId);

    }

}