/**
 * Created by georg on 2018-03-14.
 */

@IsTest
public class icTestBLPlanogramVersionTrigger {
    public static  icBLPlanogramVersionTrigger.IClass obj = (icBLPlanogramVersionTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramVersionTrigger');

    @IsTest
    static void testonBeforeInsert() {
        List<rtxPlanogram_Version__c> newList = new List<rtxPlanogram_Version__c>();
        rtxPlanogram__c parent = new rtxPlanogram__c();
        insert parent;
        rtxPlanogram_Version__c anItem = new rtxPlanogram_Version__c();
        anItem.Planogram__c = parent.Id;
        anItem.Start_Date__c = Date.today();
        anItem.End_Date__c = Date.today() + 2;
        newList.add(anItem);

        rtxPlanogram_Version__c anItem2 = new rtxPlanogram_Version__c();
        anItem2.Planogram__c = parent.Id;
        anItem2.Start_Date__c = Date.today() + 3;
        anItem2.End_Date__c = Date.today() + 5;
        newList.add(anItem2);

        rtxPlanogram_Version__c anItem3 = new rtxPlanogram_Version__c();
        anItem3.Planogram__c = parent.Id;
        anItem3.Start_Date__c = Date.today() + 3;
        anItem3.End_Date__c = Date.today() + 5;
        newList.add(anItem3);


        newList.add(anItem);
        obj.onBeforeInsert(newList);
    }

    @IsTest
    static void testonAfterInsertOverlap() {
        Map<Id, rtxPlanogram_Version__c> newMap = new Map<Id, rtxPlanogram_Version__c>();
        rtxPlanogram__c parent = new rtxPlanogram__c();
        insert parent;
        try{
        rtxPlanogram_Version__c anItem = new rtxPlanogram_Version__c();
        anItem.Planogram__c = parent.Id;
        anItem.Start_Date__c = Date.today();
        anItem.End_Date__c = Date.today() + 2;
        insert anItem;
        newMap.put(anItem.Id, anItem);

        rtxPlanogram_Version__c anItem2 = new rtxPlanogram_Version__c();
        anItem2.Planogram__c = parent.Id;
        anItem2.Start_Date__c = Date.today() + 3;
        anItem2.End_Date__c = Date.today() + 5;
        insert anItem2;
        newMap.put(anItem2.Id, anItem2);

        rtxPlanogram_Version__c anItem3 = new rtxPlanogram_Version__c();
        anItem3.Planogram__c = parent.Id;
        anItem3.Start_Date__c = Date.today() + 3;
        anItem3.End_Date__c = Date.today() + 5;
        insert anItem3;
        newMap.put(anItem3.Id, anItem3);


            obj.onAfterInsert(newMap);
        }
        catch(Exception ex) {
            System.assertNotEquals('', ex.getMessage());
        }
    }

    @IsTest
    static void testonBeforeUpdate() {
        Map<Id, rtxPlanogram_Version__c> oldMap = new Map<Id, rtxPlanogram_Version__c>();
        Map<Id, rtxPlanogram_Version__c> newMap = new Map<Id, rtxPlanogram_Version__c>();
        List<rtxPlanogram_Version__c> newList = new List<rtxPlanogram_Version__c>();
        rtxPlanogram__c parent = new rtxPlanogram__c();
        parent.rtxActive__c = true;
        parent.Planogram_Version__c = null;
        insert parent;

        rtxPlanogram_Version__c anItem = new rtxPlanogram_Version__c();
        anItem.Planogram__c = parent.Id;
        anItem.Start_Date__c = Date.today();
        anItem.End_Date__c = Date.today() + 1;
        insert anItem;
        oldMap.put(anItem.Id, anItem);
        newMap.put(anItem.Id, anItem);

        rtxPlanogram_Version__c anItem2 = new rtxPlanogram_Version__c();
        anItem2.Planogram__c = parent.Id;
        anItem2.Start_Date__c = Date.today() + 2;
        anItem2.End_Date__c = Date.today() + 3;
        insert anItem2;
        oldMap.put(anItem2.Id, anItem2);

        rtxPlanogram_Version__c anItem3 = new rtxPlanogram_Version__c();
        anItem3.Planogram__c = parent.Id;
        anItem3.Start_Date__c = Date.today() + 4;
        anItem3.End_Date__c = Date.today() + 5;
        insert anItem3;
        oldMap.put(anItem3.Id, anItem3);

        newList.add(anItem);
        obj.onBeforeUpdate(oldMap, newMap);
    }
}