/**
 * Created by Andrea Pissinis on 2018-03-02.
 */
@isTest
private class icTestBLauditTasks {

    static List<AuditTask__c> auditTasksL = new List<AuditTask__c>();
    static List<AuditTask__c> auditTasksM = new List<AuditTask__c>();
    static List<SObject> objectsL = new List<SObject>();
    static icDTOAuditTaskScreen auditTaskScreen = new icDTOAuditTaskScreen();
    static icDTOAuditTaskScreen auditTaskScreen2 = new icDTOAuditTaskScreen();
    static icDTOAuditTaskScreen auditTaskScreen3 = new icDTOAuditTaskScreen();

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();
    private static icTestHelperAuditTask auditTaskCreator = new icTestHelperAuditTask();


    static void initTest(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOauditTasksMock', new icREPOauditTasksMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOauditTasksMock','getAuditTasks',auditTasksL);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icMapAuditTaskScreenMock', new icMapAuditTaskScreenMock());
        icTestMockUtilities.Tracer.SetReturnValue('icMapAuditTaskScreenMock', 'mapToDTO', auditTaskScreen );

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icMAPauditTaskMock', new icMAPauditTaskMock());
        icTestMockUtilities.Tracer.SetReturnValue('icMAPauditTaskMock','mapToAuditTask',auditTaskScreen2);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOGenericMock', new icREPOGenericMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOGenericMock','getValues',objectsL);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOauditTasksMock', new icREPOauditTasksMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOauditTasksMock','mapToAuditTask',auditTasksM);

    }

    static StoreVisit__c initializeEnvironment(Integer mode){

        if (mode==1) {
            Account acc = accountCreator.createAccount();
            AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
            Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
            StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
            ML__c mml = marketListCreator.createActiveML();
            rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
            rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
            Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
            Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
            AuditTask__c aDone = auditTaskCreator.createAudiTaskDone(acc, p, s);
            AuditTask__c aNotDone = auditTaskCreator.createAuditTaskNotDone(acc, p, s);
            update rtx;
            rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];
            return s;
        }else if (mode == 2){
            Account acc = accountCreator.createAccount();
            AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
            Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
            StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
            ML__c mml = marketListCreator.createActiveML();
            rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
            rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
            Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
            Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
            AuditTask__c aDone = auditTaskCreator.createAudiTaskDone(acc, pO, s);
            AuditTask__c aNotDone = auditTaskCreator.createAuditTaskNotDone(acc, pO, s);
            AuditTask__c aNotDone2 = auditTaskCreator.createAuditTaskNotDone(acc, p, s);
            update rtx;
            rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];
            return s;
        }else{
            Account acc = accountCreator.createAccount();
            AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
            Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
            StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
            ML__c mml = marketListCreator.createActiveML();
            rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
            rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
            Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
            Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
            update rtx;
            rtxRetail_Execution_Campaign__c checking = [SELECT Id, rtxCurrent__c FROM rtxRetail_Execution_Campaign__c WHERE Id = :rtx.Id];
            return s;
        }

    }

   /* static testMethod void testGetAuditTasks(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');

        Id storeid = icTestHelperUtility.getFakeId(StoreVisit__c.SObjectType);
        List<AuditTask__c> resultA = atBlbl.getAuditTasks(storeid);

        System.assertEquals(resultA, auditTasksL);

    }*/

/*    static testMethod void testGetAuditTaskScreen(){
        initTest();
        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');

        StoreVisit__c s = initializeEnvironment(1);

        icDTOAuditTaskScreen taskScreenReturn = atBLbl.getAuditTaskScreen(s.Id);

    }*/

    static testMethod void testCreateAuditTasks(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');
        List<StoreVisit__c> sl = new List<StoreVisit__c>();

        StoreVisit__c s = initializeEnvironment(1);

        sl.add(s);

        atBLbl.createAuditTasks(sl, 'Merchandiser');

    }


    static testMethod void testCreateAuditTasksNoTasksDone(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');
        List<StoreVisit__c> sl = new List<StoreVisit__c>();

        StoreVisit__c s = initializeEnvironment(2);

        sl.add(s);

        atBLbl.createAuditTasks(sl, 'Merchandiser');

    }


    static testMethod void testCreateAuditTasksNoTasks(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');
        List<StoreVisit__c> sl = new List<StoreVisit__c>();

        StoreVisit__c s = initializeEnvironment(3);

        sl.add(s);

        atBLbl.createAuditTasks(sl, 'Merchandiser');

    }

/*    static testMethod void testSaveAuditTask(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');
        icDTOAuditTaskScreen ats = atBLbl.saveAuditTasks(atBLbl.getAuditTaskScreen(icTestHelperUtility.getFakeId(StoreVisit__c.SObjectType)));

    }*/

    static testMethod void testDeleteTasks(){
        initTest();

        icBLAuditTasks.IClass atBLbl = (icBLAuditTasks.IClass) icObjectFactory.getsingletoninstance('icBLAuditTasks');
        List<StoreVisit__c> sl = new List<StoreVisit__c>();

        StoreVisit__c s = initializeEnvironment(1);
        sl.add(s);

        atBLbl.deleteAuditTasksByStoreVisits(sl);
    }


}