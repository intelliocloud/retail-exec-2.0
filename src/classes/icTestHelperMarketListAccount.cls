/**
 * Created by georg on 2018-03-07.
 */

@IsTest
public class icTestHelperMarketListAccount {
    public Market_List_Account__c createMarketListAccount(Id parentId, Id AccId){

        Market_List_Account__c result = new Market_List_Account__c(
                account__c = AccId,
                marketList__c = parentId);

        insert result;
        return result;
    }
}