/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icBLauditProductMock implements icBLauditProduct.IClass{

    public icDTOAuditProduct getAuditProducts(Id storeVisitId){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAuditProducts');
        params.put('storeVisitId', storeVisitId);
        return (icDTOAuditProduct) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAuditProducts');

    }

    public List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveAuditProducts');
        params.put('auditProducts', auditProducts);
        return (List<AuditProduct__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveAuditProducts');

    }


    public void createAuditProducts(List<StoreVisit__c> newStoreVisits){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createAuditProducts');
        params.put('newStoreVisits', newStoreVisits);

    }

    public void deleteAuditProductsByStoreVisits(List<StoreVisit__c> st){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'deleteAuditProductsByStoreVisits');
        params.put('st', st);
    }


    public void deleteAuditProducts(List<AuditProduct__c> auditProducts) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'deleteAuditProducts');
        params.put('auditProducts', auditProducts);
    }

    public List<AuditProduct__c> getNotDoneAuditProductByVisitIds(List<Id> visitIds){
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getNotDoneAuditProductByVisitIds');
        params.put('visitIds', visitIds);
        return (List<AuditProduct__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getNotDoneAuditProductByVisitIds');
    }
}