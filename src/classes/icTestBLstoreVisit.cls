/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLstoreVisit {

    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperAccount accountcreator = new icTestHelperAccount();

    public static testMethod void testcreateStore(){

        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());

        OrgCallSettings__c og = new OrgCallSettings__c();
        og.Visit_Duration_Default_Value__c = 60;
        insert og;
        Account a = accountcreator.createAccount();

        icBLstoreVisit.IClass apBL = (icBLstoreVisit.IClass) icObjectFactory.getsingletoninstance('icBLstoreVisit');
        apbl.createStoreVisit(a.Id, 2 , 3 , 1991 , 15);



    }

}