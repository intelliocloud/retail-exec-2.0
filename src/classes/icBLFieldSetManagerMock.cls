/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icBLFieldSetManagerMock implements icBLFieldSetManager.IClass{

    public Boolean hasFieldSet(String sObjectName, String fieldSetName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'hasFieldSet');
        params.put('sObjectName', sObjectName);
        params.put('fieldSetName', fieldSetName);
        return (Boolean) icTestMockUtilities.Tracer.GetReturnValue(this, 'hasFieldSet');

    }

    public List<Schema.FieldSetMember> getFields(String sObjectName, String fieldSetName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getFields');
        params.put('sObjectName', sObjectName);
        params.put('fieldSetName', fieldSetName);
        return (List<Schema.FieldSetMember>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getFields');

    }

    public Schema.DescribeFieldResult getFieldMetadata(String sObjectName, String fieldSetName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getFieldMetadata');
        params.put('sObjectName', sObjectName);
        params.put('fieldSetName', fieldSetName);
        return (Schema.DescribeFieldResult) icTestMockUtilities.Tracer.GetReturnValue(this, 'getFieldMetadata');

    }

    public List<String> getAPINames(String sObjectName, String fieldSetName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAPINames');
        params.put('sObjectName', sObjectName);
        params.put('fieldSetName', fieldSetName);
        return (List<String>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAPINames');

    }

    public List<SelectOption> GetPickListValues(Map<String, Schema.sObjectType> globalSchema,
            String sObjectName, String fieldSetMember){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'GetPickListValues');
        params.put('globalSchema', globalSchema);
        params.put('sObjectName', sObjectName);
        params.put('fieldSetMember', fieldSetMember);
        return (List<SelectOption>) icTestMockUtilities.Tracer.GetReturnValue(this, 'GetPickListValues');

    }

    public Map<String, List<String>> GetObjectsAndFieldsFromDataSet(Map<String, Schema.sObjectType> globalSchema,
            List<Schema.FieldSetMember> fieldSet, String sObjectName){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'GetObjectsAndFieldsFromDataSet');
        params.put('globalSchema', globalSchema);
        params.put('fieldSetMember', fieldSet);
        params.put('sObjectName', sObjectName);
        return (Map<String, List<String>>) icTestMockUtilities.Tracer.GetReturnValue(this, 'GetObjectsAndFieldsFromDataSet');

    }

    public Map<String, List<SelectOption>> GetMapFieldObjectsToSelectedOptions(Map<String, List<String>> fieldToObjectsFilter,
            Map<String, Schema.sObjectType> globalSchema, String sObjectType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'GetMapFieldObjectsToSelectedOptions');
        params.put('fieldToObjectsFilter', fieldToObjectsFilter);
        params.put('globalSchema', globalSchema);
        params.put('sObjectType', sObjectType);
        return (Map<String, List<SelectOption>>) icTestMockUtilities.Tracer.GetReturnValue(this, 'GetMapFieldObjectsToSelectedOptions');

    }
}