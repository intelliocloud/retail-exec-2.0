/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icTestBLPromotionTrigger {
    private static List<Market_List_Account__c> mlaList = new List<Market_List_Account__c>();
    private static List<rtxPromotion_Store__c> psList = new List<rtxPromotion_Store__c>();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();

    public static testMethod void testBLPromTrigger(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOPromotionStore', new icREPOPromotionStoreMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOPromotionStoreMock','getPromotionStoreByPromotionIds', psList);

        Account acc = accountCreator.createAccount();
        ML__c ml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtCamp = retailExecCreator.createApprovedCurrentMerchandiserCampaign(ml);
        Market_List_Account__c mlAcc = new Market_List_Account__c();
        mlAcc.marketList__c = ml.Id;
        mlAcc.account__c = acc.Id;
        insert mlAcc;

        Promotion__c p = new Promotion__c();
        p.Name = 'testPromotion';
        p.rtxIn_Store_Start_Date__c = DateTime.now().date() ;
        p.rtxIn_Store_End_Date__c = DateTime.now().date().addDays(+2) ;
        p.rtxApproved__c = FALSE;
        p.rtxMarket_List__c = ml.Id;
        insert p;

        p.rtxApproved__c = TRUE;
        update p;

        Promotion__c pApp = new Promotion__c();
        pApp.Name = 'testingApproved';
        pApp.rtxIn_Store_Start_Date__c = DateTime.now().date() ;
        pApp.rtxIn_Store_End_Date__c = DateTime.now().date().addDays(+2) ;
        p.rtxMarket_List__c = ml.Id;
        insert pApp;

        pApp.rtxArchived__c = TRUE;
        update pApp;

    }

}