/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestCTRLlistView {

    public static icDTOGenericListView dto = new icDTOGenericListView();

    public static testMethod void testM(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLGeneric', new icBLGenericMock());
        icTestMockUtilities.Tracer.SetReturnValue('icBLGenericMock','getFilteredItems', dto);

        icCTRLlistView.getListViews('StoreVisit__c');
        icCTRLlistView.getFilteredItems('Name','StoreVisit__c');

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLGenericMock', 'getFilteredItems'));
    }

}