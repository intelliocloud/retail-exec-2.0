/**
 * Created by Andrea Pissinis on 2018-02-09.
 */

public with sharing class icBLAuditTaskTrigger implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        void onBeforeUpdate(List<AuditTask__c> updatedAuditTask);

    }

    public class Impl implements IClass {

        private icREPOGeneric.IClass genREPO = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icBLstoreVisit.IClass svBL = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLStoreVisit');

        public void onBeforeUpdate(List<AuditTask__c> updatedAuditTask) {

            List<Id> storeVisitIds = new List<Id>();
            Map<Id, StoreVisit__c> readOnlyStoreVisitMap = new Map<Id, StoreVisit__c>();

            for(AuditTask__c at : updatedAuditTask){
                storeVisitIds.add(at.Store_Visit__c);
            }

            readOnlyStoreVisitMap = svBL.getReadOnlyVisitMapByIds(storeVisitIds);

            for (AuditTask__c at : updatedAuditTask) {

                if (readOnlyStoreVisitMap.containsKey(at.Store_Visit__c)){

                    at.addError(Label.Read_Only_AuditProducts_Warning);

                }
            }

        }

    }
}