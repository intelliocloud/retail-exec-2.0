/**
 * Created by georg on 2018-02-20.
 */

public with sharing class icBLPlanogramTrigger implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        void onBeforeInsert(List<rtxPlanogram__c> newList);
        void onBeforeUpdate(Map<Id, rtxPlanogram__c> oldMap, Map<Id, rtxPlanogram__c> newMap);
    }

    public class Impl implements IClass {
        private icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icBLPlanogramVersion.IClass pvBL = (icBLPlanogramVersion.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramVersion');

        public void onBeforeInsert(List<rtxPlanogram__c> newList){
        }
        public void onBeforeUpdate(Map<Id, rtxPlanogram__c> oldMap, Map<Id, rtxPlanogram__c> newMap) {
            List<rtxPlanogram__c> recordsWhenSetActive = new List<rtxPlanogram__c>();
            List<Id> activePlanogramIds = new List<Id>();

            boolean actionIsActive = false;
            boolean actionIsArchived = false;

            for( Id planogramId : newMap.keySet() ){
                if(newMap.get(planogramId).rtxActive__c){
                   activePlanogramIds.add(planogramId);
                }
            }

            if(!activePlanogramIds.isEmpty()){

                List<rtxPlanogram_Version__c> pvList = new List<rtxPlanogram_Version__c>();
                pvList = pvBL.getCurrentPlanogramVersionsByPlanogramIds(activePlanogramIds);

                List<Id> planoWithVersionIds = new List<Id>();

                for(rtxPlanogram_Version__c pv : pvList){

                    newMap.get(pv.Planogram__c).Planogram_Version__c = pv.Id;
                    planoWithVersionIds.add(pv.Planogram__c);
                }

                for(Id idTmp : newMap.keySet()){
                    if(!planoWithVersionIds.contains(idTmp)){
                        newMap.get(idTmp).Planogram_Version__c = null;
                    }
                }

            }


        }

    }
}