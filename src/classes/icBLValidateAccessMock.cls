/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icBLValidateAccessMock {

    public Boolean fieldsAreAccessible(List<String> fields, String sObjectType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'fieldsAreAccessible');
        params.put('fields', fields);
        params.put('sObjectType', sObjectType);
        return (Boolean) icTestMockUtilities.Tracer.GetReturnValue(this, 'fieldsAreAccessible');

    }

    public Boolean fieldsAreUpdateable(List<String> fields, String sObjectType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'fieldsAreUpdateable');
        params.put('fields', fields);
        params.put('sObjectType', sObjectType);
        return (Boolean) icTestMockUtilities.Tracer.GetReturnValue(this, 'fieldsAreUpdateable');

    }

    public List<Schema.FieldSetMember> getAccessiblesFieldSetMembers(List<Schema.FieldSetMember> fields, String sObjectType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAccessiblesFieldSetMembers');
        params.put('fields', fields);
        params.put('sObjectType', sObjectType);
        return (List<Schema.FieldSetMember>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAccessiblesFieldSetMembers');

    }
}