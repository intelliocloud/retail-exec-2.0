/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOlistView {

    static testMethod void testdto(){
        icDTOlistView dto = new icDTOlistView();
        System.assertEquals(null, dto.columns);
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.orderBy);
        System.assertEquals(null, dto.query);
        System.assertEquals(null, dto.scope);
        System.assertEquals(null, dto.sObjectType);
        System.assertEquals(null, dto.whereCondition);

    }
}