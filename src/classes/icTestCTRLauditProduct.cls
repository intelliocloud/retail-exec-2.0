/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestCTRLauditProduct {
    static icDTOAuditProduct apdto = new icDTOAuditProduct();

    static testMethod void test(){
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLauditProduct', new icBLauditProductMock());
        icTestMockUtilities.Tracer.SetReturnValue('icBLauditProductMock','getAuditProducts', apdto);

        List<AuditProduct__c> aPs = new List<AuditProduct__c>();
        icCTRLauditProduct.test('test');
        icCTRLauditProduct.getAuditProducts(icTestHelperUtility.getFakeId(StoreVisit__c.SObjectType));
        icCTRLauditProduct.saveAuditProducts(aPs);

    }

}