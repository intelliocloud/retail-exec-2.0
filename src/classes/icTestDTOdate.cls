/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOdate {

    static testMethod void testdto(){
        icDTOdate dto = new icDTOdate();
        System.assertEquals(null, dto.dayName);
        System.assertEquals(null, dto.dayDate);
        System.assertEquals(null, dto.year);
        System.assertEquals(null, dto.month);
        System.assertEquals(null, dto.day);

    }

}