/**
 * Created by François Poirier on 12/21/2017.
 */

global with sharing class icCTRLgeneric {


    private static icBLGeneric.IClass bl = (icBLGeneric.IClass) icObjectFactory.GetSingletonInstance('icBLGeneric');

    @AuraEnabled
    global static void updateValue(string obj){
        system.debug('in update value');
        system.debug('obj ===> ' + obj);
        SObject oobj;
        try {
            oobj = (SObject) JSON.deserialize(obj, SObject.class);
            system.debug('oobj ===> ' + oobj);
            bl.updateValue(oobj);
        } catch(Exception ex) {
            System.debug('Exception occured in icCTRLgeneric.updateValue: ' + ex.getMessage());
        }
    }
/*
    global static void test(){
        ContentDocumentLink test = [SELECT ContentDocumentId FROM ContentDocumentLink where LinkedEntityId = '0011N00001EtNDtQAN'];
        system.debug('test ===> ' + test);
    }
    */
}