/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icREPOGenericMock implements icREPOGeneric.IClass{

    public List<sObject> getValues(String sObjectType, List<String> fieldsList, List<String> criteriaList, List<String> orderByList, Integer limitSize){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getValues');
        params.put('sObjectType', sObjectType);
        params.put('fieldsList', fieldsList);
        params.put('criteriaList', criteriaList);
        params.put('orderByList', orderByList);
        params.put('limitSize', limitSize);
        return (List<sObject>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getValues');

    }

    public icDTOServerResponse deleteValues(String sObjectType, List<String> criteriaList){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'deleteValues');
        params.put('sObjectType', sObjectType);
        params.put('criteriaList', criteriaList);
        return (icDTOServerResponse) icTestMockUtilities.Tracer.GetReturnValue(this, 'deleteValues');

    }
    public Attachment saveFile(String parentId, String fileName, String base64Data, String contentType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveFile');
        params.put('parentId', parentId);
        params.put('fileName', fileName);
        params.put('base64Data', base64Data);
        params.put('contentType', contentType);
        return (Attachment) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveFile');

    }
    public icDTOServerResponse updateValues(sObject obj){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'updateValues');
        params.put('obj', obj);
        return (icDTOServerResponse) icTestMockUtilities.Tracer.GetReturnValue(this, 'updateValues');

    }
    public String insertValues(sObject obj){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'insertValues');
        params.put('obj', obj);
        return (String) icTestMockUtilities.Tracer.GetReturnValue(this, 'insertValues');

    }
    public icDTOServerResponse upsertValues(sObject obj){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'upsertValues');
        params.put('obj', obj);
        return (icDTOServerResponse) icTestMockUtilities.Tracer.GetReturnValue(this, 'upsertValues');

    }
    public icDTOServerResponse upsertValues(sObject[] objs){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'upsertValues');
        params.put('objs', objs);
        return (icDTOServerResponse) icTestMockUtilities.Tracer.GetReturnValue(this, 'upsertValues');

    }

    public List<Account> getValues(String query) {
        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getValues');
        params.put('query', query);
        return (List<Account>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getValues');
    }
}