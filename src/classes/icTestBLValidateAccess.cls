/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestBLValidateAccess {

    public static testMethod void testFieldsAreAccessible(){
        List<String> fields = new List<String>();
        fields.add('Name');
        fields.add('rtxDistrict__c');
        fields.add('rtxAudit_Code__c');
        fields.add('CreatedById');

        List<Schema.FieldSetMember> fL = new List<Schema.FieldSetMember>();
        fL.addAll(SObjectType.Account.FieldSets.visit_schedule_filter.getFields());

        List<Schema.FieldSetMember> fLS = new List<Schema.FieldSetMember>();

        fLS.addAll(SObjectType.StoreVisit__c.FieldSets.ReadOnlyException.getFields());

        icBLValidateAccess.IClass atBLbl = (icBLValidateAccess.IClass) icObjectFactory.getsingletoninstance('icBLValidateAccess');
        atBLbl.fieldsAreAccessible(fields, 'Account');
        try{
            atBLbl.fieldsAreUpdateable(fields, 'Account');
        }catch(Exception e){
            //throw new AuraHandledException('fields not updateable');
        }
        atBLbl.getAccessiblesFieldSetMembers(fL, 'Account');
        atBLbl.getAccessiblesFieldSetMembers(fLS, 'StoreVisit__c');

    }

}