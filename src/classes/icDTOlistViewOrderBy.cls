/**
 * Created by François Poirier on 1/30/2018.
 */

public with sharing class icDTOlistViewOrderBy {

    public String fieldNameOrPath {get;set;}
    public String nullsPosition {get;set;}
    public String sortDirection {get;set;}
}