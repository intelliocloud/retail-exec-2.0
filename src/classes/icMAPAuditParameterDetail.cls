/**
 * Created by georg on 2018-02-26.
 */

public with sharing class icMAPAuditParameterDetail implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        icDTOAuditParameterDetail recordToDTO(Audit_Parameter_Detail__c auditParameterDetail);
        List<icDTOAuditParameterDetail> listToDTO(List<Audit_Parameter_Detail__c> auditParameterDetails);
    }

    public class Impl implements IClass {

        public icDTOAuditParameterDetail recordToDTO(Audit_Parameter_Detail__c apd) {
            icDTOAuditParameterDetail dto = new icDTOAuditParameterDetail();

            icDTOAuditParameters apDto = new icDTOAuditParameters();
            apDto.id = apd.Audit_Parameters__r.Id;
            apDto.name = apd.Audit_Parameters__r.Name;
            apDto.description = apd.Audit_Parameters__r.rtxDescription__c;
            apDto.active = apd.Audit_Parameters__r.rtxIsActive__c;
            apDto.uniqueId = apd.Audit_Parameters__r.rtxUnique_ID__c;

            dto.id = apd.Id;
            dto.name = apd.Name;
            dto.addAuditTaskProducts = apd.rtxAdd_Audit_Task_Products__c;
            dto.addPromotionalProductsToAudit = apd.rtxAdd_Promotional_Products_to_Audit__c;
            dto.auditParameter = apd.Audit_Parameters__c;
            dto.auditParameterRecord = apDto;
            dto.auditTaskSortingField = apd.rtxAudit_Task_Sorting_Field__c;
            dto.auditTaskSortingOrder = apd.rtxAudit_Task_Sorting_Order__c;
            dto.displayAuditFilterSet = apd.rtxDisplay_Audit_Filter_Set__c;
            dto.displayAuditMultiEditSet = apd.rtxDisplay_Audit_Multiedit_Set__c;
            dto.displayAuditSortingField = apd.rtxDisplay_Audit_Sorting_Field__c;
            dto.displayAuditSortingOrder = apd.rtxDisplay_Audit_Sorting_Order__c;
            dto.displayAuditType = apd.rtxDisplay_Audit_Type__c;
            dto.displayFieldSet = apd.rtxDisplay_Field_Set__c;
            dto.displayProductFieldSet = apd.rtxDisplay_Product_Field_Set__c;
            dto.executionCampaignFieldSet = apd.rtxExecution_Campaign_Field_Set__c;
            dto.executionCampaignSortingSet = apd.rtxExecution_Campaign_Sorting_Field__c;
            dto.executionCampaignSortingOrder = apd.rtxExecution_Campaign_Sorting_Order__c;
            dto.productAuditFieldSet = apd.rtxProduct_Audit_Field_Set__c;
            dto.productAuditFilterSet = apd.rtxProduct_Audit_Filter_Set__c;
            dto.productAuditMultiEditSet = apd.rtxProduct_Audit_Multi_Edit_Set__c;
            dto.productAuditSortingField = apd.rtxProduct_Audit_Sorting_Field__c;
            dto.productAuditSortingOrder = apd.rtxProduct_Audit_Sorting_Order__c;
            dto.promotionAudit = apd.rtxPromotion_Audit__c;
            dto.shelfAuditFieldSet = apd.rtxShelf_Audit_Field_Set__c;
            dto.shelfAuditSubtype = apd.rtxShelf_Audit_Subtype__c;
            dto.shelfAuditType = apd.rtxShelf_Audit_Type__c;
            dto.storeAuditCode = apd.rtxStore_Audit_Code__c;
            dto.taskAuditType = apd.rtxTask_Audit_Type__c;
            dto.userAuditProfile = apd.rtxUser_Audit_Profile__c;
            dto.visitAuditCode = apd.rtxVisit_Audit_Code__c;
            return dto;
        }

        public List<icDTOAuditParameterDetail> listToDTO(List<Audit_Parameter_Detail__c> apdList){
            List<icDTOAuditParameterDetail> dtos = new List<icDTOAuditParameterDetail>();

            for(Audit_Parameter_Detail__c apd : apdList){
                icDTOAuditParameterDetail dto = new icDTOAuditParameterDetail();
                dto = recordToDTO(apd);
                dtos.add(dto);
            }
            return dtos;
        }
    }
}