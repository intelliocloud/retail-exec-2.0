/**
 * Created by Andrea Pissinis on 2018-04-16.
 */

public with sharing class icBLAuditParameterTrigger implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        void onBeforeInsert(List<AuditParameters__c> auditParams);
        void onBeforeUpdate(List<AuditParameters__c> auditParams);
    }

    public class Impl implements IClass {

        private icBLAuditParameter.IClass apBL = (icBLAuditParameter.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameter');

        public void onBeforeInsert(List<AuditParameters__c> auditParams) {

            this.validateAlreadyActive(auditParams);

        }

        public void onBeforeUpdate(List<AuditParameters__c> auditParams) {

            this.validateAlreadyActive(auditParams);


        }

        private void validateAlreadyActive(List<AuditParameters__c> auditParams){

            Boolean newIsActiveFound = false;
            for(AuditParameters__c ap : auditParams){
                if(ap.rtxIsActive__c){

                    if(newIsActiveFound){
                        ap.rtxIsActive__c = false;
                    }
                    else {
                        newIsActiveFound = true;
                    }
                }
            }
            if(newIsActiveFound) {

                Boolean alreadyActiveFound = false;
                List<AuditParameters__c> activeAuditParameters = new List<AuditParameters__c>();
                for(AuditParameters__c activeAP : activeAuditParameters){
                    activeAP.rtxIsActive__c = false;
                    alreadyActiveFound = true;
                }

                if(alreadyActiveFound) {
                    apBL.saveAuditParameters(activeAuditParameters);
                }
            }

        }

    }

}