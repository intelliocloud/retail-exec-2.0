/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestMAPAuditParameterDetail {
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();

    public static testMethod void testMAPAPD(){

        AuditParameters__c aP = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c aPD = parameterCreator.createAuditParameterDetail(aP);

        List<Audit_Parameter_Detail__c> aPDL = new List<Audit_Parameter_Detail__c>();
        aPDL.add(aPD);

        icMAPAuditParameterDetail.IClass aumap = (icMAPAuditParameterDetail.IClass) icObjectFactory.getsingletoninstance('icMAPAuditParameterDetail');
        aumap.listToDTO(aPDL);
        aumap.recordToDTO(aPD);
    }

}