/**
 * Created by François Poirier on 12/20/2017.
 */

global with sharing class icDTOAuditProduct {

    @AuraEnabled global List<icDTOfieldSet> filterFieldSet {get;set;}
    @AuraEnabled global List<icDTOfieldSet> tableFieldSet {get;set;}
    @AuraEnabled global List<icDTOGenericRecord> records {get;set;}

}