/**
 * Created by François Poirier on 1/26/2018.
 */

global with sharing class icDTOvisitSchedule {


    @AuraEnabled global List<icDTOdate> dates {get;set;}
    @AuraEnabled global List<icDTOhour> hours {get;set;}
    @AuraEnabled global Map<String, List<StoreVisit__c>> visits {get;set;}

}