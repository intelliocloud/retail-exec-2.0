/**
 * Created by Francois Poirier on 2018-11-15.
 */

global class icBatchMarketListAccounts implements Database.Batchable<sObject> {

    private icBLMarketList.IClass mlBL = (icBLMarketList.IClass) icObjectFactory.GetSingletonInstance('icBLMarketList');

    String query;
    String marketListId;

    global icBatchMarketListAccounts(Id mlId){

        ML__c marketList = new ML__c();
        marketList = mlBL.getMarketListById(mlId);
        query = this.getQueryFromMarketList(marketList);

        marketListId = mlId;
    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        List<Market_List_Account__c> mlAccounts = new List<Market_List_Account__c>();

        for(SObject s : scope){

            Market_List_Account__c mlAccount = new Market_List_Account__c();
            Account act = (Account) s;

            mlAccount.account__c = act.Id;
            mlAccount.marketList__c = marketListId;
            mlAccount.rtxSelection_Type__c = 'Automatic';

            mlAccounts.add(mlAccount);

        }

        insert mlAccounts;
    }

    global void finish(Database.BatchableContext bc){

    }

    private String getQueryFromMarketList(ML__c marketList){

        List<String> criterias = new List<String>();
        List<icDTOfilterItem> dtoFilters = new List<icDTOfilterItem>();
        String queryString = 'SELECT Id FROM Account WHERE ';

        if(String.isNotBlank(marketList.rtxJSON_Filters__c)) {
            system.debug('filtersJSON ===> ' + marketList.rtxJSON_Filters__c);

            dtoFilters = (List<icDTOfilterItem>) JSON.deserialize(marketList.rtxJSON_Filters__c, List<icDTOfilterItem>.class);
            system.debug('dtoFilters ===> ' + dtoFilters);


            if (String.isEmpty(marketList.rtxFiltersLogic__c)) {
                Integer i = 0;

                for (icDTOfilterItem dtoFilter : dtoFilters) {

                    String tmpCriteria = '  ';
                    icDTOoperator dtOoperator = dtoFilter.operator;
                    //icDTOoperator dtOoperator = (icDTOoperator) JSON.deserialize(dtoFilter.operator, icDTOoperator.class);
                    if (i > 0) {
                        tmpCriteria = tmpCriteria + ' AND ';
                    }
                    tmpCriteria = tmpCriteria + dtoFilter.fieldName + ' ' + dtOoperator.operator;
                    if (dtOoperator.operator == 'LIKE') {
                        tmpCriteria = tmpCriteria + ' \'%' + dtoFilter.value + '%\'';
                    } else {
                        tmpCriteria = tmpCriteria + ' \'' + dtoFilter.value + '\'';
                    }
                    criterias.add(tmpCriteria);
                    i++;
                }
            } else {

                String tmpCriteria = '';
                tmpCriteria = this.parseFilterLogic(marketList.rtxFiltersLogic__c, dtoFilters);
                criterias.add(tmpCriteria);
            }
        }


        for(String criteria : criterias){
            queryString += criteria;
        }

        return queryString;
    }

    private String parseFilterLogic(String filterLogic, List<icDTOfilterItem> filters){

        Integer i = 1;

        for(icDTOfilterItem filter : filters){

            String regex = String.valueOf(i);
            Pattern regexPattern = Pattern.compile(regex);
            Matcher regexMatcher = regexPattern.matcher(filterLogic);

            if(regexMatcher.find()){

                String tmpCriteria = ' ';
                icDTOoperator dtOoperator = filter.operator;
                //icDTOoperator dtOoperator = (icDTOoperator) JSON.deserialize(dtoFilter.operator, icDTOoperator.class);

                tmpCriteria = filter.fieldName + ' ' + dtOoperator.operator;
                if (dtOoperator.operator == 'LIKE') {
                    tmpCriteria = tmpCriteria + ' \'%' + filter.value + '%\'';
                } else {
                    tmpCriteria = tmpCriteria + ' \'' + filter.value + '\'';
                }
                tmpCriteria = tmpCriteria + ' ';

                filterLogic = filterLogic.replaceAll(regex, tmpCriteria);
            }
            i++;
        }
        return filterLogic;
    }

}