/**
 * Created by François Poirier on 2/8/2018.
 */

global with sharing class icDTOhour {

    @AuraEnabled global String strHour {get;set;}
    @AuraEnabled global Integer hour {get;set;}

}