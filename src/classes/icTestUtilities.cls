/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestUtilities {
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();

    public static testMethod void testUti(){
        icUtilities.convertFields(Schema.SObjectType.Account.fieldSets.visit_schedule_filter.getFields(), Account.SObjectType);

        icUtilities.getLookupName('StoreVisit__c');
        icUtilities.getLookupName('AuditProduct__r');
        Account a = accountCreator.createAccount();

        icUtilities.getRelationshipFieldValue(a,'Name');
        icUtilities.getRelationshipFieldValue(null,'');
        icUtilities.getRelationshipFieldValue(a,'Owner.Id');
    }

}