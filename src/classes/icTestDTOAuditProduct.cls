/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOAuditProduct {

    static testMethod void testdto(){

        icDTOAuditProduct dto = new icDTOAuditProduct();
        System.assertEquals(null, dto.filterFieldSet);
        System.assertEquals(null, dto.tableFieldSet);
        System.assertEquals(null, dto.records);
    }

}