/**
 * Created by François Poirier on 11/29/2017.
 */

@isTest
private class icTestDTOServerResponse {

    static testMethod void testServerResponse() {

        icDTOServerResponse dto = new icDTOServerResponse();
        System.assertEquals(null, dto.isSuccess);
        System.assertEquals(null, dto.errorMessage);

    }
}