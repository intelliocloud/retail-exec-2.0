/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestHelperAuditTask {

    public AuditTask__c createAudiTaskDone(Account acc, Priority__c p, StoreVisit__c s){
        AuditTask__c aDone = new AuditTask__c();
        aDone.Name = 'testing';
        aDone.Account__c = acc.id;
        aDone.Priority_Task__c = p.Id;
        aDone.Store_Visit__c = s.Id;
        aDone.rtxDone__c = TRUE;

        insert aDone;
        return aDone;
    }

    public AuditTask__c createAuditTaskNotDone(Account acc, Priority__c p, StoreVisit__c s){

        AuditTask__c a = new AuditTask__c();
        a.Name = 'testing';
        a.Account__c = acc.id;
        a.Priority_Task__c = p.Id;
        a.Store_Visit__c = s.Id;
        a.rtxDone__c = FALSE;
        insert a;
        return a;
    }

}