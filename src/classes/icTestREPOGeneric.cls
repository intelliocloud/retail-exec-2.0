/**
 * Created by François Poirier on 11/29/2017.
 */

@isTest

public with sharing class icTestREPOGeneric {
    private static icBLFieldSetManager.IClass fieldSetManagerBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
    private static icREPOGeneric.IClass repo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static String sObjectType;
    private static List<String> fieldsList = new List<String>();
    private static List<String> criteriaList = new List<String>();
    private static List<String> orderByList = new List<String>();
    private static Integer limitSize;

    static testMethod void setup(){
        List<String> nameFields = fieldSetManagerBL.getAPINames('Account', 'visit_schedule_filter');
        List<String> descriptionFields = fieldSetManagerBL.getAPINames('Account', '');
        fieldsList.addAll(nameFields);
        fieldsList.addAll(descriptionFields);
        criteriaList = new List<String>();
        orderByList = new List<String>();
        orderByList.add('Id');
        limitSize = 10;
        sObjectType = 'Account';
    }

    static Account createAccount() {
        Account acc = new Account();
        acc.Name = 'testName';
        insert acc;
        return acc;
    }

    static testMethod void testGetValues(){

        createAccount();
        setup();
        List<sObject> objs = repo.getValues(sObjectType, fieldsList, criteriaList, orderByList, limitSize);
        system.assert(objs.size() > 0);
    }

    static testMethod void testGetValuesGeneric(){
        sObjectType = 'User';
        fieldsList.add('Name');
        criteriaList = null;
        limitSize = 100;
        List<sObject> objs = repo.getValues(sObjectType, fieldsList, criteriaList, orderByList, limitSize);
        System.assertNotEquals(null, objs);
    }

    static testMethod void testDeleteValuesGeneric(){
        sObjectType = 'Account';
        criteriaList.add('Name = \'' + 'Test' + '\'');
        icDTOServerResponse objs = repo.deleteValues(sObjectType, criteriaList);
        System.assertNotEquals(null, objs);
    }

    static testMethod void testSaveFileGeneric(){
        Account acc = new Account();
        acc.Name = 'Test';
        insert acc;
        String parentId = acc.Id;
        String fileName = 'test.png';
        String base64Data = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        String contentType = 'image/png';
        Attachment obj = repo.saveFile(parentId, fileName, base64Data, contentType);
        System.assertNotEquals(null, obj);
    }

    static testMethod void testUpdateValuesGeneric(){
        Account acc = new Account();
        Account a = accountCreator.createAccount();
        icDTOServerResponse obj = repo.updateValues(a);
        System.assertNotEquals(null, obj);
    }

    static testMethod void testUpsert(){
        Account a = accountCreator.createAccount();
        Account act = new Account();
        icDTOServerResponse obj = repo.upsertValues(a);
        icDTOServerResponse objct = repo.upsertValues(act);
        System.assertNotEquals(null,obj);
        System.assertNotEquals(null,objct);
        List<Account> acL = new List<Account>();
        acL.add(a);
        icDTOServerResponse listResp = repo.upsertValues(acL);
        System.assertNotEquals(null,listResp);
    }

    static testMethod void testInsertValues(){
        Account a = new Account();
        a.Name = 'test';
        a.rtxDistrict__c = 'A';
        a.rtxSegmentation__c = 'A';
        String obj = repo.insertValues(a);
        System.assertNotEquals(null, obj);
    }

}