/**
 * Created by Francois Poirier on 2018-11-09.
 */

public with sharing class icREPOAccount implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Account> getAccountsByIds(List<Id> accountIds);
        List<Account> saveAccounts(List<Account> accounts);
    }

    public class Impl implements IClass {

        public List<Account> getAccountsByIds(List<Id> accountIds){

            List<Account> accounts = new List<Account>();
            accounts = [
                SELECT  Id,
                        rtxLastVisitDate__c
                FROM    Account
                WHERE   Id IN : accountIds
            ];

            return accounts;

        }

        public List<Account> saveAccounts(List<Account> accounts){

            upsert accounts;
            return accounts;
        }
    }

}