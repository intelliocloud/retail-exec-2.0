/**
 * Created by Andrea Pissinis on 2018-03-06.
 */
@isTest
public with sharing class icTestHelperAuditParameter {

    public AuditParameters__c createActiveAuditParameter(){
        AuditParameters__c ap = new AuditParameters__c();
        ap.Name='testAP';
        ap.rtxIsActive__c = TRUE;
        insert ap;
        return ap;
    }


    public Audit_Parameter_Detail__c createAuditParameterDetail(AuditParameters__c ap){
        Audit_Parameter_Detail__c apd = new Audit_Parameter_Detail__c();
        apd.Audit_Parameters__c = ap.Id;
        apd.rtxUser_Audit_Profile__c = 'Merchandiser';
        apd.rtxVisit_Audit_Code__c = 'Standard';
        apd.rtxStore_Audit_Code__c = 'Standard';
        apd.rtxShelf_Audit_Type__c = 'Products';
        apd.rtxDisplay_Product_Field_Set__c ='none';
        apd.rtxProduct_Audit_Field_Set__c = 'audit_product_table';
        apd.rtxProduct_Audit_Filter_Set__c = 'audit_product_filter';
        apd.rtxPromotion_Audit__c = 'Yes';
        apd.rtxAdd_Promotional_Products_to_Audit__c = 'Yes';
        apd.rtxDisplay_Audit_Type__c = 'None';
        apd.rtxTask_Audit_Type__c = 'Standard';
        apd.rtxAdd_Audit_Task_Products__c = 'Yes';
        apd.rtxExecution_Campaign_Sorting_Field__c = 'None';
        apd.rtxAudit_Task_Sorting_Field__c = 'None';
        apd.rtxAudit_Task_Sorting_Order__c = 'Ascending';
        apd.rtxExecution_Campaign_Sorting_Order__c = 'Ascending';
        apd.rtxShelf_Audit_Subtype__c = 'None';
        apd.rtxDisplay_Audit_Type__c = 'None';
        insert apd;
        return apd;
    }
}