/**
 * Created by Francois Poirier on 2018-11-22.
 */

global with sharing class icBatchRetailExecutionCampaignAccounts implements Database.Batchable<sObject> {

    String query;
    String marketListId;
    String reCampaignId;

    global icBatchRetailExecutionCampaignAccounts(Id mlId, Id retailExecCampaignId){

        query = 'SELECT Id, account__c, account__r.RecordType.Name,  marketList__c FROM Market_List_Account__c WHERE marketList__c = \'' + String.valueOf(mlId) + '\' AND rtxExcluded__c = false';
        marketListId = mlId;
        reCampaignId = retailExecCampaignId;

    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        list<rtxCampaign_Account__c> campaignAccountToSave = new List<rtxCampaign_Account__c>();

        for (SObject s : scope) {
            Market_List_Account__c mlAct = (Market_List_Account__c) s;
            rtxCampaign_Account__c newCampaignAccount = new rtxCampaign_Account__c();
            newCampaignAccount.rtxAccount__c = mlAct.account__c;
            newCampaignAccount.rtxRetail_Execution_Campaign__c = reCampaignId;
            campaignAccountToSave.add(newCampaignAccount);
        }

        insert campaignAccountToSave;
    }

    global void finish(Database.BatchableContext bc){

    }

}