/**
 * Created by François Poirier on 12/8/2017.
 */

public with sharing class icREPOauditProduct  implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<AuditProduct__c> getAuditProducts(Id storeVisitId);
        List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts);
        List<AuditProduct__c> getNotDoneAuditProducts();
        List<AuditProduct__c> getExistingAuditProducts();
        List<AuditProduct__c> getNotDoneAuditProductByVisitIds(List<Id> visitIds);
        void deleteAuditProducts(List<AuditProduct__c> auditProducts);
        List<Product__c> getAuditableProducts();

        }

    public class Impl implements IClass{
        private icREPOGeneric.IClass genREPO = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');

        public List<AuditProduct__c>  getAuditProducts(Id storeVisitId){

            List<AuditProduct__c> retour = new List<AuditProduct__c>();

            retour = [
                    SELECT  Name,
                            rtxBrand__c,
                            rtxCategory__c,
                            rtxDone__c,
                            rtxException__c,
                            rtxNF__c,
                            rtxOOS__c,
                            rtxPrice__c,
                            Product__r.rtxProduct_Image_Link__c,
                            Product__r.ParentCategory__r.Name,
                            Product__r.ParentBrand__r.Name
                    FROM    AuditProduct__c
                    WHERE   StoreVisit__c = :storeVisitId
            ];

            return retour;
        }

        public List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts){
            system.debug('in save audit Product REPO');
            system.debug('auditProducts before update ===> ' + auditProducts);
            update auditProducts;
            system.debug('auditProducts after update ===> ' + auditProducts);
            return auditProducts;
        }

        public List<AuditProduct__c> getNotDoneAuditProducts(){

            List<String> AuditProductsFields = new List<String>{'StoreVisit__c'};
            List<String> auditProductsCriteria = new List<String>{'rtxDone__c = FALSE'};

            List<AuditProduct__c> auditPs = genREPO.getValues('AuditProduct__c', AuditProductsFields, auditProductsCriteria, null, 50000);

            return auditPs;
        }

        public List<AuditProduct__c> getExistingAuditProducts(){

            List<String> AuditProductsField = new List<String>{'StoreVisit__c', 'rtxDone__c' ,'Product__c'};

            List<AuditProduct__c> auditPs = genREPO.getValues('AuditProduct__c', AuditProductsField, null, null, 50000);

            return auditPs;
        }

        public List<AuditProduct__c> getNotDoneAuditProductByVisitIds(List<Id> visitIds){

            List<AuditProduct__c> auditProducts = new List<AuditProduct__c>();
            auditProducts = [
                    SELECT  Id,
                            rtxDone__c,
                            StoreVisit__c
                    FROM    AuditProduct__c
                    WHERE   StoreVisit__c IN :visitIds
                    AND     rtxDone__c = false
            ];

            return auditProducts;
        }

        public void deleteAuditProducts(List<AuditProduct__c> auditProducts){
            delete auditProducts;
        }

        public List<Product__c> getAuditableProducts() {

            List<Product__c> productList = new List<Product__c>();

            productList = [
                    SELECT Id,
                            Name,
                            UserRecordAccess.HasReadAccess
                    FROM Product__c
                    WHERE rtxIsActive__c = TRUE
                    AND rtxCurrent__c = TRUE
                    AND rtxAllow_Audit__c = TRUE
            ];

            return productList;
        }
    }
}