/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icBLMarketListAccountMock {

    public List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getMarketListAccountsByMarketIds');
        params.put('marketIds', marketIds);
        return (List<Market_List_Account__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getMarketListAccountsByMarketIds');

    }

    public Map<Id, List<Id>> generateMLAMap(List<Market_List_Account__c> mla){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'generateMLAMap');
        params.put('mla', mla);
        return (Map<Id, List<Id>>) icTestMockUtilities.Tracer.GetReturnValue(this, 'generateMLAMap');

    }

}