/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icMAPAuditParameterDetailMock implements icMAPAuditParameterDetail.IClass{

    public icDTOAuditParameterDetail recordToDTO(Audit_Parameter_Detail__c auditParameterDetail) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'recordToDTO');
        params.put('auditParameterDetail', auditParameterDetail);
        return (icDTOAuditParameterDetail) icTestMockUtilities.Tracer.GetReturnValue(this, 'recordToDTO');
    }

    public List<icDTOAuditParameterDetail> listToDTO(List<Audit_Parameter_Detail__c> auditParameterDetails) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'listToDTO');
        params.put('auditParameterDetails', auditParameterDetails);
        return (List<icDTOAuditParameterDetail>) icTestMockUtilities.Tracer.GetReturnValue(this, 'listToDTO');
    }
}