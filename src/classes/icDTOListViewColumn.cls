/**
 * Created by François Poirier on 1/30/2018.
 */

public with sharing class icDTOListViewColumn {

    public String ascendingLabel {get;set;}
    public String descendingLabel {get;set;}
    public String fieldNameOrPath {get;set;}
    public Boolean hidden {get;set;}
    public String label {get;set;}
    public String selectListItem {get;set;}
    public String sortDirection {get;set;}
    public Integer sortIndex {get;set;}
    public Boolean sortable {get;set;}
    public String type {get;set;}

}