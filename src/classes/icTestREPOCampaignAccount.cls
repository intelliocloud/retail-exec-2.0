/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOCampaignAccount {

    public static testMethod void testRepo(){
        icREPOCampaignAccount.IClass caRep = (icREPOCampaignAccount.IClass) icObjectFactory.getsingletoninstance('icREPOCampaignAccount');
        List<Id> caL = new List<Id>();
        caL.add(icTestHelperUtility.getFakeId(rtxCampaign_Account__c.SObjectType));

        caRep.getCampaignAccountsByRetailExecutionCampaignIds(caL);
    }

}