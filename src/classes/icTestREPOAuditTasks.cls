/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOAuditTasks {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();

    public static testMethod void testRepo(){

        Account a = accountCreator.createAccount();
        parameterCreator.createAuditParameterDetail(parameterCreator.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(a);
        List<AuditTask__c> atsL = new List<AuditTask__c>();

        icREPOauditTasks.IClass atRP = (icREPOauditTasks.IClass) icObjectFactory.getsingletoninstance('icREPOauditTasks');
        //atRP.getAuditTasks(icTestHelperUtility.getFakeId(StoreVisit__c.SObjectType));
        //tRP.saveAuditTasks(atsL);
    }

}