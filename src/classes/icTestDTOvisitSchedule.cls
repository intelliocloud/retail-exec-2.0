/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOvisitSchedule {

    static testMethod void testdto(){
        icDTOvisitSchedule dto = new icDTOvisitSchedule();
        System.assertEquals(null, dto.dates);
        System.assertEquals(null, dto.hours);
        System.assertEquals(null, dto.visits);

    }
}