/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestMAPfieldSet {

    public static testMethod void testM(){

        icMAPfieldSet.IClass atBLbl = (icMAPfieldSet.IClass) icObjectFactory.getsingletoninstance('icMAPfieldSet');

        List<Schema.FieldSetMember> fL = new List<Schema.FieldSetMember>();
        fL.addAll(SObjectType.Account.FieldSets.visit_schedule_filter.getFields());

        List<icDTOListViewColumn> llL = new List<icDTOListViewColumn>();
        icDTOListViewColumn cl = new icDTOListViewColumn();
        cl.hidden = FALSE;
        llL.add(cl);

        atBLbl.mapToDTO(fL);
        atBLbl.mapToDTO(llL);
    }

}