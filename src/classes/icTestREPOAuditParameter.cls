/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOAuditParameter {

    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAccount accountCreator = new icTestHelperAccount();

    public static testMethod void testrepo(){
        AuditParameters__c aP = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c aPD = parameterCreator.createAuditParameterDetail(aP);
        Account a = accountCreator.createAccount();
        StoreVisit__c s = storeVisitCreator.createStoreVisit(a);

    }

}