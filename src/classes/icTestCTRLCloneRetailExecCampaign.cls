/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestCTRLCloneRetailExecCampaign {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();

    public static List<rtxRetail_Execution_Campaign__c> retL = new List<rtxRetail_Execution_Campaign__c>();
    public static List<Priority__c> prioL = new List<Priority__c>();
    public static List<icDTOpickList> dtoL = new List<icDTOpickList>();

    public static testMethod void testExecCamp(){

        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
        Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
        Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
        List<Priority__c> ps = new List<Priority__c>();
        ps.add(p);
        ps.add(pO);


        List<String> plValuesStatus = new List<String>();
        List<String> plValuesRole = new List<String>();

        Schema.DescribeFieldResult flRes = rtxRetail_Execution_Campaign__c.rtxStatus__c.getDescribe();
        List<Schema.PicklistEntry> ple = flRes.getPicklistValues();
        for (Schema.PicklistEntry pls : ple){
            plValuesStatus.add(pls.getLabel());
        }
        Schema.DescribeFieldResult flResV = rtxRetail_Execution_Campaign__c.rtxTarget_Role__c.getDescribe();
        List<Schema.PicklistEntry> plv = flResV.getPicklistValues();
        for (Schema.PicklistEntry plvs : plv){
            plValuesRole.add(plvs.getLabel());
        }

        rtx.rtxArchived__c = FALSE;
        rtx.rtxDescription__c = '';
        rtx.rtxStatus__c = plValuesStatus.get(0);
        update rtx;

        icCTRLCloneRetailExecCampaign.recComponentInit(rtx.Id);
        icCTRLCloneRetailExecCampaign.createREC(rtx, ps, plValuesRole.get(0), plValuesStatus.get(0));


    }


}