/**
 * Created by François Poirier on 2/14/2018.
 */

public with sharing class icREPOMarketList implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        ML__c getMarketListById(Id mlId);
        List<ML__c> getMarketListsByIds(List<Id> mlIds);
        void saveMaketList(ML__c ml);
    }

    public class Impl implements IClass {

        public ML__c getMarketListById(Id mlId){

            ML__c ml = new ML__c();
            List<ML__c> mls = new List<ML__c>();

            mls = [
                    SELECT  id,
                            rtxFiltersLogic__c,
                            rtxJSON_Filters__c
                    FROM    ML__c
                    WHERE   id =: mlId
            ];

            if(mls.size()>0){
                ml = mls[0];
            }

            return ml;
        }

        public List<ML__c> getMarketListsByIds(List<Id> mlIds){

            List<ML__c> mls = new List<ML__c>();

            mls = [
                    SELECT  id,
                            rtxFiltersLogic__c,
                            rtxJSON_Filters__c
                    FROM    ML__c
                    WHERE   id =: mlIds
            ];

            return mls;
        }

        public void saveMaketList(ML__c ml){
            upsert ml;
        }
    }
}