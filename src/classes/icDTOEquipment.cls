/**
 * Created by Andrea Pissinis on 2018-03-15.
 */

global with sharing class icDTOEquipment {

    @AuraEnabled global List<icDTOfieldSet> filterFieldSet {get;set;}
    @AuraEnabled global List<icDTOfieldSet> tableFieldSet {get;set;}
    @AuraEnabled global List<icDTOGenericRecord> records {get;set;}
}