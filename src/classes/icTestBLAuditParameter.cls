/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestBLAuditParameter {

    private static icTestHelperAuditParameter apCreator = new icTestHelperAuditParameter();
    private static icTestHelperAccount accCreator = new icTestHelperAccount();
    private static icTestHelperStoreVisit svCreator = new icTestHelperStoreVisit();

    static StoreVisit__c initTest(){
        Account a = accCreator.createAccount();
        AuditParameters__c p = apCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c pd = apCreator.createAuditParameterDetail(p);
        StoreVisit__c s = svCreator.createStoreVisit(a);
        return s;
    }


    static testMethod void testGetAuditRole(){
        initTest();

        icBLAuditParameter.IClass apBL = (icBLAuditParameter.IClass) icObjectFactory.getsingletoninstance('icBLAuditParameter');
        apBL.getAuditRole();

    }

}