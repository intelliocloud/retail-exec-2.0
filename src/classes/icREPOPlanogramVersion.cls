/**
 * Created by Francois Poirier on 2018-11-12.
 */

public with sharing class icREPOPlanogramVersion implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {
        List<rtxPlanogram_Version__c> getCurrentPlanogramVersionsByPlanogramIds(List<Id> planogramIds);
        List<rtxPlanogram_Version__c> getPlanogramVersionNotInListAndByPlanogramIds(List<Id> planogramIds, List<Id> planogramVersionIds);
    }

    public class Impl implements IClass {

        public List<rtxPlanogram_Version__c> getCurrentPlanogramVersionsByPlanogramIds(List<Id> planogramIds){

            List<rtxPlanogram_Version__c> pvList = new List<rtxPlanogram_Version__c>();
            pvList = [
                    SELECT  Id,
                            Planogram__c
                    FROM    rtxPlanogram_Version__c
                    WHERE   Planogram__c IN :planogramIds AND Current__c = TRUE ORDER BY CreatedDate desc LIMIT 50000];

            return pvList;

        }

        public List<rtxPlanogram_Version__c> getPlanogramVersionNotInListAndByPlanogramIds(List<Id> planogramIds, List<Id> planogramVersionIds){

            List<rtxPlanogram_Version__c> planogramVersions = new List<rtxPlanogram_Version__c>();
            planogramVersions = [
                    SELECT  Id,
                            Planogram__c,
                            Start_Date__c,
                            End_Date__c
                    FROM    rtxPlanogram_Version__c
                    WHERE   Id NOT IN : planogramVersionIds
                    AND     Planogram__c IN : planogramIds
            ];

            return planogramVersions;
        }
    }

}