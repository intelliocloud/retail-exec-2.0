/**
 * Created by georg on 2018-02-26.
 */

public with sharing class icBLAuditParameterDetail implements icIClass {

    public Object GetInstance() {
        return new Impl();
    }

    public Interface IClass {
        Map<String, icDTOAuditParameterDetail> getDTOAPDForActiveAP();
    }

    public class Impl implements IClass {
        private icMAPAuditParameterDetail.IClass apdMap = (icMAPAuditParameterDetail.IClass) icObjectFactory.GetSingletonInstance('icMAPAuditParameterDetail');
        private icREPOAuditParameterDetail.IClass apdRepo = (icREPOAuditParameterDetail.IClass) icObjectFactory.GetSingletonInstance('icREPOAuditParameterDetail');

        public Map<String, icDTOAuditParameterDetail> getDTOAPDForActiveAP() {
            Map<String, icDTOAuditParameterDetail> result = new Map<String, icDTOAuditParameterDetail>();
            List<Audit_Parameter_Detail__c> queryResult = apdRepo.returnAPDWithFullAUActiveList();
            List<icDTOAuditParameterDetail> queryIntoDto = apdMap.listToDTO(queryResult);

            for (icDTOAuditParameterDetail dtoApdTmp : queryIntoDto){
                result.put(dtoApdTmp.userAuditProfile, dtoApdTmp);
            }

            return result;
        }

    }
}