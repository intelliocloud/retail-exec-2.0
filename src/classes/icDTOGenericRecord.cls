/**
 * Created by François Poirier on 12/22/2017.
 */

global with sharing class icDTOGenericRecord {

    @AuraEnabled global String id {get;set;}
    @AuraEnabled global List<icDTOGenericField> fields {get;set;}

}