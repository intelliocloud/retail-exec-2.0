/**
 * Created by François Poirier on 1/22/2018.
 */

public with sharing class icMAPauditTask implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOAuditTask mapToDTO(AuditTask__c auditTask);
        List<icDTOAuditTask> mapToDTO(List<AuditTask__c> auditTasks);
        AuditTask__c mapToAuditTask(icDTOAuditTask dtoAuditTask);
        List<AuditTask__c> mapToAuditTask(List<icDTOAuditTask> dtoAuditTasks);

    }

    public class Impl implements IClass {

        public icDTOAuditTask mapToDTO(AuditTask__c auditTask){

            icDTOAuditTask dto = new icDTOAuditTask();
            dto.id = auditTask.Id;
            dto.name = auditTask.Name;
            dto.comment = auditTask.rtxComment__c;
            dto.done = auditTask.rtxDone__c;
            dto.priority = auditTask.rtxPriority__c;
            dto.answerType = auditTask.Priority_Task__r.rtxAnswerType__c;
            dto.stringChoices = auditTask.Priority_Task__r.rtxChoices__c;
            dto.value = auditTask.rtxValue__c;
            dto.mustDo = auditTask.rtxMustDo__c;

            return dto;

        }

        public List<icDTOAuditTask> mapToDTO(List<AuditTask__c> auditTasks){

            List<icDTOAuditTask> dtos = new List<icDTOAuditTask>();

            for(AuditTask__c a : auditTasks){

                icDTOAuditTask dto = new icDTOAuditTask();
                dto = mapToDTO(a);
                dtos.add(dto);
            }

            return dtos;
        }

        public AuditTask__c mapToAuditTask(icDTOAuditTask dtoAuditTask){

            AuditTask__c at = new AuditTask__c();

            at.Id = dtoAuditTask.id;
            at.Name = dtoAuditTask.name;
            at.rtxComment__c = dtoAuditTask.comment;
            at.rtxDone__c = dtoAuditTask.done;
            if(String.isNotBlank(dtoAuditTask.value)) {
                at.rtxValue__c = dtoAuditTask.value;
            }

            return at;
        }

        public List<AuditTask__c> mapToAuditTask(List<icDTOAuditTask> dtoAuditTasks){

            List<AuditTask__c> auditTasks = new List<AuditTask__c>();

            for(icDTOAuditTask dtoAuditTask : dtoAuditTasks){
                AuditTask__c at = new AuditTask__c();
                at = mapToAuditTask(dtoAuditTask);
                auditTasks.add(at);
            }

            return auditTasks;
        }

    }

}