/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTeststoreVisitSchedule {

    private static icTestHelperAccount accountcreator = new icTestHelperAccount();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperAuditParameter aP = new icTestHelperAuditParameter();


    public static testMethod void testGetVisitSchedule(){

        Account a = accountcreator.createAccount();
        ap.createAuditParameterDetail(ap.createActiveAuditParameter());
        StoreVisit__c s = storeVisitCreator.createStoreVisit(a);

        icBLstoreVisitSchedule.IClass tap = (icBLstoreVisitSchedule.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisitSchedule');
        tap.getVisitSchedule('');


    }


}