/**
 * Created by Andrea Pissinis on 2018-03-15.
 */
@isTest
public with sharing class icTestDTOEquipment {

    public static testMethod void testdtoequipment(){
        icDTOEquipment dtoEq = new icDTOEquipment();
        System.assertEquals(null, dtoEq.filterFieldSet);
        System.assertEquals(null, dtoEq.tableFieldSet);
        System.assertEquals(null, dtoEq.records);

    }

    public static testMethod void testDTOSection(){
        icDTOSection dtoSect = new icDTOSection();
        System.assertEquals(null, dtoSect.filterFieldSet);
        System.assertEquals(null, dtoSect.tableFieldSet);
        System.assertEquals(null, dtoSect.records);

    }
}