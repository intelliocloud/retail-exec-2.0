/**
 * Created by Francois Poirier on 2018-11-12.
 */

public with sharing class icBLPlanogramVersion  implements icIClass {
    public Object GetInstance() {
        return new Impl();
    }

    public Interface IClass {
        List<rtxPlanogram_Version__c> getCurrentPlanogramVersionsByPlanogramIds(List<Id> planogramIds);
        List<rtxPlanogram_Version__c> getPlanogramVersionNotInListAndByPlanogramIds(List<Id> planogramIds, List<Id> planogramVersionIds);
    }

    public class Impl implements IClass {

        private icREPOPlanogramVersion.IClass pvRepo = (icREPOPlanogramVersion.IClass) icObjectFactory.GetSingletonInstance('icREPOPlanogramVersion');

        public List<rtxPlanogram_Version__c> getCurrentPlanogramVersionsByPlanogramIds(List<Id> planogramIds) {


            return pvRepo.getCurrentPlanogramVersionsByPlanogramIds(planogramIds);

        }

        public List<rtxPlanogram_Version__c> getPlanogramVersionNotInListAndByPlanogramIds(List<Id> planogramIds, List<Id> planogramVersionIds){

            return pvRepo.getPlanogramVersionNotInListAndByPlanogramIds(planogramIds, planogramVersionIds);

        }

    }
}