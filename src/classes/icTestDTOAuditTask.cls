/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOAuditTask {

    static testMethod void testdto(){
        icDTOAuditTask dto = new icDTOAuditTask();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.Name);
        System.assertEquals(null, dto.comment);
        System.assertEquals(null, dto.done);
        System.assertEquals(null, dto.priority);
        System.assertEquals(null, dto.value);
        System.assertEquals(null, dto.answerType);
        System.assertEquals(null, dto.stringChoices);

    }
}