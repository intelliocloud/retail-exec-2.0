/**
 * Created by François Poirier on 1/30/2018.
 */

public with sharing class icDTOlistViewWhere {

    public String field {get;set;}
    public String operator {get;set;}
    public List<String> values {get;set;}

}