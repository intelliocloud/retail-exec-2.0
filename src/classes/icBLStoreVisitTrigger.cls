/**
 * Created by François Poirier on 9/19/2017.
 */

public with sharing class icBLStoreVisitTrigger implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        void onBeforeInsert(List<StoreVisit__c> newStoreVisits);
        void onAfterInsert(List<StoreVisit__c> newStoreVisits);
        void onBeforeUpdate(List<StoreVisit__c> newStoreVisits);
        void onAfterUpdate(List<StoreVisit__c> newStoreVisits);
    }

    public class Impl implements IClass {

        private icBLauditTasks.IClass atBL = (icBLauditTasks.IClass) icObjectFactory.GetSingletonInstance('icBLauditTasks');
        private icBLauditProduct.IClass apBL = (icBLauditProduct.IClass) icObjectFactory.GetSingletonInstance('icBLauditProduct');
        private icBLAuditParameter.IClass acBL = (icBLAuditParameter.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameter');
        private icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
        private icBLstoreVisit.IClass visitBL = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisit');
        private icBLaccount.IClass accountBL = (icBLaccount.IClass) icObjectFactory.GetSingletonInstance('icBLaccount');
        private icBLEvent.IClass eventBL = (icBLEvent.IClass) icObjectFactory.GetSingletonInstance('icBLEvent');

        public void onBeforeInsert(List<StoreVisit__c> newStoreVisits) {

            List<Id> accountIds = new List<Id>();

            for(StoreVisit__c sv : newStoreVisits){
                accountIds.add(sv.Store__c);
            }


            List<StoreVisit__c> stvisits = new List<StoreVisit__c>();
            stvisits = visitBL.getCompletedVisitsByAccountIds(accountIds);

            this.setTimeZoneOffset(newStoreVisits);
            icDTOAuditParameterDetail params = new icDTOAuditParameterDetail();
            params = utilBL.getAuditParameters();

            for (StoreVisit__c sv : newStoreVisits) {
                sv.Name = sv.rtxVisitSubject__c.replaceFirst(':\\d\\dZ', '');
                system.debug('sv.Name ===> ' + sv.Name);
                sv.rtxAudit_Code__c = params.visitAuditCode;

                if (!sv.rtxCompleted__c){

                    if (!stvisits.isEmpty()){

                        StoreVisit__c target;
                        StoreVisit__c myTarget;
                        DateTime targetdate = DateTime.newInstance(0,0,0,0,0,0);
                        DateTime myTargetdate = DateTime.newInstance(0,0,0,0,0,0);

                        for (StoreVisit__c s : stvisits){

                            if (s.Store__c == sv.Store__c){
                                DateTime temp = s.rtxEnd__c;
                                if (temp > targetdate) {
                                    targetdate = temp;
                                    target = s;
                                }
                                if (s.OwnerID == sv.OwnerId){
                                    DateTime myTemp = s.rtxEnd__c;
                                    if (myTemp > myTargetdate) {
                                        myTargetdate = myTemp;
                                        myTarget = s;
                                    }
                                }
                            }
                        }

                        if (target != null) sv.LastVisit__c = target.Id;
                        if (myTarget != null) sv.MyLastVisit__c = myTarget.Id;

                    }
                    List<StoreVisit__c> todaysStoreVisits = new List<StoreVisit__c>();
                    todaysStoreVisits= visitBL.retrieveStoreVisitsToActivate(newStoreVisits);

                    if (todaysStoreVisits.contains(sv)) {
                        sv.rtxLastActivated__c = Datetime.now();
                    }
                }
            }
            this.setDefaultDuration(newStoreVisits);
        }

        public void onAfterInsert(List<StoreVisit__c> newStoreVisits) {
            List<StoreVisit__c> todaysStoreVisits = new List<StoreVisit__c>();
            List<Event> eventsToCreate = new List<Event>();

            todaysStoreVisits= visitBL.retrieveStoreVisitsToActivate(newStoreVisits);
            if (!todaysStoreVisits.isEmpty()) {
                atBL.createAuditTasks(todaysStoreVisits, acBL.getAuditRole());
                apBL.createAuditProducts(todaysStoreVisits);
            }

            for (StoreVisit__c s : newStoreVisits){
                OrgCallSettings__c companySettings = OrgCallSettings__c.getInstance();
                if (companySettings.rtxStandardEventSync__c == TRUE) {

                    Event visitEvent = new Event();
                    Id shadowEventrecordtypeId = Schema.SObjectType.Event.getRecordTypeInfosByName().get('ShadowStoreVisitEvent').getRecordTypeId();
                    visitEvent.Subject = s.Name;
                    visitEvent.WhatId = s.Store__c;
                    visitEvent.OwnerId = s.OwnerId;
                    visitEvent.StartDateTime = s.rtxDate__c;
                    visitEvent.EndDateTime = s.rtxEnd__c;
                    visitEvent.Status__c = s.rtxVisitStatus__c;
                    visitEvent.rtxStoreVisit__c = s.Id;
                    visitEvent.Id = s.rtxEvent__c;
                    visitEvent.RecordTypeId = shadowEventrecordtypeId;


                    eventsToCreate.add(visitEvent);
                }
            }

            if (!eventsToCreate.isEmpty()){
                eventBL.saveEvents(eventsToCreate);
            }
        }

        public void onBeforeUpdate(List<StoreVisit__c> newStoreVisits) {


            List<Id> newStoreVisitStoreIds = new List<Id>();
            List<Id> newStoreVisitIds = new List<Id>();

            for(StoreVisit__c s : newStoreVisits){
                newStoreVisitStoreIds.add(s.Store__c);
                newStoreVisitIds.add(s.Id);
            }


            List<StoreVisit__c> stvisits = new List<StoreVisit__c>();
            stvisits = visitBL.getCompletedVisitsByAccountIds(newStoreVisitStoreIds);

            List<Account> stores = new List<Account>();
            stores = accountBL.getAccountsByIds(newStoreVisitStoreIds);

            List<AuditTask__c> auditTasks = new List<AuditTask__c>();
            auditTasks = atBL.getNotDoneTasksByVisitIds(newStoreVisitIds);

            List<AuditProduct__c> auditProducts = new List<AuditProduct__c>();
            auditProducts = apBL.getNotDoneAuditProductByVisitIds(newStoreVisitIds);

            List<StoreVisit__c> storeVisitsToCreateNew = new List<StoreVisit__c>();
            List<Account> storesToBeUpdated = new List<Account>();
            List<AuditProduct__c> productsToBeDeleted = new List<AuditProduct__c>();
            List<AuditTask__c> tasksToBeDeleted = new List<AuditTask__c>();
            this.setTimeZoneOffset(newStoreVisits);

            for (StoreVisit__c sv : newStoreVisits) {
                sv.Name = sv.rtxVisitSubject__c.replaceFirst(':\\d\\dZ', '');
                system.debug('sv.Name ===> ' + sv.Name);
                StoreVisit__c oldStoreVisit = (StoreVisit__c)Trigger.oldMap.get(sv.Id);

                if (oldStoreVisit.rtxVisitStatus__c != null){

                    if ( (!oldStoreVisit.rtxVisitStatus__c.equals( 'Completed' )) && sv.rtxVisitStatus__c.equals( 'Completed' )){
                        sv.rtxCompleted__c = TRUE;
                        sv.rtxEnd__c = System.NOW();
                    }
                }

                if (!sv.rtxCompleted__c){
                    if (!stvisits.isEmpty()){

                        StoreVisit__c target;
                        StoreVisit__c myTarget;
                        DateTime targetdate = DateTime.newInstance(0,0,0,0,0,0);
                        DateTime myTargetdate = DateTime.newInstance(0,0,0,0,0,0);

                        for (StoreVisit__c s : stvisits){

                            if (s.Store__c == sv.Store__c){

                                DateTime temp = s.rtxEnd__c;
                                if (temp > targetdate) {
                                    targetdate = temp;
                                    target = s;
                                }

                                if (s.OwnerID == sv.OwnerId){
                                    DateTime myTemp = s.rtxEnd__c;
                                    if (myTemp > myTargetdate) {
                                        myTargetdate = myTemp;
                                        myTarget = s;
                                    }
                                }
                            }
                        }

                        if (target != null) sv.LastVisit__c = target.Id;
                        if (myTarget != null) {
                            sv.MyLastVisit__c = myTarget.Id;
                        }else{
                            sv.MyLastVisit__c = null;
                        }

                    }

                    if (oldStoreVisit.rtxDate__c != sv.rtxDate__c){
                        this.setDefaultDuration(newStoreVisits);
                        if (sv.rtxDate__c.date() == DateTime.now().date()) {
                            sv.rtxLastActivated__c = Datetime.now();
                        }
                    }
                    if (oldStoreVisit.rtxDate__c.date() == DateTime.now().date() && sv.rtxDate__c.date() != DateTime.now().date()){
                        if (!sv.rtxCompleted__c && sv.rtxLastActivated__c != null){
                            sv.rtxLastActivated__c = null;
                        }
                    }

                } else {
                    if (!oldStoreVisit.rtxCompleted__c) {
                        if (sv.rtxNextVisitDate__c != null) {
                            StoreVisit__c newStV = new StoreVisit__c();
                            newStV.Name = 'set by formula rule';
                            newStV.OwnerId = sv.OwnerId;
                            newStV.Store__c = sv.Store__c;
                            newStV.rtxDate__c = sv.rtxNextVisitDate__c;
                            storeVisitsToCreateNew.add(newStV);
                        }
                        for (Account a : stores) {
                            if (a.Id == sv.Store__c) {
                                a.rtxLastVisitDate__c = sv.rtxDate__c;
                                storesToBeUpdated.add(a);
                            }
                        }

                        OrgCallSettings__c companySettings = OrgCallSettings__c.getInstance();
                        if (companySettings.DeleteAuditProducts__c) {
                            for (AuditProduct__c aP : auditProducts) {
                                if (ap.StoreVisit__c == sv.Id) {
                                    productsToBeDeleted.add(aP);
                                }
                            }
                        }
                        if(companySettings.DeleteAuditTasks__c){
                            for (AuditTask__c aT : auditTasks) {
                                if (aT.Store_Visit__c == sv.Id) {
                                    tasksToBeDeleted.add(aT);
                                }
                            }
                        }
                    }

                    if (oldStoreVisit.rtxRead_Only__c == TRUE && sv.rtxRead_Only__c == TRUE) {
                        List<String> changedFields = new List<String>();
                        List<Schema.FieldSetMember> fieldsAcceptedToBeChanged = Schema.SObjectType.StoreVisit__c.fieldSets.getMap().get('ReadOnlyException').getFields();
                        List<String> fieldsallowed = new List<String>();
                        List<Schema.SObjectField> fieldLists = Schema.getGlobalDescribe().get('StoreVisit__c').getDescribe().fields.getMap().values();
                        sObject oldStoreVisitObject = Trigger.oldMap.get(sv.Id);
                        sObject newStoreVisitObject = Trigger.newMap.get(sv.Id);

                        for (Schema.sObjectField field : fieldLists) {
                            Object oldV = oldStoreVisitObject.get(field);
                            Object newV = newStoreVisitObject.get(field);
                            if (oldV != newV) {
                                changedFields.add(field.getDescribe().getLabel());
                            }
                        }

                        for (FieldSetMember m : fieldsAcceptedToBeChanged) {
                            fieldsallowed.add(m.getLabel());
                        }

                        Integer flag = 0;
                        for (String field : changedFields) {
                            if (!fieldsallowed.contains(field)) {
                                flag++;
                            }
                        }
                        if (flag != 0) {
                            sv.addError(Label.Read_Only_Trigger_Warning);
                        }

                    }
                }

            }
            if(!storeVisitsToCreateNew.isEmpty()){
                visitBL.saveStoreVisits(storeVisitsToCreateNew);
            }
            if(!storesToBeUpdated.isEmpty()){
                accountBL.saveAccounts(storesToBeUpdated);
            }
            if(!productsToBeDeleted.isEmpty()){
                apBL.deleteAuditProducts(productsToBeDeleted);
            }
            if(!tasksToBeDeleted.isEmpty()){
                atBL.deleteAuditTasks(tasksToBeDeleted);
            }
        }

        public void onAfterUpdate(List<StoreVisit__c> newStoreVisits) {

            List<StoreVisit__c> todaysStoreVisits = new List<StoreVisit__c>();
            List<StoreVisit__c> changedDateStoreVisits = new List<StoreVisit__c>();
            List<StoreVisit__c> toDeactivateStoreVisits = new List<StoreVisit__c>();

            List<Id> storeVisitIds = new List<Id>();
            for(StoreVisit__c sv : newStoreVisits){
                storeVisitIds.add(sv.Id);
            }

            List<Event> events = new List<Event>();
            events = eventBL.getEventsByVisitIds(storeVisitIds);

            List<Event> eventsToUpdate = new List<Event>();

            for (StoreVisit__c svc : newStoreVisits){
                StoreVisit__c oldStoreVisit = (StoreVisit__c)Trigger.oldMap.get(svc.Id);
                if (oldStoreVisit.rtxDate__c.date() != DateTime.now().date() && svc.rtxDate__c.date() == DateTime.now().date()){
                    changedDateStoreVisits.add(svc);
                }
                if (oldStoreVisit.rtxDate__c.date() == DateTime.now().date() && svc.rtxDate__c.date() != DateTime.now().date()){
                    if (!svc.rtxCompleted__c && oldStoreVisit.rtxLastActivated__c != null){
                        toDeactivateStoreVisits.add(svc);
                    }
                }
                if ((oldStoreVisit.rtxDate__c != svc.rtxDate__c)|(oldStoreVisit.rtxEnd__c != svc.rtxEnd__c)|(oldStoreVisit.rtxVisitStatus__c != svc.rtxVisitStatus__c)|(oldStoreVisit.Store__c != svc.Store__c)|(oldStoreVisit.OwnerId != svc.OwnerId)){
                    for (Event e : events){
                        if (e.rtxStoreVisit__c == svc.Id){
                            e.Subject = svc.Name;
                            e.OwnerId = svc.OwnerId;
                            e.StartDateTime = svc.rtxDate__c;
                            e.EndDateTime = svc.rtxEnd__c;
                            e.Status__c = svc.rtxVisitStatus__c;
                            e.WhatId = svc.Store__c;

                            eventsToUpdate.add(e);
                        }
                    }
                }
            }

            if(!changedDateStoreVisits.isEmpty()){
                todaysStoreVisits= visitBL.retrieveStoreVisitsToActivate(changedDateStoreVisits);
            }

            if (!todaysStoreVisits.isEmpty()) {
                atBL.createAuditTasks(todaysStoreVisits, acBL.getAuditRole());
                apBL.createAuditProducts(todaysStoreVisits);
            }

            if (!toDeactivateStoreVisits.isEmpty()) {
                atBL.deleteAuditTasksByStoreVisits(toDeactivateStoreVisits);
                apBL.deleteAuditProductsByStoreVisits(toDeactivateStoreVisits);
            }
            if (! eventsToUpdate.isEmpty()){
                eventBL.saveEvents(eventsToUpdate);
            }

        }

        private void setDefaultDuration(List<StoreVisit__c> newStoreVisits) {

            OrgCallSettings__c companySettings = OrgCallSettings__c.getInstance();
            Decimal defaultDurationInMinutes = companySettings.Visit_Duration_Default_Value__c;
            for (StoreVisit__c s : newStoreVisits) {
                s.rtxEnd__c = s.rtxDate__c + (defaultDurationInMinutes / 60 / 24);
            }
        }

        private void setTimeZoneOffset(List<StoreVisit__c> newStoreVisits){
            TimeZone tz;
            tz = UserInfo.getTimeZone();
            for(StoreVisit__c sv : newStoreVisits){
                Integer offset = tz.getOffset(sv.rtxDate__c);
                sv.rtxTimeZoneOffset__c = offset;
            }
        }

    }
}