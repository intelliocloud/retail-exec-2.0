/**
 * Created by georg on 2018-02-15.
 */

public with sharing class icCTRLCloneRetailExecCampaign {
    @AuraEnabled
    public static icDTOCloneRetailExecCampaign recComponentInit(Id recId){
        icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        icBLutilities.IClass UtilBL = (icBLutilities.IClass) icObjectFactory.getSingletonInstance('icBLUtilities');

        icDTOCloneRetailExecCampaign result = new icDTOCloneRetailExecCampaign();
        List<String> recFields = new List<String>{'rtxArchived__c',
                'rtxCampaign_ID__c',
                'Name',
                'rtxCampaign_Priority__c',
                'CreatedById',
                'rtxCurrent__c',
                'rtxDescription__c',
                'rtxEnd_Date__c',
                'rtxInitiative__c',
                'LastModifiedById',
                'rtxLast_Refreshed__c',
                'rtxMarket_List__c',
                'rtxPromotion__c',
                'rtxStart_Date__c',
                'rtxStatus__c',
                'rtxTarget_Role__c'};

        List<String> priorityFields = new List<String>{'rtxAnswerType__c',
                'rtxCategory__c',
                'rtxChoices__c',
                'CreatedById',
                'rtxDescription__c',
                'rtxFrequency__c',
                'LastModifiedById',
                'rtxMustDo__c',
                'rtxPriority__c',
                'rtxPriority_ID__c',
                'Name',
                'rtxRetail_Execution_Campaign__c',
                'rtxSequence__c',
                'rtxTime_Estimate__c'};

        List<String> criterias = new List<String>();
        criterias.add('Id = \'' + recId + '\'');

        result.recRecord = (rtxRetail_Execution_Campaign__c)genericRepo.getValues('rtxRetail_Execution_Campaign__c', recFields, criterias, new List<String>() ,1)[0];
        result.targetRoleOptions = UtilBL.getPickListValues('rtxRetail_Execution_Campaign__c', 'rtxTarget_Role__c');
        result.statusOptions = UtilBL.getPickListValues('rtxRetail_Execution_Campaign__c', 'rtxStatus__c');

        criterias = new List<String>();
        criterias.add('rtxRetail_Execution_Campaign__c = \'' + recId + '\'');
        result.priorityChildren = (List<Priority__c>)genericRepo.getValues('Priority__c', priorityFields, criterias, new List<String>() ,10000);

        for (icDTOpickList option : result.targetRoleOptions){
            if(option.label == result.recRecord.rtxTarget_Role__c){
                option.isSelected = true;
            }
        }
        for (icDTOpickList option : result.statusOptions){
            if(option.label == result.recRecord.rtxStatus__c){
                option.isSelected = true;
            }
        }
        return result;
    }

    @AuraEnabled
    public static void createREC(rtxRetail_Execution_Campaign__c newRec, List<Priority__c> priorityList,
            String targetRoleOption, String statusOption ){

        System.debug('targetRoleOptions: '+targetRoleOption +'-statusOptions: '+statusOption);
        rtxRetail_Execution_Campaign__c theNewRecord = new rtxRetail_Execution_Campaign__c(
                rtxArchived__c = newRec.rtxArchived__c,
                Name=newRec.Name,
                rtxCampaign_Priority__c=newRec.rtxCampaign_Priority__c,
                rtxDescription__c=newRec.rtxDescription__c,
                rtxEnd_Date__c=newRec.rtxEnd_Date__c,
                rtxInitiative__c=newRec.rtxInitiative__c,
                rtxLast_Refreshed__c=newRec.rtxLast_Refreshed__c,
                rtxMarket_List__c=newRec.rtxMarket_List__c,
                rtxPromotion__c=newRec.rtxPromotion__c,
                rtxStart_Date__c=newRec.rtxStart_Date__c,
                rtxStatus__c = statusOption,
                rtxTarget_Role__c = targetRoleOption
        );

        insert theNewRecord;

        List<Priority__c> listNewPriorities = new List<Priority__c>();

        for (Priority__c priority : priorityList) {
            Priority__c newPriority = new Priority__c();
            newPriority.rtxAnswerType__c=priority.rtxAnswerType__c;
            newPriority.rtxCategory__c = priority.rtxCategory__c;
            newPriority.rtxChoices__c=priority.rtxChoices__c;
            newPriority.rtxDescription__c=priority.rtxDescription__c;
            newPriority.rtxFrequency__c=priority.rtxFrequency__c;
            newPriority.rtxMustDo__c=priority.rtxMustDo__c;
            newPriority.rtxPriority__c=priority.rtxPriority__c;
            newPriority.Name=priority.Name;
            newPriority.rtxRetail_Execution_Campaign__c=theNewRecord.Id;
            newPriority.rtxSequence__c=priority.rtxSequence__c;

            listNewPriorities.add(newPriority);
        }
        insert listNewPriorities;
    }
}