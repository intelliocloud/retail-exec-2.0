/**
 * Created by François Poirier on 12/11/2017.
 */

public with sharing class icBLMarketListAccount implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds);
        Map<Id, List<Id>> generateMLAMap(List<Market_List_Account__c> mla);
        void createMarketListAccounts(List<ML__c> mls);
        }

    public class Impl implements IClass {

        private icREPOMarketListAccount.IClass mlaRepo = (icREPOMarketListAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOMarketListAccount');

        public List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds){

            return mlaRepo.getMarketListAccountsByMarketIds(marketIds);

        }

        public Map<Id, List<Id>> generateMLAMap(List<Market_List_Account__c> mla) {

            Map<Id, List<Id>> mlaMap = new Map<Id, List<Id>>();

            for (Market_List_Account__c m : mla) {

                if (!mlaMap.containsKey(m.marketList__c)) {
                    mlaMap.put(m.marketList__c, new List<Id>());
                }

                mlaMap.get(m.marketList__c).add(m.account__c);
            }

            return mlaMap;
        }

        public void createMarketListAccounts(List<ML__c> mls){

            //In order to get rid of the 10k records limit we moved the creation logic to a batch.
            //Since we can now have over 10k records, we also need to delete them in batches.
            //this batch will delete all automatic accounts and will then launch the market list account creation batch

            for(ML__c ml : mls) {
                Database.executeBatch(new icBatchDeleteMarketListAccounts(ml.Id));
            }


        }

    }

}