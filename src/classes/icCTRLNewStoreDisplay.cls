/**
 * Created by Andrea Pissinis on 2018-03-19.
 */

global class icCTRLNewStoreDisplay {

    @AuraEnabled
    global static icDTOEquipment getEquipments(Id accountId) {

        icBLFieldSetManager.IClass fieldSetBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
        icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMAPfieldSet');
        icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');
        icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

        icDTOEquipment eq = new icDTOEquipment();

        List<String> criterias = new List<String>();
        String crit = '\'Standard\'';
        criterias.add('rtxTarget_Audience__c = ' + crit );
        criterias.add('AND rtxActive__c = TRUE');

        List<String> apFields = fieldSetBL.getAPINames('rtxEquipment__c', 'temporaryfieldset');
        List<String> orderBy = new List<String>();
        orderBy.add('Name ASC');
        List<rtxEquipment__c> equipmentsL = (List<rtxEquipment__c>) genericRepo.getValues('rtxEquipment__c', apFields, criterias,
                orderBy, 50000);

        List<icDTOfieldSet> tableFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('rtxEquipment__c','temporaryfieldset'));
        system.debug('tablefieldSet ===> ' + tableFieldSet);
        eq.records = recordMap.mapToDTO(tableFieldSet, equipmentsL, 'rtxEquipment__c');
        eq.filterFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('rtxEquipment__c', 'temporaryfieldset'));
        eq.tableFieldSet = tableFieldSet;

        system.debug('dtoEquipment ===> ' + eq);

        return eq;
    }


    @AuraEnabled
    global static icDTOSection getMasterDisplays(Id accountId) {

        icBLFieldSetManager.IClass fieldSetBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
        icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMAPfieldSet');
        icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');
        icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

        icDTOSection sec = new icDTOSection();

        Account parentId = [SELECT ParentId FROM Account WHERE Id= :accountId];

        if (parentId.ParentId != null){

            List<String> criterias = new List<String>();
            criterias.add('Account__c = \'' + parentId.ParentId + '\' ');
            criterias.add('AND rtxCurrent__c = TRUE');
            List<String> apFields = fieldSetBL.getAPINames('Section__c', 'temporaryfieldset');
            List<String> orderBy = new List<String>();
            orderBy.add('Name ASC');
            List<Section__c> sectionsL = (List<Section__c>) genericRepo.getValues('Section__c', apFields, criterias,
                    orderBy, 50000);

            List<icDTOfieldSet> tableFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('Section__c','temporaryfieldset'));
            system.debug('tablefieldSet ===> ' + tableFieldSet);

            if (!sectionsL.isEmpty()) {
                sec.records = recordMap.mapToDTO(tableFieldSet, sectionsL, 'Section__c');
                sec.filterFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('Section__c', 'temporaryfieldset'));
                sec.tableFieldSet = tableFieldSet;

                system.debug('dtoSection ===> ' + sec);

                return sec;
            } else return null;



        }else return null;

    }

    @AuraEnabled
    global static void createDisplays(Id accountId, List<Id> eqs){

        List<Section__c> sectionsToCreate = new List<Section__c>();
        Id presentoirModele = [SELECT Id FROM RecordType WHERE SobjectType = 'Section__c' and Name = 'Master Display' LIMIT 1].Id;
        Id presentoirMagasin = [SELECT Id FROM RecordType WHERE SobjectType = 'Section__c' and Name = 'Store Display' LIMIT 1].Id;

        if (eqs.get(0).getSobjectType() == rtxEquipment__c.SObjectType ){

            List<rtxEquipment__c> checkingExistance = [SELECT Id, Name, rtxEquipment_Type__c FROM rtxEquipment__c WHERE Id IN :eqs];

            for (Id idEq : eqs){

                rtxEquipment__c eqTarget;

                for (rtxEquipment__c equipments : checkingExistance){
                    if (equipments.Id == idEq){
                        eqTarget = equipments;
                    }
                }

                Section__c masterDisplay = new Section__c();
                masterDisplay.Name = eqTarget.Name;
                masterDisplay.Account__c = accountId;
                masterDisplay.rtxEquipment__c = idEq;
                masterDisplay.rtxIsActive__c = true;
                masterDisplay.rtxType__c = eqTarget.rtxEquipment_Type__c;

                //Id presentoirModele = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Store Display').getRecordTypeId();

                masterDisplay.RecordTypeId = presentoirModele;

                sectionsToCreate.add(masterDisplay);
            }
        }else if (eqs.get(0).getSobjectType() == Section__c.SObjectType){

            List<Section__c> sectionExisted = [SELECT Id, Name, rtxType__c FROM Section__c WHERE Id IN :eqs];

            for (Id idSect : eqs){

                Section__c secTarget;

                for (Section__c s : sectionExisted){
                    if (s.Id == idSect){
                        secTarget = s;
                    }
                }

                Section__c storeDisplay = new Section__c();
                storeDisplay.Name = secTarget.Name;
                storeDisplay.Account__c = accountId;
                storeDisplay.rtxIsActive__c = true;
                storeDisplay.rtxType__c = secTarget.rtxType__c;
                //Id presentoirMagasin = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Store Display').getRecordTypeId();
                storeDisplay.RecordTypeId = presentoirMagasin;

                sectionsToCreate.add(storeDisplay);

            }


        }

        if (!sectionsToCreate.isEmpty()){
            insert sectionsToCreate;
        }
    }



}