/**
 * Created by François Poirier on 12/8/2017.
 */

public with sharing class icBLauditProduct implements icIClass{

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOAuditProduct getAuditProducts(Id storeVisitId);
        List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts);
        //List<AuditProduct__c> saveAuditProducts(List<Id> ids,String nf, String price,String oos,String exc, String done);
        void createAuditProducts(List<StoreVisit__c> newStoreVisits);
        void deleteAuditProductsByStoreVisits(List<StoreVisit__c> st);
        void deleteAuditProducts(List<AuditProduct__c> auditProducts);
        List<AuditProduct__c> getNotDoneAuditProductByVisitIds(List<Id> visitIds);

    }

    public class Impl implements IClass{
        private icREPOauditProduct.IClass auditProductRepo = (icREPOauditProduct.IClass) icObjectFactory.getSingletonInstance('icREPOauditProduct');
        private icBLFieldSetManager.IClass fieldSetBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
        private icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMAPfieldSet');
        private icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');
        private icREPOauditProduct.IClass apREPO = (icREPOauditProduct.IClass) icObjectFactory.GetSingletonInstance('icREPOauditProduct');
        private icBLutilities.IClass utilBL = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');
        private icDTOAuditParameterDetail auditParameters = utilBL.getAuditParameters();

        public icDTOAuditProduct getAuditProducts(Id storeVisitId){

            icDTOAuditProduct dtoAuditProduct = new icDTOAuditProduct();

            List<String> criterias = new List<String>();
            criterias.add('StoreVisit__c = \'' + storeVisitId + '\'');
            List<String> apFields = fieldSetBL.getAPINames('AuditProduct__c', auditParameters.productAuditFieldSet);
            List<String> apFilterFields = fieldSetBL.getAPINames('AuditProduct__c', auditParameters.productAuditFilterSet);

            for (String s : apFilterFields){
                if (!apFields.contains(s)){
                    apFields.add(s);
                }
            }

            String orderFinal = '';

            List<icDTOfieldSet> tableFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('AuditProduct__c', auditParameters.productAuditFieldSet));

            tableFieldSet.addAll(fieldSetMap.maptoDTOHidden(fieldSetBL.getFields('AuditProduct__c', auditParameters.productAuditFilterSet), tableFieldSet));


            List<String> tableFieldSetLabels = new List<String>();
            for (icDTOfieldSet fS : tableFieldSet){
                tableFieldSetLabels.add(fS.apiName);
            }

            if (tableFieldSetLabels.contains(auditParameters.productAuditSortingField)){
                orderFinal += auditParameters.productAuditSortingField;

                if (auditParameters.productAuditSortingOrder.equals('Ascending')){
                    orderFinal += ' ASC' ;
                }else{
                    orderFinal += ' DESC';
                }
            }

            List<String> orderBy = new List<String>();
            if (orderFinal.length() > 0){
                orderBy.add(orderFinal);
            }else{
                orderBy.add('Name ASC');
            }

            List<AuditProduct__c> auditProducts = (List<AuditProduct__c>) genericRepo.getValues('AuditProduct__c', apFields, criterias,
                    orderBy, 50000);

            dtoAuditProduct.records = recordMap.mapToDTO(tableFieldSet, auditProducts, 'AuditProduct__c');
            dtoAuditProduct.filterFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('AuditProduct__c', auditParameters.productAuditFilterSet));
            dtoAuditProduct.tableFieldSet = tableFieldSet;

            system.debug('dtoAuditProduct ===> ' + dtoAuditProduct);
            return dtoAuditProduct;
            //return auditProductRepo.getAuditProducts(storeVisitId);
        }

        public List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts){

            system.debug('in saveAuditProducts List<AuditProduct__c in BL');
            return auditProductRepo.saveAuditProducts(auditProducts);
        }
        /*
        public List<AuditProduct__c> saveAuditProducts(List<Id> ids,String nf, String price,String oos,String exc, String done){
            List<AuditProduct__c> auditProducts = new List<AuditProduct__c>();
            for(Id auditPID : ids) {
                AuditProduct__c auditProduct = new AuditProduct__c(Id = auditPID);
                if (nf != '') auditProduct.rtxNF__c = Integer.valueOf(nf);
                if (price != '') auditProduct.rtxPrice__c = Decimal.valueOf(price);
                if (oos != '') auditProduct.rtxOOS__c = Boolean.valueOf(oos);
                if (exc != '') auditProduct.rtxException__c = Boolean.valueOf(exc);
                if (done != '') auditProduct.rtxDone__c = Boolean.valueOf(done);

                auditProducts.add(auditProduct);
            }

            return saveAuditProducts(auditProducts);
        }
        */
        public void createAuditProducts(List<StoreVisit__c> newStoreVisits) {

            List<AuditProduct__c> alreadyExistingAuditProducts = apREPO.getExistingAuditProducts();
            List<AuditProduct__c> apList = new List<AuditProduct__c>();
            List<Product__c> productList = new List<Product__c>();
            productList = this.getAuditableProducts();

            for (StoreVisit__c s : newStoreVisits) {

                for (Product__c p : productList) {
                    System.debug('Here!');
                    if(p.UserRecordAccess.HasReadAccess){
                        System.debug('Because User!');
                        Integer flag = 0;
                        for ( AuditProduct__c aPP : alreadyExistingAuditProducts ){
                            if ((aPP.StoreVisit__c == s.Id) && (aPP.Product__c == p.Id)){
                                flag++;
                            }
                        }

                        if (flag == 0){
                            AuditProduct__c ap = new AuditProduct__c();

                            ap.Product__c = p.Id;
                            ap.StoreVisit__c = s.Id;
                            ap.Name = p.Name;

                            apList.add(ap);
                        }

                    }
                }
            }
            insert apList;
        }

        public void deleteAuditProductsByStoreVisits(List<StoreVisit__c> st){
            List<AuditProduct__c> auditProductsNotDone = apREPO.getNotDoneAuditProducts();
            List<AuditProduct__c> auditProductsToDelete = new List<AuditProduct__c>();

            for (StoreVisit__c s : st){
                for (AuditProduct__c a : auditProductsNotDone){
                    if (a.StoreVisit__c == s.Id){
                        auditProductsToDelete.add(a);
                    }
                }
            }

            apREPO.deleteAuditProducts(auditProductsToDelete);

        }

        public List<AuditProduct__c> getNotDoneAuditProductByVisitIds(List<Id> visitIds){

            return apREPO.getNotDoneAuditProductByVisitIds(visitIds);
        }

        public void deleteAuditProducts(List<AuditProduct__c> auditProducts){

            apREPO.deleteAuditProducts(auditProducts);
        }

        private List<Product__c> getAuditableProducts() {

            return apREPO.getAuditableProducts();
        }
    }
}