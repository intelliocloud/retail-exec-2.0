/**
 * Created by Andrea Pissinis on 2018-03-05.
 */
@isTest
public with sharing class icMapAuditTaskScreenMock {

    public icDTOAuditTaskScreen mapToDTO(Map<Id, List<AuditTask__c>> mapCampaingATs){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'mapToDTO');
        params.put('mapCampaingATs', mapCampaingATs);
        return (icDTOAuditTaskScreen) icTestMockUtilities.Tracer.GetReturnValue(this, 'mapToDTO');

    }
}