/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icREPOauditProductMock {

    public List<AuditProduct__c> getAuditProducts(Id storeVisitId){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAuditProducts');
        params.put('storeVisitId', storeVisitId);
        return (List<AuditProduct__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAuditProducts');

    }

    public List<AuditProduct__c> saveAuditProducts(List<AuditProduct__c> auditProducts){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveAuditProducts');
        params.put('auditProducts', auditProducts);
        return (List<AuditProduct__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveAuditProducts');

    }
}