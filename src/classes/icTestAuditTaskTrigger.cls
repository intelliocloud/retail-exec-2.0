/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestAuditTaskTrigger {

    private static icTestHelperAccount accountCreator = new icTestHelperAccount();
    private static icTestHelperAuditParameter parameterCreator = new icTestHelperAuditParameter();
    private static icTestHelperStoreVisit storeVisitCreator = new icTestHelperStoreVisit();
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();
    private static icTestHelperRetailExecutionCampaign retailExecCreator = new icTestHelperRetailExecutionCampaign();
    private static icTestHelperPriority priorityCreator = new icTestHelperPriority();
    private static icTestHelperAuditTask auditTaskCreator = new icTestHelperAuditTask();

    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLAuditTaskTrigger', new icBLAuditTaskTriggerMock());
    }

    public static testMethod void testOnBeforeUpdate(){

        testSetup();
        Account acc = accountCreator.createAccount();
        AuditParameters__c ap = parameterCreator.createActiveAuditParameter();
        Audit_Parameter_Detail__c apd = parameterCreator.createAuditParameterDetail(ap);
        StoreVisit__c s = storeVisitCreator.createStoreVisit(acc);
        ML__c mml = marketListCreator.createActiveML();
        rtxRetail_Execution_Campaign__c rtx = retailExecCreator.createApprovedCurrentMerchandiserCampaign(mml);
        rtxCampaign_Account__c acp = retailExecCreator.createCampaignAccount(rtx, acc);
        Priority__c p = priorityCreator.createAskAlwaysPriority(rtx);
        Priority__c pO = priorityCreator.createAskOnlyOncePriority(rtx);
        AuditTask__c aDone = auditTaskCreator.createAudiTaskDone(acc, p, s);

        aDone.Name = 'testUpdate';
        update aDone;

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLAuditTaskTriggerMock', 'onBeforeUpdate'));

    }
}