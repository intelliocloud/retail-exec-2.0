public without sharing class icActivateVisitManualAction {


    private static icBLstoreVisit.IClass svBL = (icBLstoreVisit.IClass) icObjectFactory.GetSingletonInstance('icBLstoreVisit');

    @AuraEnabled
    public static StoreVisit__c getStoreVisit(Id storevisitId){
        StoreVisit__c storeVisit = new StoreVisit__c();

        storeVisit = svBL.getStoreVisitById(storevisitId);

        if (!storeVisit.rtxCompleted__c && storeVisit.rtxDate__c.date() == DateTime.now().date()){
            return storeVisit;
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static StoreVisit__c updateStoreVisitRecord(StoreVisit__c sc){
        sc.rtxDate__c = System.NOW();
        upsert sc;
        return sc;
    }
}