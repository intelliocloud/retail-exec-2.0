/**
 * Created by François Poirier on 11/29/2017.
 */

@isTest

public with sharing class icTestValidateAccess {
    static icBLValidateAccess.IClass validateAccessBL = (icBLValidateAccess.IClass) icObjectFactory.GetSingletonInstance('icBLValidateAccess');

    @isTest static void testFieldsAreAccessibles(){

        List<String> fields = new List<String>{'Id', 'Name'};
        Boolean result = validateAccessBL.fieldsAreAccessible(fields, 'Account');

        system.assert(result);
    }

    @isTest static void testFieldsAreUpdateables(){

        List<String> fields = new List<String>{'Name'};
        Boolean result = validateAccessBL.fieldsAreUpdateable(fields, 'Account');

        system.assert(result);
    }

    @isTest static void testGetAccessibleFieldSetMembers(){
        List<Schema.FieldSetMember> fields = new List<Schema.FieldSetMember>();

        List<Schema.FieldSetMember> accessibleFields = validateAccessBL.getAccessiblesFieldSetMembers(fields, 'Account');

        system.assertEquals(fields, accessibleFields);
    }

}