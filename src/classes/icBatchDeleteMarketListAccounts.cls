/**
 * Created by Francois Poirier on 2018-11-21.
 */

global with sharing class icBatchDeleteMarketListAccounts implements Database.Batchable<sObject> {

    private icBLMarketList.IClass mlBL = (icBLMarketList.IClass) icObjectFactory.GetSingletonInstance('icBLMarketList');

    String query;
    String marketListId;

    global icBatchDeleteMarketListAccounts(Id mlId){

        query = 'SELECT  Id FROM Market_List_Account__c WHERE marketList__c = \'' + mlId + '\' AND rtxSelection_Type__c = \'Automatic\'';
        marketListId = mlId;

    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        delete scope;

    }

    global void finish(Database.BatchableContext bc){

        Database.executeBatch(new icBatchMarketListAccounts(marketListId));

    }

}