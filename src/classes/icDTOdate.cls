/**
 * Created by François Poirier on 2/7/2018.
 */

global with sharing class icDTOdate {

    @AuraEnabled global String dayName {get;set;}
    @AuraEnabled global String dayDate {get;set;}
    @AuraEnabled global String year {get;set;}
    @AuraEnabled global String month {get;set;}
    @AuraEnabled global String day {get;set;}

}