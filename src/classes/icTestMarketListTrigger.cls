/**
 * Created by Andrea Pissinis on 2018-03-08.
 */
@isTest
public with sharing class icTestMarketListTrigger {
    private static icTestHelperMarketList marketListCreator = new icTestHelperMarketList();

    public static void testSetup()
    {
        icObjectFactory.MockSingletonInstance('icBLMarketListTrigger', new icBlMarketListTriggerMock());
    }

    public static testMethod void testOnBeforeInsert(){
        testSetup();

        marketListCreator.createActiveML();
    }

}