/**
 * Created by Francois Poirier on 2018-11-07.
 */

public with sharing class icREPOPriority implements icIClass {
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Priority__c> getPrioritiesByCampaignIds(List<Id> campaignIds);
    }

    public class Impl implements IClass {

        public List<Priority__c> getPrioritiesByCampaignIds(List<Id> campaignIds){

            List<Priority__c> priorities = new List<Priority__c>();
            priorities = [
                    SELECT  Id,
                            Name,
                            rtxFrequency__c,
                            rtxRetail_Execution_Campaign__c,
                            rtxSequence__c
                    FROM    Priority__c
                    WHERE   rtxRetail_Execution_Campaign__c IN :campaignIds
            ];

            return priorities;

        }
    }

}