/**
 * Created by François Poirier on 2/23/2018.
 */

public with sharing class icBLaccount implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOMarketListScreen getAccountsForMaketList(icDTOMarketListScreen mlDTO);
        List<Account> getAccountsByIds(List<Id> accountIds);
        List<Account> saveAccounts(List<Account> accounts);
    }

    public class Impl implements IClass {

        private icBLFieldSetManager.IClass fieldSetBL = (icBLFieldSetManager.IClass) icObjectFactory.GetSingletonInstance('icBLFieldSetManager');
        private icREPOGeneric.IClass genericRepo = (icREPOGeneric.IClass) icObjectFactory.GetSingletonInstance('icREPOGeneric');
        private icMAPfieldSet.IClass fieldSetMap = (icMAPfieldSet.IClass) icObjectFactory.GetSingletonInstance('icMAPfieldSet');
        private icMAPGenericRecord.IClass recordMap = (icMAPGenericRecord.IClass) icObjectFactory.GetSingletonInstance('icMAPGenericRecord');
        private icREPOAccount.IClass accountRepo = (icREPOAccount.IClass) icObjectFactory.GetSingletonInstance('icREPOAccount');

        public icDTOMarketListScreen getAccountsForMaketList(icDTOMarketListScreen mlDTO){

            system.debug('starting getAccountsForMarketList at ' + Datetime.now());
            List<String> criterias = new List<String>();
            List<icDTOfilterItem> dtoFilters = new List<icDTOfilterItem>();
            if(String.isNotBlank(mlDTO.filtersJSON)) {
                system.debug('filtersJSON ===> ' + mlDTO.filtersJSON);

                dtoFilters = (List<icDTOfilterItem>) JSON.deserialize(mlDTO.filtersJSON, List<icDTOfilterItem>.class);
                system.debug('dtoFilters ===> ' + dtoFilters);


                if(String.isEmpty(mlDTO.filtersLogic)) {
                    Integer i = 0;

                    for (icDTOfilterItem dtoFilter : dtoFilters) {

                        String tmpCriteria = '  ';
                        icDTOoperator dtOoperator = dtoFilter.operator;
                        //icDTOoperator dtOoperator = (icDTOoperator) JSON.deserialize(dtoFilter.operator, icDTOoperator.class);
                        if (i > 0) {
                            tmpCriteria = tmpCriteria + ' AND ';
                        }
                        tmpCriteria = tmpCriteria + dtoFilter.fieldName + ' ' + dtOoperator.operator;
                        if (dtOoperator.operator == 'LIKE') {
                            tmpCriteria = tmpCriteria + ' \'%' + dtoFilter.value + '%\'';
                        } else {
                            tmpCriteria = tmpCriteria + ' \'' + dtoFilter.value + '\'';
                        }
                        criterias.add(tmpCriteria);
                        i++;
                    }
                }
                else{

                    String tmpCriteria = '';
                    tmpCriteria = this.parseFilterLogic(mlDTO.filtersLogic, dtoFilters);
                    criterias.add(tmpCriteria);
                }
            }
            List<String> fields = new List<String>();
            //TODO: use audit parameters instead of hard coded fieldset nanme
            fields = fieldSetBL.getAPINames('Account', 'marketListFilter');
            List<String> orderBy = new List<String>();
            orderBy.add('Name ASC');
            system.debug('before get Values ' + Datetime.now());
            List<Account> accounts = (List<Account>) genericRepo.getValues('Account', fields, criterias, orderBy, 10000);
            system.debug('after get Values ' + Datetime.now());
            system.debug('before get tablefieldset ' + Datetime.now());
            List<icDTOfieldSet> tableFieldSet = fieldSetMap.mapToDTO(fieldSetBL.getFields('Account', 'marketListFilter'));
            system.debug('after get tableFieldSet ' + Datetime.now());
            mlDTO.records = recordMap.mapToDTO(tableFieldSet, accounts, 'Account');
            system.debug('after Map records to DTO ' + Datetime.now());

            mlDTO.tableFieldSet = tableFieldSet;

            system.debug('Exiting method at ' + Datetime.now());
            return mlDTO;

        }

        public List<Account> getAccountsByIds(List<Id> accountIds){

            return accountRepo.getAccountsByIds(accountIds);

        }

        public List<Account> saveAccounts(List<Account> accounts){

            return accountRepo.saveAccounts(accounts);

        }

        private String parseFilterLogic(String filterLogic, List<icDTOfilterItem> filters){

            Integer i = 1;

            for(icDTOfilterItem filter : filters){

                String regex = String.valueOf(i);
                Pattern regexPattern = Pattern.compile(regex);
                Matcher regexMatcher = regexPattern.matcher(filterLogic);

                if(regexMatcher.find()){

                    String tmpCriteria = ' ';
                    icDTOoperator dtOoperator = filter.operator;
                    //icDTOoperator dtOoperator = (icDTOoperator) JSON.deserialize(dtoFilter.operator, icDTOoperator.class);

                    tmpCriteria = filter.fieldName + ' ' + dtOoperator.operator;
                    if (dtOoperator.operator == 'LIKE') {
                        tmpCriteria = tmpCriteria + ' \'%' + filter.value + '%\'';
                    } else {
                        tmpCriteria = tmpCriteria + ' \'' + filter.value + '\'';
                    }
                    tmpCriteria = tmpCriteria + ' ';

                    filterLogic = filterLogic.replaceAll(regex, tmpCriteria);
                }
                i++;
            }
            return filterLogic;
        }




    }

}