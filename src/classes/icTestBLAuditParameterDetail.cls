/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icTestBLAuditParameterDetail {

    private static List<Audit_Parameter_Detail__c> queryResult = new List<Audit_Parameter_Detail__c>();
    private static List<icDTOAuditParameterDetail> queryIntoDto = new List<icDTOAuditParameterDetail>();

    public static testMethod void testBLAPD(){

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icMAPAuditParameterDetail', new icMAPAuditParameterDetailMock());
        icTestMockUtilities.Tracer.SetReturnValue('icMAPAuditParameterDetailMock','listToDTO', queryIntoDto);

        icTestMockUtilities.Mocker.SetMockSingletonInstance('icREPOAuditParameterDetail', new icREPOAuditParameterDetailMock());
        icTestMockUtilities.Tracer.SetReturnValue('icREPOAuditParameterDetailMock','returnAPDWithFullAUActiveList', queryResult);


        icBLAuditParameterDetail.IClass LAPD = (icBLAuditParameterDetail.IClass) icObjectFactory.getsingletoninstance('icBLAuditParameterDetail');
        LAPD.getDTOAPDForActiveAP();
    }

}