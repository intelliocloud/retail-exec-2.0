/**
 * Created by Denis Roy on 2018-04-17.
 */
@isTest
public with sharing class icTestCTRLNewStoreVisit {

    public static testMethod void test_createNewStoreVisit() {
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLstoreVisit', new icBLstoreVisitMock());

        StoreVisit__c mockStoreVisit = new StoreVisit__c(Id = 'a0O6A000000wczVUAQ');
        icTestMockUtilities.Tracer.SetReturnValue('icBLstoreVisitMock', 'createStoreVisit', mockStoreVisit);

        String newStoreVisitId = icCTRLNewStoreVisit.createNewStoreVisit('0016A00000NEpFaQAL');

        System.assertEquals(true, icTestMockUtilities.Tracer.HasBeenCalled(
                'icBLstoreVisitMock', 'createStoreVisit'));
        System.assertEquals('a0O6A000000wczVUAQ', newStoreVisitId);
    }
}