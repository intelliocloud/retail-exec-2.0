/**
 * Created by François Poirier on 12/22/2017.
 */

global with sharing class icDTOGenericField {

    @AuraEnabled global String id {get;set;}
    @AuraEnabled global String label {get;set;}
    @AuraEnabled global String value {get;set;}
    @AuraEnabled global Boolean bValue {get;set;}
    @AuraEnabled global String apiName {get;set;}
    @AuraEnabled global String fieldType {get;set;}
    @AuraEnabled global String sObjectType {get;set;}
    @AuraEnabled global Boolean readOnly {get;set;}
    @AuraEnabled global Boolean showLabel {get;set;}
    @AuraEnabled global Boolean hidden {get;set;}

}