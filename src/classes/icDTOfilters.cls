/**
 * Created by François Poirier on 2/27/2018.
 */

public with sharing class icDTOfilters {

    public List<icDTOfilterItem> filters {get;set;}

}