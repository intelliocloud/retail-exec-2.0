/**
 * Created by Francois Poirier on 2018-11-22.
 */

global with sharing class icBatchDeleteRetailExecCampaignAccounts implements Database.Batchable<sObject> {

    String query;

    global icBatchDeleteRetailExecCampaignAccounts(List<Id> retailExecCampaignIds){

        query = 'SELECT Id FROM rtxPromotion_Store__c WHERE rtxPromotion__c IN (';
        Integer i = 1;
        for(Id tmpId : retailExecCampaignIds){
            query += '\'' + String.valueOf(tmpId) + '\'';
            if(i<retailExecCampaignIds.size()){
                query += ', ';
            }
            else {
                query += ')';
            }
            i++;
        }

    }

    global Database.QueryLocator start(Database.BatchableContext bc){

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext bc, List<SObject> scope){

        delete scope;

    }

    global void finish(Database.BatchableContext bc){

    }



}