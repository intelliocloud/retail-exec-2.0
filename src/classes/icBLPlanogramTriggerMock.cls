/**
 * Created by georg on 2018-03-05.
 */
@isTest
public class icBLPlanogramTriggerMock implements icBLPlanogramTrigger.IClass{
    public void onBeforeInsert(List<rtxPlanogram__c> newList){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
        parms.put('newList',newList);
    }
    public void onBeforeUpdate(Map<Id, rtxPlanogram__c> oldMap, Map<Id, rtxPlanogram__c> newMap){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
        parms.put('oldMap',oldMap);
        parms.put('newMap',newMap);
    }

}