/**
 * Created by François Poirier on 2/2/2018.
 */

public with sharing class icREPOstoreVisit implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {


        List<StoreVisit__c> save(List<StoreVisit__c> storeVisits);
        List<StoreVisit__c> getVisitsByDates(Date startDate, Date endDate);
        List<StoreVisit__c> getCompletedStoreVisits();
        List<StoreVisit__c> getCompletedVisitsByAccountIds(List<Id> accountIds);
        List<StoreVisit__c> getVisitsByIds(List<Id> visitIds);

    }

    public class Impl implements IClass {

        public List<StoreVisit__c> save(List<StoreVisit__c> storeVisits){

            upsert storeVisits;
            return storeVisits;
        }

        public List<StoreVisit__c> getVisitsByDates(Date startDate, Date endDate){

            List<StoreVisit__c> visits = new List<StoreVisit__c>();

            visits = [
                    SELECT  id,
                            Name,
                            rtxDate__c,
                            Store__r.Name
                    FROM    StoreVisit__c
                    WHERE   rtxDate__c >= : startDate
                    AND     rtxDate__c <= : endDate
            ];

            return visits;
        }

        public List<StoreVisit__c> getCompletedStoreVisits(){
            List<StoreVisit__c> storesvisit = new List<StoreVisit__c>();

            storesvisit = [
                    SELECT
                            Id,
                            rtxEnd__c,
                            Store__c,
                            OwnerId
                    FROM StoreVisit__c
                    WHERE rtxCompleted__c = TRUE
            ];

            return storesvisit;
        }

        public List<StoreVisit__c> getCompletedVisitsByAccountIds(List<Id> accountIds){

            List<StoreVisit__c> visits = new List<StoreVisit__c>();
            visits = [
                    SELECT  Id,
                            rtxEnd__c,
                            Store__c,
                            OwnerId
                    FROM    StoreVisit__c
                    WHERE   rtxCompleted__c = true
                    AND     Store__c IN :accountIds
            ];

            return visits;
        }

        public List<StoreVisit__c> getVisitsByIds(List<Id> visitIds){

            List<StoreVisit__c> storeVisits = new List<StoreVisit__c>();
            storeVisits = [
                    SELECT  Id,
                            rtxCompleted__c,
                            rtxDate__c,
                            rtxRead_Only__c
                    FROM    StoreVisit__c
                    WHERE   Id IN :visitIds
            ];

            return storeVisits;
        }

    }

}