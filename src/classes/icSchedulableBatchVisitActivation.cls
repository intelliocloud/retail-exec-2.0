/**
 * Created by Andrea Pissinis on 2018-02-16.
 */

global with sharing class icSchedulableBatchVisitActivation implements Schedulable {


    global void execute(SchedulableContext ctx) {

        Database.executeBatch(new icBatchActivateStoreVisits(), 10);

    }


}