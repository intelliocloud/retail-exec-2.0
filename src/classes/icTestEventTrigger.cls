/**
 * Created by Andrea Pissinis on 2018-04-09.
 */
@isTest
public with sharing class icTestEventTrigger {

    public static testMethod void testTrigger(){

        Event e = new Event();
        e.Subject = 'testing';
        e.StartDateTime = DateTime.now();
        e.EndDateTime = DateTime.now().addHours(+1);
        insert e;

        e.EndDateTime = DateTime.now().addMinutes(+1);
        update e;
    }

}