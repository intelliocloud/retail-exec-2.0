/**
 * Created by François Poirier on 12/15/2017.
 */

global with sharing class icDTOfieldSet {

    @AuraEnabled global String apiName {get;set;}
    @AuraEnabled global Boolean required {get;set;}
    @AuraEnabled global String label {get;set;}
    @AuraEnabled global String type {get;set;}
    @AuraEnabled global Boolean hidden {get;set;}
}