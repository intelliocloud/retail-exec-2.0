/**
 * Created by François Poirier on 12/22/2017.
 */

public with sharing class icMAPGenericRecord implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<icDTOGenericRecord> mapToDTO(List<icDTOfieldSet> fieldset, List<SObject> objects, String sObjectName);
    }


    public class Impl implements IClass{

        private icBLutilities.IClass util = (icBLutilities.IClass) icObjectFactory.GetSingletonInstance('icBLutilities');

        public List<icDTOGenericRecord> mapToDTO(List<icDTOfieldSet> fieldset, List<SObject> objects, String sObjectName){

            List<icDTOGenericRecord> records = new List<icDTOGenericRecord>();

            for(SObject obj : objects){

                //system.debug('obj ===> ' + obj);

                icDTOGenericRecord record = new icDTOGenericRecord();
                List<icDTOGenericField> fields = new List<icDTOGenericField>();

                record.id = String.valueOf(obj.get('id'));

                for( icDTOfieldSet f : fieldset){


                    icDTOGenericField field = new icDTOGenericField();
                    field.id = record.id;
                    field.apiName = f.apiName;
                    field.label = f.label;
                    field.fieldType = f.type;
                    if (f.hidden){
                        field.hidden = true;
                    }else{
                        field.hidden = false;
                    }
                    // system.debug('field type ===> ' + field.fieldType);
                    // system.debug('field apiName ===> ' + field.apiName);

                    if(field.apiName.contains('.')){

                        String[] nameArray = field.apiName.split('\\.');
                        sObject relObject = obj.getSObject(nameArray[0]);
                        field.value = String.valueOf(relObject.get(nameArray[1]));

                    }
                    else {
                        field.value = String.valueOf(obj.get(field.apiName));
                    }
                    if(field.fieldType == 'BOOLEAN'){
                        field.bValue = Boolean.valueOf(field.value);
                        // system.debug('field bValue ===> ' + field.bValue);
                    }

                    if(field.fieldType == 'PICKLIST'){
                        List<icDTOpickList> picklistValues = new List<icDTOpickList>();
                        picklistValues = util.getPickListValues(sObjectName, field.apiName);
                        for(icDTOpickList val : picklistValues){
                            if(val.value == field.value){
                                field.value = val.label;
                                break;
                            }
                        }
                    }
                    if(String.isBlank(field.value)){
                        field.value = '';
                    }
                    //system.debug('field.value ===> ' + field.value);
                    field.sObjectType = sObjectName;
                    //TODO: only a test for name not editable in grid.
                    field.readOnly = field.apiName == 'Name' ? true : false;
                    //TODO: all fields should not display labels for this itteration so it is hard coded but we need to make this dynamic
                    field.showLabel = false;
                    fields.add(field);

                }
                record.fields = new List<icDTOGenericField>();
                record.fields.addAll(fields);
                records.add(record);
            }


            return records;
        }
    }
}