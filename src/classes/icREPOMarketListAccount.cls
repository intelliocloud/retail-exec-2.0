/**
 * Created by François Poirier on 12/11/2017.
 */

public with sharing class icREPOMarketListAccount implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds);
        List<Market_List_Account__c> getMarketListAccountsByMarketListAndExcluded(Set<Id> activeRetailExecCampaignAndMarketListIds);
        void deleteAutomaticAccountsByMLId(List<Id> mlIds);

    }

    public class Impl implements IClass {


        public List<Market_List_Account__c> getMarketListAccountsByMarketIds(List<Id> marketIds) {

            List<Market_List_Account__c> mla = new List<Market_List_Account__c>();

            mla = [
                    SELECT Id,
                            Account__c,
                            marketList__c
                    FROM Market_List_Account__c
                    WHERE marketList__c in :marketIds
            ];

            return mla;
        }

        public List<Market_List_Account__c> getMarketListAccountsByMarketListAndExcluded(Set<Id> marketListIds) {
            return [
                    SELECT Id, account__c, account__r.RecordType.Name,  marketList__c
                    FROM Market_List_Account__c
                    WHERE marketList__c IN :marketListIds
                    AND rtxExcluded__c = false];
        }

        public void deleteAutomaticAccountsByMLId(List<Id> mlIds){

            Delete [SELECT  Id
                    FROM    Market_List_Account__c
                    WHERE   marketList__c in: mlIds
                    AND     rtxSelection_Type__c = 'Automatic' LIMIT 10000];
        }
    }

}