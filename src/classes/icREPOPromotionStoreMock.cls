/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icREPOPromotionStoreMock implements icREPOPromotionStore.IClass{


    public List<rtxPromotion_Store__c> getPromotionStoreByPromotionIds(List<Id> recIds) {

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getPromotionStoreByPromotionIds');
        params.put('recIds', recIds);
        return (List<rtxPromotion_Store__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getPromotionStoreByPromotionIds');

    }
}