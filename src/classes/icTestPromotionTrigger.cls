/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
public with sharing class icTestPromotionTrigger {

    public static testMethod void testPromotionTrigger(){
        Promotion__c p = new Promotion__c();
        p.Name = 'testPromotion';
        p.rtxIn_Store_Start_Date__c = DateTime.now().date() ;
        p.rtxIn_Store_End_Date__c = DateTime.now().date().addDays(+2) ;
        p.rtxApproved__c = FALSE;
        insert p;

        p.rtxApproved__c = TRUE;
        update p;
    }

}