/**
 * Created by georg on 2018-02-26.
 */

global with sharing class icDTOAuditParameters {
    @AuraEnabled global String id {get; set;}
    @AuraEnabled global String name {get; set;}
    @AuraEnabled global String description {get; set;}
    @AuraEnabled global Boolean active {get; set;}
    @AuraEnabled global String uniqueId {get; set;}
}