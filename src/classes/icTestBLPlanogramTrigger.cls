/**
 * Created by georg on 2018-03-05.
 */

@IsTest
private class icTestBLPlanogramTrigger {
    public static  icBLPlanogramTrigger.IClass obj = (icBLPlanogramTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramTrigger');
    static List<rtxPlanogram_Version__c> pvList = new List<rtxPlanogram_Version__c>();
    static List<rtxPlanogram__c> pList = new List<rtxPlanogram__c>();

    public static testMethod void testonBeforeInsert()
    {
        rtxPlanogram__c p1 = new rtxPlanogram__c();
        rtxPlanogram__c p2 = new rtxPlanogram__c();

        List<rtxPlanogram__c> newList = new List<rtxPlanogram__c>();
        newList.add(p1);
        newList.add(p2);
        obj.onBeforeInsert(newList);
        System.assertEquals(2, newList.size());
    }

    public static testMethod void testonBeforeUpdate()
    {
        icTestMockUtilities.Mocker.SetMockSingletonInstance('icBLPlanogramTrigger',  new icBLPlanogramTriggerMock());
        Map<Id, rtxPlanogram__c> oldMap = new Map<Id, rtxPlanogram__c>();
        Map<Id, rtxPlanogram__c> newMap = new Map<Id, rtxPlanogram__c>();
        Id theId = icTestHelperUtility.getFakeId(rtxPlanogram__c.SObjectType);

        rtxPlanogram__c p1 = new rtxPlanogram__c();
        p1.Id = theId;
        p1.rtxActive__c = false;
        oldMap.put(theId, p1);

        pList.add(p1);

        p1 = new rtxPlanogram__c();
        p1.Id = theId;
        p1.rtxActive__c = true;
        newMap.put(theId, p1);

        rtxPlanogram_Version__c pv = new rtxPlanogram_Version__c();

        pv.Planogram__c = theId;
        pvList.add(pv);

        obj.onBeforeUpdate(oldMap, newMap);
    }
}