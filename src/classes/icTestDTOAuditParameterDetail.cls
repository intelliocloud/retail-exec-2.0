/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOAuditParameterDetail {

    static testMethod void test(){
        icDTOAuditParameterDetail dto = new icDTOAuditParameterDetail();
        System.assertEquals(null, dto.id);
        System.assertEquals(null, dto.name);
        System.assertEquals(null, dto.addAuditTaskProducts);
        System.assertEquals(null, dto.addPromotionalProductsToAudit);
        System.assertEquals(null, dto.auditParameter);
        System.assertEquals(null, dto.auditParameterRecord);
        System.assertEquals(null, dto.auditTaskSortingField);
        System.assertEquals(null, dto.auditTaskSortingOrder);
        System.assertEquals(null, dto.displayAuditFilterSet);
        System.assertEquals(null, dto.displayAuditMultiEditSet);
        System.assertEquals(null, dto.displayAuditSortingField);
        System.assertEquals(null, dto.displayAuditSortingOrder);
        System.assertEquals(null, dto.displayAuditType);
        System.assertEquals(null, dto.displayFieldSet);
        System.assertEquals(null, dto.displayProductFieldSet);
        System.assertEquals(null, dto.executionCampaignFieldSet);
        System.assertEquals(null, dto.executionCampaignSortingSet);
        System.assertEquals(null, dto.executionCampaignSortingOrder);
        System.assertEquals(null, dto.productAuditFieldSet);
        System.assertEquals(null, dto.productAuditFilterSet);
        System.assertEquals(null, dto.productAuditMultiEditSet);
        System.assertEquals(null, dto.productAuditSortingField);
        System.assertEquals(null, dto.productAuditSortingOrder);
        System.assertEquals(null, dto.promotionAudit);
        System.assertEquals(null, dto.shelfAuditFieldSet);
        System.assertEquals(null, dto.shelfAuditSubtype);
        System.assertEquals(null, dto.shelfAuditType);
        System.assertEquals(null, dto.storeAuditCode);
        System.assertEquals(null, dto.taskAuditType);
        System.assertEquals(null, dto.userAuditProfile);
        System.assertEquals(null, dto.visitAuditCode);

    }

}