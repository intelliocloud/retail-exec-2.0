public with sharing class icCTRLLookup {
	
	@AuraEnabled
	public static List<sObject> getItems(String searchObject, String searchFields, String searchCriteria) {
	    /*
		// Check to make sure all fields are accessible to this user
		String[] fieldsToCheck = new String[] {'Id', 'Name', 'Price__c', 'Quantity__c', 'Packed__c', 'CreatedDate'};

		Map<String,Schema.SObjectField> fieldDescribeTokens = Schema.SObjectType.Camping_Item__c.fields.getMap();

		for(String field : fieldsToCheck) {
			if( ! fieldDescribeTokens.get(field).getDescribe().isAccessible()) {
				throw new System.NoAccessException();
				return null;
			}
		}

		// OK, they're cool, let 'em through
		return [SELECT Id, Name, Price__c, Quantity__c, Packed__c, CreatedDate FROM Camping_Item__c LIMIT 1];

		[SELECT  
		    RecordId
		    , HasReadAccess
		    , HasEditAccess
		    , HasDeleteAccess 
		FROM  
		    UserRecordAccess 
		WHERE  
		    UserId=:UserInfo.getUserId() 
		    AND 
		    RecordId =: Account.Id];

		*/

		String searchQuery='FIND \'*' + searchCriteria + '*\' IN ALL FIELDS RETURNING ' + searchObject + '(' + searchFields + ')';

		List<List<sObject>> searchResults = search.query(searchQuery);

		return searchResults[0];
	}
}