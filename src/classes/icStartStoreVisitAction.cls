public without sharing class icStartStoreVisitAction {

    @AuraEnabled
    public static StoreVisit__c getStoreVisit(Id storevisitId){
        StoreVisit__c storevis = [ SELECT Id, rtxDate__c, rtxCompleted__c FROM StoreVisit__c WHERE Id = :storevisitId];
        if (!storevis.rtxCompleted__c){
            return storevis;
        }else{
            return null;
        }
    }
    
    @AuraEnabled
    public static StoreVisit__c updateStoreVisitRecord(StoreVisit__c sc){
        sc.rtxDate__c = System.NOW();
        upsert sc;
        return sc;
    }
}