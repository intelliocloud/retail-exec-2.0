/**
 * Created by georg on 2018-03-07.
 */

@IsTest
public class icBLPlanogramVersionTriggerMock implements icBLPlanogramVersionTrigger.IClass{
    public void onBeforeInsert(List<rtxPlanogram_Version__c> newList){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeInsert');
        parms.put('newList',newList);
    }
    public void onAfterInsert(Map<Id, rtxPlanogram_Version__c> newMap){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onAfterInsert');
        parms.put('newMap',newMap);
    }
    public void onBeforeUpdate(Map<Id, rtxPlanogram_Version__c> oldMap, Map<Id, rtxPlanogram_Version__c> newMap){
        Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, 'onBeforeUpdate');
        parms.put('oldMap',oldMap);
        parms.put('newMap',newMap);
    }
}