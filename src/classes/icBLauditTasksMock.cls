/**
 * Created by François Poirier on 12/12/2017.
 */

@isTest
public with sharing class icBLauditTasksMock {

    public List<AuditTask__c> getAuditTasks(Id storeVisitId){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAuditTasks');
        params.put('storeVisitId', storeVisitId);
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAuditTasks');

    }

    public icDTOAuditTaskScreen getAuditTaskScreen(Id storeVisitId){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getAuditTaskScreen');
        params.put('storeVisitId', storeVisitId);
        return (icDTOAuditTaskScreen) icTestMockUtilities.Tracer.GetReturnValue(this, 'getAuditTaskScreen');
    }

    public List<AuditTask__c> saveAuditTasks(List<AuditTask__c> auditTasks){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'saveAuditTasks');
        params.put('auditTasks', auditTasks);
        return (List<AuditTask__c>) icTestMockUtilities.Tracer.GetReturnValue(this, 'saveAuditTasks');

    }

    public void createAuditTasks(List<StoreVisit__c> newStoreVisits){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'createAuditTasks');
        params.put('newStoreVisits', newStoreVisits);

    }

    public void deleteAuditTasks(List<StoreVisit__c> deactivatedStoreVisits){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'deleteAuditTasks');
        params.put('newStoreVisits', deactivatedStoreVisits);

    }
}