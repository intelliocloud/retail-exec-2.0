/**
 * Created by Andrea Pissinis on 2018-03-07.
 */
@isTest
public with sharing class icTestHelperPriority {

    public Priority__c createAskAlwaysPriority(rtxRetail_Execution_Campaign__c rtx){

        Priority__c p = new Priority__c();
        p.Name = 'tesdtings';
        p.rtxRetail_Execution_Campaign__c = rtx.Id;
        p.rtxCategory__c = 'Training';
        p.rtxAnswerType__c = 'Free Text';
        p.rtxFrequency__c = 'Ask Always';
        p.rtxSequence__c = 1;
        p.rtxPriority__c = '2';
        p.rtxTask_Audit_Type__c = 'Standard';
        insert p;
        return p;
    }

    public Priority__c createAskOnlyOncePriority(rtxRetail_Execution_Campaign__c rtx){

        Priority__c pO = new Priority__c();
        pO.Name = 'tesdtings';
        pO.rtxRetail_Execution_Campaign__c = rtx.Id;
        pO.rtxCategory__c = 'Training';
        pO.rtxAnswerType__c = 'Free Text';
        pO.rtxFrequency__c = 'Ask Only Once';
        pO.rtxSequence__c = 1;
        pO.rtxPriority__c = '2';
        pO.rtxTask_Audit_Type__c = 'Standard';
        insert pO;
        return pO;
    }

}