/**
 * Created by François Poirier on 11/29/2017.
 */

public with sharing class icBLFieldSetManager implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass{

        Boolean hasFieldSet(String sObjectName, String fieldSetName);
        List<Schema.FieldSetMember> getFields(String sObjectName, String fieldSetName);
        Schema.DescribeFieldResult getFieldMetadata(String sObjectName, String fieldSetName);
        List<String> getAPINames(String sObjectName, String fieldSetName);

        List<SelectOption> GetPickListValues(Map<String, Schema.sObjectType> globalSchema,
                String sObjectName, String fieldSetMember);

        Map<String, List<String>> GetObjectsAndFieldsFromDataSet(Map<String, Schema.sObjectType> globalSchema,
                List<Schema.FieldSetMember> fieldSet, String sObjectName);

        Map<String, List<SelectOption>> GetMapFieldObjectsToSelectedOptions(Map<String, List<String>> fieldToObjectsFilter,
                Map<String, Schema.sObjectType> globalSchema, String sObjectType);
    }

    public class Impl implements IClass{

        icBLValidateAccess.IClass validateAccessBL = (icBLValidateAccess.IClass) icObjectFactory.GetSingletonInstance('icBLValidateAccess');

        public Boolean hasFieldSet(String sObjectName, String fieldSetName){

            Boolean retour = false;

            Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();
            List<Schema.FieldSetMember> fields = new List<Schema.FieldSetMember>();
            Schema.DescribeSObjectResult describeResult = globalSchema.get(sObjectName).getDescribe();
            Schema.FieldSet myFieldset = describeResult.fieldSets.getMap().get(fieldSetName);

            if (myFieldset != null){
                retour = true;
            }

            return retour;

        }

        public List<Schema.FieldSetMember> getFields(String sObjectName, String fieldSetName){

            Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();
            List<Schema.FieldSetMember> fields;
            Schema.DescribeSObjectResult describeResult = globalSchema.get(sObjectName).getDescribe();
            Schema.FieldSet myFieldset = describeResult.fieldSets.getMap().get(fieldSetName);

            if (myFieldset != null){
                fields = myFieldset.getFields();
                fields = validateAccessBL.getAccessiblesFieldSetMembers(fields, sObjectName);
            }
            return fields;
        }

        public Schema.DescribeFieldResult getFieldMetadata(String sObjectName, String fieldSetName){
            Map<String, Schema.sObjectType> globalSchema = Schema.getGlobalDescribe();
            Schema.DescribeSObjectResult describeResult = globalSchema.get(sObjectName).getDescribe();
            Map<String, Schema.SObjectField> mapSObjectFields = describeResult.fields.getMap();

            Schema.DescribeFieldResult fieldResult= mapSObjectFields.get(fieldSetName).getDescribe();

            return fieldResult;
        }

        public List<String> getAPINames(String sObjectName, String fieldSetName){

            List<String> fieldNames = new List<String>();

            if (hasFieldSet(sObjectName, fieldSetName)){
                List<Schema.FieldSetMember> fields = getFields(sObjectName,fieldSetName);
                for (Schema.FieldSetMember f : fields) {
                    fieldNames.add(f.getFieldPath());
                }
            }
            return fieldNames;
        }

        public List<SelectOption> GetPickListValues(Map<String, Schema.sObjectType> globalSchema,
                String sObjectName, String fieldSetMember){

            List<SelectOption> options = new List<SelectOption>();

            Schema.DescribeSObjectResult describeResult = globalSchema.get(sObjectName).getDescribe();
            Map<String, Schema.SObjectField> mapSObjectFields = describeResult.fields.getMap();

            Schema.DescribeFieldResult fieldResult= mapSObjectFields.get(fieldSetMember).getDescribe();

            if (fieldResult.getType() == Schema.DisplayType.Boolean){
                options.add(new SelectOption('false', System.Label.False));
                options.add(new SelectOption('true', System.Label.True));
            }

            else {

                List<Schema.PicklistEntry> pickListEntries = fieldResult.getPicklistValues();

                for(PicklistEntry pickListEntryItem: pickListEntries){
                    options.add(new SelectOption(pickListEntryItem.getValue(), pickListEntryItem.getLabel()));
                }
            }

            return options;
        }

        public Map<String, List<String>> GetObjectsAndFieldsFromDataSet(Map<String, Schema.sObjectType> globalSchema,
                List<Schema.FieldSetMember> fieldSet, String sObjectName){

            Map<String, List<String>> objectToFieldsMap = new Map<String, List<String>>();

            Schema.DescribeSObjectResult describeResult = globalSchema.get(sObjectName).getDescribe();

            System.debug('fieldset is '+fieldSet);

            for (Schema.FieldSetMember fieldMemberItem: fieldSet){

                String[] fields = fieldMemberItem.getFieldPath().split('\\.');

                Integer fieldsSize = fields.size();

                String fieldName = (fieldsSize>1)? fields[fieldsSize-1] : fields[0];

                String objName = (fieldsSize>1)? fields[fieldsSize-2] : sObjectName;

                objName = (fieldsSize>1) ? objName.replace('__r', '__c'): objName;

                if (!objectToFieldsMap.containsKey(objName)){
                    objectToFieldsMap.put(objName, new List<String>());
                }

                List<String> newList = objectToFieldsMap.get(objName);
                newList.add(fieldName);
            }

            System.debug('object to field is '+objectToFieldsMap);

            return objectToFieldsMap;
        }

        public Map<String, List<SelectOption>> GetMapFieldObjectsToSelectedOptions(Map<String, List<String>> fieldToObjectsFilter,
                Map<String, Schema.sObjectType> globalSchema, String sObjectType){

            Map<String, List<SelectOption>> mapToSelectedOptions = new Map<String, List<SelectOption>>();

            for (String objectName: fieldToObjectsFilter.keySet()){
                List<String> fieldNames = fieldToObjectsFilter.get(objectName);

                for (String fieldName: fieldNames){
                    List<SelectOption> selectedOption = GetPickListValues(globalSchema, objectName, fieldName);

                    String key = (objectName == sObjectType) ? objectName : objectName.replace('__c', '__r');
                    key = key+'.'+fieldName;

                    mapToSelectedOptions.put(key, selectedOption);
                }
            }

            return mapToSelectedOptions;
        }
    }
}