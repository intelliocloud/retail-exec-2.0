/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icBLGenericMock implements icBLGeneric.IClass{

    public void updateValue(sObject obj){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'updateValue');
        params.put('obj', obj);

    }
    public icDTOGenericListView getFilteredItems(String filterId, String objectType){

        Map<String, object> params = icTestMockUtilities.Tracer.RegisterCall(this, 'getFilteredItems');
        params.put('filterId', filterId);
        params.put('objectType',objectType);
        return (icDTOGenericListView) icTestMockUtilities.Tracer.GetReturnValue(this, 'getFilteredItems');

    }
}