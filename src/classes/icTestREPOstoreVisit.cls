/**
 * Created by Andrea Pissinis on 2018-03-12.
 */
@isTest
public with sharing class icTestREPOstoreVisit {

    public static testMethod void testRepoSV(){

        List<StoreVisit__c> svsL = new List<StoreVisit__c>();

        icREPOstoreVisit.IClass svRep = (icREPOstoreVisit.IClass) icObjectFactory.getsingletoninstance('icREPOstoreVisit');
        svRep.save(svsL);
        svRep.getVisitsByDates(DateTime.newInstance(0,0,0,0,0,0).date(),DateTime.newInstance(0,0,0,0,0,0).date());
        svRep.getCompletedStoreVisits();
    }
}