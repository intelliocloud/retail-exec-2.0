/**
 * Created by Andrea Pissinis on 2018-03-09.
 */
@isTest
public with sharing class icTestDTOListViewColumn {

    static testMethod void testdto(){
        icDTOListViewColumn dto = new icDTOListViewColumn();
        System.assertEquals(null, dto.ascendingLabel);
        System.assertEquals(null, dto.descendingLabel);
        System.assertEquals(null, dto.fieldNameOrPath);
        System.assertEquals(null, dto.hidden);
        System.assertEquals(null, dto.label);
        System.assertEquals(null, dto.selectListItem);
        System.assertEquals(null, dto.sortDirection);
        System.assertEquals(null, dto.sortIndex);
        System.assertEquals(null, dto.sortable);
        System.assertEquals(null, dto.type);

    }
}