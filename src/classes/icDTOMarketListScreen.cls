/**
 * Created by François Poirier on 2/14/2018.
 */

global with sharing class icDTOMarketListScreen {

    @AuraEnabled global String id {get;set;}
    @AuraEnabled global String filtersJSON {get;set;}
    @AuraEnabled global String filtersLogic {get;set;}
    @AuraEnabled global List<icDTOpickList> fields {get;set;}
    @AuraEnabled global Boolean hasError {get;set;}
    @AuraEnabled global String errorCode {get;set;}
    @AuraEnabled global List<icDTOoperator> operators {get;set;}
    @AuraEnabled global List<icDTOfieldSet> tableFieldSet {get;set;}
    @AuraEnabled global List<icDTOGenericRecord> records {get;set;}

}