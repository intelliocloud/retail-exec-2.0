/**
 * Created by Andrea Pissinis on 2018-03-19.
 */
@isTest
public with sharing class icTestCTRLNewStoreDisplay {
    private static icTestHelperAccount accCreator = new icTestHelperAccount();

    public static testMethod void testStoreDisplay(){
        Account acc = accCreator.createAccount();
        Account parent = accCreator.createAccount();

        acc.ParentId = parent.Id;
        update acc;


        List<Id> eqs = new List<Id>();
        rtxEquipment__c eq = new rtxEquipment__c();
        eq.Name = 'testing';
        insert eq;
        eqs.add(eq.Id);
        icCTRLNewStoreDisplay.createDisplays(acc.Id, eqs);


        List<Id> secs = new List<Id>();
        Section__c se = new Section__c();
        se.Name = 'testing';
        se.Account__c = acc.Id;
        Id presentoirMagasin = Schema.SObjectType.Section__c.getRecordTypeInfosByName().get('Store Display').getRecordTypeId();
        se.RecordTypeId = presentoirMagasin;
        insert se;
        secs.add(se.Id);

        try {
            icCTRLNewStoreDisplay.createDisplays(acc.Id, secs);
            icCTRLNewStoreDisplay.getMasterDisplays(acc.Id);
            icCTRLNewStoreDisplay.getEquipments(acc.Id);

        }catch (Exception e){

        }

    }

}