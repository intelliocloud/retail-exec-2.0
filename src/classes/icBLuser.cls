/**
 * Created by Francois Poirier on 2018-03-13.
 */

public with sharing class icBLuser implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        User getCurrentUserInfo();
        String getCurrentUserAuditRole();

    }

    public class Impl implements IClass {

        private icREPOuser.IClass userRepo = (icREPOuser.IClass) icObjectFactory.GetSingletonInstance('icREPOuser');

        public User getCurrentUserInfo(){
            return userRepo.getCurrentUserInfo();
        }

        public String getCurrentUserAuditRole(){
            User currentUser = new User();
            currentUser = getCurrentUserInfo();
            return currentUser.rtxAuditRole__c;
        }
    }

}