/**
 * Created by Andrea Pissinis on 2018-03-13.
 */
@isTest
global class icHttpResponseGeneratorMock implements HttpCalloutMock{

    global HttpResponse respond(HTTPRequest req){

        System.assertEquals('callout:rtxCallMeBack/services/data/v41.0/sobjects/Account/listviews/Name/describe', req.getEndpoint());
        System.assertEquals('GET', req.getMethod());

        HttpResponse res = new HttpResponse();
        res.setHeader('', '');
        icDTOlistView body = new icDTOlistView();

        body.query = ('SELECT id FROM Account');
        body.columns = new List<icDTOListViewColumn>();

        res.setBody(JSON.serialize(body));
        res.setStatus(('OK'));
        res.setStatusCode(200);
        return res;
    }

}