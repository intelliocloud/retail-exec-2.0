/**
 * Created by François Poirier on 1/23/2018.
 */

public with sharing class icMapAuditTaskScreen implements icIClass {

    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        icDTOAuditTaskScreen mapToDTO(Map<Id, List<AuditTask__c>> mapCampaingATs);

    }

    public class Impl implements IClass {

        private icMapRetailExecutionCampaign.IClass campaignMap = (icMapRetailExecutionCampaign.IClass) icObjectFactory.GetSingletonInstance('icMapRetailExecutionCampaign');

        public icDTOAuditTaskScreen mapToDTO(Map<Id, List<AuditTask__c>> mapCampaingATs){

            icDTOAuditTaskScreen dto = new icDTOAuditTaskScreen();
            List<icDTOretailExecutionCampaign> campaignDtos = new List<icDTOretailExecutionCampaign>();

            campaignDtos = campaignMap.mapToDTO(mapCampaingATs);

            dto.campaigns = campaignDtos;

            return dto;

        }
    }
}