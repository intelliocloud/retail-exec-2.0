/**
 * Created by Andrea Pissinis on 2018-02-26.
 */

public with sharing class icBLMarketListTrigger implements icIClass{
    public Object GetInstance(){
        return new Impl();
    }

    public Interface IClass {

        void onBeforeUpdate(List<ML__c> newMLs);
    }

    public class Impl implements IClass {

        private icBLMarketListAccount.IClass mlaBL = (icBLMarketListAccount.IClass) icObjectFactory.GetSingletonInstance('icBLMarketListAccount');


        public void onBeforeUpdate(List<ML__c> newMLs){

            List<ML__c> MLsToActivate = new List<ML__c>();

            for (ML__c ml : newMLs){
                ML__c olMl = (ML__c)Trigger.oldMap.get(ml.Id);

                if (olMl.rtxActive__c == FALSE && ml.rtxActive__c == TRUE && String.isNotBlank(ml.rtxJSON_Filters__c)){
                    MLsToActivate.add(ml);

                }else if (olMl.rtxRefresh__c == FALSE && ml.rtxRefresh__c == TRUE){
                    ml.rtxLast_Refresh_Date__c = DateTime.now().date();
                    if(String.isNotBlank(ml.rtxJSON_Filters__c)) {
                        MLsToActivate.add(ml);
                    }
                    ml.rtxRefresh__c = FALSE;
                }

            }
            if (!MLsToActivate.isEmpty()){
                mlaBL.createMarketListAccounts(MLsToActivate);
            }

        }


    }

}