/**
 * Created by georg on 2018-02-20.
 */

trigger icPlanogramVersionTrigger on rtxPlanogram_Version__c (before insert, before update, after insert) {
    system.debug('in trigger icPlanogramVersionTrigger');

    icBLPlanogramVersionTrigger.IClass bls = (icBLPlanogramVersionTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramVersionTrigger');

    if(Trigger.isBefore && Trigger.isInsert){
        system.debug('in trigger before Insert');
            bls.onBeforeInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isInsert){
        system.debug('in trigger after Insert');
        bls.onAfterInsert(Trigger.newMap);
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        system.debug('in trigger before update');
        bls.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
}