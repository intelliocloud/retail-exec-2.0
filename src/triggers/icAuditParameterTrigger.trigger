/**
 * Created by Andrea Pissinis on 2018-04-16.
 */

trigger icAuditParameterTrigger on AuditParameters__c (before insert, before update) {

    icBLAuditParameterTrigger.IClass icAPT = (icBLAuditParameterTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditParameterTrigger');

    if (icAvoidRecursion.isFirstRun()) {

        if (Trigger.isBefore && Trigger.isUpdate) {
            icAPT.onBeforeUpdate(Trigger.new);
        }
        if (Trigger.isBefore && Trigger.isInsert) {
            icAPT.onBeforeInsert(Trigger.new);
        }
    }
}