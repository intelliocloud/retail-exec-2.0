/**
 * Created by georg on 2018-02-20.
 */

trigger icPlanogramTrigger on rtxPlanogram__c (before insert, before update) {
    system.debug('in trigger icPlanogramTrigger');

    icBLPlanogramTrigger.IClass bls = (icBLPlanogramTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLPlanogramTrigger');

    if(Trigger.isBefore && Trigger.isInsert){
        system.debug('in trigger before Insert');
        bls.onBeforeInsert(Trigger.new);
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        system.debug('in trigger before update');
        bls.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
}