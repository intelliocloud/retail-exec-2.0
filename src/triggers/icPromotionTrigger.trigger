/**
 * Created by georg on 2018-02-19.
 */

trigger icPromotionTrigger on Promotion__c (before insert, before update) {
    system.debug('in trigger icPromotionTrigger');

    icBLPromotionTrigger.IClass bls = (icBLPromotionTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLPromotionTrigger');

    if(Trigger.isBefore && Trigger.isInsert){
        system.debug('in trigger before Insert');
        bls.onBeforeInsert(Trigger.oldMap, Trigger.newMap);
    }

    if(Trigger.isBefore && Trigger.isUpdate){
        system.debug('in trigger before update');
        bls.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
}