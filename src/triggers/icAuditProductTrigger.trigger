/**
 * Created by Andrea Pissinis on 2018-02-09.
 */

trigger icAuditProductTrigger on AuditProduct__c (before update) {

    icBLAuditProductTrigger.IClass icAPT = (icBLAuditProductTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditProductTrigger');
    if(Trigger.isBefore && Trigger.isUpdate){
        icAPT.onBeforeUpdate(Trigger.new);
    }
}