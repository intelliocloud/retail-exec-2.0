/**
 * Created by Andrea Pissinis on 2018-02-26.
 */

trigger icMarketListTrigger on ML__c (before update) {

    icBLMarketListTrigger.IClass mlt = (icBLMarketListTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLMarketListTrigger');


    if(Trigger.isBefore && Trigger.isUpdate){

        mlt.onBeforeUpdate(Trigger.new);
    }

}