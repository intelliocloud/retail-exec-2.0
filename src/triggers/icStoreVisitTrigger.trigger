/**
 * Created by François Poirier on 9/19/2017.
 */

trigger icStoreVisitTrigger on StoreVisit__c (before insert, after insert, before update, after update) {

    system.debug('in trigger');

    icBLStoreVisitTrigger.IClass bls = (icBLStoreVisitTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLStoreVisitTrigger');

    if(Trigger.isBefore && Trigger.isInsert){
        system.debug('in trigger before update');
        bls.onBeforeInsert(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isInsert){

        bls.onAfterInsert(Trigger.new);
    }

    if(Trigger.isBefore && Trigger.isUpdate){

        bls.onBeforeUpdate(Trigger.new);
    }

    if(Trigger.isAfter && Trigger.isUpdate){

        bls.onAfterUpdate(Trigger.new);
    }
}