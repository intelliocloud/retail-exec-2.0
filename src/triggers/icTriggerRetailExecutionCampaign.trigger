/**
 * Created by georg on 2018-02-08.
 */

trigger icTriggerRetailExecutionCampaign on rtxRetail_Execution_Campaign__c (before insert, before update) {

    system.debug('in trigger icTriggerRetailExecutionCampaign');

    icBLRetailExecutionCampaignTrigger.IClass bls = (icBLRetailExecutionCampaignTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLRetailExecutionCampaignTrigger');

    if(Trigger.isBefore && Trigger.isUpdate){
        system.debug('in trigger before update');
        bls.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
    }
}