/**
 * Created by Andrea Pissinis on 2018-02-09.
 */

trigger icAuditTaskTrigger on AuditTask__c (before update) {

    icBLAuditTaskTrigger.IClass icAT = (icBLAuditTaskTrigger.IClass) icObjectFactory.GetSingletonInstance('icBLAuditTaskTrigger');

    if(Trigger.isBefore && Trigger.isUpdate){
        icAT.onBeforeUpdate(Trigger.new);
    }
}