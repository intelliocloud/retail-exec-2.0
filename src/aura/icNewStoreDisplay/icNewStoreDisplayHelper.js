({
    doInit : function(component, event)
    {

        var recordId = component.get("v.recordId");
        var helper = this;
        var action = this.callServerAction(component, event, "getEquipments",
            {accountId: recordId}, {
                onSuccess: function (response){var result = response.getReturnValue();
                    //console.log("** result ===> " +  JSON.stringify(result));
                    component.set("v.filterFieldSetE", result.filterFieldSet);
                    component.set("v.tableFieldSetE", result.tableFieldSet);
                    component.set("v.equipmentsList",result.records);
                    //console.log('records ===> ' + JSON.stringify(result.records));
                    helper.setHeaderValuesE(component, event);
                }});
        $A.enqueueAction(action);

        var actionM = this.callServerAction(component, event, "getMasterDisplays",
            {accountId: recordId}, {
                onSuccess: function (response) {
                    var result = response.getReturnValue();
                    if (result != null) {

                    //console.log("** result ===> " +  JSON.stringify(result));
                    component.set("v.filterFieldSetM", result.filterFieldSet);
                    component.set("v.tableFieldSetM", result.tableFieldSet);
                    component.set("v.masterDisplaysList", result.records);
                    //console.log('records ===> ' + JSON.stringify(result.records));
                    helper.setHeaderValuesM(component, event);
                	}
                }});
        $A.enqueueAction(actionM);
    },
    setHeaderValuesE : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSetE");
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });

        component.set("v.headerValuesE", headerValues);
    },
    setHeaderValuesM : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSetM");
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });

        component.set("v.headerValuesM", headerValues);
    },
    handleClick : function (component, event) {
        console.log('in button');
        var recordId = component.get("v.recordId");

        var secSelected= [];
        secSelected = component.get("v.masterDisplaysList");

        var eqSelected = [];
        eqSelected = component.get("v.equipmentsList");
        var eqToPass = [];

        if (secSelected.length > 0){

            for (var i=0; i < secSelected.length; i++){
                console.log ('is it selected ' + secSelected[i].selected);
                if (secSelected[i].selected){
                    eqToPass.push(secSelected[i].id);
                }
            }
        }else{
            for (var j=0; j < eqSelected.length; j++){
                console.log ('is it selected ' + eqSelected[j].selected);
                if (eqSelected[j].selected){
                    eqToPass.push(eqSelected[j].id);
                }
            }
        }
        console.log('Creating this now ' + eqToPass);
        var action = this.callServerAction(component, event, "createDisplays",
            {accountId: recordId, eqs: eqToPass}, {
                onSuccess: function (response){var result = response.getReturnValue();
                    if(eqToPass.length > 0){

                        $A.get("e.force:closeQuickAction").fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "success",
                            "message" : $A.get("$Label.c.StoreDisplayCreated")
                        });
                        toastEvent.fire();
                    }

                }});
        $A.enqueueAction(action);

    },
    handleCloseClick : function (component, event){
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    }
})