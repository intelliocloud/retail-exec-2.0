({
    doInit : function(component, event, helper) {

        helper.doInit(component, event);

    },
    handleClick : function(component, event, helper){
        helper.handleClick(component, event);
    },
    handleCloseClick : function(component, event, helper){
        helper.handleCloseClick(component,event);
    }
})