/**
 * Created by François Poirier on 1/25/2018.
 */
({
    doInit : function (component, event, helper) {

        helper.init(component, event);

    },

    showNextWeek : function (component, event, helper) {

        helper.showNextWeek(component, event);

    },

    showPreviousWeek : function (component, event, helper) {

        helper.showPreviousWeek(component, event);

    }
})