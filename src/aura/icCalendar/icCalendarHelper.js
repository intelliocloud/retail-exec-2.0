/**
 * Created by François Poirier on 1/25/2018.
 */
({
    init : function (component, event) {

        this.setPreviousButton(component);
        this.setcolClass(component);

    },
    setcolClass : function (component) {

      var dates = component.get("v.dates");
      var numberOfDates = dates.length;
      //add the hours col to the number of columns
      var numberOfCols = numberOfDates + 1;
      var colClass = 'slds-col slds-size--1-of-' + numberOfCols.toString() + ' slds-box';

      component.set("v.colClass", colClass);


    },

    setPreviousButton : function (component) {

        console.log('in set Previous btn');
        var isDisabled = true;
        var dates = component.get("v.dates");
        if (dates.length > 0){
            console.log('in dates.length>0');
            var date0 = new Date(dates[0].year, dates[0].month - 1, dates[0].day);
            console.log('dates 0 ===> ' + date0);
            var dateNow = new Date();
            console.log('now ===> ' + dateNow);
            if(date0 > dateNow){

                isDisabled = false;
            }
        }
        console.log('isDisabled ===> ' + isDisabled);
        component.set("v.disablePrevious", isDisabled);
    },

    formatDate : function (date) {



        var monthNames = [
            "January", "February", "March",
            "April", "May", "June", "July",
            "August", "September", "October",
            "November", "December"
        ];

        var monthNumbers = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ];


        var day = date.getDate();
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return  monthNumbers[monthIndex] + '/' + day + '/' + year;

    },
    getDateDay : function (date) {

        var dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var dayIndex = date.getDay();

        return dayNames[dayIndex];
    },
    showNextWeek : function (component, event) {

        var evt = component.getEvent("evtShowNextWeek");
        evt.fire();

    },
    showPreviousWeek : function (component, event) {

        var evt = component.getEvent("evtShowPreviousWeek");
        evt.fire();

    }

})