({
	doInit : function(component, event) {
		var item = component.get("v.item");
		//var objectType = component.get("v.objectType");
		var objectLabelField = component.get("v.objectLabelField");
		var objectSubLabelField = component.get("v.objectSubLabelField");
		var objectValueField = component.get("v.objectValueField");

		component.set("v.itemLabel", item[objectLabelField]);
		component.set("v.itemSubLabel", item[objectSubLabelField]);
		component.set("v.itemValue", item[objectValueField]);
	},

	doSelect : function(component, event) {
		var item = component.get("v.item");
		component.set("v.selectedItem", item);

		/* old way with event
		var objectType = component.get("v.objectType");
		var objectLabelField = component.get("v.objectLabelField");
		var objectValueField = component.get("v.objectValueField");

		var uiListItemSelect = component.getEvent("uiListItemSelect");
		
		uiListItemSelect.setParams({
			"object": item,
			"objectType": objectType,
			"objectLabelField": objectLabelField,
			"objectValueField": objectValueField			
		});
		uiListItemSelect.fire();
		*/
	}
})