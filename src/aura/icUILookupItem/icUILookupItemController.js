({
	// Read object input info and populate listItem label and value
	doInit: function(component, event, helper) {
		helper.doInit(component, event);
	},

	doSelect: function(component, event, helper) {
		helper.doSelect(component, event);
	}
})