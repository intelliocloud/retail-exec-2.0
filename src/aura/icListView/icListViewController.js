/**
 * Created by François Poirier on 1/29/2018.
 */
({
    doInit : function (component, event, helper) {

        helper.doInit(component, event);

    },
    handleOnChange : function (component, event, helper) {

        console.log('in handle on change controller');
        helper.getFilteredItems(component, event);

    }
})