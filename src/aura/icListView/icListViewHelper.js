/**
 * Created by François Poirier on 1/29/2018.
 */
({
    doInit : function (component, event) {

        var objectType = component.get("v.objectType");
        var action = this.callServerAction(component, event, "getListViews",
            {objectType : objectType}, {
            onSuccess : function (response) {

                var listViewList = response.getReturnValue();
                component.set("v.listViewList", listViewList);
            }
            });
        $A.enqueueAction(action);
    },

    getFilteredItems : function (component, event) {

        var helper = this;
        console.log('in getFilteredItems helper');
        var selectedList = component.find("selectedViewId").get("v.value");
        var objectType = component.get('v.objectType');

        console.log("selected List ===> " + selectedList);

        var action = this.callServerAction(component, event, "getFilteredItems",
            {filterId : selectedList, objectType: objectType},
            {
                onSuccess : function(response){

                    var results = response.getReturnValue();
                    console.log('results ===> ' + JSON.stringify(results));
                    component.set("v.itemList", results.records);
                    console.log('results.records.length ===> ' + results.records.length)
                    if(results.records.length >0){
                        component.set("v.showTable", true);
                    }
                    else {
                        component.set("v.showTable", false);
                    }
                    component.set("v.tableFieldSet", results.tableFieldSet);
                    helper.setHeaderValues(component, event);
                }
            });
        $A.enqueueAction(action);
    },

    setHeaderValues : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSet");
        console.log('tableFieldSet ===> ' + JSON.stringify(tableFieldSet));
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });

        console.log('header values ===> ' + headerValues);
        component.set("v.headerValues", headerValues);
    }

})