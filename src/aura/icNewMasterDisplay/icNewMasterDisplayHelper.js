({
    doInit : function(component, event)
    {

        var recordId = component.get("v.recordId");
        var helper = this;
        var action = this.callServerAction(component, event, "getEquipments",
            {accountId: recordId}, {
                onSuccess: function (response){var result = response.getReturnValue();
                   console.log("** result ===> " +  JSON.stringify(result));
                    component.set("v.filterFieldSet", result.filterFieldSet);
                    component.set("v.tableFieldSet", result.tableFieldSet);
                    component.set("v.equipmentsList",result.records);
                   console.log('records ===> ' + JSON.stringify(result.records));
                    helper.setHeaderValues(component, event);
                }});
        $A.enqueueAction(action);
    },
    setHeaderValues : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSet");
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });

        component.set("v.headerValues", headerValues);
    },
    handleClick : function (component, event) {
        console.log('in button');
        var recordId = component.get("v.recordId");

        var eqSelected = [];
        eqSelected = component.get("v.equipmentsList");
        var eqToPass = [];

        for (var i=0; i < eqSelected.length; i++){
            console.log ('is it selected ' + eqSelected[i].selected);
            if (eqSelected[i].selected){
                eqToPass.push(eqSelected[i].id);

            }
        }

        var action = this.callServerAction(component, event, "createDisplays",
            {accountId: recordId, eqs: eqToPass}, {
                onSuccess: function (response){var result = response.getReturnValue();
                    if(eqToPass.length > 0){

                        $A.get("e.force:closeQuickAction").fire();
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "success",
                            "message" : $A.get("$Label.c.MasterDisplayCreated")
                        });
                        toastEvent.fire();
                    }

                }});
        $A.enqueueAction(action);

    },
    handleCloseClick : function (component, event){
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    }
})