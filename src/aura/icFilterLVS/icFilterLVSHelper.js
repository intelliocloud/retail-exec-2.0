/**
 * Created by François Poirier on 2/23/2018.
 */
({


    addFilter : function (component, event) {

        var showAddFilter = component.get("v.showAddFilter");
        var filters = component.get("v.filters");
        var index = filters.length;
        var currentFilter = {};

        currentFilter.field = "";
        currentFilter.value = "";
        currentFilter.fieldName = "";
        currentFilter.index = index;

        filters.push(currentFilter);


        component.set("v.filters", filters);
        component.set("v.showAddFilter", !showAddFilter);
        component.set("v.currentFilter", currentFilter);

    },

    parseFiltersJSON : function (component, event){

        console.log('in parseFiltersJSON');

        var filtersJSON = component.get("v.filtersJSON");
        if(filtersJSON) {
            var filtersObjects = [];
            filtersObjects = JSON.parse(filtersJSON);
            filtersObjects.forEach(function (filter) {
                //filter.jsonOperator = filter.jsonOperator
                filter.operator = JSON.parse(filter.jsonOperator);
                console.log('filter.jsonOperator ===? ' + filter.jsonOperator);
            });
            console.log("filters object after parse ===> " + JSON.stringify(filtersObjects));
            component.set("v.filters", filtersObjects);
        }
        var fields = component.get("V.fields");
        var fieldsObj = {};
        fields.forEach(function (field) {

            fieldsObj[field.value] = field;

        });
        component.set("v.fieldsObj", fieldsObj);
    },

    saveCurrentFilter : function (component, event) {

        var currentFilter = component.get("v.currentFilter");
        console.log('currentFilter ===> ' + JSON.stringify(currentFilter));
        var strField = currentFilter.pickListvalue;
        console.log('field string ===> ' + strField);
        var objField = JSON.parse(strField);

        var filters = component.get("v.filters");
        filters[currentFilter.index].field = objField.label;
        filters[currentFilter.index].fieldName = objField.name;
        filters[currentFilter.index].operator = JSON.parse(currentFilter.jsonOperator);
        console.log("currentFilter value ===> " + currentFilter.value);
        filters[currentFilter.index].value = currentFilter.value;




        component.set("v.filters", filters);
        component.set("v.filtersJSON", JSON.stringify(filters));
        console.log("filters JSON ===> " + JSON.stringify(filters));
        component.set("v.currentFilter", null);
        component.set("v.showAddFilter", false);


    },

    editCurrentFilter : function (component, event) {

        var clickedItem = event.currentTarget;
        var filterIndex = clickedItem.dataset.id;

        console.log('index ===> ' + filterIndex);
        var filters = component.get("v.filters");
        var currentfilter = filters[filterIndex];
       // currentfilter.jsonOperator = JSON.stringify(currentfilter.operator);
        console.log('currentfilter.operator ===> ' + currentfilter.jsonOperator);
        console.log('currentFilter edit ===> ' + JSON.stringify(currentfilter));
        component.set("v.currentFilter", currentfilter);
        component.set("v.showAddFilter", true);
    },

    cancelCurrentFilter : function (component, event) {

        var currentFilter = component.get("v.currentFilter");
        var filters = component.get("v.filters");


        console.log('current Filter field name ===>  ' + filters[currentFilter.index].fieldName);
        if(filters[currentFilter.index].fieldName == '') {
            filters.splice(currentFilter.index, 1);
            component.set("v.filters", filters);
        }
        component.set("v.currentFilter", null);
        component.set("v.showAddFilter", false);

    },

    removeCurrentFilter : function (component, event) {

        var currentFilter = component.get("v.currentFilter");
        var filters = component.get("v.filters");


        console.log('current Filter field name ===>  ' + filters[currentFilter.index].fieldName);

        filters.splice(currentFilter.index, 1);
        component.set("v.filters", filters);
        component.set("v.filtersJSON", JSON.stringify(filters));
        component.set("v.currentFilter", null);
        component.set("v.showAddFilter", false);

    },

    showFilterLogic : function (component, event) {

        var showFilterLogic = component.get("v.showFilterLogic");
        this.setDefaultLogic(component);
        showFilterLogic = !showFilterLogic
        component.set("v.showFilterLogic", showFilterLogic);

        if(!showFilterLogic){
            component.set("v.filterLogic", null);
            component.set("v.showSaveLogic", false);
        }

    },

    setDefaultLogic : function (component) {

        var filterLogic = component.get("v.filterLogic");

        if(!filterLogic){
            filterLogic = '';
            var filters = component.get("v.filters");
            var numberOfFilters = filters.length;
            var i = 1;
            filters.forEach(function (filter) {
                filterLogic += i.toString();
                if(i<numberOfFilters){
                    filterLogic += ' AND ';
                }
                i++;
            });
        }

        component.set("v.filterLogic", filterLogic);
    },

    showSaveLogicBtn : function (component, event) {

        component.set("v.showSaveLogic", true);
    },

    saveLogic : function (component, event) {

        var isValid = this.validateFilterLogic(component);
        if(!isValid){
            console.log('**** bad input ****');
            var logicCmp = component.find("filterLogic");
            logicCmp.set("v.validity", {valid:false, badInput:true});
            logicCmp.focus();

        }
        else{
            component.getEvent("evtFilterLogicUpdated").fire();

        }
        console.log('is Valid ===> ' + isValid);

    },

    validateFilterLogic : function (component) {

        var filters = component.get("v.filters");
        var numberOfFilters = filters.length;
        var filterLogic = component.get("v.filterLogic");
        var isValid = true;

        var logicArray = filterLogic.split("");
        var openBrakets = 0;
        var nextValidChar = "";
        logicArray.forEach(function (currentChar) {

            console.log("validation character ===> " + currentChar);

            if(currentChar != " ") {
                console.log("current char is not space");
                if (nextValidChar == "") {
                    console.log("next valid char is not set");
                    switch (currentChar) {
                        case "(":
                            openBrakets++;
                            break;
                        case ")":
                            if (openBrakets > 0) {
                                openBrakets--;
                            }
                            else {
                                isValid = false;
                            }
                            break;
                        case "A":
                        case "a":
                            nextValidChar = "N";
                            break;
                        case "O":
                        case "o":
                            nextValidChar = "R";
                            break;
                        default:
                            console.log("parseInt ===< " + parseInt(currentChar));
                            if(parseInt(currentChar) == NaN || parseInt(currentChar) < 1 || parseInt(currentChar) > numberOfFilters){
                                isValid = false;
                            }
                    }
                }
                else {
                    console.log('valid char is ===> ' + nextValidChar);

                    if(currentChar.toUpperCase() != nextValidChar){
                        isValid = false;
                    }
                    else{
                        if(currentChar.toUpperCase() == "N"){
                            nextValidChar = "D";
                        }
                        if(currentChar.toUpperCase() == "D" || currentChar.toUpperCase() == "R") {
                            nextValidChar = "";
                        }
                    }
                }
            }

        });

        if(openBrakets != 0){

            isValid = false;

        }
        return isValid;

    },

    setCurrentFieldType : function (component, event) {

        var currentFilter = component.get("v.currentFilter");
        var field = JSON.parse(currentFilter.pickListvalue);
        console.log('current filter type ===> ' + field.type);

        console.log('current Filter ===> ' + currentFilter.pickListvalue);
        component.set("v.currentField", field);
        component.set("v.currentType", field.type);
    },

    setSelectedItem : function (component, event) {

        var selectedItem = component.get("v.selectedItem");
        console.log("selected Item ===> " + JSON.stringify(selectedItem));
        var currentFilter = component.get("v.currentFilter");
        currentFilter.value = selectedItem.Id;
        component.set("v.currentFilter", currentFilter);
    },

    setLogicVisibility : function (component, event) {

        var filterLogic = component.get("v.filterLogic");
        console.log('filter logic in set visibility ===> ' + filterLogic);
        var showLogic = false;
        if(filterLogic){
            showLogic = true;
        }
        component.set("v.showFilterLogic", showLogic);
    }

})