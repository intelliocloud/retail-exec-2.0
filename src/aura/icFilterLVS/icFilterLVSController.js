/**
 * Created by François Poirier on 2/23/2018.
 */
({

    handleAddFilterClick : function (component, event, helper) {

        helper.addFilter(component, event);

    },
    handleSaveCurrentFilterClick : function (component, event, helper) {

        helper.saveCurrentFilter(component, event);

    },
    handleCancelCurrentFilterClick : function (component, event, helper) {

        helper.cancelCurrentFilter(component, event);

    },
    handleFilterItemClick : function (component, event, helper) {

        helper.editCurrentFilter(component, event);
    },
    parseFiltersJSON : function (component, event, helper) {

        helper.parseFiltersJSON(component, event);

    },
    handleRemoveCurrentFilterClick : function (component, event, helper) {

        helper.removeCurrentFilter(component, event);

    },

    handleShowFilterLogicClick : function (component, event, helper) {

        helper.showFilterLogic(component, event);

    },

    handleLogicChange : function (component, event, helper) {

        helper.showSaveLogicBtn(component, event);

    },

    handleSaveLogicClick : function (component, event, helper) {

        helper.saveLogic(component, event);

    },

    handlefilterFieldChange : function (component, event, helper) {

        helper.setCurrentFieldType(component, event);

    },

    handleSelectedItemChange : function (component, event, helper) {

        helper.setSelectedItem(component, event);

    },

    handleFilterLogicChange : function (component, event, helper) {

        helper.setLogicVisibility(component, event);

    }

})