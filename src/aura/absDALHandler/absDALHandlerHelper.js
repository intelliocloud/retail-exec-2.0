/* Created Date : July 21th, 2016 - Incloud (Lucas Ennouchi)
 * Last Modified Date : July 21th, 2016 - Incloud (Lucas Ennouchi)
 * Note : for now, the function is hard coded to only use the first dal component.
 */
({
  callServerAction : function(component,event,actionName,serverParams,clientParams) {
    var dal = component.get("v.dal")[0];
   // console.log('dal ===> ' + dal);
 /*   console.log('in callserveraction in absDALHandler');
    console.log('action name ===> ' + actionName);
    console.log('serverParams ===> ' + JSON.stringify(serverParams));
      console.log('clientParams ===> ' + JSON.stringify(clientParams));*/
    dal.callServerAction(actionName,serverParams,clientParams);
  }
})