({
    drag : function(component,event) {
        //console.log(event);
        var value = component.get("v.value");
        var displayValue = component.get("v.displayValue");
        //console.log('value ===> ' + value + ' display Value ===> ' + displayValue);
        
        event.dataTransfer.setData("value",JSON.stringify(
            {
                value: value,
                displayValue: displayValue
            }
        ));
    }
})