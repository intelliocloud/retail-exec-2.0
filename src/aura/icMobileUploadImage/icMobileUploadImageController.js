({
    doInit : function(component, event, helper){
        var action = component.get("c.uploadImageMobile");
        action.setParams({"parentId" : component.get("v.recordId")});

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                component.set("v.item", response.getReturnValue());
            }else{
                console.log("problem getting storevisit");
            }
        });
        $A.enqueueAction(action);
    },
    handleUploadFinished : function (component, event, helper){
    	var uplAction = component.get("c.getProfilePicture");
    	uplAction.setParams({"parentId" : component.get("v.recordId")});

    	$A.enqueueAction(uplAction);
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    },
    handleCloseClick : function (component, event){
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    }
})