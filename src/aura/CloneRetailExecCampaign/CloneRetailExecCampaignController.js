/**
 * Created by georg on 2018-02-15.
 */
({
    doInit: function (component, event, helper) {
        helper.doInit(component, event);
    },
    goCloning: function (component, event, helper) {
        helper.goCloning(component, event);
    }
}) 