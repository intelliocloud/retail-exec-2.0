({
    doInit: function (component, event) {
        var action = this.callServerAction(component, event, "recComponentInit", {recId : component.get("v.recordId")},
            {
                onSuccess: function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        component.set("v.newREC", response.getReturnValue().recRecord);
                        component.set("v.targetRoleOptions", response.getReturnValue().targetRoleOptions);
                        component.set("v.statusOptions", response.getReturnValue().statusOptions);
                        component.set("v.priorityChildren", response.getReturnValue().priorityChildren);
                    }
                }
            });
        $A.enqueueAction(action);
    },
    goCloning: function (component, event) {
        var CloneSuccessMessage = $A.get("$Label.c.CloneSuccessMessage");
        var CloneErrorMessage = $A.get("$Label.c.CloneErrorMessage");
        var SuccessString = $A.get("$Label.c.SuccessString");
        var ErrorString = $A.get("$Label.c.ErrorString");

        var action = component.get("c.createREC");
        action.setParams({
            "newRec": component.get("v.newREC"),
            "priorityList": component.get("v.priorityChildren"),
            "targetRoleOption": component.find("TargetRole").get("v.value"),
            "statusOption": component.find("Status").get("v.value")
        });

        action.setCallback(this, function(response){
            var state = response.getState();
            if (state == "SUCCESS"){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: SuccessString,
                    message: CloneSuccessMessage,
                    type: "success"
                });
                toastEvent.fire();

                console.log("SUCCESS!!");
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title: ErrorString,
                    message: CloneErrorMessage,
                    type: "error"
                });
                toastEvent.fire();

                console.log("There was an error when cloning!!");
            }
        });
        $A.enqueueAction(action);
    }
})