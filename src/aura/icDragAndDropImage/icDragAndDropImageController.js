({
    onInit: function(component) {
        var action = component.get("c.getProfilePicture"); 
        action.setParams({
            parentId: component.get("v.recordId"),
        });
        action.setCallback(this, function(a) {
            var attachment = a.getReturnValue();
            console.log(attachment);
            if (attachment && attachment.Id) {
	            component.set('v.pictureSrc', '/sfc/servlet.shepherd/version/download/'
                                                  + attachment.Id);
            }
        });
        $A.enqueueAction(action);

        var typeaction = component.get("c.getObjectTypeLabel");
        typeaction.setParams({
            parentId:component.get("v.recordId"),
        });
        typeaction.setCallback(this, function(e){
            var objectTypes = e.getReturnValue();
            component.set('v.objectTypes',objectTypes);
            console.log('*the objecttype is ' + objectTypes);
        });
        $A.enqueueAction(typeaction);
    },
    
    onDragOver: function(component, event) {
        event.preventDefault();
    },

    onDrop: function(component, event, helper) {
		event.stopPropagation();
        event.preventDefault();
        event.dataTransfer.dropEffect = 'copy';
        var files = event.dataTransfer.files;
        if (files.length>1) {
            return alert($A.get("$Label.c.Youcanonlyuploadoneimage"));
        }
        helper.readFile(component, helper, files[0]);
	}
})