/**
 * Created by François Poirier on 12/21/2017.
 */
({
    save : function (component, event) {

        var myObject = {};
        //var myObjects = [];
        var myAttributes = {};
        myAttributes.type = component.get("v.sObjectType");
        var id = component.get("v.id");
        var value = component.get("v.value");
        var fieldName = component.get("v.apiName");

        myObject.Attributes = myAttributes;
        myObject.id = id;
        myObject[fieldName] = value;
        var myStringObject = JSON.stringify(myObject);
        //myObjects.push(myObject);

        console.log('myStringObject ===> ' + myStringObject);

        var action = this.callServerAction(component, event, "updateValue",
            {obj : myStringObject}, {onSuccess : function (response){
                console.log('response ===> ' + JSON.stringify(response));
                }
        });

        $A.enqueueAction(action);

    },

    updateBooleanValue : function (component, event) {

        var type = component.get("v.fieldType");
        if(type === 'BOOLEAN') {
            var stringValue = component.get("v.value");
            var booleanValue = false;


            if (stringValue.toUpperCase() === 'TRUE') {
                booleanValue = true;
            }

            component.set("v.checked", booleanValue);
        }
    },

    handleChangeCheckbox : function (component, event) {

        var bValue = component.get("v.checked");
        if(bValue != null && bValue != undefined) {
            component.set("v.value", bValue);
        }
        //this.save(component, event);
    }
})