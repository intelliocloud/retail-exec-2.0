/**
 * Created by François Poirier on 12/21/2017.
 */
({
    handleChange : function (component, event, helper) {

        console.log('in save');
        helper.save(component, event);

    },

    handleChangeCheckbox : function (component, event, helper) {

        helper.handleChangeCheckbox(component, event);

    },

    updateBooleanValue : function (component, event, helper) {

        helper.updateBooleanValue(component, event);

    }
})