/**
 * Created by François Poirier on 1/26/2018.
 */
({
    init : function (component, event) {
console.log('in init');

        var helper = this;
        var action = this.callServerAction(component, event, "getVisitSchedule",
            {
                startDate : ""
            },
            {
                onSuccess: function (response) {
                var result = response.getReturnValue();
                console.log("result ===> " +  JSON.stringify(result));
                component.set("v.dates", result.dates);
                component.set("v.hours", result.hours);
                component.set("v.events", result.visits);
                /*component.set("v.filterFieldSet", result.filterFieldSet);
                component.set("v.tableFieldSet", result.tableFieldSet);
                component.set("v.accounts", result.accountRecords);
                component.set("v.filteredAccounts", result.accountRecords);
                helper.setHeaderValues(component, event);*/
            }});
        $A.enqueueAction(action);

    },

    setHeaderValues : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSet");
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });
        console.log('header Values ===> ' + headerValues);
        component.set("v.headerValues", headerValues);
    },

    toggleFilter : function (component, event) {

        var filterIsExpended = component.get("v.filterIsExpended");
        component.set("v.filterIsExpended", !filterIsExpended);

    },

    toggleGrid : function (component, event) {

        var gridIsExpended = component.get("v.gridIsExpended");
        component.set("v.gridIsExpended", !gridIsExpended);

    },

    createStoreVisit : function (component, event) {

        var params = event.getParams();
        console.log('params ===> ' + JSON.stringify(params));
        var action = this.callServerAction(component, event, "createStoreVisit",
            {
                accountId : params.id,
                mois : params.mois.toString(),
                jour : params.jour.toString(),
                an : params.an.toString(),
                heure : params.heure.toString()
            },
            {
                onSuccess: function (response) {

                    console.log('we are back from visit creation!');
                    var results = response.getReturnValue();
                    component.set("v.events", results.visits);
                }
            }
            );

        $A.enqueueAction(action);
    },

    createVisitFromClick : function (component, event) {

        var accounts = component.get("v.accounts");
        var accountId;

        for(var i = 0; i<accounts.length; i++){

            if(accounts[i].selected){
                accountId = accounts[i].id
                break;
            }
        }

        if(accountId) {
            var params = event.getParams();
            console.log('params ===> ' + JSON.stringify(params));
            var action = this.callServerAction(component, event, "createStoreVisit",
                {
                    accountId: accountId,
                    mois: params.mois.toString(),
                    jour: params.jour.toString(),
                    an: params.an.toString(),
                    heure: params.heure.toString()
                },
                {
                    onSuccess: function (response) {

                        console.log('we are back from visit creation!');
                        var results = response.getReturnValue();
                        component.set("v.events", results.visits);
                    }
                }
            );

            $A.enqueueAction(action);
        }
    },

    showNextWeek : function (component, event) {
        console.log('in shownextweek');

        var dates = component.get("v.dates");
        var startDate = new Date(dates[dates.length - 1].dayDate);
        var offset = startDate.getTimezoneOffset();
        startDate = new Date(startDate.getTime() + offset*60000);
        console.log('last date without manip ===> ' + startDate);
        startDate.setDate(startDate.getDate()+1);
        console.log('last date after add 1 day ===> ' + startDate);
        var adjustedDate = new Date(startDate.getTime() + offset*60000);
        console.log('last date after add offset ===> ' + startDate);
        var year = startDate.getFullYear().toString();
        var month = (startDate.getMonth()+1).toString();
        var day = startDate.getDate().toString();
        var strDate = year + "-" + month + "-" + day;

        console.log('last date after manip ===> ' + strDate);

        var helper = this;
        var action = this.callServerAction(component, event, "getVisitSchedule",
            {
                startDate : strDate
            },
            {
                onSuccess: function (response) {
                    var result = response.getReturnValue();
                    console.log("result ===> " +  JSON.stringify(result));
                    component.set("v.dates", result.dates);
                    component.set("v.hours", result.hours);
                    component.set("v.events", result.visits);
                    /*component.set("v.filterFieldSet", result.filterFieldSet);
                    component.set("v.tableFieldSet", result.tableFieldSet);
                    component.set("v.accounts", result.accountRecords);
                    component.set("v.filteredAccounts", result.accountRecords);
                    helper.setHeaderValues(component, event);*/
                }});
        $A.enqueueAction(action);

    },

    showPreviousWeek : function (component, event) {
        console.log('in previous week');
        var dates = component.get("v.dates");
        var startDate = new Date(dates[0].dayDate);
        var offset = startDate.getTimezoneOffset();
        startDate = new Date(startDate.getTime() + offset*60000);
        console.log('first date without manip ===> ' + startDate);
        startDate.setDate(startDate.getDate()+1);
        console.log('first date after remove 7 day ===> ' + startDate);
        var adjustedDate = new Date(startDate.getTime() + offset*60000);
        console.log('first date after add offset ===> ' + startDate);
        var year = startDate.getFullYear().toString();
        var month = (startDate.getMonth()+1).toString();
        var day = startDate.getDate().toString();
        var strDate = year + "-" + month + "-" + day;

        console.log('first date after manip ===> ' + strDate);



        var helper = this;
        var action = this.callServerAction(component, event, "getVisitSchedule",
            {
                startDate : strDate
            },
            {
                onSuccess: function (response) {
                    var result = response.getReturnValue();
                    console.log("result ===> " +  JSON.stringify(result));
                    component.set("v.dates", result.dates);
                    component.set("v.hours", result.hours);
                    component.set("v.events", result.visits);
                    /*component.set("v.filterFieldSet", result.filterFieldSet);
                    component.set("v.tableFieldSet", result.tableFieldSet);
                    component.set("v.accounts", result.accountRecords);
                    component.set("v.filteredAccounts", result.accountRecords);
                    helper.setHeaderValues(component, event);*/
                }});
        $A.enqueueAction(action);

    }


})