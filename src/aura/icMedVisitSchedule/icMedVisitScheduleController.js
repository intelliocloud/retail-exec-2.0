/**
 * Created by François Poirier on 1/26/2018.
 */
({
    doInit : function (component, event, helper) {

        helper.init(component, event);

    },
    toggleFilter : function (component, event, helper) {

        helper.toggleFilter(component, event);

    },
    toggleGrid : function (component, event, helper) {

        helper.toggleGrid(component, event);

    },
    handleCellItemDrop : function (component, event, helper) {

        helper.createStoreVisit(component, event);
    },
    handleCellClick : function (component, event, helper) {

        helper.createVisitFromClick(component, event);

    },
    showNextWeek : function (component, event, helper) {

        helper.showNextWeek(component, event, helper);

    },
    showPreviousWeek : function (component, event, helper) {

        helper.showPreviousWeek(component, event);

    }
})