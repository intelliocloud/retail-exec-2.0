({
  
    dragOver: function(component, event){
        event.preventDefault();
    },

    drop: function(component, event){
    
        var parameterValue = JSON.parse(event.dataTransfer.getData("value")); //component.get("v.hiddenValue");
        var parentId = component.get("v.parentId");

        if(parameterValue.value != parentId){
            component.set("v.displayValue", parameterValue.displayValue);
            //console.log("displayValue ===> " +  parameterValue.displayValue);
            //console.log("value ===> " +  parameterValue.value);
            //console.log("parentId ===> " + parentId);
            component.set("v.value",  parameterValue.value);
            component.getEvent("itmDrop").fire();
        }
        else
        {
            alert("This item cannot be dropped here");
        }
    },
    clear : function(component, event){
        component.set("v.value", null);
        component.set("v.displayValue", "");
        component.getEvent("dropCleared").fire();
    }
})