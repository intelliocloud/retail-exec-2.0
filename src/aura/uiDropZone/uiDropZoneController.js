({

    handleOnDragOver: function(component, event, helper){
        helper.dragOver(component, event);
    },
    handleOnDrop : function(component, event, helper){
        helper.drop(component, event);
    },
    handleClear : function(component, event, helper){
        helper.clear(component, event);
    }
})