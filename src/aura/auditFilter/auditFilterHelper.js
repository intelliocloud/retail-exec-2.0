({
	init : function(component, event) {

		this.setFilterFields(component, event);
	},

	setFilterFields : function (component, event) {

		var filterSet = component.get("v.filterFields");
		//console.log('filterSet ===> ' + JSON.stringify(filterSet));
		var items = component.get("v.items");
		//console.log('items ===> ' + JSON.stringify(items));
		var filterOptions = {};

		filterSet.forEach(function(filter){

			var tmpOptions = [];

			if(filter.type === 'BOOLEAN'){

				tmpOptions.push($A.get("$Label.c.True").toLowerCase());
				tmpOptions.push($A.get("$Label.c.False").toLowerCase());
			}
			else {

                items.forEach(function (item) {


                    item.fields.forEach(function (field) {
                        //console.log('field.apiName ===> ' + field.apiName);
                        //console.log('filter.apiName ===> ' + filter.apiName);


                        if (field.apiName === filter.apiName) {
                            if (tmpOptions.indexOf(field.value) === -1) {
                                tmpOptions.push(field.value);
                            }
                        }

                    });
                });

            }

            filter.options = tmpOptions;
		});


		component.set("v.filterFields", filterSet);
    },

	setFilters : function (component, event) {

        var filters = component.get("v.filterFields");
        //var field = event.target.name;
        var field = event.getSource().get("v.name");
        console.log('field ===> ' + field);
        var value = event.target.value;
        console.log('value ===> ' + value);


        filters.forEach(function (filter) {
            if(filter.apiName === field){
                filter.value = value;
            }
        });


        component.set("v.filterFields", filters);
		this.filter(component, event);
    },

	filter : function (component, event) {

		console.log('in filter ');
		var items = component.get("v.items");
		console.log('items ===> ' + JSON.stringify(items));
		var filters = component.get("v.filterFields");

		//console.log('**** 1 *****');
		var filteredItems = [];
		items.forEach(function(item){
		    item.selected = false;
			filteredItems.push(item);
		});
        //console.log('**** 2 *****');
		filters.forEach(function(filter){

			var i=0;
			if(filter.value) {

				console.log('filter apiName ===> ' + filter.apiName);
				for(i=0;i<filteredItems.length;i++) {

					filteredItems[i].fields.forEach(function (field) {

						if(field.apiName === filter.apiName){
							if(field.value.toString() !== filter.value.toString()){
								filteredItems.splice(i, 1);
								i--;

							}
						}
					})
                }
            }

		});
       // console.log('**** 3 *****');
		console.log('filtered items ===> ' + JSON.stringify(filteredItems));
		component.set("v.filteredItems", filteredItems);

    },

    clearFilters : function (component, event) {

        var filters = component.get("v.filterFields");
        var filterForm = component.find("filterForm");

        filters.forEach(function(filter){
        	filter.value = null;
		});

        filterForm.forEach(function(element){

        	element.set("v.value", null);
		});

        component.set("v.filterFields", filters);
        this.filter(component, event);

    }

})