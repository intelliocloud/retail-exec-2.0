({
	doInit : function (component, event, helper) {

		helper.init(component, event);

    },
	updateFilter : function(component, event, helper) {
		/*var filter = component.get("v.filter");
		if(!filter || filter=='') filter = {};
		filter[event.target.id] = event.target.value;
		component.set("v.filter",filter);*/
		helper.setFilters(component, event);
	},
	clearFilters : function (component, event, helper) {

		helper.clearFilters(component, event);
    }
})