/**
 * Created by François Poirier on 9/20/2017.
 */
({
    doInit : function (component, event, helper) {
        helper.init(component, event);
    },

    onblurHandler : function (component, event, helper) {
        helper.save(component, event);
    },

    handleChange : function (component, event, helper) {
        helper.updateRadioAnswer(component, event);
    },
    handleMenuSelect : function (component, event, helper) {

        helper.updateRadioAnswer(component, event);
    },
    handleClick : function(component, event, helper){
        helper.handleClick(component, event);
    }
})