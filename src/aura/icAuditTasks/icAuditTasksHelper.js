/**
 * Created by François Poirier on 9/20/2017.
 */
({
    init : function (component, event) {
        var helper = this;
        var action = this.callServerAction(component, event, "getAuditTaskScreen",
            {storeVisitId: component.get("v.recordId")}, {
                onSuccess: function (response) {
                    var result = response.getReturnValue();
                    console.log("result in callback", JSON.stringify(result));
                    helper.parseChoices(result);
                    component.set("v.auditTaskScreen",result);
                }
            });
        console.log('before enqueue');
        $A.enqueueAction(action);

    },
    
    parseChoices : function (auditTaskScreen) {

        auditTaskScreen.campaigns.forEach(function (campaign){

            campaign.auditTasks.forEach(function (at) {
                if(at.answerType == "Multiple Choice"){
                    var choices = [];

                    choices = at.stringChoices.split(",");
                    at.choices = [];
                    var emptyChoice = {};
                    emptyChoice.label = '';
                    emptyChoice.value = '';
                    at.choices.push(emptyChoice);
                    //console.log(at.stringChoices);
                    choices.forEach(function(choice){
                      //  console.log('choice ===> ' + choice);
                        var tmpChoice = {};
                        tmpChoice.label = choice;
                        tmpChoice.value = choice;
                        at.choices.push(tmpChoice);
                    });

                    console.log(at.choices);
                }
            });

        });
    },

    save : function (component, event) {

       var auditTaskScreen = component.get("v.auditTaskScreen");

        auditTaskScreen.campaigns.forEach(function(campaign){


            campaign.auditTasks.forEach(function(at){
                console.log('at.Name ===> ' + at.name);
                console.log('at.rtxValue__c ===> ' + at.value);

                if(at.value){
                    at.done = true;
                }
                else {
                   at.done = false;
               }
            });

        });

        component.set("v.auditTaskScreen", auditTaskScreen);

        var strAuditTaskScreen = JSON.stringify(auditTaskScreen);

        var action = this.callServerAction(component, event, "saveAuditTasks",
            {auditTaskScreen: strAuditTaskScreen},
            {
                onSuccess: function(response){

                }
            }
            );
        $A.enqueueAction(action);
    },

    updateRadioAnswer : function (component, event) {

        console.log('event source value ===> ' + event.getSource().get("v.value"));
        var value = event.getSource().get("v.value");
        var index = event.getSource().get("v.name");
        var indexes = [];
        indexes = index.split(':');
        var campIndex = indexes[0];
        var atIndex = indexes[1];

        console.log('index ===> ' + index);
        var auditTaskScreen = component.get("v.auditTaskScreen");

        console.log(' auditTasks[index].rtxValue__c before ===> ' +  auditTaskScreen.campaigns[campIndex].auditTasks[atIndex].value);


        auditTaskScreen.campaigns[campIndex].auditTasks[atIndex].value = value;
        console.log(' auditTasks[index].rtxValue__c after ===> ' +  auditTaskScreen.campaigns[campIndex].auditTasks[atIndex].value);
        component.set("v.auditTaskScreen", auditTaskScreen);

        this.save(component, event);

    },
    handleClick : function (component, event) {
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    }

})