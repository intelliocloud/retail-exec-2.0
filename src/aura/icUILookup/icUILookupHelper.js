({
	doSearch: function(component) {

		var objectType = component.get("v.objectType");
		var objectFields = component.get("v.objectFields");
		var searchCriteria = component.get("v.searchCriteria");
		var uiListInput = component.find("uiListInput");

		var validSearch = true;

		//clear any errors
		uiListInput.set("v.errors", null);

		//make sure search term entered
		if ($A.util.isEmpty(searchCriteria)){
			validSearch = false;
			uiListInput.set("v.errors", [{message:"Search term can't be blank."}]);			
		} else {
			// make sure search term has at least 2 caracters
			if (searchCriteria.length < 2){
				validSearch = false;
				uiListInput.set("v.errors", [{message:"Search term must be at least 2 caracters."}]);
			}
		}
		
		if(validSearch) {
			var action = component.get("c.getItems");
			action.setAbortable();
			action.setParams({"searchObject": objectType, "searchFields":objectFields, "searchCriteria":searchCriteria});

			action.setCallback(this, function(response) {
				var state = response.getState();
				if (component.isValid() && state === "SUCCESS") {
					// Get the search matches
                	var matches = response.getReturnValue();
					component.set("v.items", matches);

					var spanResultInfo = component.find('spanResultInfo').getElement();
					var objectNamePlural = component.get("v.objectNamePlural");
					var searchCriteria = component.get("v.searchCriteria");
					if(matches.length > 0) {
						spanResultInfo.innerHTML = "Searching for " + objectNamePlural + " with &quot;" + searchCriteria + "&quot;";
					} else {
						spanResultInfo.innerHTML = "No results for " + objectNamePlural + " with &quot;" + searchCriteria + "&quot;";
					}

					var uiListContainer = component.find('uiListContainer');
					$A.util.addClass(uiListContainer, 'slds-is-open');
				}
				else // Handle any error by reporting it
				{
					var errors = response.getError();

					if (errors) {
						if (errors[0] && errors[0].message) {
							this.displayErrorToast(errors[0].message);
						}
					} else {
						this.displayErrorToast('Unknown error.');
					}

					var uiListContainer = component.find('uiListContainer');
					$A.util.removeClass(uiListContainer, 'slds-is-open');
				}
			});

			// Send action off to be executed
			$A.enqueueAction(action);
		} else {
			var uiListContainer = component.find('uiListContainer');
			$A.util.removeClass(uiListContainer, 'slds-is-open');
		}
	},

	doSelect : function(component, event) {
		var selectedItem = component.get("v.selectedItem");
		var objectLabelField = component.get("v.objectLabelField");
		var objectValueField = component.get("v.objectValueField");

		if(selectedItem != null) {
			component.set("v.searchCriteria", selectedItem[objectLabelField]);

			var uiListContainer = component.find('uiListContainer');
			$A.util.removeClass(uiListContainer, 'slds-is-open');
			$A.util.addClass(uiListContainer, 'slds-has-selection');

			// Show the selected element
			var selectedSection = component.find("selectedSection");
			$A.util.removeClass(selectedSection, 'slds-hide');
		}
	},

	doClear : function(component, event) {

		component.set("v.searchCriteria", '');

		var uiListContainer = component.find('uiListContainer');
		$A.util.removeClass(uiListContainer, 'slds-is-open');
		$A.util.removeClass(uiListContainer, 'slds-has-selection');

		// Show the selected element
		var selectedSection = component.find("selectedSection");
		$A.util.addClass(selectedSection, 'slds-hide');
	},

	displayErrorToast: function (message) {
		var toast = $A.get("e.force:showToast");

		// For lightning1 show the toast
		if (toast) {

			//fire the toast event in Salesforce1
			toast.setParams({
				"type":"error",
				"title": "Error",
				"message": message,
				"delay":3
			});

			toast.fire();
		} else  { // otherwise throw an alert
			alert(title + ': ' + message);
		}
	}
})