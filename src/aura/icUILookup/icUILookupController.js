({
	doSearch: function(component, event, helper) {
		helper.doSearch(component);
	},

	doSelect: function(component, event, helper) {
		helper.doSelect(component, event);
	},

	doClear: function(component, event, helper) {
		helper.doClear(component, event);
	}
})