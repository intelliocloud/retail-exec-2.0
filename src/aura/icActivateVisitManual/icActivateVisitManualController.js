({
	doInit : function(component, event, helper){
        var action = component.get("c.getStoreVisit");
        action.setParams({"storevisitId" : component.get("v.recordId")});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state == "SUCCESS"){
                component.set("v.storevisit", response.getReturnValue());
            }else{
                console.log("problem getting storevisit");
            }
        });
        $A.enqueueAction(action);
    },
    requestCall : function(component, event, helper){
        var updateCall = component.get("c.updateStoreVisitRecord");
        updateCall.setParams({
            "sc" : component.get("v.storevisit")
        });
        updateCall.setCallback(this, function(response){
            var state = response.getState();
            $A.get("e.force:closeQuickAction").fire();
            $A.get("e.force:refreshView").fire();
            /*if(state === "SUCCESS"){
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title" : "Call requested",
                    "message" : "A call has been requested."
                });
 
                //Update the UI: closePanel, show toast, refresh page
                $A.get("e.force:closeQuickAction").fire();
                resultsToast.fire();
                $A.get("e.force:refreshView").fire();
            }else if(state === "ERROR"){
                console.log('Problem updating call, response state '+state);
            }else{
                console.log('Unknown problem: '+state);
            }*/
        });
        //send the request to updateCall
        $A.enqueueAction(updateCall);

    },
    cancelCall : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    }
})