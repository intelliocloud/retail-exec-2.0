({
    doInit : function(component, event)
    {

        var recordId = component.get("v.recordId");
        var helper = this;
    	var action = this.callServerAction(component, event, "getAuditProducts",
			{storeVisitId: recordId}, {
    		onSuccess: function (response){
    			var result = response.getReturnValue();
                //console.log("result ===> " +  JSON.stringify(result));
                component.set("v.filterFieldSet", result.filterFieldSet);
                component.set("v.tableFieldSet", result.tableFieldSet);
                component.set("v.auditProducts",result.records);
                //console.log('records ===> ' + JSON.stringify(result.records));
                console.log('records set');
                component.set("v.filteredAuditProducts", result.records);
                helper.setHeaderValues(component, event);
    		}});
        $A.enqueueAction(action);
    },

    setHeaderValues : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSet");
        var headerValues = [];

        tableFieldSet.forEach(function(field){
            if (field.hidden === false){

                headerValues.push(field.label);

            }
        });

        component.set("v.headerValues", headerValues);
    },

    /*saveSelectedRows : function(component, event) {
        console.log("saveSelectedRows");
        var form = event.getSource();
        var ids = component.get("v.selectedAuditProducts");
        var nf = form.find("nf").get("v.value");
        console.log('nf ===> ' + nf);
        var price = '';
        var oos = form.find("oos").get("v.checked");
        var exc = form.find("exception").get("v.checked");
        var done = form.find("done").get("v.checked");

        var auditProducts = component.get("v.auditProducts");

        auditProducts.forEach(function(auditProduct){
           if(auditProduct.selected){
              console.log('auditProduct before ===> ' + JSON.stringify(auditProduct));
               auditProduct.rtxNF__c = nf ? nf : auditProduct.rtxNF__c;
               auditProduct.rtxOOS__c = oos ? oos : auditProduct.rtxOOS__c;
               auditProduct.rtxException__c = exc ? exc : auditProduct.rtxException__c;
               auditProduct.rtxDone__c = done ? done : auditProduct.rtxDone__c;
               console.log('auditProduct after ===> ' + JSON.stringify(auditProduct));
           }
        });

        component.set("v.auditProducts", auditProducts);

        var action = this.callServerAction(component, event, "saveAuditProducts",
            {auditProducts : auditProducts}, {});
        $A.enqueueAction(action);
    },

    updateSelectedRows : function(component, event) {
        var selected = event.getParam("selected");
        var selectedAuditProducts = component.get("v.selectedAuditProducts");
        var itemId = event.getSource().get("v.itemId");
        var index = selectedAuditProducts.indexOf(itemId)

        if(selected) selectedAuditProducts.push(itemId);
        else if(index>-1) selectedAuditProducts.splice(index,1);
        console.debug("selectedAuditProducts",selectedAuditProducts);
        component.set("v.selectedAuditProducts",selectedAuditProducts);
    },

    showAuditProductDetails : function(component, event) {
        component.find("details").set("v.recordId",event.getParam("recordId"));
        $A.util.addClass(component.find("slide-1"),'slider_prev');
        $A.util.removeClass(component.find("slide-2"),'slider_next');
    },

    hideAuditProductDetails : function(component, event, helper) {
        component.find("details").set("v.recordId",null);
        $A.util.removeClass(component.find("slide-1"),'slider_prev');
        $A.util.addClass(component.find("slide-2"),'slider_next');
    },*/

    toggleRowsSelection : function(component, event) {
        console.log(event.target);
        var rows = component.find("listAuditProducts").find("bodyRow");
        var selectedAuditProducts = [];
        if($A.util.hasClass(component.find("btn-select-rows"),'slds-not-selected')) {
            for(var i=0;i<rows.length;i++) {
                rows[i].set("v.selected",true);
                selectedAuditProducts.push(rows[i].get("v.itemId"));
            }
            $A.util.removeClass(component.find("btn-select-rows"),'slds-not-selected');
            $A.util.addClass(component.find("btn-select-rows"),'slds-is-selected');
        } else {
            for(var i=0;i<rows.length;i++) {
                rows[i].set("v.selected",false);
            }
            $A.util.addClass(component.find("btn-select-rows"),'slds-not-selected');
            $A.util.removeClass(component.find("btn-select-rows"),'slds-is-selected');
        }
        component.set("v.selectedAuditProducts",selectedAuditProducts);

    },

    toggleFilter : function (component, event) {

        var filterIsExpended = component.get("v.filterIsExpended");
        console.log('filterIsExpended before click ===> ' + filterIsExpended);
        component.set("v.filterIsExpended", !filterIsExpended);
        filterIsExpended = component.get("v.filterIsExpended");
        console.log('filterIsExpended before click ===> ' + filterIsExpended);

    },

    toggleForm : function (component, event) {

        var formIsExpended = component.get("v.formIsExpended");
        component.set("v.formIsExpended", !formIsExpended);

    },
    handleClick : function (component, event) {
        var wasDismissed = $A.get("e.force:closeQuickAction").fire();
    }

})