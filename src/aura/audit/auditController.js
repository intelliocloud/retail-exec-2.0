({
	doInit : function(component, event, helper) {

		helper.doInit(component, event);

	},
	saveSelectedRows : function(component, event, helper) {

		helper.saveSelectedRows(component, event);

	},

	updateSelectedRows : function(component, event, helper) {

		helper.updateSelectedRows(component, event);

	},

	showAuditProductDetails : function(component, event, helper) {

		helper.showAuditProductDetails(component, event);

	},

	hideAuditProductDetails : function(component, event, helper) {

		helper.hideAuditProductDetails(component, event);

	},

	toggleRowsSelection : function(component, event, helper) {

		helper.toggleRowsSelection(component, event);

	},
    toggleFilter : function (component, event, helper) {
        helper.toggleFilter(component, event);
    },
    toggleForm : function (component, event, helper) {
        helper.toggleForm(component, event);
    },
    handleClick : function(component, event, helper){
        helper.handleClick(component, event);
    }
})