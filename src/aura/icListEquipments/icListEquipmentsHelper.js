({
    toggleSelectAll : function (component, event) {
        console.log('in toggleSelectAll');
        var allSelected = component.get("v.allSelected");
        // allSelected = !allSelected;
        var items = component.get("v.items");

        items.forEach(function(item){
            item.selected = allSelected;
            //console.log('item ===> ' + JSON.stringify(item));
        });

        component.set("v.items", items);
    },

    setSizeClass : function (component, event) {

        var numberOfCols = 0;
        var headerValues = [];
        headerValues = component.get("v.headerValues");
        var sldsSizeClass = $A.get("$Label.c.icDataGridSizeClass");

        console.log('header values ===> ' + headerValues);
        numberOfCols = headerValues.length;
        console.log('number of Cols before adjustments ===> ' + numberOfCols);
        if(component.get("v.selectable")){
            numberOfCols++;
        }

        if(component.get("v.showDetailButton")){
            numberOfCols++;
        }

        if(numberOfCols > 8){
            numberOfCols = 12;
        }

        console.log('number of Cols after adjustments ===> ' + numberOfCols);

        sldsSizeClass = sldsSizeClass + numberOfCols;
        console.log('sldsSizeClass ===> ' + sldsSizeClass);

        component.set("v.sldsSizeClass", sldsSizeClass);

    },

    unselectAll : function (component, event) {


        var items = component.get("v.items");
        var allSelected = true;

        items.forEach(function (item) {

            if(!item.selected){
                allSelected = false;
            }
        });

        component.set("v.allSelected", allSelected);

    }
})