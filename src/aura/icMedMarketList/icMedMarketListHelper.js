/**
 * Created by François Poirier on 2/15/2018.
 */
({
    doInit: function (component, event) {
        console.log('in init medMaketList Helper');
        var helper = this;
        var action = this.callServerAction(component, event, "getMarketListById",
            {mlId: component.get("v.recordId")},
            {
                onSuccess: function (response) {
                    var results = response.getReturnValue();
                    console.log('Back from backend at ' + Date.now());
                    console.log('filters JSON in init ===> ' + results.filtersJSON);
                    component.set("v.filtersLogic", results.filtersLogic);
                    component.set("v.filtersJSON", results.filtersJSON);
                    component.set("v.fields", results.fields);
                    component.set("v.operators", results.operators);
                    component.set("v.tableFieldSet", results.tableFieldSet);
                    component.set("v.currentPage", 1);

                    var currentItems = results.records.slice(0, 50);
                    console.log('this is the current items ===> ' + JSON.stringify(currentItems));
                    component.set("v.filteredItems", currentItems);
                    var testLen = component.get("v.filteredItems");
                    component.set("v.items", results.records);
                    console.log('number of records v2 ===> ' + testLen.length);
                    helper.setHeaderValues(component, event);
                    var spinner = component.find("MLSpinner");
                    $A.util.toggleClass(spinner, "slds-hide");
                }
            });
        console.log('Enqueueing action at ' + Date.now());
        $A.enqueueAction(action);
    },

    setHeaderValues : function (component, event) {

        var tableFieldSet = component.get("v.tableFieldSet");
        var headerValues = [];

        tableFieldSet.forEach(function(field){

            headerValues.push(field.label);

        });

        component.set("v.headerValues", headerValues);
    },

    saveFilters : function (component, event) {

        var spinner = component.find("MLSpinner");
        $A.util.toggleClass(spinner, "slds-hide");
        console.log('filters logic in save filters ===> ' + component.get("v.filtersLogic"));
        var helper = this;
        var action = this.callServerAction(component, event, "saveMarketListFilters",
            {
                mlId: component.get("v.recordId"),
                filters: component.get("v.filtersJSON"),
                logic: component.get("v.filtersLogic")
            },
            {
                onSuccess: function (response) {
                    var results = response.getReturnValue();
                    /* component.set("v.items", results.records);
                     component.set("v.filteredItems", results.records);*/
                    var currentItems = results.records.slice(0, 50);

                    component.set("v.filteredItems", currentItems);
                    var testLen = component.get("v.filteredItems");
                    component.set("v.items", results.records);
                    component.set("v.currentPage", 1);
                    helper.setPageNavigation(component, event);
                    console.log('number of records after save v2 ===> ' + testLen.length);
                    console.log('number of total records ===> ' + results.records.length);
                    var spinner = component.find("MLSpinner");
                    $A.util.toggleClass(spinner, "slds-hide");
                    console.log('end ' + Date.now());
                }
            });
        $A.enqueueAction(action);
    },

    showNextPage : function (component, event) {

        var currentPage = component.get("v.currentPage");
        var begin = currentPage * 50;
        var end = begin + 50;

        var records = component.get("v.items");
        var currentRecords = records.slice(begin, end);
        currentPage ++;
        component.set("v.currentPage", currentPage);
        component.set("v.filteredItems", currentRecords);

    },

    showPreviousPage : function (component, event) {

        var currentPage = component.get("v.currentPage");
        currentPage --;
        var begin = (currentPage - 1) * 50;
        var end = begin + 50;

        var records = component.get("v.items");
        var currentRecords = records.slice(begin, end);
        component.set("v.currentPage", currentPage);
        component.set("v.filteredItems", currentRecords);

    },
    setPageNavigation : function (component, event) {

        var currentPage = component.get("v.currentPage");
        var records = component.get("v.items");
        var numberOfRecords = records.length;
        var disablePrevious = false;
        var disableNext = false;

        if(currentPage <=1){
            disablePrevious = true;
        }

        if(currentPage >= numberOfRecords/50){
            disableNext = true;
        }

        component.set("v.disablePrevious", disablePrevious);
        component.set("v.disableNext", disableNext);

    }
})