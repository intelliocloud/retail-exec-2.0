/**
 * Created by François Poirier on 2/15/2018.
 */
({
    doInit : function (component, event, helper) {

        helper.doInit(component, event);

    },
    saveFilters : function (component, event, helper) {

        helper.saveFilters(component,event);

    },
    handleShowNextPage : function (component, event, helper) {

        helper.showNextPage(component, event);

    },
    handleShowPreviousPage : function (component, event, helper) {

        helper.showPreviousPage(component, event);

    },
    handlePageChange : function (component, event, helper) {

        helper.setPageNavigation(component, event);

    }
})