({
	doInit : function(component, event, helper) {
		helper.doInit(component, event);
	},

	doCancel : function(component, event, helper) {
		helper.doCancel(component, event);
	},

	doGo : function(component, event, helper) {
		helper.doGo(component, event);
	}
})