({
	doInit : function(component, event) {
        var recordId = component.get("v.recordId");

        if(recordId != null) {
            this.doCreateStoreVisit(component, event, recordId);
        }
	},

	doCancel : function(component, event) {
		var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "icr_StoreVisit__c"
        });
        homeEvent.fire();
	},

	doGo : function(component, event) {
		var selectedAccount = component.get("v.selectedAccount");
        
        if(selectedAccount == null || selectedAccount === undefined) {
            component.set("v.resultMessage", "Please select an account.");
        } else {
            this.doCreateStoreVisit(component, event, selectedAccount.Id);
        }
    },

    doCreateStoreVisit : function(component, event, accountId) {
        var action = this.callServerAction(
            component,
            event,
            "createNewStoreVisit",
            {
                accountId : accountId
            },
            {
                onSuccess: function (response) {
                    var result = response.getReturnValue();
                    console.log("result ===> " +  JSON.stringify(result));
                    var navEvt = $A.get("e.force:navigateToSObject");
                    navEvt.setParams({
                        "recordId": result,
                        "slideDevName": "detail"
                    });
                    navEvt.fire();
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "type": "success",
                        "title": $A.get("$Label.c.success"),
                        "message": $A.get("$Label.c.visitCreatedSuccessfully")
                    });
                    toastEvent.fire();

                }
            });
        $A.enqueueAction(action);
    }
})