({
	save : function(component, event, helper) {

		helper.save(component, event);

	},

	init : function (component, event, helper) {

		helper.init(component, event);

    },

	handleChange : function (component, event, helper) {

		helper.fieldIsChanged(component, event);

    },
    handleChange2 : function (component, event, helper) {

        helper.fieldIsChanged2(component, event);

    },

	reset : function (component, event, helper) {

		helper.reset(component, event);

    }
})