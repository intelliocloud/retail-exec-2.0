({
	init : function(component, event) {

	var items = component.get("v.items");
		var fields = [];

		if(items.length > 0){

			var strFields = JSON.stringify(items[0].fields);
			fields = JSON.parse(strFields);
			fields.forEach(function (field) {
				field.isChanged = false;
				field.bValue = false;
			})
		}

		component.set("v.fields", fields);

	},

    fieldIsChanged : function (component, event) {

		//var name = event.currentTarget.name;
		var name = event.getSource().get("v.name");
		console.log('name ===> ' + name);
		var fields = component.get("v.fields");

		fields.forEach(function (field) {
			if(field.apiName === name){
				field.isChanged = true;
				field.class = "slds-box mycss_class slds-box_xx-small" ;
				console.log("field ---> " + field.apiName + ' is changed');
			}
		});

		component.set("v.fields", fields);

    },

    fieldIsChanged2 : function (component, event) {

        var name = event.target.name;
        var fields = component.get("v.fields");

        fields.forEach(function (field) {
            if(field.apiName === name){
                field.isChanged2 = true;
                field.class2 = "slds-box mycss_class slds-box_xx-small" ;
                console.log("field ---> " + field.apiName + ' is changed');
            }
        });

        component.set("v.fields", fields);

    },

	save : function (component, event) {

		var fields = component.get("v.fields");
		var items = component.get("v.items");
		console.log('in save');

		var i = 0;
		items.forEach(function (item) {

			console.log('item before update ===> ' + JSON.stringify(item) );
			if(item.selected){
				console.log('item ' + i + ' is selected');

				fields.forEach(function (field) {

					if(field.isChanged){
						console.log('field apiName ===> ' + field.apiName);
						item.fields.forEach(function(itmField){
							console.log('itmField.apiName ===> ' + itmField.apiName);
							if(itmField.apiName === field.apiName){
								if(field.fieldType === "BOOLEAN"){

									itmField.bValue = field.bValue;
								}
								else {
                                    itmField.value = field.value;
                                }
							}
						});
					}
				});

				console.log('item after update ===> ' + JSON.stringify(item));
			}
		});

		component.set("v.items", items);
		this.reset(component, event);
    },

	reset : function (component, event) {

		var fields = component.get("v.fields");

		fields.forEach(function (field) {

			field.value = null;
			field.bValue = false;
			field.isChanged = false;
			field.class = "";

		})

		component.set("v.fields", fields);
    }
})