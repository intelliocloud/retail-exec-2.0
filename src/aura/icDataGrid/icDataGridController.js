/**
 * Created by François Poirier on 12/27/2017.
 */
({
    toggleSelectAll : function (component, event, helper) {

        helper.toggleSelectAll(component, event);

    },

    setSizeClass : function (component, event, helper) {

        helper.setSizeClass(component, event);

    },

    showDetails : function (component, event, helper) {

        helper.showDetails(component, event);

    },
    unselectAll : function (component, event, helper) {

        helper.unselectAll(component, event);
    },
    showNext : function (component, event, helper) {

        helper.showNext(component, event);

    },
    showPrevious : function (component, event, helper) {

        helper.showPrevious(component, event);

    }
})