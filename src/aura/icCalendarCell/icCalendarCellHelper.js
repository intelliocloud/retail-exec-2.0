/**
 * Created by François Poirier on 2/2/2018.
 */
({
    handleItmDrop : function (component, event) {

        var itemId = component.get("v.itemId");
        var objDate = component.get("v.objDate");
        var time = component.get("v.time");
        var cellId = component.get("v.cellId");
       // console.log('cell Id in handle drop ===> ' + cellId);

        //time = time + ':00';

        var cmpEvent = component.getEvent("calendarCellItemDrop");
        cmpEvent.setParam("id", itemId);
        cmpEvent.setParam("an", objDate.year);
        cmpEvent.setParam("mois", objDate.month);
        cmpEvent.setParam("jour", objDate.day);
        cmpEvent.setParam("heure", time);
        cmpEvent.fire();

    },
    handleClick : function (component, event) {

        var objDate = component.get("v.objDate");
        var time = component.get("v.time");
        var cellId = component.get("v.cellId");
        var cmpEvent = component.getEvent("calendarCellClick");

        cmpEvent.setParam("an", objDate.year);
        cmpEvent.setParam("mois", objDate.month);
        cmpEvent.setParam("jour", objDate.day);
        cmpEvent.setParam("heure", time);
        cmpEvent.fire();

    },
    loadEvents : function (component, event) {

        var cellId = component.get("v.cellId");
        var events = component.get("v.events");

       //console.log('Cell id ===> ' + cellId);
        //console.log('events ===> ' + JSON.stringify(events));
        var displayValue = "";

        var displayValues = [];

        displayValues = events[cellId];

        //console.log('display Values ===> ' + JSON.stringify(displayValues));

        if (displayValues) {
            displayValues.forEach(function (value) {
                displayValue = displayValue + value.Name + '\n';
            });
          //  console.log('display value ===> ' + displayValue);
            component.set("v.displayValue", displayValue);
        }
    }
})