/**
 * Created by François Poirier on 11/27/2017.
 */
({
    callServerAction : function(component, event, actionName, serverParams, clientParams) {
        console.log('in callServerAction in absDALHelper');

        //var serverActionName = event.getParam('arguments').serverActionName;
        var serverActionName = actionName;
        console.log('serverActionName ===> ' + JSON.stringify(serverActionName));
        //var clientParams = event.getParam('arguments').clientParams;
        //console.log('clientParams ===> ' + JSON.stringify(clientParams));
        //var serverParams = event.getParam('arguments').serverParams;
        //console.log('serverParams ===> ' + JSON.stringify(serverParams));
        var action = component.get("c." + serverActionName);
        //console.log('action ===> ' + action);
console.log("**** AA ****");
        action.setParams(serverParams);
        action.setCallback(this, function(response) {
            if (response.getState() === "SUCCESS") {
                console.log('*** A ***');
                if(clientParams.onSuccess) {
                   // console.log('results ===> ' + JSON.stringify(response.getReturnValue()));
                    clientParams.onSuccess(response);
                } //trigger function onSuccess()

                if(clientParams.eventOnSuccess) {
                    var eventOnSuccess = component.getEvent(clientParams.eventOnSuccess.name);
                    eventOnSuccess.setParams(clientParams.eventOnSuccess.params);
                    if(eventOnSuccess) eventOnSuccess.fire();
                }

            }
            else if (response.getState() === "ERROR") {
                console.log('*** B ***');
                var errors = response.getError();
                if (errors) { //by default, log the error with the message
                    errors.forEach(function (error) {
                        console.log('Error Message: ' + error.message);
                    });
                    /*/if (errors[0] && errors[0].message) {
                        console.log("Error message: " + errors[0].message);*/

                        /*$A.get("e.c:evtShowToast").setParams({
                            type:"error",
                            title:"Error with back end operation",
                            message:errors[0].message
                        }).fire();
                    }*/
                } else {
                     console.log('*** C ***');
                    // console.log("Unknown error");
                }

                if(clientParams.onError) clientParams.onError(response); //trigger function onError()
            }
        });
        /*action.setCallback(this, function (response) {
            var myMessage =  response.getReturnValue();
            component.set("v.message", myMessage);

        });*/
        console.log('action ===> ' + JSON.stringify(action));
        //$A.enqueueAction(action);
        return action;
        //component.set("v.message", "toto");

    },
    abv : function (component, event) {

        return "coucou";

    }
})