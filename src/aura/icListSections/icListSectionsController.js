({
    toggleSelectAll : function (component, event, helper) {

        helper.toggleSelectAll(component, event);

    },

    setSizeClass : function (component, event, helper) {

        helper.setSizeClass(component, event);

    },

    showDetails : function (component, event, helper) {

        helper.showDetails(component, event);

    },
    unselectAll : function (component, event, helper) {

        helper.unselectAll(component, event);
    },
    toggleSelection : function (component, event, helper){
        helper.toggleSelection(component, event);
    }
})