<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>InStoreStartDate__c</fullName>
        <externalId>false</externalId>
        <formula>Promotion__r.rtxIn_Store_Start_Date__c</formula>
        <label>In Store Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product__c</referenceTo>
        <relationshipLabel>Promotion Products</relationshipLabel>
        <relationshipName>Promotion_Products</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>true</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Promotion_Product_Category_Brand__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Promotion Product Brand/Category</label>
        <referenceTo>PromotionProdBrandCat__c</referenceTo>
        <relationshipLabel>Promotion Products</relationshipLabel>
        <relationshipName>Promotion_Products</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Promotion__c</fullName>
        <externalId>false</externalId>
        <label>Promotion</label>
        <referenceTo>Promotion__c</referenceTo>
        <relationshipLabel>Promotion Products</relationshipLabel>
        <relationshipName>Promotion_Products</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>rtxAmount_Discount__c</fullName>
        <externalId>false</externalId>
        <label>Amount Discount</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>rtxBOGO__c</fullName>
        <externalId>false</externalId>
        <label>BOGO</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>2 for 1</fullName>
                    <default>false</default>
                    <label>2 for 1</label>
                </value>
                <value>
                    <fullName>3 for 2</fullName>
                    <default>false</default>
                    <label>3 for 2</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>rtxDetails__c</fullName>
        <externalId>false</externalId>
        <label>Details</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>rtxDiscount__c</fullName>
        <externalId>false</externalId>
        <label>% Discount</label>
        <precision>3</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>rtxExcluded__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Excluded</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>rtxInStoreEndDate__c</fullName>
        <externalId>false</externalId>
        <formula>Promotion__r.rtxIn_Store_End_Date__c</formula>
        <label>In Store End Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>rtxInStoreStartDate__c</fullName>
        <externalId>false</externalId>
        <formula>Promotion__r.rtxIn_Store_Start_Date__c</formula>
        <label>In Store Start Date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>rtxPromotion_Type__c</fullName>
        <externalId>false</externalId>
        <label>Promotion Type</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>% Discount</fullName>
                    <default>false</default>
                    <label>% Discount</label>
                </value>
                <value>
                    <fullName>Amount Discount</fullName>
                    <default>false</default>
                    <label>Amount Discount</label>
                </value>
                <value>
                    <fullName>BOGO</fullName>
                    <default>false</default>
                    <label>BOGO</label>
                </value>
                <value>
                    <fullName>Coupon</fullName>
                    <default>false</default>
                    <label>Coupon</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>rtxSummary__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISBLANK(TEXT(rtxPromotion_Type__c)), &quot;&quot;, TEXT(rtxPromotion_Type__c) &amp; &quot;: &quot; &amp;
IF(TEXT(rtxPromotion_Type__c) = &quot;Amount Rebate&quot;, &quot;$&quot; &amp; TEXT(rtxAmount_Discount__c),
IF(TEXT(rtxPromotion_Type__c) = &quot;% Rebate&quot;, TEXT(rtxDiscount__c*100) &amp; &quot;%&quot;,
IF(TEXT(rtxPromotion_Type__c) = &quot;Buy X Get Y Free&quot;, TEXT(rtxBOGO__c) ,
IF(TEXT(rtxPromotion_Type__c) = &quot;Coupon&quot;, IF(ISBLANK(rtxAmount_Discount__c),&quot;&quot;,&quot;$&quot; &amp; TEXT(rtxAmount_Discount__c) &amp; &quot; Rebate&quot;) &amp; IF(ISBLANK(rtxDiscount__c),&quot;&quot;,TEXT(rtxDiscount__c*100) &amp; &quot;% Rebate&quot;)
, &quot;&quot;)))))</formula>
        <label>Summary</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <label>Promotion Product</label>
    <nameField>
        <displayFormat>PMPR-{0000000000}</displayFormat>
        <label>Promotion Product</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Promotion Products</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Promotion__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Promotion_Product_Category_Brand__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>rtxExcluded__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>rtxSummary__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>rtxPromotion_Type__c</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
