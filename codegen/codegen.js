/**
* Javascript utilities to generate APEX code.
* Jorge Luna
* Date: 2014-11-18
*/
 
var fs = require('fs');
var BASE_CLASS_PATH = './src/classes/';
var TEMPLATE_PATH = './codegen_templates/';
var CODEGEN_STATS_FILE = './codestats/stats.json';
var BACKUP_PATH = './codegen_bak/';
var CLSMETATEMPLATE = 'classmetatemplate.cls-meta.xml';

var parseFile = function (fileName){
  var file = fs.readFileSync(fileName,'utf8');
  var parsedFile = [];
  
  parsedFile = file.toString().split('\n');
  return parsedFile;
};

var writeFile = function(fileName, fileContent){
  if(fs.existsSync(fileName)){
    fs.unlinkSync(fileName);
  }
  fs.writeFileSync(fileName, fileContent);  
};

var writeClassFile = function (fileName, fileContent){
  if(fs.existsSync(fileName)){
    var nameParts = fileName.split('/');
    var oldfile = fs.readFileSync(fileName,'utf-8');
    if(!fs.existsSync(BACKUP_PATH)){
      fs.mkdirSync(BACKUP_PATH);
    }
    writeFile(BACKUP_PATH + nameParts[nameParts.length - 1], oldfile);
  }else{
    var template = fs.readFileSync(TEMPLATE_PATH + CLSMETATEMPLATE, 'utf-8');
    writeFile(fileName + '-meta.xml', template);
  }
  
  writeFile(fileName, fileContent);
};

var removeCommentedCode = function (codeFile){
  var newCode = [];
  var inComment = false;

  for(i = 0; i < codeFile.length; i++){
    var line = String(codeFile[i]);
    var skipping = false;
    var skipToken = '';
    var newline = '';
  
    for(j = 0; j < line.length; j++){
      if((line[j] === '\'' || line[j] === '"') && !inComment){
        if(!skipping){
          skipping = true;
          skipToken = line[j];
        }else if(line[j-1] !== '\\' && line[j] === skipToken){
          skipping = false;          
        }        
      }else if(!skipping){
        if(j < line.length - 1 && line[j] === '/' && line[j+1] === '/' && !inComment){
          break;
        }
        if(j < line.length - 1 && line[j] === '/' && line[j+1] === '*'){
          inComment = true;
        }
        if(j < line.length - 1 && line[j] === '*' && line[j+1] === '/'){
          inComment = false;
          j++;
          continue;
        }
        if(!inComment){
          newline += line[j];
        }
          
      }else{
        newline += line[j];
      }
    }    
    
    if(newline !== ''){
      newCode[newCode.length] = newline;
    }
  }
  
  return newCode;
};

var removePadding = function (processFile){
  var newCode = [];

  for(i = 0; i < processFile.length; i++){    
    var temp = String(processFile[i]).trim();
    do{
      temp = temp.replace('\t',' ');        
    }while(temp.indexOf('\t') >= 0);
    
    do{
      temp = temp.replace('  ',' ');        
    }while(temp.indexOf('  ') >= 0);
    
    temp = temp.replace(' {', '{');
    
    if(temp.length > 0){
      newCode[newCode.length] = temp.trim();
    }
  }
  
  return newCode;
};

var normalizeLines = function (processFile){
  var singleLineCode = processFile.join('');
  var newCode = [];
  var line = 0;
  var skipQuote = false;
  var skipChar = '';
  
  //split Lines on '{' '}' or ';' skip quoted strings
  for(i = 0; i < singleLineCode.length; i++){
    var Character = singleLineCode[i];    
    var PrevCharacter = (i>0)? singleLineCode[i-1] : '';
    newCode[line] = (newCode[line])? newCode[line] + Character : Character;
    if(Character === '\'' || Character === '"'){
      if(skipQuote && Character === skipChar && PrevCharacter !== '\\'){
        skipChar = '';
        skipQuote = false;
      }else if(!skipQuote){
        skipChar = Character;
        skipQuote = true;
      }
    }
    if(!skipQuote){
      line += ( Character === ';' || Character === '}' || Character === '{')? 1 : 0;
    }
  }
  
  for(i = 0; i < newCode.length; i++){
    newCode[i] = newCode[i].trim();
  }
  
  return newCode;
};

var extractBlockSection = function (tokenPhrase, processCode){
  var newCode = [];
  var extractFlag = false;
  
  for(i = 0; i < processCode.length; i++){
    if(processCode[i] === '}' && extractFlag){
      break;
    }
    if(extractFlag){
      newCode[newCode.length] = processCode[i];
    }
    if(processCode[i].toUpperCase() === tokenPhrase){
      extractFlag = true;
    }
  }
  
  return newCode;
};

var addMockedFunction = function (functionDef){
  var mockFunc = [];
  var returnType = functionDef.split(' ')[0];
  var functionName = functionDef.split(' ')[1].split('(')[0].trim();
  var parameters = functionDef.split('(')[1].split(')')[0].trim();
  var arrayParms = parameters.split(',');
  
  mockFunc[mockFunc.length] = '  public ' + functionDef.replace(';','{');
  mockFunc[mockFunc.length] = '    Map<String, object> parms = icTestMockUtilities.Tracer.RegisterCall(this, \'' + 
                              functionName + '\');';
  
  for(var i=0; i < arrayParms.length; i++){
    var parmName = arrayParms[i].trim().split(' ')[1];
    mockFunc[mockFunc.length] = '    parms.put(\'' + parmName + '\',' + parmName + ');';
  }
  
  if(returnType.toUpperCase() !== 'VOID'){
    mockFunc[mockFunc.length] = '    Return (' + returnType + 
      ') icTestMockUtilities.Tracer.GetReturnValue(this, \'' + 
      functionName + '\');';
  }
  mockFunc[mockFunc.length] = '  }';
  
  return mockFunc.join('\n');
};

var generateMock = function (className){
  var filename = className + '.cls';
  var code = parseFile('./src/classes/' + filename);  
  var mockFile = '/*******************************************\n';
  mockFile +=    '*                                          *\n';
  mockFile +=    '*     MOCK Generated By Auto-Mock APEX     *\n';
  mockFile +=    '*                                          *\n';
  mockFile +=    '*******************************************/\n';
  mockFile +=    '\n';
  mockFile +=    '@isTest\n';
  mockFile +=    'public class ' + className + 'Mock implements ' + className + '.IClass{\n\n';
  
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  code = extractBlockSection('PUBLIC INTERFACE ICLASS{', code);
  
  for(var i = 0; i < code.length; i++){
     mockFile += addMockedFunction(code[i]) + '\n\n';
  }
  
  mockFile += '}';

  writeClassFile(BASE_CLASS_PATH + className + 'Mock.cls', mockFile);
};

var extractImplementedMethods = function (inputCode){
  var outputCode = [];
  
  for(i = 0; i < inputCode.length; i++){
    if(inputCode[i].toUpperCase().indexOf('PUBLIC') === 0){
      var lineUpper = inputCode[i].toUpperCase();
      if(lineUpper.indexOf('INTERFACE') >= 0 || lineUpper.indexOf('CLASS') >= 0){
        continue;
      }
      if(inputCode[i].indexOf('{') === inputCode[i].length - 1 &&
        inputCode[i].toUpperCase().indexOf('GETINSTANCE()') < 0){
        outputCode[outputCode.length] = inputCode[i].substring(inputCode[i].indexOf(' ') + 1).replace('{', ';');
      }
    }  
  }
  
  return outputCode;
};

var removeExistingInterface = function (inputCode, className){
  var outputCode = [];
  var skipInterface = false;
  var instanceFn = false;
  var insertLine = 0;
  var implementClass = false;

  for(i = 0; i < inputCode.length; i++){
    if(inputCode[i].toUpperCase().indexOf('CLASS ') >= 0 && 
       inputCode[i].toUpperCase().indexOf(' ' + className.toUpperCase()) >= 0){
      insertLine = i + 1;      
    }else if(inputCode[i].toUpperCase().indexOf('PUBLIC INTERFACE ICLASS') >= 0){
      insertLine = i;
      if(inputCode[i].indexOf('}') < 0){
        skipInterface = true;
      }
    }else if(inputCode[i].toUpperCase().indexOf('IMPLEMENTS ICLASS') > 0){
      implementClass = true;
    }else if(inputCode[i].toUpperCase().indexOf('GETINSTANCE()') > 0){
      instanceFn = true;
    }else if(skipInterface && inputCode[i].indexOf('}') >= 0){
      skipInterface = false;
      continue;
    }
    
    if(!skipInterface){
        outputCode[outputCode.length] = inputCode[i];
    }
  }
  
  return {code: outputCode, 
          insertAt: insertLine, 
          isImplemented: implementClass,
          getInstanceFn: instanceFn};
};

var addInterface = function (interfaceCode, insertionCode){
  var newCode = [];
  var i = 0;
  newCode[newCode.length] = '  public Interface IClass{';
  for(i=0; i< interfaceCode.length; i++){
    newCode[newCode.length] = '    ' + interfaceCode[i].trim();
  }
  newCode[newCode.length] = '  ' + '}';

  if(!insertionCode.getInstanceFn){
    insertionCode.code.splice(insertionCode.insertAt ++,0,
                              '  public Object GetInstance(){\n    return new ClassImpl();\n  }\n');
  }
  insertionCode.code.splice(insertionCode.insertAt ++,0,newCode.join('\n'));
  if(!insertionCode.isImplemented){
    insertionCode.code.splice(insertionCode.insertAt ++,0,'\n  class ClassImpl implements IClass{');
    for(i=insertionCode.insertAt; i < insertionCode.code.length; i++){
      insertionCode.code[i] = '  ' + insertionCode.code[i];
    }
    insertionCode.code.push('}');
  }
  return insertionCode.code;  
};

var extractInterface = function (className, override){
  var filename = className + '.cls';
  var originalCode = parseFile(BASE_CLASS_PATH + filename);
  var code = originalCode;
  
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  code = extractImplementedMethods(code);
  
  var insertionCode = removeExistingInterface(originalCode, className);
  var newCode = addInterface(code, insertionCode);  
  var newFile = newCode.join('\n');

  var postfix = (override)? '' : '_new'; 
  writeClassFile(BASE_CLASS_PATH + className + postfix + '.cls', newFile);
};

var createClass = function (className, template){
  var file = parseFile(TEMPLATE_PATH + template + '.cls');
  var fileStr = file.join('\n');
  fileStr = fileStr.replace('%CLASSNAME%',className);
  writeClassFile(BASE_CLASS_PATH + className + '.cls', fileStr);
};

var retrieveListOfClasses = function (){
  var filelist = fs.readdirSync(BASE_CLASS_PATH);
  var classList = [];
  for(i=0; i < filelist.length; i++){
    partName = filelist[i].split('.');
    if(partName[partName.length - 1] === 'cls'){
      classList[classList.length] = filelist[i];
    }
  }
  return classList;
};

var getFunctionName = function (line){
  var lineParts = line.split(' ');
  for(i=0; i < lineParts.length; i++){
    if(lineParts[i] ==='('){ 
      return lineParts[i-1];
    }
    if(lineParts[i].indexOf('(') >=0){
      return lineParts[i].substring(0,lineParts[i].indexOf('('));
    }
  }
  
  return '';
};

var computeLines = function (blockCode, stats){
  stats.lines = (stats.lines)? stats.lines : 0;
  stats.nesting = (stats.nesting)? stats.nesting : 0;
  for(var i=0; i < blockCode.length; i++){
    stats.lines++;
    if(blockCode[i].innerBlock){
      stats = computeLines(blockCode[i].innerBlock, stats);
    }
  }
  stats.nesting++;
  return stats;
};

var parseFunctions = function (blockCode){
  var outputFunctions = [];
  
  for(var i=0; i < blockCode.length; i++){
    var blockLine = blockCode[i].line; 
    if(blockLine[blockLine.length-1] === '{' &&
       blockLine.toUpperCase().indexOf('INTERFACE') < 0 &&
       blockLine.toUpperCase().indexOf('CLASS') < 0){
      var innerStat = computeLines(blockCode[i].innerBlock,{});
      outputFunctions[outputFunctions.length] = {
        name: getFunctionName(blockLine),
        length: innerStat.lines,
        nesting: innerStat.nesting
      };
    }
  }
  
  return outputFunctions;
};

var parseProperties = function (code){
  return [];
};

var parseSubClasses = function (code){
  return [];
};

var parseClass = function (blockCode){
  var i=0;
  do{
    if(blockCode[i].line.toUpperCase().indexOf('CLASS') >= 0){
      var phrases = blockCode[i].line.split(' ');
      for(var j=0; j< phrases.length; j++){
        if(phrases[j].toUpperCase() === 'CLASS'){
          var name = phrases[j+1].replace('{','');
          return {name : name, innerBlock : blockCode[i].innerBlock};
        }
      }
    }
  }while(++i < blockCode.length);
  return '';
};

var extractBlocks = function (code){
  var blockCode = [];
  do{
    var newBlock = {line : code.shift()};
    blockCode[blockCode.length] = newBlock;
    if(newBlock.line[newBlock.line.length-1] === '{'){
      var retBlock = extractBlocks(code);
      newBlock.innerBlock = retBlock.innerBlock;
      code = retBlock.code;
    }
    if(newBlock.line[newBlock.line.length-1] === '}'){
      return {innerBlock: blockCode, code: code};
    }
  }while(code.length > 0);
  
  return {innerBlock: blockCode, code : []};
};

var addStatsOfClass = function (fileName, stats){
  var code = parseFile(BASE_CLASS_PATH + fileName);
  code = removeCommentedCode(code);
  code = removePadding(code);
  code = normalizeLines(code);
  
  var classIdx = stats.classes.length;
  stats.classes[classIdx] = {
    lines : 0,
    name : '',
    functions : [],
    properties : [],
    subclasses : []
  };
  stats.classes[classIdx].lines = code.length;
  var blockCode = extractBlocks(code).innerBlock;
  var classRet = parseClass(blockCode);
  stats.classes[classIdx].name =  classRet.name;
  if (stats.classes[classIdx].name === ''){
    //Maybe an Interface or other
    //for now ignore it.
    stats.classes.pop();
    return stats;
  }   
  console.log('class parsed:' +  stats.classes[classIdx].name);
  if(classRet.innerBlock){
    stats.classes[classIdx].functions = parseFunctions(classRet.innerBlock, stats);
  }
//   stats.classes[classIdx].properties = parseProperties(code);
//   stats.classes[classIdx].subclasses = parseSubClasses();
  return stats;
};

var analyzeCode = function(){
  var fileset = retrieveListOfClasses();
  
  var statsObject = {classes : []};
  for(var i=0; i < fileset.length; i++){
    statsObject = addStatsOfClass(fileset[i], statsObject);      
  }
  //computeStatsTotals(statsObject);
 //println(statsObject.toString());
 writeFile(CODEGEN_STATS_FILE, JSON.stringify(statsObject));
};

exports.getParametersForAction = function(action){
  var parms;
  switch(action){
    case 'generateMock' :
      parms = ['className'];
      break;
    case 'extractInterface' :
      parms = ['className', 'override'];
      break;
    case 'createClass' :
      parms = ['className', 'template'];
      break;
  }
  return parms;
};

exports.codegen = function(action, params){  
  console.log('APEX Code Generator: ' + action);
  switch(action){
    case 'generateMock' :
      generateMock(params.className);
      break;
    case 'extractInterface' :
      extractInterface(params.className, params.override);
      break;
    case 'createClass' :
      params.template = (params.template || 'simpleclass');
      createClass(params.className, params.template);
      break;
    case 'analyzeCode' :
      analyzeCode();
      break;
    default:
      console.log(action + ': Invalid Action Found!');
  }
}; 
