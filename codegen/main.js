/**
* APEX Code generator NODE App.
* User: ivancho72
* Date: 2014-12-14
* Time: 03:22 PM
*/
(function() {
  var prompt = require('prompt');
  var optimist = require('optimist');
  var generator = require ('./codegen');
  
  prompt.override = optimist.argv;
  prompt.start();
  prompt.get(['action'], 
             function(err, result){
    var parms = generator.getParametersForAction(result.action);
    if(parms){
      prompt.override = optimist.argv;
      prompt.start();
      prompt.get(parms,function(err, newRes){
        generator.codegen(result.action, newRes);
      });
    }else{
      generator.codegen(result.action);
    }
  });    
}).call(this);
